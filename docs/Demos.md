# Demos  
The following demos describe different attacks that can be performed with TProxy:

* [MySQL Command Hijacking](MySQL-Command-Hijacking.md)
* [StripTLS attack on SMTP](StripTLS-attack-on-SMTP.md)
* [HTTP2 Slow Read Attack](HTTP2-Slow-Read-Attack.md)
* [NFS Command Injection](NFS-Command-Injection.md)
