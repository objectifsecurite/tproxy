# Graphical User Interface  
The GUI is composed of two tabs: the connection tab and the message tab.

## Connection tab
The connection tab contains a list of all connections captured by TProxy. Double-clicking on a connection displays its corresponding captured packets in the message tab.

![Connection tab](img/tproxy_connection_tab.png)

## Message tab
The message tab is composed of four main areas, as described below:

![Message tab](img/tproxy_message_tab.png)

1. The text field is used to enter a new filter, which can then be used as one of the three filter types (explained in 4). The two buttons on the right respectively forward and discard a selected packet that was intercepted with an interception filter.
2. The main table in the middle of the window displays a list of captured messages.
3. The bottom pane is composed of three tabs that display the selected packet content: a text editor, a hexadecimal editor and the packet's dissection tree. A more detailed description of each tab is included below.
4. The leftmost column contains one pane per filter type (for more details about filters, see [TProxy Overview](TProxy-Overview.md) ). One can use the filter entered in the text field as a capture, interception or script interception filter using the corresponding "+" button.

### Editors
The text and hexadecimal editors display the selected packet content as text and hexadecimal digits respectively. They also allow the user to modify the content of a packet that was intercepted with an interception filter. The modified packet can then be forwarded to its destination with the _Forward_ button.

<p float="left">
<img src="../img/tproxy_text_editor.png" width="180"/> 
<img src="../img/tproxy_hex_editor.png" width="560"/>
</p>


### Dissection tree

When dissection is enabled (default), the dissection tab displays the dissection tree corresponding to the selected packet, which facilitates packet monitoring and editing. One can indeed double-click on a specific field in the dissection tree to retrieve the corresponding bytes in the hexadecimal editor. 

![TProxy dissection](img/tproxy_dissection.png)


