# Command Line Interface  
The CLI offers TProxy's basic functionalities without requiring GUI-specific libraries. It is especially useful to monitor packets and to redirect them through a running script.  
Below is a view of TProxy's CLI:

![TProxy CLI](img/tproxy_cli.png)

The green line announces a new connection and is followed by packets intercepted between the corresponding client and server. At the bottom of the message list, a command prompt can be used to manage script interception filters. As displayed by the command `help`, one can use `+` and `-` to add, respectively remove, a script interception filter. For further information about filters, refer to [TProxy Overview](TProxy-Overview.md).

