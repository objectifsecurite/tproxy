# Getting Started  


TProxy's binaries can be downloaded from the [latest release](https://gitlab.com/objectifsecurite/tproxy/-/releases) or can be built from the source code. For building instructions, refer to the [README](https://gitlab.com/objectifsecurite/tproxy/-/blob/master/README.md).

This section gives step-by-step instructions on how to launch TProxy. We consider a situation where TProxy is used to capture HTTPS traffic (port 443) between a client at `10.0.0.2` and a server at `10.0.0.3`.


## 1. Getting in the middle  

The first step is to redirect traffic to our machine in order to get in the middle of the connection we want to intercept. This can be done using ARP spoofing, DNS cache poisoning and other techniques. One can also get physically in the middle of a wired connection using two Ethernet cables connected between the client and the MitM and between the MitM and the server, respectively. 

We do not provide further instructions on how to perform such MitM attacks since this is out of scope of this project.

## 2. Redirecting traffic  
TProxy captures traffic on port 9080. This means that once we are in the middle of the targeted connection, the desired traffic has to be routed to the proxy listening on port 9080. To this end, the shell script `iptables.sh` is provided. It can be run as follows: 

```
./iptables.sh <cmd> [options]
init		    Initialize TPROXY mode
flush		    Flush all TPROXY rules
list		    List all TPROXY rules set
forward		    Forward trafic coming from ip or interface
	<src ip|in interface> <enable|disable>
masquerade	    Masquerade trafic coming from ip or interface
	<src ip|in interface> <enable|disable>
interface	    Intercept traffic on selected interface
	<interface> <enable|disable>
address		    Intercept traffic on selected IP address
	<src|dst> <address> <enable|disable>
port		    Intercept traffic on selected TCP port
	<src|dst> <port> <enable|disable>

```
In our case, we want TProxy to capture HTTPS traffic between `10.0.0.2` and `10.0.0.3`. This can be done in several ways using the script `iptables.sh`. For instance, we can redirect traffic with destination port 443 (HTTPS) :

```shell
$ ./iptables.sh port dst 443 enable
```

or redirect traffic with destination IP `10.0.0.3` :

```shell
$ ./iptables.sh address dst 10.0.0.3 enable
```

## 3. Running TProxy  

Tproxy can be run with different options:

```
Usage: tproxy [options]

  -c, --config-file <value>
                           Configuration file to load (default: tproxy.conf)
  -d, --dump-file <value>  Name of the pcap dump file (default: logs/dump.pcap)
  -D, --disable-dump       Disable pcap dump (default: false)
  -G, --disable-gui        Disable gui (default: false)
  -W, --disable-dissection
                           Disable wireshark dissection (default: false)
  -h, --help               Show this help
```

In our case, we will run TProxy with default options :

```shell
$ ./tproxy
```

This will launch the GUI if it has been compiled. Otherwise, the CLI will be run. In both cases, captured packets are displayed. One can then add filters to intercept and modify specific packets as described in [Command Line Interface](Command-Line-Interface.md) and [Graphical User Interface](Graphical-User-Interface.md).



