# TProxy
TProxy is an interception proxy for TCP traffic. It can be used to monitor, drop, modify or inject packets in an existing TCP connection. For monitoring purposes, TProxy has the ability to decrypt incoming TLS traffic and re-encrypt outgoing packets. It also leverages Wireshark dissectors to build a dissection tree of each intercepted packet.

TProxy's binaries can be downloaded from the [latest release](https://gitlab.com/objectifsecurite/tproxy/-/releases) or can be built from the source code. Refer to the [README](https://gitlab.com/objectifsecurite/tproxy/-/blob/master/README.md) for building instruction.

## Technologies
The following programming langugages and libraries are used:

-  Java JDK 11  
-  Scala 2.12  
-  akka 2.5.17  
-  upickle 0.9.5  
-  Wireshark libraries: `libpcap-dev`, `libwireshark-dev`, `libwiretap-dev`,  `libcap-dev `  
-  Glib 2.0  
-  Qt Jambi 4.8.7 (GUI)  
-  mill 0.10.4 (for compilation)  

## Contents
[Overview](TProxy-Overview.md)  
[Getting Started](Getting-Started.md)  
[Project Structure](Project-Structure.md)  
[Command Line Interface](Command-Line-Interface.md)  
[Graphical User Interface](Graphical-User-Interface.md)  
[Scripts](Scripts.md)  
[Demos](Demos.md)  

## Authors

Tproxy was developed by the company [Objectif Sécurité](https://objectif-securite.ch/en). The authors are:  

-  Bertrand Mesot  
-  Delphine Peter  
-  Cédric Tissières  
-  Sylvain Heiniger  

