# Project Structure  

TProxy's source code is organised as follows :

```
tproxy
├── common
│   └── src
│       └── main
│           └── scala
│               └── ...
│
├── core
│   └── src
│       ├── main
│       │   ├── java
│       │   │   └── ...
│       │   ├── others
│       │   │   └── ...
│       │   └── scala
│       │       └── ...
│       └── test
│           └── scala
│               └── ...
├── gui
│   └── src
│       ├── main
│       │   └── scala
│       │       └── ...
│       └── test
│           └── scala
│               └── ...
├── wireshark
│   └── src
│       └── main
│           ├── java
│           │   └── ...
│           ├── others
│           │   └── ...
│           └── scala
│                └── ...

```

The project is composed of 4 sub-modules: 

* `common` : contains the abstractions shared between sub-modules.
* `core` : contains the core code of TProxy as well as a basic CLI to run it.
* `gui` : contains the source code for TProxy's GUI. The use of a separate sub-module allows you to compile and run TProxy in "CLI-only" mode (sub-module `core`) without having GUI-specific libraries installed.
* `wireshark` : contains the Wireshark dissector as a server listening on port 9082. Upon receiving a dissection request for a given frame, it replies with the corresponding dissection tree serialized in JSON.  
We made this sub-module completely independent from the rest of the code for license purposes: the Wireshark library used in this sub-module being licensed under GPL, it restricts the license of all modules loading it (statically or dynamically). Thus, by using this library on an independent server, only the sub-module `wireshark` has to be licensed under GPL.

