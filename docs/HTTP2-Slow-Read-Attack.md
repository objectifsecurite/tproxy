# HTTP/2 Slow Read attack

## Context

The HTTP/2 protocol is a revision of HTTP/1.x that  aims to improve  performance. Optimisations mainly come from compression and multiplexing. With HTTP/2, data can be sent over different streams in order to parallelize requests. Thus, conversely to HTTP/1.x, multiple requests can be sent simultaneously without opening several TCP connections. 

Flow control is performed on a per-stream basis. That is, each stream has a specific flow control window. A client can use the `WINDOW_UPDATE` setting option to change the maximum amount of data the server is allowed to send on a specific stream.

Multiplexing is a great improvement in terms of overhead, but may also threaten accessibility due to intensive resource consumption, as we will see in this attack. Slow Read Attacks ([CVE-2016-1546](https://httpd.apache.org/security/vulnerabilities_24.html)) indeed aim to consume a vulnerable server's resources by leaving many streams open. In fact, HTTP/2 servers allocate a thread for each open stream. Thus, by setting low stream window sizes and opening many streams, one can consume all worker threads on the server, which results in Denial-of-Service (DoS).


We aim to reproduce a Slow Read attack with TProxy when capturing an HTTP/2 connection between a client and a server. Assuming that the client requests a large resource to the server, we will intercept the client's packets and modify both the `INITIAL_WINDOWS_SIZE` the `WINDOW_UPDATE` values for all streams in order for each packet to allow a few more bytes to be sent by the server. Then, we expect the server not to be able to serve other clients. This attack is a bit theoretical and depends on the server resources, but it aims to show how TProxy can be used to modify some fields in a binary protocol.

## Setup

- We consider a client trying to connect to an HTTP/2 server at 10.0.0.2

- HTTP/2 traffic is redirected through TProxy with the command:

```bash
$ sudo ./iptables.sh address dst 10.0.0.2 enable
```

- TProxy is run with the command:

```bash
$ sudo ./tproxy
```

  

## The attack



![](demo/http2/img/http2_traffic.png)

The above screenshot shows part of the original HTTP/2 traffic captured between the client and the server. The *Initial Windows Size* field that we have to modify is displayed in the dissection tab corresponding to the highlighted packets. We also observe several `WINDOW_UPDATE` requests sent by the client. 

In order to modify the latter field values, we first set an interception filter. The first HTTP/2 packet intercepted is displayed below:

![](demo/http2/img/initial_window_size_dissection.png)



We will modify the highlighted *Initial Windows Size* value, as well as the `WINDOW_UPDATE` field contained in the same packet. In order to retrieve the bytes corresponding to the latter fields in the hexadecimal editor, one should select the field in the dissection tree. When going to the hexadecimal editor, the corresponding bytes are highlighted:

![](demo/http2/img/initial_window_size_hex.png)



We set the latter bytes to `0x10` to reduce the initial window size for each stream. Then, we locate the `WINDOW_UPDATE` field in the dissection tree and proceed similarly to modify its value. As shown below, we set it to 3 in order to slow down the connection:



![](demo/http2/img/window_update_dissection.png)

![](demo/http2/img/window_update_hex.png)



If we keep modifying all intercepted `WINDOW_UPDATE` values, the connection will eventually consume most resources on the server side.