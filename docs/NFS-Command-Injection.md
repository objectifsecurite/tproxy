# NFS Command Injection

In this attack demo, we leverage TProxy's script interception to automate illegitimate file access on an NFSv3 shared file system.

## NFSv3 background

Network File System (NFS) is a protocol used for distributed file systems. That is, an NFS server offers mount points in which remote clients can access and modify files. Although NFSv4 exists and improves known security flaws present in version 3, NFSv3 is still widely deployed in practice.

#### The protocol

Once a file system is mounted on a remote machine, the client can access shared files in the same way as on their local file system. Upon each access, NFS requests (or *calls*) and replies are exchanged between the client and the server to maintain the shared file system up to date. Such NFS packets are encapsulated in Open Network Computing Remote Procedure Call (ONC RPC) requests and responses. An example of an NFS request is depicted below.

<div align="center">
<img src="../img/nfs_request.png">
</div>


The `XID` field is a sequence number that is used to match replies with their corresponding requests. There is no need to increment them in a particular way, but each request must have a unique XID. If an XID is used in two different calls, the server detects retransmission and does not answer the second request.

The `Procedure` field describes the NFS procedure called in the request (e.g., `GETATTR`, `LOOKUP` , `MKDIR`, `READ`, `WRITE`, etc). Each procedure has specific arguments that are passed in the NFS payload. For more details on those procedures, see [RFC 1813](https://tools.ietf.org/html/rfc1813).

#### File Handles

Most NFS procedures refer to files on the shared file system with their corresponding file handle. The `LOOKUP` procedure specifically allows a client to retrieve the handle corresponding to a file (or directory) name. The request attributes should contain a file (or directory) name as well as its parent directory's handle, and the server replies with the file handle. 

#### Authentication

NFSv3 authentication is mainly based on the host and its corresponding User ID (UID) and Group ID (GID) to determine access rights. The server also determines which IP addresses are allowed to access each mount point.

There are three main types of authentication (called *flavors*) in NFSv3: 

1.  `AUTH_NONE` : no authentication is required.

2. `AUTH_UNIX` : the client sends in each request their UID and GID, which are mapped to corresponding UID and GID on the server. They are then used to determine access rights. It is important to note that the integrity of such IDs cannot be verified by the server. Thus, one can easily gain access to a file by modifying their UID and GID. In this way, an attacker can bypass this authentication by spoofing his IP address and selecting the right UID/GID pair. There is no need for being in the middle of the communication between a legitimate client and server.  
   Authentication fields (called *Credentials*) under `AUTH_UNIX` are depicted below:

  <div align="center">
  <img src="../img/auth_unix.png">
  </div>

3. `RPCSEC_GSS` : the server leverages Kerberos V5 to authenticate clients and establish a so-called context. Then, the RPC headers up to (and including) the credentials of all requests and replies are signed to guarantee their integrity. This security is called `krb5`.  There also exist `krbi`, which adds integrity checks on NFS data, and `krbp` that checks integrity and encrypts NFS data. 

   In this attack demo, we are using `krb5`. Thus, when intercepting an NFS packet, we can modify NFS data but not RPC headers, which are signed. In particular, the NFS procedure called in a request cannot be modified. That is why our attack script cannot directly inject NFS requests: instead, it has to wait  for the desired procedure to be called by the client and  modify its arguments encoded in the NFS payload.  

   `RPCSEC_GSS` credentials and verifier with `krb5` security are depicted below. The highlighted field `krb5_sgn_cksum` contains the RPC header signature.
   

  <div align="center">
  <img src="../img/rpcsec_gss.png">
  </div>

   For more details on `RPCSEC_GSS`, see [RFC 2203](https://tools.ietf.org/html/rfc2203).

## Setup

- An NFSv3 server is running in a docker container. It offers a mount point at `/home` that is protected with Kerberos V5 authentication (`krb5`). 

- A KDC server is running in another docker container to manage Kerberos authentication.

- A client is authenticated with a valid Kerberos ticket and has mounted the `/home` folder from our NFS server. We do not assume that the mount traffic is intercepted with TProxy. That is, the client may have mounted the file system before the attack started. 

- NFS traffic between the host and the NFS server is redirected through TProxy with the command:

```shell
$ sudo ./iptables.sh port dst 2049 enable
```

- TProxy is run in CLI-only mode as follows:

```shell
$ sudo ./tproxy -G
```

## Attack goal

Our goal is to use TProxy to intercept traffic between an authenticated client and an NFSv3 server with a script that mimics a shell on the shared file system. In particular, the shell offers commands to read, write, create and delete files with the same rights as the targeted client's. Moreover, the mount point requires `RPCSEC_GSS` `krb5` security. We will show how such a script can be used to read an SSH private key stored in `/home/.ssh/` assuming that the authenticated client has read access to this file.

## Interception script

The script we implemented to handle NFS traffic captured by TProxy can be found in `scripts/nfs_shell.py`. It implements a shell that offers the following commands : 

```
$ help
Usage:
	ls <path>			List files and directories
	touch <file>			Create an empty file
	mkdir <dir>			Create an empty directory
	rm <file>			Remove a file
	rmdir <dir>			Remove a directory
	cat <file>			Display file content
	read <in> <out>			Copy the remote file <in> into the local file <out>
	write <in> <out>		Copy the local file <in> into the remote file <out>
	exit				Exit
	help				Display this help
```

Each command is executed on the shared file system on behalf of the authenticated client whose traffic is intercepted. To this end, the script injects NFS requests in the intercepted connection. The three authentication types described above are supported: `AUTH_NONE`, `AUTH_UNIX` and `RPCSEC_GSS` with `krb5`. 

With the two former authentication types, the script can inject as many packets as needed by using the client's credentials and verifier fields with a fresh XID.  Therefore, all shell commands are instantly executed by sending NFS requests to the server and reading their replies. In parallel, the script forwards client's traffic in both directions. Thus, the client cannot detect the MitM attack. However, as mentioned before, the use of TProxy for such an attack is not necessary: one does not have to be "in the middle" to send those requests to the server. That is why we will not show the attack on `AUTH_NONE` and `AUTH_UNIX` authentications in this demo.

With `RPCSEC_GSS` security, we can only inject packets if we know the secret key used to sign RPC headers. Since this is not the case, our script has to intercept a request from the client with the desired procedure and modify NFS data. Thus, to execute a command, our shell first waits for the client to send the corresponding NFS call, which may take some time in practice. For the client not to freeze and endlessly wait for a reply, we have to send them a reply. However, because replies' RPC headers are also signed, we cannot forge them. We can either send the server's reply to our tampered query as is, or modify its NFS data with, for instance, an error. We chose the second alternative, and replace the reply's NFS payload with an I/O error (`NFS3ERR_IO`). In general, this causes the client to send back their request with a fresh XID. In this way, the client eventually gets a truthful reply from the server (when the shell does not need to intercept the request anymore). This approach does not compromises the client's mounted file system.

## The Attack

The first step is to run our script:

```shell
$ ./nfs_shell.py
```

Then, a script interception filter should be used in TProxy to redirect to our running script all traffic coming from and going to the targeted client :

<img src="../img/tproxy_script_filter.png">


The script will not launch the shell immediately. It is first listening for NFS traffic to detect the authentication flavor and get the handle for the directory that will be the root of the shell. To make sure that the latter handle corresponds to a directory (and not a file), the script sends a `GETATTR` request. That is, it waits to intercept a `GETATTR` call from the client and changes the requested handle. If the response confirms that the intercepted handle corresponds to a directory, the shell is started. 

<div align="center">
<img src="../img/script_start.png">
</div>


First, we execute a `ls` command to list files and directories in the shell root directory:

<img src="../img/script_ls_home.png">

The first output line shows that the shell is waiting for the client to send the NFS call corresponding to the `ls` command, which is the procedure `READDIRPLUS`.  The command output then shows several directories including `.ssh/`. Let's see what is inside :

<img src="../img/script_ls_ssh.png">

Again, the script has to wait for some queries to be sent by the client: a `LOOKUP` call to retrieve `.ssh` 's  handle and a `READDIRPLUS` to execute `ls`.

The directory seems to contain an SSH key pair: `id_rsa` and `id_rsa.pub`. We can now try to read the content of the private key :

<img src="../img/script_cat_ssh_key.png">

One should note that even though we modified the client's `LOOKUP` and `READ` requests to read the private key, the client does not receive the content of that private key in their response. This would indeed look suspicious. As explained before, I/O errors are sent back to the client for each intercepted request. In this way, the client will keep sending requests as long as they fail, and will eventually receive the intended file content. 


Below is an execution of the attack as seen on the shell launched by our script: 

![demo](demo/nfs/demo.gif)


