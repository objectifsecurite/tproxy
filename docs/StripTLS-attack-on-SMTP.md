# StripTLS attack on SMTP

## Setup

- A Postfix server is running in a docker container at 10.6.0.2 and is listening for SMTP traffic on port 25.

  All traffic to port 25 is redirected to TProxy with the command:

```shell
$ sudo ./iptables.sh port dst 25 enable
```

- TProxy is run as follows:

```shell
$ sudo ./tproxy
```

## Context

The SMTP protocol is used to send emails from one mail server to another. Since it is pretty old, this protocol was not designed to support standard security features such as TLS. In order to encrypt communication while keeping backward compatibility, SMTP was extended with *Opportunistic TLS* or *STARTTLS*. As shown in the diagram below, a regular TCP connection is open between two mail servers and TLS may be added on top of it if both servers support it. In order to start a TLS negotation, the destination server first advertises STARTTLS, and the source server replies with a STARTTLS request.

<div align="center">
<img src="../demo/striptls/img/starttls.png" width="500">
</div>


After a successful TLS negotation, all traffic between the two servers is encrypted as in a regular TLS connection.

In this attack, we consider a source mail server that sends an email to the Postfix mail server running at 10.6.0.2. To this end, we run the following `swaks` command on the host machine, which simulates traffic from the source mail server:

```shell
swaks --from user1@mymail.com --to user2@mymail.com --server 10.6.0.2:25 -tlso --header "Subject: test subject" --body "test email"
```

The flag `-tlso`, or `--tls-optional`, specifies that the client should accept a clear text connection if the STARTTLS negotiation fails. Similarly, the destination server has been configured in the same way.

When the above command is run, we observe the following traffic :

<div align="center">
<img src="../demo/striptls/img/wireshark_starttls.png">
</div>

The highlighted packet shows that the destination server advertises STARTTLS (among other things). Then, the source server requests STARTTLS and all subsequent data are encrypted.

Our goal is to intercept with TProxy the above packet advertising STARTTLS and remove the highlighted line. The source server will then think that its destination does not support TLS. Similarly, the destination server will think that the source does not support TLS since STARTTLS was not requested. As a result, all traffic between the two servers will be sent in clear text. This downgrade attack is called STRIPTLS. 

## The attack

In order to intercept the above traffic in TProxy, we use the interception filter `srv.port == 25`. Then, we forward all packets until the advertisements shown above. After deleting the line advertising STARTTLS, we forward the tampered packet shown below and remove the interception filter.  

<div align="center">
<img src="../demo/striptls/img/tproxy_starttls_removed.png" width="800">
</div>

No difference is observed in the traffic seen through TProxy because TLS-encrypted packets are always decrypted before being displayed. Therefore, one cannot notice if data was originally sent encrypted or in clear text. However, the email content can now be seen in clear text in Wireshark :

<div align="center">
<img src="../demo/striptls/img/wireshark_cleartext.png">
</div>

This confirms that our STRIPTLS attack was successful. An adversary laying between the two servers can now monitor and tamper clear text traffic.

