# MySQL Command Hijacking

## Setup

- A MySQL server is running in a docker container at `10.5.0.2`. Its SQL database is called `tproxy_demo` and contains a single table `users` as such:


<div align="center">
<img src="../demo/mysql/img/users_db.png">  
</div>  


  

- All traffic to port 3306 (MySQL port open on the server) is redirected to TProxy with the command :

```bash
$ sudo ./iptables.sh port dst 3306 enable
```

  Another option would be to redirect all traffic to the server with:

```sh
$ sudo ./iptables.sh address dst 10.5.0.2 enable
```

- TProxy is run as follows:

```sh
$ sudo ./tproxy
```

## Context

A user connects as root to the MySQL server  :

```shell
$ mysql -h 10.5.0.2 -uroot -p
```

and sends the following command :

```mysql
MySQL [(none)]> SELECT * FROM tproxy_demo.users;
```

Our goal is to intercept and modify the latter MySQL query with TProxy's GUI editor.

## The attack

When the original query is sent, we observe the following traffic through Tproxy: 


<img src="../demo/mysql/img/tproxy_original_query.png" width="800">


The first four bytes of the packet are MySQL headers composed of the payload length (3 bytes, little-endian) followed by a sequence ID (1 byte). Thus, in order for the server to accept our tampered command, we have to modify the payload length accordingly.

The first step of the attack is to intercept the above traffic in Tproxy using the interception filter `srv.ip == 10.5.0.2`.  Then, we change the content of the *Request Query* packet from `SELECT * FROM tproxy_demo.users` to `DROP TABLE tproxy_demo.users`. The new command's length being 29 bytes, we adapt the first byte of the MySQL headers from 0x20 to 0x1d. We can now forward the tampered packet and its response.


<img src="../demo/mysql/img/tproxy_modified_query.png" width="800">


The client sees the following message in response from their query :

<img src="../demo/mysql/img/query_response.png">

Finally, we check that the table `users` has indeed been deleted : 

<img src="../demo/mysql/img/check_deleted_table.png">


The [video](demo/mysql/video/demo.mp4) below shows an execution of this attack:  

<video style="width:100%" controls src="../demo/mysql/video/demo.mp4" type="video/mp4" />






