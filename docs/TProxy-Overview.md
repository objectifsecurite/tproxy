# Overview

The figure below shows how traffic captured between a client and a server flows through TProxy. Note that some packets may be blocked manually or by a script (see section **Filters** below). In this case, they would not follow the entire path to their destination.


![TProxy Overview](img/tproxy_overview.png)

The blue parts of the flow are optional. If Wireshark dissection is enabled (default) and if the dissection server is reachable, TProxy requests the dissection of each intercepted frame. To this end, it sends a request to the Wireshark dissector running on port 9082, which replies with the corresponding dissection tree parsed in JSON. 

Incoming packets may further be redirected through a script running on port 9081. This script should send back to TProxy the packet(s) (if any) to forward. Note that there is no restriction on the number of packets sent by the script to TProxy: one can block, modify or inject as many packets as needed. For more details, refer to section [Scripts](Scripts.md). In order to redirect the desired packets to a running script, one should use interception filters.

## Filters

Three types of filters are available: capture filters, interception filters and script interception filters. They can all be set in the GUI, while the CLI only allows to set script interception filters (fore more details, see [Command Line Interface](Command-Line-Interface.md) and [Graphical User Interface](Graphical-User-Interface.md)).

Filters can be applied on ports, IP addresses, connection IDs and specific packet types. All three types of filters have the same syntax:

* Port: `[clt|srv].port [==|!=] <port_number>`
* IP Address: `[clt|srv].ip [==|!=] <ip_address>`
* Connection ID: `cid [==|!=] <connection_id>`
* Specific packet: `[close|data|request|response|ssl]`
* Boolean operators: `!`, `&`, `|`


### Capture filters
Capture filters are used to narrow the number of packets displayed according to a filter criterion. For example, when using the display filter `srv.ip == 192.168.1.1`, only packets with server IP `192.168.1.1` are displayed.  
These filters can only be used in the GUI. For further information on how to set a capture filter, refer to [Graphical User Interface](Graphical-User-Interface.md).

### Interception filters
Interception filters are used to block packets in the GUI. Once intercepted, packets can be manually modified, forwarded or dropped as needed.  
These filters can only be used in the GUI. For further information on how to set an interception filter, refer to [Graphical User Interface](Graphical-User-Interface.md).

### Script interception filters

Script interception filters are used to redirect filtered packets to a script running on port 9081. Thus, one can write a script to automatically handle incoming packets, which is often more convenient than manual modifications using interception filters.   
These filters can be set both in the GUI and in the CLI. For further information on how to set a script interception filter, refer to [Graphical User Interface](Graphical-User-Interface.md) and [Command Line Interface](Command-Line-Interface.md).

