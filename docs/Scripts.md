# Scripts

Script interception filters can be set in TProxy in order to redirect filtered traffic to a script running on port 9081 (see [Overview](TProxy-Overview.md)). This is especially useful to automatically modify, block or forward packets on-the-fly.

Messages exchanged between TProxy and the script are serialized in JSON and must have the following syntax:

```json
{
    "cid": <conneciton_id> ,
    "msg_type" : <"request"|"response"> ,
    "cltAddr" : {"ip" : <clt_ip>, "port": <clt_port> } ,
    "srvAddr" : {"ip" : <srv_ip>, "port": <srv_port> } ,
    "close" : <true|false> ,
    "data"  : <data> ,
}
```

Below is an example of a valid message : 

```json
{
    "cid": "abcd1234" ,
    "msg_type" : "response" ,
    "cltAddr" : {"ip" : "10.0.0.2", "port": 5678 } ,
    "srvAddr" : {"ip" : "10.0.0.3", "port": 443 } ,
    "close" : false ,
    "data"  : "HTTP/1.1 200 OK" ,
}
```

The above data format is pretty much the only restriction on the script's behaviour. The script itself can be written in any programming language and can do anything with the intercepted messages. In particular, there is no restrictions on the number of packets sent from the script to TProxy: the script can modify and drop intercepted messages, as well as inject new messages. 


A few examples of Python scripts are provided in the folder `scripts/`. If you want to keep the same structure, you can define your own Python script by creating a class that extends our super-class `Tproxy` defined in `tproxy_script.py`. In this case, your custom sub-class should only implement the method `handle(msg: Message)`. All lower-level behaviours - such as sending and reassembling TCP segments - are already implemented in the class `Tproxy`.