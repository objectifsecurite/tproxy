package ch.os.tproxy.wireshark.dissection;

import ch.os.tproxy.common.natlib.NatlibLoader;

import java.lang.ref.Cleaner;

public class DissectTree implements AutoCloseable {
    static {
        NatlibLoader.load("tproxy_wireshark_dissection", true);
        NatlibLoader.load("tproxy_wireshark_dissection_jni", true);
    }

    private final static Cleaner cleaner = Cleaner.create();
    private final Cleaner.Cleanable cleanable;
    private final EdtState edtState;

    private String protocol;
    private String info;
    private int frameNumber;
    private DissectTreeNode root = null;

    public DissectTree(long edt) {
        this.protocol = getProtocol(edt);
        this.info = getInfo(edt);
        this.frameNumber = getFrameNumber(edt);

        this.edtState = new EdtState(edt);
        cleanable = cleaner.register(this, edtState);
    }

    public long getPointer() {
        return edtState.edt;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInfo() {
        return info;
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public int getOffset() {
        DissectTreeNode tcp = getRoot().findTag("tcp");
        assert tcp != null;
        return tcp.getPosition() + tcp.getLength();
    }

    public DissectTreeNode getRoot() {
        if (root == null) root = new DissectTreeNode(this, getRoot(edtState.edt));
        return root;
    }

    @Override
    public void close() {
        cleanable.clean();
    }

    private static class EdtState implements Runnable {
        private long edt;

        private EdtState(long edt) {
            this.edt = edt;
        }

        @Override
        public void run() {
            if (edt != 0) {
                releaseEdt(edt);
                edt = 0;
            }
        }
    }

    protected static native void releaseEdt(long edt);

    protected static native long getRoot(long edt);

    protected static native String getProtocol(long edt);

    protected static native String getInfo(long edt);

    protected static native int getFrameNumber(long edt);


}
