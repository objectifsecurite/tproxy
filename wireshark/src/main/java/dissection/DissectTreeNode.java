package ch.os.tproxy.wireshark.dissection;

import ch.os.tproxy.common.natlib.NatlibLoader;

import java.util.ArrayList;

public class DissectTreeNode {
    static {
        NatlibLoader.load("tproxy_wireshark_dissection", true);
        NatlibLoader.load("tproxy_wireshark_dissection_jni", true);
    }

    private DissectTree tree;
    private DissectTreeNode parent;
    private boolean root = false;
    private final long pointer;
    private ArrayList<DissectTreeNode> siblings = null;
    private ArrayList<DissectTreeNode> children = null;

    public DissectTreeNode(DissectTree tree, DissectTreeNode parent, long pointer) {
        this.tree = tree;
        this.parent = parent;
        this.pointer = pointer;
    }

    public DissectTreeNode(DissectTree tree, long pointer) {
        this(tree, null, pointer);
        this.root = true;
    }

    private ArrayList<DissectTreeNode> getSiblings() {
        if (siblings != null) return siblings;
        siblings = new ArrayList<DissectTreeNode>();
        DissectTreeNode brother = getNext();
        while (brother != null) {
            siblings.add(brother);
            brother = brother.getNext();
        }
        return siblings;
    }

    private ArrayList<DissectTreeNode> getChildren() {
        if (children != null) return children;
        children = new ArrayList<DissectTreeNode>();
        DissectTreeNode child = getFirstChild();
        if (child != null) {
            children.add(child);
            children.addAll(child.getSiblings());
        }
        return children;
    }

    public DissectTreeNode getParent() {
        return parent;
    }


    public DissectTreeNode findTag(String tag) {
        if (this.getAbbrev().equals(tag)) return this;
        for (DissectTreeNode child : getChildren()) {
            DissectTreeNode grandchild = child.findTag(tag);
            if (grandchild != null) {
                return grandchild;
            }
        }
        if (parent == null && root) { // Check siblings if first generation
            for (DissectTreeNode brother : getSiblings()) {
                DissectTreeNode nephew = brother.findTag(tag);
                if (nephew != null) {
                    return nephew;
                }
            }
        }
        return null;
    }


    public DissectTreeNode getFirstChild() {
        long child = getFirstChild(pointer);
        if (child == 0) return null;
        return new DissectTreeNode(tree, this, child);
    }

    public DissectTreeNode getNext() {
        long next = getNext(pointer);
        if (next == 0) return null;
        return new DissectTreeNode(tree, parent, next);
    }

    public long getPointer() {
        return pointer;
    }

    public int getLength() {
        return getLength(pointer);
    }

    public int getPosition() {
        return getPosition(pointer);
    }

    public String getAbbrev() {
        return getAbbrev(pointer);
    }

    public String getName() {
        return getName(pointer);
    }

    public String getValue() {
        return getValue(tree.getPointer(), pointer);
    }

    public boolean hasByteValue() {
        return hasByteValue(pointer);
    }

    public boolean isTextItem() {
        return getName().equals("Text item");
    }

    protected static native long getFirstChild(long node);

    protected static native long getNext(long node);

    protected static native int getLength(long node);

    protected static native int getPosition(long node);

    protected static native String getAbbrev(long node);

    protected static native String getName(long node);

    protected static native String getValue(long edt, long node);

    protected static native boolean hasByteValue(long node);

}

