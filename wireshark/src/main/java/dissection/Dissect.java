package ch.os.tproxy.wireshark.dissection;

import ch.os.tproxy.common.natlib.NatlibLoader;

public class Dissect {
    static {
        NatlibLoader.load("tproxy_wireshark_dissection", true);
        NatlibLoader.load("tproxy_wireshark_dissection_jni", true);
    }

    public static DissectTree dissectFrameForProto(byte[] data, String proto) {
        return new DissectTree(dissect(data, data.length, proto));
    }

    public static DissectTree dissectFrame(byte[] data) {
        return dissectFrameForProto(data, "");
    }

    public static native void init();

    public static native synchronized long dissect(byte[] pcapData, int size, String proto);
}

