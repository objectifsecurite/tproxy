#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>
#include <stdio.h>

#include "pcap_frame.h"

void pcap_frame_release(pcap_frame_t *frame)
{
  free(frame->data);
  free(frame);
}

pcap_frame_t *pcap_frame_wrap(char *buff, uint32_t size)
{
  pcap_frame_t *frame = (pcap_frame_t*)malloc(sizeof(pcap_frame_t));

  frame->data = buff;
  frame->size = size;
  frame->pcap_hdr = (struct pcaphdr*)buff;
  frame->eth_hdr  = (struct ethhdr*)(frame->pcap_hdr + 1);
  frame->ip_hdr   = (struct iphdr*) (frame->eth_hdr  + 1);
  frame->tcp_hdr  = (struct tcphdr*)(frame->ip_hdr   + 1);

  assert(ntohs(frame->ip_hdr->tot_len) <=
  	 size-sizeof(struct pcaphdr)-sizeof(struct ethhdr));

  return frame;
}
