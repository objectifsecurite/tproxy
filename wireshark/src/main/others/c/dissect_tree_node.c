#include <epan/epan.h>
#include <epan/epan_dissect.h>
#include <epan/packet.h>

#include "dissect_tree_node.h"

/* from epan/print.c */
static int proto_data = -1;
/* Returns an g_malloced string */
const gchar* get_node_field_value(field_info* fi, epan_dissect_t* edt) {
  if (fi->hfinfo->id == hf_text_only) {
    /* Text label.
     * Get the text */
    if (fi->rep) {
      return g_strdup(fi->rep->representation);
    }
    else {
      return get_field_hex_value(edt->pi.data_src, fi);
    }
  }
  else if (fi->hfinfo->id == proto_data) {
    /* Uninterpreted data, i.e., the "Data" protocol, is
     * printed as a field instead of a protocol. */
    return get_field_hex_value(edt->pi.data_src, fi);
  }
  else {
    /* Normal protocols and fields */
    gchar      *dfilter_string;

    switch (fi->hfinfo->type)
      {
      case FT_PROTOCOL:
	/* Print out the full details for the protocol. */
	if (fi->rep) {
	  return g_strdup(fi->rep->representation);
	} else {
	  /* Just print out the protocol abbreviation */
	  return g_strdup(fi->hfinfo->abbrev);
	}
      case FT_NONE:
	/* Return "1" so that the presence of a field of type
	 * FT_NONE can be checked when using -T fields */
	return g_strdup("1");
      default:
	dfilter_string = fvalue_to_string_repr(NULL, &fi->value, FTREPR_DISPLAY, fi->hfinfo->display);
	if (dfilter_string != NULL) {
	  return dfilter_string;
	} else {
	  return get_field_hex_value(edt->pi.data_src, fi);
	}
      }
  }
}

/* from epan/print.c */
const gchar* get_field_hex_value(GSList *src_list, field_info *fi) {
  const guint8 *pd;

  if (!fi->ds_tvb)
    return NULL;

  if (fi->length > tvb_captured_length_remaining(fi->ds_tvb, fi->start)) {
    return g_strdup("field length invalid!");
  }

  /* Find the data for this field. */
  pd = get_field_data(src_list, fi);

  if (pd) {
    int        i;
    gchar     *buffer;
    gchar     *p;
    int        len;
    const int  chars_per_byte = 2;

    len    = chars_per_byte * fi->length;
    buffer = (gchar *)g_malloc(sizeof(gchar)*(len + 1));
    buffer[len] = '\0'; /* Ensure NULL termination in bad cases */
    p = buffer;
    /* Print a simple hex dump */
    for (i = 0 ; i < fi->length; i++) {
      g_snprintf(p, chars_per_byte+1, "%02x", pd[i]);
      p += chars_per_byte;
    }
    return buffer;
  } else {
    return NULL;
  }
}

/* from epan/print.c */
const guint8 *get_field_data(GSList *src_list, field_info *fi) {
  GSList   *src_le;
  tvbuff_t *src_tvb;
  gint      length, tvbuff_length;
  struct data_source *src;
  
  for (src_le = src_list; src_le != NULL; src_le = src_le->next) {
    src = (struct data_source *)src_le->data;
    src_tvb = get_data_source_tvb(src);
    if (fi->ds_tvb == src_tvb) {
      /*
       * Found it.
       *
       * XXX - a field can have a length that runs past
       * the end of the tvbuff.  Ideally, that should
       * be fixed when adding an item to the protocol
       * tree, but checking the length when doing
       * that could be expensive.  Until we fix that,
       * we'll do the check here.
       */
      tvbuff_length = tvb_captured_length_remaining(src_tvb,
						    fi->start);
      if (tvbuff_length < 0) {
	return NULL;
      }
      length = fi->length;
      if (length > tvbuff_length)
	length = tvbuff_length;
      return tvb_get_ptr(src_tvb, fi->start, length);
    }
  }
  g_assert_not_reached();
  return NULL;  /* not found */
}

gboolean has_byte_value(field_info* fi){
    return fi->hfinfo->type == FT_BYTES || fi->hfinfo->type == FT_UINT_BYTES;
}

