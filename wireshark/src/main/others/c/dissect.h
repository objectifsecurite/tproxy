#ifndef DISSECT_H
#define DISSECT_H

#include <epan/epan_dissect.h>
#include "pcap_frame.h"

void dissect_init(void);

epan_dissect_t *dissect_frame_for_proto(pcap_frame_t *frame, const char *proto_name);
epan_dissect_t *dissect_frame(pcap_frame_t *frame);

#endif
