#ifndef DISSECT_TREE_NODE_H
#define DISSECT_TREE_NODE_H

const gchar* get_node_field_value(field_info* fi, epan_dissect_t* edt);
const gchar* get_field_hex_value(GSList *src_list, field_info *fi);
const guint8 *get_field_data(GSList *src_list, field_info *fi);
gboolean has_byte_value(field_info* fi);
#endif
