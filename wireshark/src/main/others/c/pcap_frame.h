#ifndef WIRESHARK_PCAP_FRAME_H
#define WIRESHARK_PCAP_FRAME_H

#include "../../../../../common/src/main/others/c/pcap_frame.h"

void pcap_frame_release(pcap_frame_t *);
pcap_frame_t *pcap_frame_wrap(char *, uint32_t);
#endif
