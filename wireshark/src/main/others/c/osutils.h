#ifndef OSUTILS_H
#define OSUTILS_H

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

typedef struct {
  uid_t uid;
  gid_t gid;
  char *dir;
} upwd_t;

upwd_t *upwd_from_passwd(struct passwd *pwd);
void upwd_free(upwd_t *upwd);
upwd_t *upwd_logged(void);

int logged_as_current_user(void);

#endif
