#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <wiretap/wtap.h>
#include <epan/packet.h>
#include <epan/proto.h>
#include <epan/epan.h>
#include <epan/epan_dissect.h>
#include <epan/ftypes/ftypes.h>
#include <epan/column.h>
#include <epan/prefs.h>
#include <epan/tap.h>
#include <epan/timestamp.h>
#include <epan/tvbuff-int.h>
#include <wsutil/privileges.h>

#include "dissect.h"

/*
 * The protocol_name_search structure is used by find_protocol_name_func()
 * to pass parameters and store results
 */
struct protocol_name_search{
  gchar              *searched_name;  /* Protocol filter name we are looking for */
  dissector_handle_t  matched_handle; /* Handle for a dissector whose protocol has the specified filter name */
  guint               nb_match;       /* How many dissectors matched searched_name */
};
typedef struct protocol_name_search *protocol_name_search_t;

/*
 * This function parses all dissectors associated with a table to find the
 * one whose protocol has the specified filter name.  It is called
 * as a reference function in a call to dissector_table_foreach_handle.
 * The name we are looking for, as well as the results, are stored in the
 * protocol_name_search struct pointed to by user_data.
 * If called using dissector_table_foreach_handle, we actually parse the
 * whole list of dissectors.
 */
static void find_protocol_name_func(const gchar *table _U_, gpointer handle, gpointer user_data)

{
  int                     proto_id;
  const gchar            *protocol_filter_name;
  protocol_name_search_t  search_info;

  g_assert(handle);

  search_info = (protocol_name_search_t)user_data;

  proto_id = dissector_handle_get_protocol_index((dissector_handle_t)handle);
  if (proto_id != -1) {
    protocol_filter_name = proto_get_protocol_filter_name(proto_id);
    g_assert(protocol_filter_name != NULL);
    if (strcmp(protocol_filter_name, search_info->searched_name) == 0) {
      /* Found a match */
      if (search_info->nb_match == 0) {
        /* Record this handle only if this is the first match */
        search_info->matched_handle = (dissector_handle_t)handle; /* Record the handle for this matching dissector */
      }
      search_info->nb_match++;
    }
  }
}

void init_cinfo(column_info *cinfo)
{
  static const char *col_fmt[] = {
    "No.",         "%m", "Time",             "%t",
    "Source",      "%s", "Source port",      "%S",
    "Destination", "%d", "Destination port", "%D",
    "Protocol",    "%p", "Length",           "%L",
    "Info",        "%i"
  };

  static const int num_cols = sizeof(col_fmt) / sizeof(char*) / 2;
  col_item_t* col_item;

  col_setup(cinfo, num_cols);

  for (int i=0; i < cinfo->num_cols; ++i) {
    col_item = &cinfo->columns[i];
    col_item->col_fmt = get_column_format_from_str(col_fmt[(i * 2) + 1]);
    col_item->col_title = strdup(col_fmt[i * 2]);

    col_item->col_custom_fields = NULL;
    col_item->col_custom_occurrence = 0;
    col_item->col_custom_dfilter = NULL;

    col_item->fmt_matx = (gboolean *) g_malloc0(sizeof(gboolean) * NUM_COL_FMTS);
    get_column_format_matches(col_item->fmt_matx, col_item->col_fmt);
    col_item->col_data = NULL;

    if (col_item->col_fmt == COL_INFO)
      col_item->col_buf = (gchar *) g_malloc(sizeof(gchar) * COL_MAX_INFO_LEN);
    else
      col_item->col_buf = (gchar *) g_malloc(sizeof(gchar) * COL_MAX_LEN);
    
    col_item->col_fence = 0;

    cinfo->col_expr.col_expr[i] = (gchar *) g_malloc(sizeof(gchar) * COL_MAX_LEN);
    cinfo->col_expr.col_expr_val[i] = (gchar *) g_malloc(sizeof(gchar) * COL_MAX_LEN);
  }

  cinfo->col_expr.col_expr[cinfo->num_cols] = NULL;
  cinfo->col_expr.col_expr_val[cinfo->num_cols] = NULL;

  for (int i=0; i < cinfo->num_cols; ++i) {
    for (int j=0; j < NUM_COL_FMTS; ++j) {
      if (!cinfo->columns[i].fmt_matx[j])
          continue;

      if (cinfo->col_first[j] == -1)
        cinfo->col_first[j] = i;

      cinfo->col_last[j] = i;
    }
  }
}

nstime_t elapsed_time;
column_info cinfo;
const frame_data *ref;
epan_t *epan;
nstime_t tm;

static const nstime_t *get_frame_ts(struct packet_provider_data *prov, guint32 frame_num)
{
  return &tm;
}

void dissect_init(void)
{
  init_process_policies();

  timestamp_set_type(TS_RELATIVE);
  timestamp_set_precision(TS_PREC_AUTO);
  timestamp_set_seconds_type(TS_SECONDS_DEFAULT);

  wtap_init(TRUE);  // FALSE if we do not want to load plugins
  epan_init(NULL, NULL, TRUE);

  prefs_reset();
  //cleanup_dissection();
  //init_dissection();
  nstime_set_zero(&elapsed_time);

  init_cinfo(&cinfo);

  epan_free(epan);

  static const struct packet_provider_funcs funcs = {
    get_frame_ts,
    NULL,
  };

  epan = epan_new(NULL, &funcs);
  nstime_set_zero(&tm);
}

epan_dissect_t *dissect_frame_for_proto(pcap_frame_t *frame, const char *proto_name)
{
  protocol_name_search_t proto_search = &(struct protocol_name_search){
    (char*)proto_name,
    NULL,
    0
  };
  dissector_table_foreach_handle("tcp.port", find_protocol_name_func, &proto_search);
  int port = ntohs(frame->tcp_hdr->dest);
  dissector_change_uint("tcp.port", port, proto_search->matched_handle);
  epan_dissect_t *edt = dissect_frame(frame);
  dissector_reset_uint("tcp.port", port);

  return edt;
}

epan_dissect_t *dissect_frame(pcap_frame_t *frame)
{
  static uint32_t count = 0;
  static uint32_t cum_bytes = 0;

  wtap_rec rec;
  wtap_rec_init(&rec);

  guint32 caplen = frame->size - sizeof(struct pcaphdr);

  rec.presence_flags = 3;
  rec.ts.secs = frame->pcap_hdr->ts_sec;
  rec.ts.nsecs = frame->pcap_hdr->ts_usec * 1000;
  rec.rec_header.packet_header.caplen = caplen;
  rec.rec_header.packet_header.len = caplen;
  rec.rec_header.packet_header.pkt_encap = WTAP_ENCAP_ETHERNET;

  ++count;

  frame_data fdata;

  frame_data_init(&fdata, count, &rec, 0, 0);
  frame_data_set_before_dissect(&fdata, &elapsed_time, &ref, NULL);

  epan_dissect_t *edt = NULL;

  uint8_t* pd = (uint8_t*)frame->eth_hdr;
  tvbuff_t *tvbuff = tvb_new_real_data(pd, caplen, caplen);

  edt = epan_dissect_new(epan, TRUE, TRUE);

  epan_dissect_run(edt, 0, &rec, tvbuff, &fdata, &cinfo);
  epan_dissect_fill_in_columns(edt, TRUE, TRUE);

  frame_data_set_after_dissect(&fdata, &cum_bytes);
  frame_data_destroy(&fdata);

  //assert(edt->pi.tcp_tree != NULL);

  if (frame->size > 65535) {
    fprintf(stderr, "WARNING %d: %d\n", count, frame->size);

    int n = atoi(cinfo.columns[0].col_data);
    fprintf(stderr, "WARNING %d, ", n);

    for (int i=1; i<cinfo.num_cols; ++i) {
      fprintf(stderr, "%s", cinfo.columns[i].col_data);

      if (i < cinfo.num_cols-1)
  	fprintf(stderr, ", ");
      else
  	fprintf(stderr, "\n");
    }
  }

  return edt;
}
