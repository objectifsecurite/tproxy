#include <stdio.h>
#include <fcntl.h>
#include <epan/column.h>

#include "osutils.h"
#include "dissect.h"
#include "pcap_frame.h"

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <pcap dump file>\n", argv[0]);
    return 1;
  }

  char *fname = argv[1];
  int dumpfd = open(fname, O_RDONLY);

  if (dumpfd < 0) {
    fprintf(stderr, "Cannot open file %s.\n", fname);
    return 1;
  }

  if (getuid() == 0)
    logged_as_current_user();
  
  dissect_init();

  char pcap_file_hdr[24] = { 0 };
  read(dumpfd, pcap_file_hdr, sizeof(pcap_file_hdr));

  struct pcaphdr pcap_hdr;
  size_t pcap_hdr_size = sizeof(struct pcaphdr);

  static char *buff = NULL;
  static uint32_t buff_size = 0;

  while (read(dumpfd, &pcap_hdr, pcap_hdr_size) == pcap_hdr_size) {
    uint32_t pkt_size = pcap_hdr.incl_len;
    uint32_t total_size = pcap_hdr_size + pkt_size;

    if (total_size > buff_size) {
      buff = realloc(buff, total_size);
      buff_size = total_size;
    }

    memcpy(buff, &pcap_hdr, pcap_hdr_size);
    
    if (read(dumpfd, buff+pcap_hdr_size, pkt_size) != pkt_size)
      break;

    pcap_frame_t *frame = pcap_frame_wrap(buff, total_size);

    epan_dissect_t *edt = dissect_frame(frame);
    printf("%s\n", edt->pi.cinfo->columns[8].col_data);

    proto_node *node = edt->tree->first_child;
    while (node != 0) {
      printf("%s\n", node->finfo->hfinfo->abbrev);
      node = node->next;
    }
  }

  return 0;
}
