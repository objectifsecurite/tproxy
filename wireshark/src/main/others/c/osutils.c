#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/prctl.h>
#include <grp.h>
#include "osutils.h"

upwd_t *upwd_from_passwd(struct passwd *pwd) {
  upwd_t *upwd = (upwd_t*)malloc(sizeof(upwd_t));

  upwd->uid = pwd->pw_uid;
  upwd->gid = pwd->pw_gid;

  size_t n = strlen(pwd->pw_dir) + 1;
  upwd->dir = (char*)malloc(n*sizeof(char));

  strncpy(upwd->dir, pwd->pw_dir, n);

  return upwd;
}

void upwd_free(upwd_t *upwd) {
  free(upwd->dir);
  free(upwd);
}

upwd_t *upwd_logged(void) {
  char *login = getlogin();
  struct passwd *pwd = getpwnam(login);

  return upwd_from_passwd(pwd);
}

/*
Inspired from
http://stackoverflow.com/questions/13183327/drop-root-uid-while-retaining-cap-sys-nice/13186076#13186076
*/

int logged_as_current_user(void) {
  char *login = getlogin();
  upwd_t *upwd = upwd_logged();

  int result = initgroups(login, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on initgroups");
#endif
    return 1;
  }

  result = setresgid(upwd->gid, upwd->gid, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on setresgid");
#endif
    return 2;
  }

  result = setresuid(upwd->uid, upwd->uid, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on setresuid");
#endif
    return 3;
  }

  size_t dirlen = strlen(upwd->dir);
  size_t varlen = 5+dirlen+1;
  char *homedir = (char*)malloc(varlen*sizeof(char));
  
  snprintf(homedir, varlen, "HOME=%s", upwd->dir);

  result = putenv(homedir);

  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on putenv");
#endif
    return 4;
  }

  return 0;
}
