/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class ch_os_tproxy_wireshark_dissection_Dissect */

#ifndef _Included_ch_os_tproxy_wireshark_dissection_Dissect
#define _Included_ch_os_tproxy_wireshark_dissection_Dissect
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     ch_os_tproxy_wireshark_dissection_Dissect
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_ch_os_tproxy_wireshark_dissection_Dissect_init
  (JNIEnv *, jclass);

/*
 * Class:     ch_os_tproxy_wireshark_dissection_Dissect
 * Method:    dissect
 * Signature: ([BILjava/lang/String;)J
 */
JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_Dissect_dissect
  (JNIEnv *, jclass, jbyteArray, jint, jstring);

#ifdef __cplusplus
}
#endif
#endif
