#include <glib.h>

#include "Dissect.h"
#include "dissect.h"
#include "pcap_frame.h"

JNIEXPORT void JNICALL Java_ch_os_tproxy_wireshark_dissection_Dissect_init(JNIEnv *env, jclass jdissector)
{
  dissect_init();
}

JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_Dissect_dissect
  (JNIEnv *env, jclass class, jbyteArray jbytes, jint size, jstring proto){

  jbyte *data = (*env)->GetByteArrayElements(env, jbytes, NULL);


  pcap_frame_t *frame = pcap_frame_wrap((char *)data, (uint32_t)size);
  const char *proto_name = (*env)->GetStringUTFChars(env, proto, 0);
  jlong edt;

  if (proto_name && proto_name[0] == '\0'){ // Empty String => No "Decode As"
    edt = (jlong) dissect_frame(frame);
    }
  else // Decode as proto_name
    edt = (jlong) dissect_frame_for_proto(frame, proto_name);

  pcap_frame_release(frame);
  (*env)->ReleaseStringUTFChars(env, proto, proto_name);

  return edt;
}
