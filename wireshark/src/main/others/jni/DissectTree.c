#include <jni.h>
#include <epan/proto.h>
#include <epan/epan_dissect.h>
#include <epan/column.h>

#include "DissectTree.h"

JNIEXPORT void JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_releaseEdt(JNIEnv *env, jclass class, jlong pointer)
{
  epan_dissect_t *edt = (epan_dissect_t*)pointer;
  epan_dissect_free(edt);
}

JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getRoot(JNIEnv *env, jclass class, jlong pointer)
{
  epan_dissect_t *edt = (epan_dissect_t*)pointer;
  proto_node *node = edt->tree->first_child;
  while (node != 0) {
    if (!strcmp(node->finfo->hfinfo->abbrev, "tcp"))
      return (jlong)node;
    node = node->next;
  }
  return (jlong)NULL;
}

JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getProtocol(JNIEnv *env, jclass class, jlong pointer)
{
  epan_dissect_t *edt = (epan_dissect_t*)pointer;

  return (*env)->NewStringUTF(env, edt->pi.cinfo->columns[6].col_data);
}

JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getInfo(JNIEnv *env, jclass class, jlong pointer)
{
  epan_dissect_t *edt = (epan_dissect_t*)pointer;

  return (*env)->NewStringUTF(env, edt->pi.cinfo->columns[8].col_data);
}

JNIEXPORT jint JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getFrameNumber(JNIEnv *env, jclass class, jlong pointer)
{
  epan_dissect_t *edt = (epan_dissect_t*)pointer;

  return (jint) atoi(edt->pi.cinfo->columns[0].col_data);
}
