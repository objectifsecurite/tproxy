/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class ch_os_tproxy_wireshark_dissection_DissectTree */

#ifndef _Included_ch_os_tproxy_wireshark_dissection_DissectTree
#define _Included_ch_os_tproxy_wireshark_dissection_DissectTree
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     ch_os_tproxy_wireshark_dissection_DissectTree
 * Method:    releaseEdt
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_releaseEdt
  (JNIEnv *, jclass, jlong);

/*
 * Class:     ch_os_tproxy_wireshark_dissection_DissectTree
 * Method:    getRoot
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getRoot
  (JNIEnv *, jclass, jlong);

/*
 * Class:     ch_os_tproxy_wireshark_dissection_DissectTree
 * Method:    getProtocol
 * Signature: (J)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getProtocol
  (JNIEnv *, jclass, jlong);

/*
 * Class:     ch_os_tproxy_wireshark_dissection_DissectTree
 * Method:    getInfo
 * Signature: (J)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getInfo
  (JNIEnv *, jclass, jlong);

/*
 * Class:     ch_os_tproxy_wireshark_dissection_DissectTree
 * Method:    getFrameNumber
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTree_getFrameNumber
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif
