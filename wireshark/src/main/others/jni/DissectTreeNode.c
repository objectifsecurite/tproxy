#include <jni.h>
#include <epan/proto.h>
#include <epan/epan_dissect.h>

#include "DissectTreeNode.h"
#include "dissect_tree_node.h"

JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getFirstChild(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  return (jlong)node->first_child;
}

JNIEXPORT jlong JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getNext(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  return (jlong)node->next;
}

JNIEXPORT jint JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getLength(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  field_info *fi = node->finfo;
  
  if (fi == NULL) return (jint)-1;
  return (jint)fi->length;
}

JNIEXPORT jint JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getPosition(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  field_info *fi = node->finfo;
  
  if (fi == NULL) return (jint)-1;

  if (node->parent && node->parent->finfo
      && (fi->start < node->parent->finfo->start)) {
    return (jint)(node->parent->finfo->start + fi->start);
  } else {
    return (jint)fi->start;
  }
}

JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getAbbrev(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  field_info *fi = node->finfo;
  if (fi == NULL) return NULL;
  return (*env)->NewStringUTF(env, fi->hfinfo->abbrev);
}

JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getName(JNIEnv *env, jclass class, jlong pointer)
{
  proto_node *node = (proto_node*)pointer;
  field_info *fi = node->finfo;
  if (fi == NULL) return (jstring)NULL;
  return (*env)->NewStringUTF(env, fi->hfinfo->name);
}

JNIEXPORT jstring JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_getValue(JNIEnv *env, jclass class,
			      jlong edt_pointer, jlong node_pointer)
{
  proto_node *node = (proto_node*)node_pointer;
  field_info *fi = node->finfo;
  if (fi == NULL) return (jstring)NULL;
  epan_dissect_t *edt = (epan_dissect_t*)edt_pointer;
  return (*env)->NewStringUTF(env, get_node_field_value(fi, edt));
}

JNIEXPORT jboolean JNICALL Java_ch_os_tproxy_wireshark_dissection_DissectTreeNode_hasByteValue
  (JNIEnv *env, jclass class, jlong node_pointer){
  proto_node *node = (proto_node*)node_pointer;
  field_info *fi = node->finfo;
  if (fi == NULL) return FALSE;

  return has_byte_value(fi);
}