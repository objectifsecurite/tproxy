package ch.os.tproxy.wireshark

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.io.{IO, Tcp}
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.log.{LogReceive, Logging}

import java.net.InetSocketAddress

object DissectionServer {
  private var actor: Option[ActorRef] = None

  private val address = new InetSocketAddress(9082)

  def getActor(implicit system: ActorSystem, logging: Logging, logReceive: LogReceive) = {
    actor match {
      case Some(a) => a
      case None =>
        val props = Props(classOf[DissectionServer], logging, logReceive)
        val a = system actorOf(props, "DissectionServer")
        actor = Some(a)
        a
    }
  }
}

class DissectionServer(_logging: Logging, _logReceive: LogReceive) extends ActorNode {

  import DissectionServer._

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive


  def onStart = {
    IO(Tcp) ! Tcp.Bind(self, address)
    Binding()
  }


  private case class Binding() extends OnEvent {
    lazy val log = logging(this)

    def onEvent = {
      case Tcp.Bound(addr) =>
        log info s"${DissectionServer.this} listening on $addr"
        Listening()

      case Tcp.CommandFailed(_) =>
        log info s"Dissection Server cannot bind dissector to $address"
        context stop self
        this

    }
  }

  private case class Listening() extends OnEvent {
    lazy val log = logging(this)

    override def onEvent: Behaviour.Receive = {
      case _: Tcp.Connected =>
        log info "New client connected"
        val props = Props(classOf[DissectionActor], sender, logging, logReceive)
        context actorOf props
        this
    }
  }
}
