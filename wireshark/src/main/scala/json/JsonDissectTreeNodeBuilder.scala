package ch.os.tproxy.wireshark.json

import ch.os.tproxy.common.json.JsonDissectTreeNode
import ch.os.tproxy.common.json.JsonDissectTreeNode.{BYTES, NULL, OTHER, TEXT}
import ch.os.tproxy.wireshark.dissection.DissectTreeNode
import ch.os.tproxy.wireshark.json.JsonDissectTreeBuilder.splitDepth

import scala.annotation.tailrec

object JsonDissectTreeNodeBuilder {
  def newJsonDissectTreeNode(dissectTreeNode: DissectTreeNode, depth: Int): (JsonDissectTreeNode, Vector[JsonDissectTreeNode]) = {
    val partialNode = getPartialNode(dissectTreeNode, depth <= splitDepth && dissectTreeNode.getNext != null)
    val (nodeWithChild, splitSubTrees) = withFirstChild(partialNode, dissectTreeNode.getFirstChild, depth)
    val (node, nextsplitSubTrees) = withNextNode(nodeWithChild, dissectTreeNode.getNext, depth)
    (node, splitSubTrees ++ nextsplitSubTrees)
  }


  private def getPartialNode(node: DissectTreeNode, pendingNextNode: Boolean = false): JsonDissectTreeNode = {
    // fields firstChild and next are left blank (null)
    val value = if (node.hasByteValue || node.isTextItem) None else Some(node.getValue)
    val valueType = node.getValue match {
      case null => NULL
      case _ if node.isTextItem => TEXT
      case _ if node.hasByteValue => BYTES
      case _ => OTHER
    }
    JsonDissectTreeNode(node.getLength, node.getPosition, node.getAbbrev, node.getName, value, valueType, pendingNextNode)
  }

  private def withFirstChild(node: JsonDissectTreeNode, child: DissectTreeNode, depth: Int): (JsonDissectTreeNode, Vector[JsonDissectTreeNode]) = {
    @tailrec
    def loop(currentNode: JsonDissectTreeNode, child: DissectTreeNode, depth: Int, splitSubTrees: Vector[JsonDissectTreeNode])
            (initialNode: JsonDissectTreeNode = currentNode): (JsonDissectTreeNode, Vector[JsonDissectTreeNode]) = {
      if (child == null) {
        (initialNode, splitSubTrees)
      } else {
        val partialChild = getPartialNode(child, depth + 1 <= splitDepth && child.getNext != null)
        val (jsonChild, newSplitSubtrees) = withNextNode(partialChild, child.getNext, depth + 1)
        currentNode.firstChild = jsonChild
        loop(jsonChild, child.getFirstChild, depth + 1, newSplitSubtrees ++ splitSubTrees)(initialNode)
      }
    }

    loop(node.copy(), child, depth, Vector.empty)()
  }


  def withNextNode(node: JsonDissectTreeNode, next: DissectTreeNode, depth: Int): (JsonDissectTreeNode, Vector[JsonDissectTreeNode]) = {
    @tailrec
    def loop(currentNode: JsonDissectTreeNode, next: DissectTreeNode, splitSubTrees: Vector[JsonDissectTreeNode])
            (initialNode: JsonDissectTreeNode = currentNode): (JsonDissectTreeNode, Vector[JsonDissectTreeNode]) = {
      if (next == null) {
        (initialNode, splitSubTrees)
      }
      else {
        val partialNext = getPartialNode(next, depth <= splitDepth && next.getNext != null)

        var (jsonNext, newSplitSubTrees) = withFirstChild(partialNext, next.getFirstChild, depth)

        if (depth <= splitDepth) {
          // save sibling subtree in a buffer instead of building a huge dissection tree: buffered subtrees are sent separately
          newSplitSubTrees = splitSubTrees ++ Vector(jsonNext) ++ newSplitSubTrees
        } else {
          newSplitSubTrees = splitSubTrees ++ newSplitSubTrees
          currentNode.next = jsonNext
        }
        loop(jsonNext, next.getNext, newSplitSubTrees)(initialNode)
      }
    }

    loop(node.copy(), next, Vector.empty)()
  }
}