package ch.os.tproxy.wireshark.json

import ch.os.tproxy.common.json.{JsonDissectTree, JsonDissectTreeNode}
import ch.os.tproxy.wireshark.dissection.{DissectTree}
import ch.os.tproxy.wireshark.json.JsonDissectTreeNodeBuilder.newJsonDissectTreeNode

object JsonDissectTreeBuilder {
  /*
    We need to split the dissection tree into subtrees in order for upickle to be able to serialize large trees
    We always split horizontally (a node's siblings are built separately), from the root down to a depth limit (= splitDepth)
   -1 = do not split, 0 = split root's siblings, 1 = split root's siblings + root's first child's siblings,  etc.
   */
  val splitDepth = 1

  def newJsonDissectTree(dissectTree: DissectTree): (JsonDissectTree, Vector[JsonDissectTreeNode]) = {
    val rootNode = dissectTree.getRoot.findTag("tcp")
    val (rootJson, splitSubTrees) = if (rootNode == null) (null, Vector.empty) else newJsonDissectTreeNode(rootNode, 0)
    val jsonDissectTree = new JsonDissectTree(dissectTree.getProtocol, dissectTree.getInfo, dissectTree.getFrameNumber,
      rootJson, dissectTree.getOffset, splitDepth)

    // Send root's siblings separately to avoid huge JsonDissecTreeNodes that cannot be serialized with upickle
    (jsonDissectTree, splitSubTrees)
  }
}