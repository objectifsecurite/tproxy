package ch.os.tproxy.wireshark

import akka.actor.ActorSystem
import ch.os.tproxy.wireshark.dissection.Dissect
import ch.os.tproxy.common.Properties
import ch.os.tproxy.common.log.{AkkaLogging, LogReceive, Logging}

object Main {

  def main(args: Array[String]): Unit = {
    Properties prependPaths("java.library.path", "native.library.path")
    implicit val system = ActorSystem("wiresharkDissection")

    implicit val logging = new AkkaLogging(system)
    implicit val logReceive = new LogReceive()

    Dissect.init()
    DissectionServer.getActor
  }
}
