package ch.os.tproxy.wireshark

import akka.actor.ActorRef
import akka.io.Tcp
import akka.io.Tcp.ConnectionClosed
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.tcp.TcpSegmentAggregator
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.json.DissectionRequest
import ch.os.tproxy.common.json.JsonDissectTree.{JsonDissectionAck, JsonDissectionError}
import ch.os.tproxy.wireshark.dissection.Dissect
import ch.os.tproxy.wireshark.json.JsonDissectTreeBuilder.newJsonDissectTree
import ujson.{IncompleteParseException, ParseException}
import upickle.core.AbortException

case class DissectionActor(connection: ActorRef, _logging: Logging, _logReceive: LogReceive) extends NetNode {

  override def onStart: Behaviour.Result =
    (Dissecting()
      <-> TcpSegmentAggregator()
      <-> Backpressure
      <-> SendNetCmdTo(connection, "dissectionClient")) withCommand SendTo(connection, Tcp.Register(self), needWrap = false)

  implicit lazy val logging: Logging = _logging

  implicit val logReceive: LogReceive = _logReceive

  private case class Dissecting() extends OnEvent {
    lazy val log = logging(this)

    override def onEvent: Behaviour.Receive = {
      case data: ByteString =>
        try {
          val req = DissectionRequest(data.utf8String)
          if (req.pktData.nonEmpty) {
            val dissectTree = Dissect.dissectFrameForProto(req.pktData, req.proto)
            if (req.tcpData.nonEmpty)
              Dissect.dissectFrame(req.tcpData) // we do not need the result of dissecting the ack
            val (jsonDissectTree, splitNodes) = newJsonDissectTree(dissectTree)
            dissectTree.close()
            var dissectionResults = Vector(ByteString(jsonDissectTree.toJson))
            splitNodes.foreach(next => dissectionResults :+= ByteString(next.toJson))
            this withCommands dissectionResults
          }
          else {
            if (req.tcpData.nonEmpty) // request only has TCP payload (e.g. SYN, FIN)
              Dissect.dissectFrame(req.tcpData)
            this withCommand ByteString(JsonDissectionAck.toJson)
          }
        }
        catch {
          case _: IncompleteParseException | _: ParseException | _: AbortException =>
            log info "Unable to parse received data"
            this withCommand ByteString(JsonDissectionError.toJson)
        }
      case _: ConnectionClosed =>
        context.stop(self)
        this
    }
  }

}
