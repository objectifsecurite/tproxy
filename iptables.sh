#!/bin/bash
set -o nounset
set -o errexit

flush_table_rules () {
    if [ -z "$1" ]; then 
	echo "Specify a table for flush_table_rules"
	exit
    fi

    #echo "flush $1 from rules $COMMENT"
    iptables -t "$1" -S | grep "$COMMENT" | grep '\-A' | sed 's/-A/-D/' | sed 's/\"//g' |
	while read RULE; do
	    iptables -t "$1" $RULE
	done
}

flush_all_rules () {
    COMMENT=${1:-"tproxy"}
    for table in filter mangle nat; do
	flush_table_rules $table "$COMMENT"
    done
}

init () {
    COMMENT="tproxy:init"
    FULL_COMMENT="-m comment --comment $COMMENT"
    if [ "$(iptables -t mangle -S | grep -c "$COMMENT")" -eq 0 ]; then
#	iptables -t mangle -N DIVERT
#	iptables -t mangle -A DIVERT -j MARK --set-mark 1 $FULL_COMMENT
#	iptables -t mangle -A DIVERT -j ACCEPT $FULL_COMMENT
  iptables -t mangle -A PREROUTING -p tcp -d 127.0.0.1 -m tos --tos 0x20 -j ACCEPT $FULL_COMMENT
	iptables -t mangle -A PREROUTING -p tcp -m socket --transparent -j MARK --set-mark 1 $FULL_COMMENT

	ip rule add fwmark 1 lookup 100
	ip route add local default dev lo table 100
    fi
}

flush () {
    COMMENT="tproxy:init"
    if [ "$(iptables -t mangle -S | grep -c "$COMMENT")" -gt 0 ]; then
	ip route del local default dev lo table 100
	ip rule del fwmark 1 lookup 100
	
	flush_all_rules
#	iptables -t mangle -F DIVERT
#	iptables -t mangle -X DIVERT
	if [ -f "/etc/sysctl.conf" ]; then
		sysctl -p
	fi
    else 
	echo "Already flushed"
    fi
    exit
}

list () {
    for table in filter mangle nat; do
	echo "Rules in table $table:"
	iptables -t $table -S |grep "tproxy" | cat
    done
}

forward () {
    case $1 in
	src)
	    if [[ -z "$2" ]]; then
		echo "Missing IP address or range."
		exit
	    fi
	
	    RULE_FORWARD_IN="-s $2 -j ACCEPT"
	    RULE_FORWARD_OUT="-d $2 -j ACCEPT"
	    ;;
	in)
	    if [[ -z "$2" ]]; then
		echo "Missing interface name."
		exit
	    fi

	    RULE_FORWARD_IN="-i $2 -j ACCEPT"
	    RULE_FORWARD_OUT="-o $2 -j ACCEPT"
	    ;;
	*)
	    echo "USAGE: ./iptables.sh forward <src ip|in interface> <enable|disable>"
	    exit
	    ;;
    esac

    COMMENT="tproxy:forward_$1_$2"
    FULL_COMMENT="-m comment --comment $COMMENT"
    
    if [ "$(iptables -t filter -S | grep -c "$COMMENT")" -eq 0 ]; then
	## 
	## It does not seem necessary to deactivate reverse path
	## filtering.
        #
        # echo 0 > /proc/sys/net/ipv4/conf/lo/rp_filter
	
        ## IP forwarding should be activated if we want to act as
        ## a router.
        #
	echo 1 > /proc/sys/net/ipv4/ip_forward

	## Disable ICMP send redirects
	#
	echo 0 | tee /proc/sys/net/ipv4/conf/*/send_redirects > /dev/null
	
        ## Forward traffic. Rules are inserted with -I to bypass
        ## potential firewall rules which might have been set up.
        ##
	iptables -t filter -I FORWARD 1 $RULE_FORWARD_IN $FULL_COMMENT
	iptables -t filter -I FORWARD 2 $RULE_FORWARD_OUT $FULL_COMMENT
    else
	echo "Already added this forward rule"
	exit
    fi
}

masquerade () {
    case $1 in
	src)
	    if [[ -z "$2" ]]; then
		echo "Missing IP address or range."
		exit
	    fi
	
	    RULE_POSTROUTING="-s $2 -j MASQUERADE"
	    ;;
	*)
	    echo "USAGE: ./iptables.sh masquerade <src ip> <enable|disable>"
	    exit
	    ;;
    esac

    COMMENT="tproxy:masquerade_$1_$2"
    FULL_COMMENT="-m comment --comment $COMMENT"
    
    if [ "$(iptables -t filter -S | grep -c "$COMMENT")" -eq 0 ]; then
        ## Activate masquerading so that the outside world can be
        ## reached.
        ## 
	iptables -t nat -A POSTROUTING $RULE_POSTROUTING $FULL_COMMENT
    else
	echo "Already added this masquerade rule"
	exit
    fi
}

interface () {
    if [ -z "$1" ]; then
	echo "You must specify the interface you want to proxify."
	exit
    fi

    COMMENT="tproxy:interface_$1"
    FULL_COMMENT="-m comment --comment $COMMENT"
    RULE_PREROUTING="-p tcp -i $1 -j TPROXY --tproxy-mark 0x1/0x1 --on-port 9080"
    RULE_INPUT="-i $1 -j ACCEPT"
	
    if [ "$(iptables -t mangle -S | grep -c "$COMMENT")" -eq 0 ]; then
        ## Reroute traffic coming from the given interface to the
        ## transparent proxy.
        #
	iptables -t mangle -A PREROUTING $RULE_PREROUTING $FULL_COMMENT

        ## Since part of the traffic might be rerouted to the
        ## transparent proxy, we must make sure that packets are
        ## allowed to go through. Again, -I is used to insert the rule
        ## before any potential firewall rule which might already be
        ## present.
        ##
	iptables -t filter -I INPUT 1 $RULE_INPUT $FULL_COMMENT
    else
	echo "Already added this interface rule"
	exit
    fi
}

address () {
    case $1 in
	src)
	    if [[ -z "$2" ]]; then
		echo "Missing source address or range."
		exit
	    fi

	    MATCH="-s"
	    ;;
	dst)
	    if [[ -z "$2" ]]; then
		echo "Missing destination address or range."
		exit
	    fi

	    MATCH="-d"
	    ;;
	*)
	    echo "USAGE: ./iptables.sh address <src|dst> <ip> <enable|disable>"
	    exit
	    ;;
    esac

    COMMENT="tproxy:address_$1_$2"
    FULL_COMMENT="-m comment --comment $COMMENT"

    if [ "$(iptables -t mangle -S | grep -c "$COMMENT")" -eq 0 ]; then
	iptables -t mangle -A PREROUTING -p tcp $MATCH $2 -j TPROXY --tproxy-mark 0x1/0x1 --on-port 9080 $FULL_COMMENT
	iptables -t mangle -A OUTPUT -p tcp $MATCH $2 -m tos ! --tos 0x20 -j MARK --set-mark 1 $FULL_COMMENT

        ## Since part of the traffic might be rerouted to the
        ## transparent proxy, we must make sure that packets are
        ## allowed to go through. Again, -I is used to insert the rule
        ## before any potential firewall rule which might already be
        ## present.
        ##
	iptables -t filter -I INPUT 1 $MATCH $2 -j ACCEPT $FULL_COMMENT
    else
	echo "Already added this ip rule"
	exit
    fi
}

port () {
    case $1 in
	src)
	    if [[ -z "$2" ]]; then
		echo "Missing source port."
		exit
	    fi

	    MATCH="-m tcp --sport"
	    ;;
	dst)
	    if [[ -z "$2" ]]; then
		echo "Missing destination port."
		exit
	    fi
	    MATCH="-m tcp --dport"
	    ;;
	*)
	    echo "USAGE: ./iptables.sh port <src|dst> <port> <enable|disable>"
	    exit
	    ;;
    esac

    COMMENT="tproxy:port_$1_$2"
    FULL_COMMENT="-m comment --comment $COMMENT"

    if [ "$(iptables -t mangle -S | grep -c "$COMMENT")" -eq 0 ]; then
	iptables -t mangle -A PREROUTING -p tcp $MATCH $2 -j TPROXY --tproxy-mark 0x1/0x1 --on-port 9080 $FULL_COMMENT
	iptables -t mangle -A OUTPUT -p tcp $MATCH $2 -m tos ! --tos 0x20 -j MARK --set-mark 1 $FULL_COMMENT

        ## Since part of the traffic might be rerouted to the
        ## transparent proxy, we must make sure that packets are
        ## allowed to go through. Again, -I is used to insert the rule
        ## before any potential firewall rule which might already be
        ## present.
        ##
	iptables -t filter -I INPUT 1 -p tcp $MATCH $2 -j ACCEPT $FULL_COMMENT
    else
	echo "Already added this ip rule"
	exit
    fi
}
    

enable_or_disable () {
    case ${@: -1} in 
	enable)
	    ${@: 1:$#-1}
	    ;;
	disable)
	    COMMENT="tproxy:$1"
	    for arg in ${@: 2:$#-2}; do COMMENT=$COMMENT"_$arg"; done
	    flush_all_rules "$COMMENT"
	    ;;
	*)
	    echo "Enable or disable?"
	    exit	
	    ;;
    esac
}


if [ "$(id -u)" -ne 0 ]; then
    echo "ERROR: you are not root"
    exit 1
fi

case $1 in
    init)
	init
	exit
	;;
    flush)
	flush 
	;;
    list)
	list
	;;
    forward)
	init
	enable_or_disable $@
	;;
    masquerade)
	init
	enable_or_disable $@
	;;
    interface)
	init
	enable_or_disable $@
	;;
    address)
	init
	enable_or_disable $@
	;;
    port)
	init
	enable_or_disable $@
	;;
# shortcuts
    http)
	init
	enable_or_disable port dst 80 $2
	;;
    https)
	init
	enable_or_disable port dst 443 $2
	;;
    localhost)
	init
	enable_or_disable address dst 127.0.0.1 $2
	;;
    *)
	echo "./iptables.sh <cmd> [options]"
	echo -e "init\t\tInitialize TPROXY mode"
	echo -e "flush\t\tFlush all TPROXY rules"
	echo -e "list\t\tList all TPROXY rules set"
	echo -e "forward\t\tForward trafic coming from ip or interface"
	echo -e "\t<src ip|in interface> <enable|disable>"
	echo -e "masquerade\tMasquerade trafic coming from ip or interface"
	echo -e "\t<src ip|in interface> <enable|disable>"
	echo -e "interface\tIntercept traffic on selected interface"
	echo -e "\t<interface> <enable|disable>"
	echo -e "address\t\tIntercept traffic on selected IP address"
	echo -e "\t<src|dst> <address> <enable|disable>"
	echo -e "port\t\tIntercept traffic on selected TCP port"
	echo -e "\t<src|dst> <port> <enable|disable>"
	exit
	;;
esac

