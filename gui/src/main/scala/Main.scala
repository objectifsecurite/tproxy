package ch.os.tproxy.gui

import ch.os.tproxy.cli.CliActor
import ch.os.tproxy.MainCli.setup
import ch.os.tproxy.common.log.{AkkaLogging, LogReceive}

object Main {
  def main(args: Array[String]): Unit = {
    val (system, config, address) = setup(args)
    implicit val logging = new AkkaLogging(system)
    implicit val logReceive = new LogReceive()

    if (config getBoolean "tproxy.disable-gui")
      system actorOf(CliActor(address), "CliActor")
    else
      system actorOf(GuiActor(address), "GuiActor")
  }
}
