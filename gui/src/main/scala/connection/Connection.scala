package ch.os.tproxy.gui.connection

import akka.actor.ActorRef
import java.net.{InetSocketAddress => Addr}
import java.util.UUID

object Connection {

  trait Status

  case object Accepted extends Status

  case object Connected extends Status

  case object Refused extends Status

  case object Closed extends Status

  def apply(id: String, cltAddr: Addr, srvAddr: Addr, proxy: ActorRef) = {
    val time = System.currentTimeMillis
    new Connection(id, time, proxy, cltAddr, srvAddr, Accepted, None)
  }
}

case class Connection(id: String,
                      time: Long,
                      proxy: ActorRef,
                      cltAddr: Addr,
                      srvAddr: Addr,
                      status: Connection.Status,
                      chunk: Option[UUID])
