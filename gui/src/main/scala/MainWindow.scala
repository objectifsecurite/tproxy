package ch.os.tproxy.gui

import scala.concurrent.Promise
import akka.actor.ActorRef
import ch.os.tproxy.common.log.Logging
import com.trolltech.qt.gui.{QApplication, QVBoxLayout, QWidget}
import ch.os.tproxy.core.filter.{Filter, FilterActor}
import ch.os.tproxy.common.behaviour.actor.message.Command
import ch.os.tproxy.gui.component.GuiComponent

class MainWindow(guiActor: ActorRef, filterActor: ActorRef)
                (implicit val logging: Logging) extends GuiComponent {

  val filter = filterActor

  val app = new QApplication(Array())
  val window = new QWidget()
  window.setWindowTitle("tproxy")
  window.resize(1090, 620)

  val layout = new QVBoxLayout()

  val mainFolder = new MainFolder(this)
  val menuBar = new MenuBar(this)
  val statusBar = new component.StatusBar

  layout.setMenuBar(menuBar.widget)
  layout.addWidget(mainFolder.widget)
  layout.addWidget(statusBar.widget)
  layout.setContentsMargins(11, 0, 11, 0)
  window.setLayout(layout)
  window.show()

  def start(guiStarted: Promise[MainWindow]) = {
    guiStarted success this
    app.exec()
    guiActor ! GuiActor.WindowClosed
  }

  def addInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.AddFilter(filter, false), false)

  def removeInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.RemoveFilter(filter, false), false)

  def addScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.AddFilter(filter, true), false)

  def removeScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.RemoveFilter(filter, true), false)
}
