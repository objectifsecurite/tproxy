package ch.os.tproxy.gui

import ch.os.tproxy.core.filter.Filter
import ch.os.tproxy.common.message.Message
import com.trolltech.qt.QSignalEmitter

class MainFolder(main: MainWindow)
  extends QSignalEmitter
    with component.MainFolder
    with component.FilterFieldAction
    with component.DoubleClickFilterAction
    with component.SelectConnectionAction
    with component.FilterViewAction
    with component.ForwardAction
    with component.InterceptAction
    with component.EditAction
    with component.EntryPointsAction {

  lazy val logging = main.logging

  override def forwardMessage(msg: Message) = super[ForwardAction].forwardMessage(msg)

  override def addInterceptFilter(filter: Filter) = super[InterceptAction].addInterceptFilter(filter)

  override def addScriptInterceptFilter(filter: Filter) = super[InterceptAction].addScriptInterceptFilter(filter)

  override def removeInterceptFilter(filter: Filter) = super[InterceptAction].removeInterceptFilter(filter)

  override def removeScriptInterceptFilter(filter: Filter) = super[InterceptAction].removeScriptInterceptFilter(filter)

  override def setEditable(_editable: Boolean) = super[EditAction].setEditable(_editable)

  override def clear = super[EditAction].clear

  lazy val forwardAction = this
  lazy val filterActor = main.filter
  lazy val statusBar = main.statusBar
  lazy val messageView = messageWindow.messageView
  lazy val filterField = messageView.filterField
  lazy val forwardButton = messageView.forwardButton
  lazy val discardButton = messageView.discardButton
  lazy val messageTable = messageView.messageTable
  lazy val editorFolder = messageView.editorFolder
  lazy val messageTableModel = messageTable.model
  lazy val connectionTableModel = connectionTable.model
  lazy val filterView = messageWindow.filterView

  override lazy val messageWindow = new component.MessageWindow with Context {
    override lazy val filterView = new component.FilterView with Context {

      override def addFilterFromField(filterType: component.FilterType.FilterType) = MainFolder.this.addFilterFromField(filterType)

      override def removeFilters(filterType: component.FilterType.FilterType, filters: Vector[Filter]) = MainFolder.this.removeFilters(filterType, filters)

      override def changeFilterSelection(filterType: component.FilterType.FilterType, filter: Filter) = MainFolder.this.changeFilterSelection(filterType, filter)

      override def swapFilter(from: component.FilterType.FilterType, to: component.FilterType.FilterType) = MainFolder.this.swapFilter(from, to)
    }
  }
}
