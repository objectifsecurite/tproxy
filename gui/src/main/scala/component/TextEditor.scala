package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{QPlainTextEdit, QTextCursor}
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.gui.message.implicits._
import java.nio.charset.{
  Charset,
  MalformedInputException,
  UnmappableCharacterException
}
import akka.util.ByteString

trait TextEditor extends Editor {

  object LineTermination extends Enumeration {
    type LineTermination = Value
    val CRLF, LF, Unknown = Value
  }

  import LineTermination._

  val widget = new QPlainTextEdit
  clear

  widget.selectionChanged.connect(this, "selectionChanged()")

  private var lineTermination: LineTermination = Unknown
  private var charset: String = "UTF-8"

  def isValidUTF8String(data: ByteString) = {
    try {
      Charset.forName("UTF-8").newDecoder.decode(data.asByteBuffer)
      true
    } catch {
      case e: MalformedInputException => false
      case e: UnmappableCharacterException => false
    }
  }

  def getLineTermination(data: ByteString) = {
    // naive implementation
    val cr = data.count(_ == 13)
    val lf = data.count(_ == 10)
    cr match {
      case `lf` => CRLF
      case 0 => LF
      case _ => Unknown
    }
  }

  def clear() = {
    widget.setPlainText("\nSelect a message to see its content.")
    widget.setReadOnly(true)
  }

  def displayMsg(msg: Option[Message], refresh: Boolean) = {
    if (msg.isDefined) {
      if (msg.get.hasData) {
        val scrollBar = widget.verticalScrollBar
        val oldPosition = scrollBar.value
        val scrolled_at_max = scrollBar.value == scrollBar.maximum

        lineTermination = getLineTermination(msg.get.data)

        val data = if (msg.get.hasModifiedData) msg.get.modifiedData else msg.get.data

        if (isValidUTF8String(data))
          charset = "UTF-8"
        else
          charset = "ISO8859_1"
        widget.setPlainText(data.decodeString(charset))
        widget.setReadOnly(false)

        if (refresh) {
          val newPosition = if (scrolled_at_max) scrollBar.maximum else oldPosition
          widget.verticalScrollBar.setValue(newPosition)
        }
      }
      else {
        widget.setReadOnly(true)
        widget.setPlainText("\nNo data.")
      }
    }
    else
      clear
  }

  def getData(): ByteString = {
    val modifiedText = widget.toPlainText

    if (lineTermination == CRLF)
      ByteString(modifiedText.replaceAll("\n", "\r\n"), charset)
    else
      ByteString(modifiedText, charset)
  }

  def selectPosition(start: Int, stop: Int) = {
    val cursor = widget.textCursor

    val bytes = getData
    val beforeText = bytes.slice(0, start)
    val selectedText = bytes.slice(start, stop)
    val beforeOffset = offset(beforeText)
    val selectedOffset = offset(selectedText)

    cursor.setPosition(start - beforeOffset)
    cursor.setPosition(stop - beforeOffset - selectedOffset, QTextCursor.MoveMode.KeepAnchor)
    widget.setTextCursor(cursor)
  }

  def selectionChanged() = {
    val cursor = widget.textCursor
    val position = if (cursor.position <= cursor.anchor) cursor.position else cursor.anchor
    val anchor = if (cursor.position <= cursor.anchor) cursor.anchor else cursor.position

    val text = widget.toPlainText
    val beforeText = text.slice(0, position)
    val selectedText = text.slice(position, anchor)
    val beforeOffset = offset(beforeText)
    val selectedOffset = offset(selectedText)

    selectedPositionChanged emit(position + beforeOffset, anchor + beforeOffset + selectedOffset)
  }

  def bytesOffset(text: String): Int = text.getBytes(charset).length - text.length

  def crlfOffset(text: String): Int = if (lineTermination == CRLF) text.count(_ == '\n') else 0

  def offset(text: String) = if (charset == "UTF-8") bytesOffset(text) + crlfOffset(text) else crlfOffset(text)

  def bytesOffset(text: ByteString): Int = text.length - text.decodeString(charset).length

  def crlfOffset(text: ByteString): Int = if (lineTermination == CRLF) text.count(_ == '\n') else 0

  def offset(text: ByteString): Int = if (charset == "UTF-8") bytesOffset(text) + crlfOffset(text) else crlfOffset(text)

}
