package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{QItemSelection, QPushButton}
import ch.os.tproxy.common.message.implicits._

trait EditAction {
  def messageTable: MessageTable

  def editorFolder: EditorFolder

  def forwardButton: QPushButton

  def discardButton: QPushButton

  private var editable = false

  val selection = messageTable.view.selectionModel()
  selection.selectionChanged.connect(this, "changeMessageSelection(QItemSelection, QItemSelection)")

  def changeMessageSelection(selected: QItemSelection, deselected: QItemSelection) = {
    if (!selected.isEmpty) {
      val row = selection.selectedRows.get(0).row
      val msg = messageTable.proxyModel.itemAt(row)
      editorFolder.setMsg(Some(msg))

      val intercepted = msg.hasIntercepted
      editorFolder.editable = editable
      enableModifications(intercepted)
    }
    else {
      editorFolder.clear
      enableModifications(false)
    }
  }

  def enableModifications(intercepted: Boolean) = {
    forwardButton.setEnabled(editable && intercepted)
    discardButton.setEnabled(editable && intercepted)
  }

  def setEditable(_editable: Boolean) = {
    editable = _editable
    editorFolder.editable = _editable
    enableModifications(false)
  }

  def clear() = {
    editorFolder.clear
    enableModifications(false)
  }
}
