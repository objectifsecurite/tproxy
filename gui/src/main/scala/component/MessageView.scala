package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{
  QFrame,
  QPushButton,
  QVBoxLayout,
  QHBoxLayout,
  QIcon,
  QSplitter
}
import com.trolltech.qt.core.Qt.Orientation.Vertical

trait MessageView extends GuiComponent {
  lazy val filterField = new FilterField
  lazy val forwardButton = new QPushButton(new QIcon("classpath:images/circle-check-2x.png"), "Forward")
  lazy val discardButton = new QPushButton(new QIcon("classpath:images/circle-x-2x.png"), "Discard")
  forwardButton.setEnabled(false)
  discardButton.setEnabled(false)
  forwardButton.setToolTip("Forward selected messages to its destination")
  discardButton.setToolTip("Discard selected messages")
  val actionBar = new QFrame
  val actionLayout = new QHBoxLayout
  actionLayout.addWidget(filterField)
  actionLayout.addWidget(forwardButton)
  actionLayout.addWidget(discardButton)
  actionLayout.setContentsMargins(0, 0, 0, 11)
  actionBar.setLayout(actionLayout)

  val splitter = new QSplitter(Vertical)
  lazy val messageTable = new MessageTable with Context
  lazy val editorFolder = new EditorFolder with Context
  splitter.addWidget(messageTable.view)
  splitter.addWidget(editorFolder.widget)

  val widget = new QFrame()
  val layout = new QVBoxLayout()
  layout.addWidget(actionBar)
  layout.addWidget(splitter)
  widget.setLayout(layout)
}
