package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{QStatusBar, QLabel}

class StatusBar() {

  val widget = new QStatusBar()

  val listening = new QLabel("Not listening")
  val connections = new QLabel("Connections: 0")
  val messages = new QLabel("Messages: 0")
  val specific = new QLabel("")

  widget.addWidget(listening, 1)
  widget.addWidget(connections, 1)
  widget.addWidget(messages, 1)
  widget.addWidget(specific, 1)

  def setListening(port: Int) = listening.setText(s"Listening on $port")

  def setConnectionsCount(count: Int) = connections.setText(s"Connections: $count")

  def setMessagesCount(count: Int) = messages.setText(s"Messages: $count")
}
