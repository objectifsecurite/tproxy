package ch.os.tproxy.gui.component

import scala.collection.JavaConverters._
import ch.os.tproxy.gui.model.ConnectionTableModel
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.Command
import com.trolltech.qt.gui.QPushButton

trait ForwardAction {
  def connectionTableModel: ConnectionTableModel

  def messageTable: MessageTable

  def forwardButton: QPushButton

  def discardButton: QPushButton

  def editorFolder: EditorFolder

  forwardButton.clicked.connect(this, "updateModifiedMessage()")
  forwardButton.clicked.connect(this, "forwardMessageClicked()")
  discardButton.clicked.connect(this, "discardMessageClicked()")

  def updateModifiedMessage() = {
    val (editedMsg, remainingDataMsg) = editorFolder.prepareMsgToForward
    if (editedMsg.get.hasData)
      messageTable.model.updateItem(editedMsg.get.id, _.setData(editedMsg.get.data))

    val connection = connectionTableModel get editedMsg.get.cid
    val cid = connection.id

    if (!remainingDataMsg.isDefined)
      connectionTableModel updateItem(cid, _.copy(chunk = None))
    else {
      messageTable.model insertItem remainingDataMsg.get
      connectionTableModel updateItem(cid, _.copy(chunk = Some(remainingDataMsg.get.id)))
    }
  }

  def forwardMessageClicked() = {
    val selected = messageTable.view.selectionModel().selectedRows()
    if (!selected.isEmpty) {
      val messages = selected.asScala.map(x => messageTable.proxyModel.itemAt(x.row))
      messages.foreach(forwardMessage)
      messageTable.view.reset()
      editorFolder.clear
    }
  }

  def forwardMessage(msg: Message) = {
    val connection = connectionTableModel.get(msg.cid)
    val sender = connection.proxy
    val newMsg = messageTable.model.updateItem(msg.id, _.remIntercepted)
    sender ! Command(newMsg)
  }

  def discardMessageClicked() = {
    val (editedMsg, remainingDataMsg) = editorFolder.prepareMsgToForward

    val selected = messageTable.view.selectionModel().selectedRows()
    if (!selected.isEmpty) {
      val messages = selected.asScala.map(x => messageTable.proxyModel.itemAt(x.row))
      messages.foreach(discardMessage)
      messageTable.view.reset()
      editorFolder.clear
    }
    if (remainingDataMsg.isDefined) {
      val msg = remainingDataMsg.get
      messageTable.model insertItem msg
      val connection = connectionTableModel get msg.cid
      if (connection.chunk.isDefined)
        connectionTableModel updateItem(msg.cid, _.copy(chunk = Some(msg.id)))
    }
  }

  def discardMessage(msg: Message) = messageTable.model.removeItem(msg)
}
