package ch.os.tproxy.gui.component

import ch.os.tproxy.common.log.Logging

trait GuiComponent {
  implicit def logging: Logging

  trait Context {
    lazy val logging = GuiComponent.this.logging
  }

}
