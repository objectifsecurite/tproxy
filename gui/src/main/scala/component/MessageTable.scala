package ch.os.tproxy.gui.component

import ch.os.tproxy.common.message.implicits._
import com.trolltech.qt.gui.{
  QTableView,
  QSortFilterProxyModel,
  QAbstractItemView
}
import com.trolltech.qt.core.{QModelIndex, Qt}
import ch.os.tproxy.gui.model.MessageTableModel
import ch.os.tproxy.core.filter.{Filter, FilterParser}

class MessageTableProxyModel(model: MessageTableModel) extends QSortFilterProxyModel {

  protected var filter = FilterParser("").get
  protected var intercepted = false

  override def filterAcceptsRow(row: Int, index: QModelIndex): Boolean = {
    if (row >= model.rowCount(index))
      false
    else {
      val msg = model.itemAt(row)
      filter(msg) && (!intercepted || (intercepted && msg.hasIntercepted))
    }
  }

  def setFilter(_filter: Filter, _intercepted: Boolean) = {
    filter = _filter
    intercepted = _intercepted
    invalidateFilter
  }

  def itemAt(row: Int) = model.itemAt(proxyRow(row))

  def proxyRow(row: Int) = mapToSource(index(row, 0)).row
}

trait MessageTable extends GuiComponent {
  val model = new MessageTableModel
  val proxyModel = new MessageTableProxyModel(model)
  proxyModel.setSourceModel(model)
  proxyModel.setDynamicSortFilter(true)

  val view = new QTableView
  view.setModel(proxyModel)
  view.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
  view.setShowGrid(false)
  view.setAlternatingRowColors(true)
  view.verticalHeader setVisible false
  view.setColumnWidth(0, 75)
  view.setColumnWidth(1, 180)
  view.setColumnWidth(2, 60)
  view.setColumnWidth(3, 30)
  view.setColumnWidth(4, 180)
  view.setColumnWidth(5, 60)
  view.setColumnWidth(6, 50)
  view.setColumnWidth(7, 198)
  view.horizontalHeader.setStretchLastSection(true)
  view.setWordWrap(false)
  view.horizontalHeader setHighlightSections false

  def resetFilter() = setFilter(FilterParser("").get, false)

  def setFilter(filter: Filter, intercepted: Boolean = false) = {
    val selection = view.selectionModel.selection
    val modelSelection = proxyModel.mapSelectionToSource(selection)
    proxyModel.setFilter(filter, intercepted)
    val proxySelection = proxyModel.mapSelectionFromSource(modelSelection)
    view.selectionModel.select(proxySelection)
    if (!proxySelection.isEmpty) {
      val index = view.selectionModel.selectedRows.get(0)
      // dirty hack
      // https://forum.qt.io/topic/27337/unlogical-running-of-scrollto-in-qtableview/2
      view.scrollToBottom()
      view.scrollToTop()
      view.scrollTo(index)
    }
  }

  def autoScroll(checked: Boolean) = {
    if (checked)
      model.rowsInserted.connect(this, "scrollToBottom()", Qt.ConnectionType.QueuedConnection)
    else
      model.rowsInserted.disconnect(this)
  }

  def scrollToBottom() = view.scrollToBottom()
}
