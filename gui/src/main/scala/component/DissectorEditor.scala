package ch.os.tproxy.gui.component

import akka.util.ByteString

import scala.collection.JavaConverters._
import com.trolltech.qt.gui.{QTreeWidget, QTreeWidgetItem, QTreeWidgetItemIterator}
import com.trolltech.qt.core.Qt
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.dissection.DissectTreeNode
import ch.os.tproxy.common.json.JsonDissectTreeNode.{BYTES, TEXT}


trait DissectorEditor extends Viewer {
  private val MAX_TEXT_LEN = 200
  val widget = new QTreeWidget
  widget.header.hide

  var offset = 0
  widget.itemClicked.connect(this, "itemClicked(QTreeWidgetItem, int)")

  def clear() = widget.clear

  private def truncateBytes(text: String) = {
    val truncated = text.take(MAX_TEXT_LEN)
    if (text.length > truncated.length) truncated + "..." else truncated
  }

  def getValueFromPcapData(node: DissectTreeNode, pcapData: ByteString): String = {
    val start = node.getPosition - offset
    val data = pcapData.slice(start, start + node.getLength)
    node.getValueType match {
      case TEXT => data.map(_.toChar).mkString.stripLineEnd
      case BYTES => data.map("%02X" format _).mkString(":")
      case _ => null
    }
  }

  def getText(node: DissectTreeNode, pcapData: ByteString) = {
    val name = node.getName
    var value = node.getValue

    if (value == null) {
      value = getValueFromPcapData(node, pcapData)
      node.setValue(value)
    }
    val text = if (node.getValueType == BYTES) truncateBytes(value) else value
    if (name != "Text item") name + ": " + text else text
  }

  def displayMsg(msg: Option[Message], refresh: Boolean = false) = {
    clear
    if (msg.isDefined && msg.get.hasDissection) {
      offset = msg.get.dissection.getOffset
      var rootNode = msg.get.dissection.getRoot.findTag("tcp").getNext
      while (rootNode != null) {
        if (rootNode.getAbbrev() == "tcp.segments")
          rootNode = rootNode.getNext
        populate(rootNode, Left(widget), msg.get.data)
        rootNode = rootNode.getNext
      }
      widget.expandAll
    }
  }


  def populate(node: DissectTreeNode, parent: Either[QTreeWidget, QTreeWidgetItem], pcapData: ByteString): Unit = {
    val item = parent match {
      case Left(x) => new QTreeWidgetItem(x)
      case Right(x) => new QTreeWidgetItem(x)
    }
    val text = getText(node, pcapData)
    item.setText(0, text)
    item.setData(0, Qt.ItemDataRole.UserRole, node)
    for ((node, row) <- node.getChildren.asScala.zipWithIndex) {
      populate(node, Right(item), pcapData)
    }
  }

  def selectPosition(start: Int, stop: Int) = {
    val iterator = new QTreeWidgetItemIterator(widget)
    var selected = iterator.current
    while (iterator.current != null) {
      val item = iterator.current
      val node = item.data(0, Qt.ItemDataRole.UserRole).asInstanceOf[DissectTreeNode]
      val nodeStart = node.getPosition - offset
      val nodeStop = nodeStart + node.getLength
      val selectedNode = selected.data(0, Qt.ItemDataRole.UserRole).asInstanceOf[DissectTreeNode]
      if (start >= nodeStart && stop <= nodeStop && selectedNode.getLength > node.getLength)
        selected = item
      iterator.next()
    }
    widget.clearSelection
    if (selected != null) {
      selected.setSelected(true)
      widget.scrollToItem(selected)
    }
  }

  def itemClicked(item: QTreeWidgetItem, col: Int) = {
    val node = item.data(0, Qt.ItemDataRole.UserRole).asInstanceOf[DissectTreeNode]
    val length = node.getLength
    val start = node.getPosition - offset
    val stop = start + length
    if (length > 0)
      selectedPositionChanged emit(start, stop)
  }
}
