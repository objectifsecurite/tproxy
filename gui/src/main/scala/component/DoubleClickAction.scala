package ch.os.tproxy.gui.component

import com.trolltech.qt.core.QModelIndex
import com.trolltech.qt.gui.QTabWidget
import ch.os.tproxy.core.filter.FilterParser

trait DoubleClickFilterAction {
  def connectionTable: ConnectionTable

  def filterField: FilterField

  def messageTable: MessageTable

  def widget: QTabWidget

  connectionTable.view.doubleClicked.connect(this, "setCidFilter(QModelIndex)")

  def setCidFilter(index: QModelIndex) = {
    val id = connectionTable.model.itemAt(index.row).id
    val filter = FilterParser("cid == \"" + id + "\"")
    filterField.setText(filter.get.toString)
    messageTable.setFilter(filter.get)
    widget.setCurrentIndex(1)
  }
}
