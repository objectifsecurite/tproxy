package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{QMenuBar, QSizePolicy}

class MenuBar {

  val widget = new QMenuBar()
  widget.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)
  val fileMenu = widget.addMenu("File")

  val aboutAct = fileMenu.addAction("About")
  lazy val autoScrollAct = fileMenu.addAction("Auto Scroll")
  autoScrollAct.setCheckable(true)

  lazy val quitAct = fileMenu.addAction("Quit")
}
