package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{QTableView, QAbstractItemView}
import ch.os.tproxy.gui.model.ConnectionTableModel

trait ConnectionTable extends GuiComponent {
  val model = new ConnectionTableModel

  val view = new QTableView
  view.setModel(model)
  view.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
  view.setShowGrid(false)
  view.setAlternatingRowColors(true)
  view.verticalHeader setVisible false
  view.horizontalHeader setStretchLastSection true
  view.setColumnWidth(0, 75)
  view.setColumnWidth(1, 200)
  view.setColumnWidth(2, 60)
  view.setColumnWidth(3, 200)
  view.setColumnWidth(4, 60)
  view.setWordWrap(false)
  view.horizontalHeader setHighlightSections false
}
