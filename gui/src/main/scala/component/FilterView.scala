package ch.os.tproxy.gui.component

import scala.collection.JavaConverters._
import ch.os.tproxy.core.filter.Filter
import com.trolltech.qt.gui.{
  QLabel,
  QPushButton,
  QIcon, QWidget,
  QHBoxLayout,
  QVBoxLayout,
  QSplitter,
  QListView,
  QItemSelectionModel,
  QItemSelection
}
import com.trolltech.qt.core.{
  Qt,
  QAbstractListModel,
  QModelIndex
}

class FilterListModel extends QAbstractListModel {
  var filters = Vector[Filter]()

  def rowCount(index: QModelIndex): Int = filters.length

  def data(index: QModelIndex, role: Int): Object = {
    if (index.row() > filters.length - 1) null
    else role match {
      case Qt.ItemDataRole.DisplayRole => displayFilter(index.row())
      case _ => null
    }
  }

  def displayFilter(row: Int): String = {
    val filter = filters(row).string.get
    if (filter == "") "All" else filter
  }
}

object FilterType extends Enumeration {
  type FilterType = Value
  val Capture, Intercept, Script = Value
}

trait FilterView extends GuiComponent {

  import FilterType._

  class SubFilterView(parent: FilterView, filterType: FilterType) {

    val intercepted = filterType != Capture

    val (titleText, addButtonToolTip, swapButtonIcon, swapButtonToolTip) = if (filterType == Capture) {
      ("<b>Captured</b>",
        "Add filter entry for capture",
        "classpath:images/disconnected-2x.png",
        "User selected filter for interception")
    } else if (filterType == Intercept) {
      ("<b>Intercepted</b>",
        "Add filter entry for interception",
        "classpath:images/connected-2x.png",
        "User selected filter for capture only")
    } else {
      ("<b>Script intercepted</b>",
        "Add filter entry for interception by script",
        "classpath:images/connected-2x.png",
        "User selected filter for capture only")
    }

    val title = new QLabel()
    title.setText(titleText)

    val addButton = new QPushButton(new QIcon("classpath:images/plus-2x.png"), "")
    addButton.setToolTip(addButtonToolTip)
    addButton.clicked.connect(this, "addFilter()")

    val delButton = new QPushButton(new QIcon("classpath:images/trash-2x.png"), "")
    delButton.setToolTip("Remove selected filter")
    delButton.clicked.connect(this, "delFilter()")

    val header = new QWidget()
    val headerLayout = new QHBoxLayout()
    headerLayout.addWidget(title)
    headerLayout.addStretch
    headerLayout.addWidget(addButton)

    if (filterType == Capture) {
      lazy val interceptButton = new QPushButton(new QIcon(swapButtonIcon), "")
      interceptButton.setToolTip(swapButtonToolTip)
      interceptButton.clicked.connect(this, "swapFilterToIntercept()")
      headerLayout.addWidget(interceptButton)
      lazy val scriptInterceptButton = new QPushButton(new QIcon("classpath:images/cogs-2x.png"), "")
      scriptInterceptButton.setToolTip("Use selected filter for interception by script (localhost:9081)")
      scriptInterceptButton.clicked.connect(this, "swapFilterToScript()")
      headerLayout.addWidget(scriptInterceptButton)
    } else {
      lazy val swapButton = new QPushButton(new QIcon(swapButtonIcon), "")
      swapButton.setToolTip(swapButtonToolTip)
      swapButton.clicked.connect(this, "swapFilterToCapture()")
      headerLayout.addWidget(swapButton)
    }

    headerLayout.addWidget(delButton)
    headerLayout.setContentsMargins(0, 0, 0, 0)
    header.setLayout(headerLayout)

    val model = new FilterListModel()
    val view = new QListView()
    view.setModel(model)

    val selectionModel = new QItemSelectionModel(model)
    view.setSelectionModel(selectionModel)
    selectionModel.selectionChanged.connect(this, "changeSelection(QItemSelection, QItemSelection)")

    val widget = new QWidget()
    val layout = new QVBoxLayout()
    layout.addWidget(header)
    layout.addWidget(view)
    widget.setLayout(layout)

    def selectedRows() = {
      selectionModel.selectedRows().asScala.toList.map(x => x.row)
    }

    def addSubFilter(filter: Filter) = {
      model.filters = model.filters :+ filter
    }

    def addFilter() = {
      parent.addFilterFromField(filterType)
    }

    def swapFilterToCapture() = {
      parent.swapFilter(filterType, Capture)
    }

    def swapFilterToIntercept() = {
      parent.swapFilter(filterType, Intercept)
    }

    def swapFilterToScript() = {
      parent.swapFilter(filterType, Script)
    }

    def delFilter() = {
      val rows = selectedRows
      val deletedFilters = model.filters.zipWithIndex.filter(x => rows.contains(x._2)).map(_._1)
      parent.removeFilters(filterType, deletedFilters)
      model.filters = model.filters.zipWithIndex.filterNot(x => rows.contains(x._2)).map(_._1)
      view.reset()
    }

    def changeSelection(selected: QItemSelection, deselected: QItemSelection) = {
      if (!selected.isEmpty) {
        val item = selected.indexes.get(0).row
        val filter = model.filters(item)
        parent.changeFilterSelection(filterType, filter)
      }
    }
  }

  lazy val captureFilterView = new SubFilterView(this, Capture)
  lazy val interceptFilterView = new SubFilterView(this, Intercept)
  lazy val scriptInterceptFilterView = new SubFilterView(this, Script)

  val widget = new QSplitter(Qt.Orientation.Vertical)
  widget.addWidget(captureFilterView.widget)
  widget.addWidget(interceptFilterView.widget)
  widget.addWidget(scriptInterceptFilterView.widget)

  def addFilterFromField(filterType: FilterType) = {}

  def removeFilters(filterType: FilterType, filters: Vector[Filter]) = {}

  def changeFilterSelection(filterType: FilterType, filter: Filter) = {}

  def swapInterceptFilter() = {}

  def swapCaptureFilter() = {}

  def swapFilter(from: FilterType, to: FilterType) = {}
}
