package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.QSplitter

trait MessageWindow extends GuiComponent {
  val widget = new QSplitter
  lazy val messageView = new MessageView with Context
  lazy val filterView = new FilterView with Context
  widget.addWidget(filterView.widget)
  widget.addWidget(messageView.widget)
  widget.setStretchFactor(0, 2)
  widget.setStretchFactor(1, 10)
}
