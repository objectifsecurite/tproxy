package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{
  QFrame,
  QWidget,
  QVBoxLayout,
  QTabWidget,
  QHBoxLayout,
  QPushButton,
  QLabel,
  QSizePolicy
}
import com.trolltech.qt.gui.QTabWidget.TabPosition.South
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.gui.message.implicits._
import com.trolltech.qt.QSignalEmitter

class WarningBar {
  val widget = new QFrame
  lazy val layout = new QHBoxLayout
  lazy val message = new QLabel("Message modified")
  lazy val button = new QPushButton("Append new data")
  widget.setLayout(layout)
  layout.addWidget(message)
  layout.addWidget(button)
  button.setFixedWidth(150)
  layout.setContentsMargins(10, 2, 10, 2)
  widget.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)
  widget.setFrameStyle(QFrame.Shape.StyledPanel.value() | QFrame.Shadow.Raised.value());
  widget.setVisible(false)
}

trait EditorFolder extends GuiComponent {
  val widget = new QWidget
  lazy val layout = new QVBoxLayout
  layout.setContentsMargins(0, 0, 0, 0)
  layout.setSpacing(0)
  widget.setLayout(layout)

  lazy val tabWidget = new QTabWidget
  tabWidget setTabPosition South

  lazy val textEditor = new TextEditor with Context
  tabWidget addTab(textEditor.widget, "Text")
  tabWidget setTabToolTip(0, "Text editor enabled only if message contents seems valid text")

  lazy val hexEditor = new HexEditor with Context
  tabWidget addTab(hexEditor.widget, "Hex")
  tabWidget setTabToolTip(1, "Hex editor shortcuts: \nInsert: switch overwrite/insert mode \nCtrl+a: select all / Ctrl+x: cut / Ctrl+c: copy / Ctrl+v: paste \nCtrl+z: undo / Ctrl+Shift+z: redo")

  lazy val dissectorEditor = new DissectorEditor with Context
  tabWidget addTab(dissectorEditor.widget, "Dissection")
  tabWidget setTabToolTip(2, "Dissection is not refreshed when message contents is edited")

  lazy val warningBar = new WarningBar
  layout.addWidget(warningBar.widget)
  layout.addWidget(tabWidget)
  warningBar.button.clicked.connect(this, "refreshEditorMsg()")

  private var displayedMsg: Option[Message] = None
  private var refreshedMsg: Option[Message] = None
  var editable = false

  dissectorEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")
  textEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")
  hexEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")

  def textChanged() = {
    val sender = QSignalEmitter.signalSender

    for (msg <- displayedMsg if msg.hasData) {
      if (sender == textEditor.widget && textEditor.widget.document.isModified) {
        val modifiedData = textEditor.getData
        displayedMsg = Some(msg setModifiedData modifiedData)

        hexEditor.widget.dataChanged.disconnect(this, "textChanged()")
        hexEditor.displayMsg(displayedMsg, true)
        hexEditor.widget.dataChanged.connect(this, "textChanged()")
      }
      else if (sender == hexEditor.widget) {
        val modifiedData = hexEditor.getData
        displayedMsg = Some(msg setModifiedData modifiedData)

        textEditor.widget.textChanged.disconnect(this, "textChanged()")
        textEditor.displayMsg(displayedMsg, true)
        textEditor.widget.textChanged.connect(this, "textChanged()")
      }

    }
  }

  def clear() = setMsg(None)

  def setMsg(msg: Option[Message], refresh: Boolean = false) = {
    displayedMsg = msg
    refreshedMsg = None

    textEditor.widget.textChanged.disconnect(this, "textChanged()")
    textEditor.displayMsg(displayedMsg, refresh)

    hexEditor.widget.dataChanged.disconnect(this, "textChanged()")
    hexEditor.displayMsg(displayedMsg, refresh)

    dissectorEditor.displayMsg(displayedMsg)
    displayWarning(false)

    textEditor.widget.textChanged.connect(this, "textChanged()")
    hexEditor.widget.dataChanged.connect(this, "textChanged()")

    if (!editable || !msg.isDefined || (msg.isDefined && !msg.get.hasData)) {
      setReadOnly(true)
      textEditor.widget.textChanged.disconnect(this, "textChanged()")
      hexEditor.widget.dataChanged.disconnect(this, "textChanged()")
    }
  }

  def getMsg() = displayedMsg

  def setReadOnly(readOnly: Boolean) = {
    textEditor.widget.setReadOnly(readOnly)
    hexEditor.widget.setReadOnly(readOnly)
  }

  def notifyNewChunk(msg: Message) = {
    if (!refreshedMsg.isDefined)
      refreshedMsg = Some(msg)
    else {
      val reassembledData = refreshedMsg.get.data ++ msg.data
      refreshedMsg = Some(refreshedMsg.get setData reassembledData)
    }
    displayWarning(true)
  }

  def displayWarning(bool: Boolean) = {
    val textEditorScrollBar = textEditor.widget.verticalScrollBar
    val textEditorScrolledAtMax = textEditorScrollBar.value == textEditorScrollBar.maximum
    val dissectorEditorScrollBar = dissectorEditor.widget.verticalScrollBar
    val dissectorEditorScrolledAtMax = dissectorEditorScrollBar.value == dissectorEditorScrollBar.maximum
    val hexEditorScrollBar = hexEditor.widget.verticalScrollBar
    val hexEditorScrolledAtMax = hexEditorScrollBar.value == hexEditorScrollBar.maximum

    warningBar.widget.setVisible(bool)

    if (textEditorScrolledAtMax) textEditorScrollBar.setValue(textEditorScrollBar.maximum)
    if (dissectorEditorScrolledAtMax) dissectorEditorScrollBar.setValue(dissectorEditorScrollBar.maximum)
    if (hexEditorScrolledAtMax) hexEditorScrollBar.setValue(hexEditorScrollBar.maximum)
  }

  def selectedPositionChanged(start: Int, stop: Int) = {
    val sender = QSignalEmitter.signalSender

    dissectorEditor.selectedPositionChanged.disconnect(this, "selectedPositionChanged(int, int)")
    textEditor.selectedPositionChanged.disconnect(this, "selectedPositionChanged(int, int)")
    hexEditor.selectedPositionChanged.disconnect(this, "selectedPositionChanged(int, int)")

    if (sender != dissectorEditor)
      dissectorEditor.selectPosition(start, stop)
    if (sender != textEditor)
      textEditor.selectPosition(start, stop)
    if (sender != hexEditor)
      hexEditor.selectPosition(start, stop)

    dissectorEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")
    textEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")
    hexEditor.selectedPositionChanged.connect(this, "selectedPositionChanged(int, int)")
  }

  def refreshEditorMsg() = {
    require(refreshedMsg.isDefined, s"refreshEditorMsg clicked without new data available")
    val data = if (displayedMsg.get.hasModifiedData) displayedMsg.get.modifiedData else displayedMsg.get.data
    val reassembledData = data ++ refreshedMsg.get.data
    var updatedMsg = displayedMsg.get setModifiedData reassembledData
    if (refreshedMsg.get.hasInfo)
      updatedMsg = updatedMsg setInfo (refreshedMsg.get.info)
    if (refreshedMsg.get.hasDissection)
      updatedMsg = updatedMsg setDissection (refreshedMsg.get.dissection)
    setMsg(Some(updatedMsg), true)
    refreshedMsg = None
  }

  def prepareMsgToForward() = {
    if (displayedMsg.isDefined && displayedMsg.get.hasModified)
      displayedMsg = Some(displayedMsg.get setData displayedMsg.get.modifiedData)
    (displayedMsg, refreshedMsg)
  }
}
