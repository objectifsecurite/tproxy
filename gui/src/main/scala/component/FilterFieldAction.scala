package ch.os.tproxy.gui.component

import ch.os.tproxy.core.filter.FilterParser
import ch.os.tproxy.common.message.implicits._
import com.trolltech.qt.gui.{QAction, QMenu}
import com.trolltech.qt.core.{Qt, QPoint}
import com.trolltech.qt.QSignalEmitter

trait FilterFieldAction {
  def messageTable: MessageTable

  def filterField: FilterField

  filterField.returnPressed.connect(this, "filterEntered()")
  filterField.clearButton.clicked.connect(this, "clearFilter()")

  def filterEntered() = {
    val text = filterField.text
    if (!text.isEmpty()) {
      val filter = FilterParser(text)
      if (filter != None)
        messageTable.setFilter(filter.get)
    }
    else {
      messageTable.resetFilter()
    }
  }

  def clearFilter() = {
    filterField.clear()
    messageTable.resetFilter()
  }

  messageTable.view.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
  messageTable.view.customContextMenuRequested.connect(this, "displayContextMenu(QPoint)")

  def displayContextMenu(point: QPoint) {
    val menu = new QMenu
    val index = messageTable.view.indexAt(point)
    val col = index.column

    val cidAct = menu.addAction("Apply connection ID as filter")
    cidAct.setData((index.row, index.column))
    cidAct.triggered.connect(this, "cidFilterClicked(boolean)")

    val netAct = menu.addAction("")
    netAct.setData((index.row, index.column))
    netAct.triggered.connect(this, "netFilterClicked(boolean)")
    col match {
      case 1 => netAct.setText("Apply client address as filter")
      case 2 => netAct.setText("Apply client port as filter")
      case 4 => netAct.setText("Apply server address as filter")
      case 5 => netAct.setText("Apply server port as filter")
      case _ => netAct.setVisible(false)
    }
    menu.popup(messageTable.view.mapToGlobal(point))
  }

  def cidFilterClicked(checked: Boolean) = {
    val action = QSignalEmitter.signalSender.asInstanceOf[QAction]
    val index = action.data.asInstanceOf[Tuple2[Int, Int]]
    val row = index._1
    val msg = messageTable.proxyModel.itemAt(row)
    filterField.setText(s"cid == ${msg.cid}")
    filterEntered
  }

  def netFilterClicked(checked: Boolean) = {
    val action = QSignalEmitter.signalSender.asInstanceOf[QAction]
    val index = action.data.asInstanceOf[Tuple2[Int, Int]]
    val row = index._1
    val msg = messageTable.proxyModel.itemAt(row)
    val col = index._2
    val filter =
      col match {
        case 1 => if (msg.hasCltAddr) s"clt.ip == ${msg.cltAddr.getAddress.getHostAddress}" else ""
        case 2 => if (msg.hasCltAddr) s"clt.port == ${msg.cltAddr.getPort.toString}" else "?"
        case 4 => if (msg.hasSrvAddr) s"srv.ip == ${msg.srvAddr.getAddress.getHostAddress}" else "?"
        case 5 => if (msg.hasSrvAddr) s"srv.port == ${msg.srvAddr.getPort.toString}" else "?"
        case _ => ""
      }

    filterField.setText(filter)
    filterEntered
  }
}
