package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{
  QLineEdit,
  QToolButton,
  QIcon,
  QCursor,
  QStyle,
  QResizeEvent
}
import com.trolltech.qt.core.Qt
import ch.os.tproxy.core.filter.FilterParser

class FilterField() extends QLineEdit {

  // http://stackoverflow.com/questions/11381865/how-to-make-an-extra-icon-in-qlineedit-like-this
  setToolTip("Enter a filter for capture or interception. Following syntax is allowed:<br>Port: [clt|srv].port [==|!=] port<br>IP Address: [clt|srv].ip [==|!=] ip_address<br>Connection: cid [==|!=] connection_id<br>Double-click on connection to prepare a connection filter<br>Specific packet: [close|data|request|response|ssl] <br>Boolean operators: !, &, |")

  val lineHeight = sizeHint.height
  val btnSize = lineHeight

  val clearButton = new QToolButton(this)
  clearButton.setIcon(new QIcon("classpath:images/edit-clear.png"))
  clearButton.setCursor(new QCursor(Qt.CursorShape.ArrowCursor))
  clearButton.setStyleSheet("QToolButton { border: none; padding: 0px;}")
  clearButton.setFixedSize(btnSize, btnSize)
  clearButton.hide

  setStyleSheet("QLineEdit { padding-right: " + rightPadding.toString + "px }")
  setMinimumHeight(lineHeight)

  this.textChanged.connect(this, "updateCloseButton(java.lang.String)")
  this.textChanged.connect(this, "setBackgroundColor(java.lang.String)")

  override def resizeEvent(event: QResizeEvent) = {
    val frameWidth = style.pixelMetric(QStyle.PixelMetric.PM_DefaultFrameWidth)
    clearButton.move(width - clearButton.width - frameWidth, 0)
  }

  def rightPadding(): Int = {
    val frameWidth = style.pixelMetric(QStyle.PixelMetric.PM_DefaultFrameWidth)
    btnSize - frameWidth
  }

  def updateCloseButton(text: java.lang.String) = {
    clearButton.setVisible(!text.isEmpty)
  }

  def setBackgroundColor(text: java.lang.String) = {
    var color = "white"
    if (!text.isEmpty()) {
      val filter = FilterParser(text)
      if (filter == None)
        color = "rgb(255, 124, 124)"
      else
        color = "rgb(126, 236, 149)"
    }

    setStyleSheet("QLineEdit { padding-right: " + rightPadding.toString + "px; background: " + color + "; }")
  }
}
