package ch.os.tproxy.gui.component.qhexedit

import scala.util.control.Breaks._
import scala.collection.mutable.ArrayBuffer
import com.trolltech.qt.core.{QByteArray, QBuffer, QIODevice}


class Chunk() {
  var data = new QByteArray("")
  var dataChanged = new QByteArray("")
  var absPos: Long = 0
}

class Chunks() {

  val ChunkSize = 0x1000
  val BufferSize = 0x10000
  val ReadChunkMask = 0xfffffffffffff000L

  private var _ioDevice: QIODevice = new QBuffer
  private var _size: Long = 0
  private var _pos: Long = 0
  private val _chunks = ArrayBuffer.empty[Chunk]

  def setIODevice(ioDevice: QIODevice): Boolean = {
    _ioDevice = ioDevice
    val ok = _ioDevice.open(QIODevice.OpenModeFlag.ReadOnly)
    if (ok) {
      _size = ioDevice.size
      _ioDevice.close
    } else {
      _ioDevice = new QBuffer
      _size = 0
    }
    _chunks.clear
    _pos = 0
    ok
  }

  def size() = _size

  def pos() = _pos

  def data(__pos: Long = 0, _maxSize: Long = -1, highlighted: QByteArray = new QByteArray("")): QByteArray = {
    var ioDelta = 0
    var chunkIdx = 0
    var chunk = new Chunk
    val buffer = new QByteArray("")
    var maxSize = _maxSize
    var pos = __pos

    if (!highlighted.isEmpty)
      highlighted.clear

    if (pos >= _size)
      buffer

    if (maxSize < 0)
      maxSize = _size
    else if (pos + maxSize > _size) {
      maxSize = _size - pos
    }

    _ioDevice.open(QIODevice.OpenModeFlag.ReadOnly)

    while (maxSize > 0) {
      chunk.absPos = Long.MaxValue
      var chunksLoopOngoing = true

      while ((chunkIdx < _chunks.size) && chunksLoopOngoing) {
        chunk = _chunks(chunkIdx)

        if (chunk.absPos > pos)
          chunksLoopOngoing = false
        else {
          chunkIdx += 1
          var count: Long = 0
          val chunksOfs = pos - chunk.absPos
          if (maxSize > chunk.data.size - chunksOfs) {
            count = chunk.data.size - chunksOfs
            ioDelta += ChunkSize - chunk.data.size
          } else
            count = maxSize
          if (count > 0) {
            buffer.append(chunk.data.mid(chunksOfs.asInstanceOf[Int], count.asInstanceOf[Int]))
            maxSize -= count
            pos += count
            if (!highlighted.isEmpty) {
              highlighted.append(chunk.dataChanged.mid(chunksOfs.asInstanceOf[Int], count.asInstanceOf[Int]))
            }
          }
        }
      }

      if (maxSize > 0 && pos < chunk.absPos) {

        var byteCount: Long = 0
        var readBuffer = new QByteArray("")

        if (chunk.absPos - pos > maxSize)
          byteCount = maxSize
        else
          byteCount = chunk.absPos - pos

        maxSize -= byteCount
        _ioDevice.seek(pos + ioDelta)
        readBuffer = _ioDevice.read(byteCount)
        buffer.append(readBuffer)
        if (!highlighted.isEmpty) {
          highlighted.append(new QByteArray(readBuffer.size, 0))
        }
        pos += readBuffer.size
      }
    }
    _ioDevice.close
    buffer
  }

  def write(iODevice: QIODevice, pos: Long, count: Long): Boolean = {
    var _count = count
    if (count == -1)
      _count = _size

    val ok = iODevice.open(QIODevice.OpenModeFlag.WriteOnly)
    if (ok) {
      for (idx <- pos until count by BufferSize) {
        val ba = data(idx, BufferSize)
        iODevice.write(ba)
      }
      iODevice.close()
    }
    ok
  }

  def setDataChanged(pos: Long, _dataChanged: Boolean) = {
    val b = if (_dataChanged) 1 else 0

    if (!(pos < 0) && !(pos >= _size)) {
      val chunkIdx = getChunkIndex(pos)
      val posInBa = pos - _chunks(chunkIdx).absPos
      _chunks(chunkIdx).dataChanged.data.setByteAt(posInBa.toInt, b.toByte)
    }
  }

  def dataChanged(pos: Long): Boolean = {
    val highlighted = new QByteArray("")
    data(pos, 1, highlighted)
    highlighted.at(0) == 1.toByte
  }

  def indexOf(ba: QByteArray, from: Long): Long = {
    var result: Long = -1
    var buffer = new QByteArray("")

    for (pos <- from until _size by BufferSize) {
      if (result >= 0)
        result
      buffer = data(pos, BufferSize + ba.size - 1)
      val findPos = buffer.indexOf(ba)
      if (findPos >= 0)
        result = pos + findPos
    }
    result
  }

  def lastIndexOf(ba: QByteArray, from: Long): Long = {
    var result: Long = -1
    var buffer = new QByteArray("")

    var pos = from
    while (pos > 0 && result < 0) {
      var sPos = pos - BufferSize - ba.size + 1
      if (sPos < 0)
        sPos = 0
      buffer = data(sPos, pos - sPos)
      val findPos = buffer.lastIndexOf(ba)
      if (findPos >= 0)
        result = sPos + findPos
      pos = pos - BufferSize
    }
    result
  }

  def insert(pos: Long, b: Byte): Boolean = {
    if (pos < 0 || pos > _size)
      false

    val chunkIdx = if (pos == size) getChunkIndex(pos - 1) else getChunkIndex(pos)

    val posInBa = pos - _chunks(chunkIdx).absPos
    _chunks(chunkIdx).data.insert(posInBa.toInt, b)
    _chunks(chunkIdx).dataChanged.insert(posInBa.toInt, 1.toByte)

    for (idx <- chunkIdx + 1 until _chunks.size) {
      _chunks(idx).absPos += 1
    }
    _size += 1
    _pos = pos
    true
  }

  def overwrite(pos: Long, b: Byte): Boolean = {
    if (pos < 0 || pos > _size)
      false

    val chunkIdx = getChunkIndex(pos)

    val posInBa = pos - _chunks(chunkIdx).absPos
    _chunks(chunkIdx).data.data.setByteAt(posInBa.toInt, b)
    _chunks(chunkIdx).dataChanged.data.setByteAt(posInBa.toInt, 1.toByte)
    _pos = pos
    true
  }

  def removeAt(pos: Long): Boolean = {
    if (pos < 0 || pos > _size)
      false

    val chunkIdx = getChunkIndex(pos)

    val posInBa = pos - _chunks(chunkIdx).absPos
    _chunks(chunkIdx).data.remove(posInBa.toInt, 1)
    _chunks(chunkIdx).dataChanged.remove(posInBa.toInt, 1)

    for (idx <- chunkIdx + 1 until _chunks.size) {
      _chunks(idx).absPos -= 1
    }
    _size -= 1
    _pos = pos
    true
  }

  def getChunkIndex(absPos: Long): Int = {
    // This routine checks, if there is already a copied chunk available. If os, it
    // returns a reference to it. If there is no copied chunk available, original
    // data will be copied into a new chunk.

    var foundIdx = -1
    var insertIdx = 0
    var ioDelta: Long = 0


    breakable {
      for (idx <- 0 until _chunks.size) {
        val chunk = _chunks(idx)
        if ((absPos >= chunk.absPos) && (absPos < (chunk.absPos + chunk.data.size))) {
          foundIdx = idx
          break
        }
        if (absPos < chunk.absPos) {
          insertIdx = idx
          break
        }
        ioDelta += chunk.data.size - ChunkSize
        insertIdx = idx + 1
      }
    }

    if (foundIdx == -1) {
      val newChunk = new Chunk
      val readAbsPos = absPos - ioDelta
      val readPos = (readAbsPos & ReadChunkMask)
      _ioDevice.open(QIODevice.OpenModeFlag.ReadOnly)
      _ioDevice.seek(readPos)
      newChunk.data = _ioDevice.read(ChunkSize)
      _ioDevice.close
      newChunk.absPos = absPos - (readAbsPos - readPos)
      newChunk.dataChanged = new QByteArray(newChunk.data.size, 0.toByte)
      _chunks.insert(insertIdx, newChunk)
      foundIdx = insertIdx
    }
    foundIdx
  }
}
