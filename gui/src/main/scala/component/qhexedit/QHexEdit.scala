package ch.os.tproxy.gui.component.qhexedit

import com.trolltech.qt.core.{QByteArray, QTimer, QRect, QIODevice, Qt, QPoint, QBuffer}
import com.trolltech.qt.gui.{QAbstractScrollArea, QFont, QFontMetrics, QColor, QPen, QBrush, QPalette, QPaintEvent, QPainter, QMouseEvent, QResizeEvent, QKeyEvent, QApplication}
import com.trolltech.qt.gui.QKeySequence.{StandardKey => SK}

class QHexEdit() extends QAbstractScrollArea {

  val HexCharsInLine = 47
  val BytesPerLine = 16

  val _chunks = new Chunks
  val _undoStack = new UndoStack(_chunks, this)

  var _pxGapAdr = 0
  var _pxGapAdrHex = 0
  var _pxGapHexAscii = 0
  var _pxCharHeight = 0
  var _pxCharWidth = 0
  var _pxCursorWidth = 0
  var _pxSelectionSub = 0
  setHexFont(new QFont("Monospace", 10))

  var _addressAreaColor = this.palette.alternateBase.color
  var _brushHighlighted = new QBrush(new QColor(0xff, 0xff, 0x99, 0xff))
  var _penHighlighted = new QPen(viewport.palette.color(QPalette.ColorRole.WindowText))
  var _brushSelection = new QBrush(this.palette.highlight.color)

  val _cursorTimer = new QTimer
  var _cursorRect = new QRect
  var _blink = true
  _cursorTimer.timeout.connect(this, "updateCursor()")
  _cursorTimer.setInterval(500)
  _cursorTimer.start

  var _addressArea = true
  var _asciiArea = true
  var _addressWidth = 4
  var _addrDigits = addressWidth
  var _pxPosHexX = 0
  var _pxPosAdrX = 0
  var _pxPosAsciiX = 0
  var _pxCursorY: Long = 0
  var _pxCursorX: Long = 0
  var _rowsShown = 0
  var _bPosFirst = 0
  var _bPosLast: Long = 0
  var _bPosCurrent: Long = 0
  var _cursorPosition: Long = 0
  var _overwriteMode = true
  var _modified = false

  var _dataShown = new QByteArray("")
  var _markedShown = new QByteArray("")
  var _hexDataShown = new QByteArray("")

  val currentSelectionChanged = new Signal2[java.lang.Long, java.lang.Long]
  val dataChanged = new Signal0
  val currentSizeChanged = new Signal1[java.lang.Long]

  verticalScrollBar.valueChanged.connect(this, "adjust()")
  _undoStack.indexChanged.connect(this, "dataChangedPrivate(int)")

  var _highlighting = true
  var _readOnly = false
  var _addressOffset: Long = 0
  var _bSelectionInit: Long = 0
  var _bSelectionBegin: Long = 0
  var _bSelectionEnd: Long = 0

  init

  var _lastEventSize: Long = 0
  val _penSelection = new QPen(new QColor(Qt.GlobalColor.white))

  def setHexFont(font: QFont) = {
    setFont(font)
    val fm = new QFontMetrics(font)
    _pxCharWidth = fm.width('2')
    _pxCharHeight = fm.height
    _pxGapAdr = _pxCharWidth / 2
    _pxGapAdrHex = _pxCharWidth
    _pxGapHexAscii = 2 * _pxCharWidth
    _pxCursorWidth = _pxCharHeight / 7
    _pxSelectionSub = _pxCharHeight / 5
    viewport.update
  }

  def setAddressAreaColor(color: QColor) = {
    _addressAreaColor = color
    viewport.update
  }

  def setHighlightingColor(color: QColor) = {
    _brushHighlighted = new QBrush(color)
    _penHighlighted = new QPen(viewport.palette.color(QPalette.ColorRole.WindowText))
    viewport.update
  }

  def setSelectionColor(color: QColor) = {
    _brushSelection = new QBrush(color)
    viewport.update
  }

  def updateCursor() = {
    _blink = if (_blink) false else true
    viewport.update(_cursorRect);
  }

  def setCursorPosition(_position: Long) = {
    var position = _position
    _blink = false
    viewport.update(_cursorRect)

    if (_overwriteMode && (position > (_chunks.size * 2 - 1)))
      position = _chunks.size * 2 - 1
    if (!_overwriteMode && (position > _chunks.size * 2))
      position = _chunks.size * 2
    if (position < 0)
      position = 0

    _cursorPosition = position
    _bPosCurrent = position / 2
    _pxCursorY = ((position / 2 - _bPosFirst) / BytesPerLine + 1) * _pxCharHeight
    val x = (position % (2 * BytesPerLine))
    _pxCursorX = (((x / 2) * 3) + (x % 2)) * _pxCharWidth + _pxPosHexX

    if (_overwriteMode)
      _cursorRect = new QRect(_pxCursorX.asInstanceOf[Int], (_pxCursorY + _pxCursorWidth).asInstanceOf[Int], _pxCharWidth, _pxCursorWidth)
    else
      _cursorRect = new QRect(_pxCursorX.asInstanceOf[Int], (_pxCursorY - _pxCharHeight + 4).asInstanceOf[Int], _pxCursorWidth, _pxCharHeight)

    // 4. Immiadately draw new cursor
    _blink = true
    viewport.update(_cursorRect)

  }

  def addressWidth() = {
    var size = _chunks.size
    var n = 1
    if (size > 0x10000) {
      n += 4;
      size /= 0x10000
    }
    if (size > 0x100) {
      n += 2;
      size /= 0x100
    }
    if (size > 0x10) {
      n += 1;
      size /= 0x10
    }

    if (n > _addressWidth) n else _addressWidth
  }

  def readBuffers() = {
    _dataShown = _chunks.data(_bPosFirst, _bPosLast - _bPosFirst + BytesPerLine + 1, _markedShown)
    _hexDataShown = new QByteArray(_dataShown.toHex)
  }

  def adjust() = {
    // recalc Graphics
    if (_addressArea) {
      _addrDigits = addressWidth
      _pxPosHexX = _pxGapAdr + _addrDigits * _pxCharWidth + _pxGapAdrHex
    } else
      _pxPosHexX = _pxGapAdrHex
    _pxPosAdrX = _pxGapAdr
    _pxPosAsciiX = _pxPosHexX + HexCharsInLine * _pxCharWidth + _pxGapHexAscii

    // set horizontalScrollBar()
    var pxWidth = _pxPosAsciiX
    if (_asciiArea)
      pxWidth += BytesPerLine * _pxCharWidth
    horizontalScrollBar.setRange(0, pxWidth - viewport.width)
    horizontalScrollBar.setPageStep(viewport.width)

    // set verticalScrollbar()
    _rowsShown = ((viewport.height - 4) / _pxCharHeight)
    val lineCount = (_chunks.size / BytesPerLine) + 1
    verticalScrollBar.setRange(0, (lineCount - _rowsShown).asInstanceOf[Int])
    verticalScrollBar.setPageStep(_rowsShown)

    val value = verticalScrollBar.value
    _bPosFirst = value * BytesPerLine
    _bPosLast = _bPosFirst + _rowsShown * BytesPerLine - 1
    if (_bPosLast >= _chunks.size)
      _bPosLast = _chunks.size - 1
    readBuffers
    setCursorPosition(_cursorPosition)
  }

  def dataChangedPrivate(x: Int) = {
    _modified = _undoStack.index != 0
    adjust
    dataChanged.emit
  }

  def setAddressWidth(width: Int) = {
    _addressWidth = width
    adjust
    setCursorPosition(_cursorPosition)
    viewport.update
  }

  def setAddressArea(addressArea: Boolean) = {
    _addressArea = addressArea
    adjust
    setCursorPosition(_cursorPosition)
    viewport.update
  }

  def setAsciiArea(asciiArea: Boolean) = {
    _asciiArea = asciiArea;
    viewport.update
  }

  def setOverwriteMode(overwriteMode: Boolean) = {
    _overwriteMode = overwriteMode;
    viewport.update
  }

  def setHighlighting(highlighting: Boolean) = {
    _highlighting = highlighting;
    viewport.update
  }

  def setReadOnly(readOnly: Boolean) = {
    _readOnly = readOnly
  }

  def setAddressOffset(offset: Long) = {
    _addressOffset = offset
    adjust
    setCursorPosition(_cursorPosition)
    viewport.update
  }

  def resetSelection(_pos: Long) = {
    var pos = _pos
    if (pos < 0) pos = 0
    if (pos > _chunks.size) pos = _chunks.size
    pos = pos / 2
    _bSelectionInit = pos
    _bSelectionBegin = pos
    _bSelectionEnd = pos
    currentSelectionChanged emit(_bSelectionBegin, _bSelectionEnd)
  }

  def setSelection(_pos: Long) = {
    var pos = _pos
    if (pos < 0) pos = 0
    if (pos > _chunks.size) pos = _chunks.size
    pos = pos / 2
    if (pos >= _bSelectionInit) {
      _bSelectionEnd = pos
      _bSelectionBegin = _bSelectionInit
    } else {
      _bSelectionBegin = pos
      _bSelectionEnd = _bSelectionInit
    }
    currentSelectionChanged emit(_bSelectionBegin, _bSelectionEnd)
  }

  def init() = {
    _undoStack.clear
    setAddressOffset(0)
    resetSelection(0)
    setCursorPosition(0)
    verticalScrollBar.setValue(0)
    _modified = false
  }

  def setData(ioDevice: QIODevice): Boolean = {
    val ok = _chunks.setIODevice(ioDevice)
    _undoStack.clear
    setAddressOffset(0)
    dataChangedPrivate(0)
    _modified = false
    ok
  }

  def clear() = {
    setData(new QBuffer)
    init
  }

  def dataAt(pos: Long, count: Long): QByteArray = {
    _chunks.data(pos, count)
  }

  def ensureVisible() = {
    if (_cursorPosition < _bPosFirst * 2)
      verticalScrollBar.setValue((_cursorPosition / 2 / BytesPerLine).asInstanceOf[Int])
    if (_cursorPosition > (_bPosFirst + (_rowsShown - 1) * BytesPerLine) * 2)
      verticalScrollBar.setValue((_cursorPosition / 2 / BytesPerLine - _rowsShown + 1).asInstanceOf[Int])
    viewport.update
  }

  protected override def paintEvent(event: QPaintEvent) = {
    val painter = new QPainter(viewport)

    if (event.rect != _cursorRect) {
      // process some useful calculations
      val pxOfsX = horizontalScrollBar.value
      val pxPosStartY = _pxCharHeight

      // draw some patterns if needed
      painter.fillRect(event.rect, viewport.palette.color(QPalette.ColorRole.Base))
      if (_addressArea)
        painter.fillRect(new QRect(-pxOfsX, event.rect.top, _pxPosHexX - _pxGapAdrHex / 2 - pxOfsX, height), _addressAreaColor)
      if (_asciiArea) {
        val linePos = _pxPosAsciiX - (_pxGapHexAscii / 2)
        painter.setPen(new QColor(Qt.GlobalColor.gray))
        painter.drawLine(linePos - pxOfsX, event.rect.top, linePos - pxOfsX, height)
      }

      painter.setPen(viewport.palette.color(QPalette.ColorRole.WindowText))

      var pxPosY: Long = 0
      // paint address area
      if (_addressArea) {

        var row = 0
        pxPosY = _pxCharHeight

        while (row <= (_dataShown.size / BytesPerLine)) {
          val address = "%x".format(_bPosFirst + row * BytesPerLine + _addressOffset).reverse.padTo(_addrDigits, '0').reverse.mkString
          painter.drawText(_pxPosAdrX - pxOfsX, pxPosY.asInstanceOf[Int], address)
          row += 1
          pxPosY += _pxCharHeight
        }
      }

      // paint hex and ascii area
      val colStandard = new QPen(viewport.palette.color(QPalette.ColorRole.WindowText))
      painter.setBackgroundMode(Qt.BGMode.TransparentMode)

      var row = 0
      pxPosY = pxPosStartY
      while (row <= _rowsShown) {
        var pxPosX = _pxPosHexX - pxOfsX
        var pxPosAsciiX2 = _pxPosAsciiX - pxOfsX
        val bPosLine = row * BytesPerLine

        var colIdx = 0
        while (bPosLine + colIdx < _dataShown.size && colIdx < BytesPerLine) {
          var c = viewport.palette.color(QPalette.ColorRole.Base)
          painter.setPen(colStandard)

          val posBa = _bPosFirst + bPosLine + colIdx
          if ((_bSelectionBegin <= posBa) && (_bSelectionEnd > posBa)) {
            c = _brushSelection.color
            painter.setPen(_penSelection)
          } else {
            if (_highlighting) {
              val pos = posBa - _bPosFirst
              if (_markedShown.size >= pos && _markedShown.at(pos.toInt) > 0) {
                c = _brushHighlighted.color
                painter.setPen(_penHighlighted)
              }
            }
          }

          // render hex value
          val r = new QRect
          if (colIdx == 0)
            r.setRect(pxPosX, (pxPosY - _pxCharHeight + _pxSelectionSub).asInstanceOf[Int], 2 * _pxCharWidth, _pxCharHeight)
          else
            r.setRect(pxPosX - _pxCharWidth, (pxPosY - _pxCharHeight + _pxSelectionSub).asInstanceOf[Int], 3 * _pxCharWidth, _pxCharHeight)
          painter.fillRect(r, c)
          val hex = _hexDataShown.mid((bPosLine + colIdx) * 2, 2)
          painter.drawText(pxPosX, pxPosY.asInstanceOf[Int], hex.toString)
          pxPosX += 3 * _pxCharWidth

          // render ascii value
          if (_asciiArea) {
            var ch = _dataShown.at(bPosLine + colIdx)
            if ((ch < 0x20) || (ch > 0x7e))
              ch = '.'
            r.setRect(pxPosAsciiX2, (pxPosY - _pxCharHeight + _pxSelectionSub).asInstanceOf[Int], _pxCharWidth, _pxCharHeight)
            painter.fillRect(r, c)
            painter.drawText(pxPosAsciiX2, pxPosY.asInstanceOf[Int], ch.toChar.toString)
            pxPosAsciiX2 = pxPosAsciiX2 + _pxCharWidth
          }
          colIdx += 1
        }
        row += 1
        pxPosY += _pxCharHeight
      }
      painter.setBackgroundMode(Qt.BGMode.TransparentMode)
      painter.setPen(viewport.palette.color(QPalette.ColorRole.WindowText))
    }

    // paint cursor
    if (_blink && !_readOnly && hasFocus)
      painter.fillRect(_cursorRect, this.palette.color(QPalette.ColorRole.WindowText))
    else
      painter.drawText(_pxCursorX.asInstanceOf[Int], _pxCursorY.asInstanceOf[Int], _hexDataShown.mid((_cursorPosition - _bPosFirst * 2).asInstanceOf[Int], 1).toString)

    // emit event, if size has changed
    if (_lastEventSize != _chunks.size) {
      _lastEventSize = _chunks.size
      currentSizeChanged.emit(_lastEventSize)
    }
  }

  def cursorPosition(pos: QPoint): Long = {
    if (pos.x >= _pxPosHexX && pos.x < (_pxPosHexX + (1 + HexCharsInLine) * _pxCharWidth)) {
      var x = (pos.x - _pxPosHexX - _pxCharWidth / 2) / _pxCharWidth
      x = (x / 3) * 2 + x % 3
      val y = ((pos.y - 3) / _pxCharHeight) * 2 * BytesPerLine
      _bPosFirst * 2 + x + y
    } else
      -1
  }

  def refresh() = {
    ensureVisible
    readBuffers
  }

  // char handling

  def insert(index: Long, ch: Byte) = {
    _undoStack.insert(index, ch)
    refresh
  }

  def remove(index: Long, len: Long) = {
    _undoStack.removeAt(index, len)
    refresh
  }

  def replace(index: Long, ch: Byte) = {
    _undoStack.overwrite(index, ch)
    refresh
  }

  // bytearray handling

  def insert(pos: Long, ba: QByteArray) = {
    _undoStack.insert(pos, ba)
    refresh
  }

  def replace(pos: Long, len: Long, ba: QByteArray) = {
    _undoStack.overwrite(pos, len, ba)
    refresh
  }

  def undo() = {
    _undoStack.undo
    setCursorPosition(_chunks.pos * 2)
    refresh
  }

  def redo() = {
    _undoStack.redo
    setCursorPosition(_chunks.pos * 2)
    refresh
  }

  protected override def mouseMoveEvent(event: QMouseEvent) = {
    _blink = false
    viewport.update
    val actPos = cursorPosition(event.pos)
    if (actPos >= 0) {
      setCursorPosition(actPos)
      setSelection(actPos)
    }
  }

  protected override def mousePressEvent(event: QMouseEvent) = {
    _blink = false
    viewport.update
    val cPos = cursorPosition(event.pos)
    if (cPos >= 0) {
      resetSelection(cPos)
      setCursorPosition(cPos)
    }
  }

  protected override def resizeEvent(event: QResizeEvent) = adjust

  protected override def keyPressEvent(event: QKeyEvent) = {

    if (event.matches(SK.MoveToNextChar)) {
      setCursorPosition(_cursorPosition + 1)
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToPreviousChar)) {
      setCursorPosition(_cursorPosition - 1)
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToEndOfLine)) {
      setCursorPosition(_cursorPosition | (2 * BytesPerLine - 1))
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToStartOfLine)) {
      setCursorPosition(_cursorPosition - (_cursorPosition % (2 * BytesPerLine)));
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToPreviousLine)) {
      setCursorPosition(_cursorPosition - (2 * BytesPerLine))
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToNextLine)) {
      setCursorPosition(_cursorPosition + (2 * BytesPerLine))
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToNextPage)) {
      setCursorPosition(_cursorPosition + (((_rowsShown - 1) * 2 * BytesPerLine)))
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToPreviousPage)) {
      setCursorPosition(_cursorPosition - (((_rowsShown - 1) * 2 * BytesPerLine)))
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToEndOfDocument)) {
      setCursorPosition(_chunks.size * 2)
      resetSelection(_cursorPosition)
    }
    else if (event.matches(SK.MoveToStartOfDocument)) {
      setCursorPosition(0)
      resetSelection(_cursorPosition)
    }

    // Select commands
    else if (event.matches(SK.SelectAll)) {
      resetSelection(0)
      setSelection(2 * _chunks.size + 1)
    }
    else if (event.matches(SK.SelectNextChar)) {
      val pos = _cursorPosition + 1
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectPreviousChar)) {
      val pos = _cursorPosition - 1
      setSelection(pos)
      setCursorPosition(pos)
    }
    else if (event.matches(SK.SelectEndOfLine)) {
      val pos = _cursorPosition - (_cursorPosition % (2 * BytesPerLine)) + (2 * BytesPerLine)
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectStartOfLine)) {
      val pos = _cursorPosition - (_cursorPosition % (2 * BytesPerLine))
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectPreviousLine)) {
      val pos = _cursorPosition - (2 * BytesPerLine)
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectNextLine)) {
      val pos = _cursorPosition + (2 * BytesPerLine)
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectNextPage)) {
      val pos = _cursorPosition + (((viewport.height / _pxCharHeight) - 1) * 2 * BytesPerLine)
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectPreviousPage)) {
      val pos = _cursorPosition - (((viewport.height / _pxCharHeight) - 1) * 2 * BytesPerLine)
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectEndOfDocument)) {
      val pos = _chunks.size * 2
      setCursorPosition(pos)
      setSelection(pos)
    }
    else if (event.matches(SK.SelectStartOfDocument)) {
      val pos = 0
      setCursorPosition(pos)
      setSelection(pos)
    }

    // Edit Commands
    if (!_readOnly) {
      if ((QApplication.keyboardModifiers.isSet(Qt.KeyboardModifier.NoModifier)) ||
        (QApplication.keyboardModifiers.isSet(Qt.KeyboardModifier.KeypadModifier))) {
        /* Hex input */
        val key = event.key()

        if ((key >= '0' && key <= '9') || (key >= 'A' && key <= 'F')) {
          if (_bSelectionBegin != _bSelectionEnd) {
            if (_overwriteMode) {
              val len = _bSelectionEnd - _bSelectionBegin
              replace(_bSelectionBegin, len, new QByteArray(len.asInstanceOf[Int], 0.toByte))
            }
            else {
              remove(_bSelectionBegin, _bSelectionEnd - _bSelectionBegin)
              _bPosCurrent = _bSelectionBegin
            }
            setCursorPosition(2 * _bPosCurrent)
            resetSelection(2 * _bPosCurrent)
          }

          // If insert mode, then insert a byte
          if (!_overwriteMode)
            if ((_cursorPosition % 2) == 0)
              insert(_bPosCurrent, 0.toByte)

          // Change content
          if (_chunks.size > 0) {
            val hexValue = _chunks.data(_bPosCurrent, 1).toHex
            if ((_cursorPosition % 2) == 0)
              hexValue.data.setByteAt(0, key.toByte)
            else
              hexValue.data.setByteAt(1, key.toByte)

            val nba = QByteArray.fromHex(hexValue)
            replace(_bPosCurrent, nba.at(0))

            setCursorPosition(_cursorPosition + 1)
            resetSelection(_cursorPosition)
          }
        }
      }

      /* Cut */
      if (event.matches(SK.Cut)) {
        val ba = _chunks.data(_bSelectionBegin, _bSelectionEnd - _bSelectionBegin).toHex
        for (idx <- 32 until ba.size by 33)
          ba.insert(idx, "\n")
        val clipboard = QApplication.clipboard()
        clipboard.setText(ba.toString)
        if (_overwriteMode) {
          val len = _bSelectionEnd - _bSelectionBegin
          replace(_bSelectionBegin, len, new QByteArray(len.asInstanceOf[Int], 0.toByte))
        }
        else
          remove(_bSelectionBegin, _bSelectionEnd - _bSelectionBegin)

        setCursorPosition(2 * _bSelectionBegin)
        resetSelection(2 * _bSelectionBegin)
      }

      /* Paste */
      if (event.matches(SK.Paste)) {
        val clipboard = QApplication.clipboard()
        val ba = QByteArray.fromHex(new QByteArray(clipboard.text))
        if (_overwriteMode)
          replace(_bPosCurrent, ba.size, ba)
        else
          insert(_bPosCurrent, ba)
        setCursorPosition(_cursorPosition + 2 * ba.size)
        resetSelection(_bSelectionBegin)
      }

      /* Delete char */
      if (event.matches(SK.Delete)) {
        if (_bSelectionBegin != _bSelectionEnd) {
          _bPosCurrent = _bSelectionBegin
          if (_overwriteMode) {
            val ba = new QByteArray((_bSelectionEnd - _bSelectionBegin).asInstanceOf[Int], 0.toByte)
            replace(_bPosCurrent, ba.size, ba)
          }
          else
            remove(_bPosCurrent, _bSelectionEnd - _bSelectionBegin)
        }
        else {
          if (_overwriteMode)
            replace(_bPosCurrent, 0.toByte)
          else
            remove(_bPosCurrent, 1)
        }
        setCursorPosition(2 * _bPosCurrent)
        resetSelection(2 * _bPosCurrent)
      }

      /* Backspace */
      if ((event.key == Qt.Key.Key_Backspace.value) && (event.modifiers.isSet(Qt.KeyboardModifier.NoModifier))) {
        if (_bSelectionBegin != _bSelectionEnd) {
          _bPosCurrent = _bSelectionBegin
          setCursorPosition(2 * _bPosCurrent)
          if (_overwriteMode) {
            val ba = new QByteArray((_bSelectionEnd - _bSelectionBegin).asInstanceOf[Int], 0.toByte)
            replace(_bPosCurrent, ba.size, ba)
          }
          else
            remove(_bPosCurrent, _bSelectionEnd - _bSelectionBegin)

          resetSelection(2 * _bPosCurrent)
        }
        else {
          _bPosCurrent -= 1
          if (_overwriteMode)
            replace(_bPosCurrent, 0.toByte)
          else
            remove(_bPosCurrent, 1)
          _bPosCurrent -= 1
          setCursorPosition(2 * _bPosCurrent)
          resetSelection(2 * _bPosCurrent)
        }
      }

      /* undo */
      if (event.matches(SK.Undo))
        undo()

      /* redo */
      if (event.matches(SK.Redo))
        redo()

    }

    /* Copy */
    if (event.matches(SK.Copy)) {
      val ba = _chunks.data(_bSelectionBegin, _bSelectionEnd - _bSelectionBegin).toHex
      for (idx <- 32 until ba.size by 33)
        ba.insert(idx, "\n")
      val clipboard = QApplication.clipboard
      clipboard.setText(ba.toString)
    }

    // Switch between insert/overwrite mode
    if (event.key == Qt.Key.Key_Insert.value && (event.modifiers.isSet(Qt.KeyboardModifier.NoModifier))) {
      setOverwriteMode(!_overwriteMode)
      setCursorPosition(_cursorPosition)
    }

    refresh
  }
}
