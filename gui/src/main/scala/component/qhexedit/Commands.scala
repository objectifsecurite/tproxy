package ch.os.tproxy.gui.component.qhexedit

import com.trolltech.qt.core.{QObject, QByteArray}
import com.trolltech.qt.gui.{QUndoStack, QUndoCommand}

object CharCommand extends Enumeration {
  type CCmd = Value
  val insert, removeAt, overwrite = Value
}

class CharCommand(chunks: Chunks, cmd: CharCommand.CCmd, charPos: Long, newChar: Byte) extends QUndoCommand {

  import CharCommand._

  val _chunks = chunks
  var _charPos = charPos
  var _newChar = newChar
  val _cmd = cmd
  var _oldChar = 0.toByte
  var _wasChanged = false


  def mergeWith(command: CharCommand): Boolean = {
    val nextCommand = command
    var result = false

    if (_cmd != removeAt) {
      if (nextCommand._cmd == overwrite && nextCommand._charPos == _charPos) {
        _newChar = nextCommand._newChar
        result = true
      }
    }
    result
  }

  override def undo() = {
    _cmd match {
      case CharCommand.insert => _chunks.removeAt(_charPos)
      case CharCommand.overwrite => {
        _chunks.overwrite(_charPos, _oldChar)
        _chunks.setDataChanged(_charPos, _wasChanged)
      }
      case CharCommand.removeAt => {
        _chunks.insert(_charPos, _oldChar)
        _chunks.setDataChanged(_charPos, _wasChanged)
      }
    }
  }

  override def redo() = {
    _cmd match {
      case CharCommand.insert => _chunks.insert(_charPos, _newChar)
      case CharCommand.overwrite => {
        _oldChar = _chunks.data(_charPos, 1).at(0)
        _wasChanged = _chunks.dataChanged(_charPos)
        _chunks.overwrite(_charPos, _newChar)
      }
      case CharCommand.removeAt => {
        _oldChar = _chunks.data(_charPos, 1).at(0)
        _wasChanged = _chunks.dataChanged(_charPos)
        _chunks.removeAt(_charPos)
      }
    }
  }
}

class UndoStack(chunks: Chunks, parent: QObject) extends QUndoStack {
  val _chunks = chunks
  val _parent = parent

  def insert(pos: Long, c: Byte) = {
    if (pos >= 0 && pos <= _chunks.size) {
      val cc = new CharCommand(_chunks, CharCommand.insert, pos, c)
      push(cc)
    }
  }

  def insert(pos: Long, ba: QByteArray) = {
    if (pos >= 0 && pos <= _chunks.size) {
      val txt = s"Inserting ${ba.size} bytes"
      beginMacro(txt)
      for (i <- 0 to ba.size - 1) {
        val cc = new CharCommand(_chunks, CharCommand.insert, pos + i, ba.at(i))
        push(cc)
      }
      endMacro
    }
  }

  def removeAt(pos: Long, len: Long) = {
    if (pos >= 0 && pos <= _chunks.size) {
      if (len == 1) {
        val cc = new CharCommand(_chunks, CharCommand.removeAt, pos, 0)
        push(cc)
      } else {
        val txt = s"Delete ${len} bytes"
        beginMacro(txt)
        for (cnt <- 0L until len) {
          val cc = new CharCommand(_chunks, CharCommand.removeAt, pos, 0)
          push(cc)
        }
        endMacro
      }
    }
  }

  def overwrite(pos: Long, c: Byte) = {
    if (pos >= 0 && pos <= _chunks.size) {
      val cc = new CharCommand(_chunks, CharCommand.overwrite, pos, c)
      push(cc)
    }
  }

  def overwrite(pos: Long, len: Long, ba: QByteArray) = {
    if (pos >= 0 && pos <= _chunks.size) {
      val txt = s"Overwrite ${ba.size} bytes"
      beginMacro(txt)
      removeAt(pos, len)
      insert(pos, ba)
      endMacro
    }
  }
}
