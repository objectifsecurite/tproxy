package ch.os.tproxy.gui.component

import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.gui.message.implicits._
import com.trolltech.qt.core.{QByteArray, QBuffer}
import akka.util.ByteString

trait HexEditor extends Editor {
  val widget = new qhexedit.QHexEdit
  clear

  widget.currentSelectionChanged.connect(this, "currentSelectionChanged(long, long)")

  def displayMsg(msg: Option[Message], refresh: Boolean) = {
    if (msg.isDefined) {
      if (msg.get.hasData) {
        val scrollBar = widget.verticalScrollBar
        val oldPosition = scrollBar.value
        val scrolled_at_max = scrollBar.value == scrollBar.maximum

        val data = if (msg.get.hasModifiedData) msg.get.modifiedData else msg.get.data
        widget.setData(new QBuffer(new QByteArray(data.toArray)))
        widget.setReadOnly(false)

        if (refresh) {
          val newPosition = if (scrolled_at_max) scrollBar.maximum else oldPosition
          widget.verticalScrollBar.setValue(newPosition)
        }
      }
      else
        widget.clear
    }
    else
      clear
  }

  def clear() = {
    widget.clear
    widget.setReadOnly(true)
  }

  def getData() = ByteString(widget.dataAt(0, -1).toByteArray)

  def selectPosition(start: Int, stop: Int) = {
    widget.setCursorPosition(start * 2)
    widget.ensureVisible()
    widget.resetSelection(start * 2)
    widget.setSelection(stop * 2)
  }

  def currentSelectionChanged(begin: Long, end: Long) = selectedPositionChanged emit(begin.toInt, end.toInt)

}
