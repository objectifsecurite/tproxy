package ch.os.tproxy.gui.component

import akka.actor.ActorRef
import ch.os.tproxy.core.filter.{Filter, FilterActor}
import ch.os.tproxy.common.behaviour.actor.message.Command

trait InterceptAction {
  def filterActor: ActorRef

  def addInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.AddFilter(filter, false), false)

  def removeInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.RemoveFilter(filter, false), false)

  def addScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.AddFilter(filter, true), false)

  def removeScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.RemoveFilter(filter, true), false)
}
