package ch.os.tproxy.gui.component

import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.gui.connection.Connection
import ch.os.tproxy.gui.model.{ConnectionTableModel, MessageTableModel}
import com.trolltech.qt.QSignalEmitter
import com.trolltech.qt.core.Qt.ConnectionType.QueuedConnection

trait EntryPointsAction extends QSignalEmitter {
  def connectionTableModel: ConnectionTableModel

  def messageTableModel: MessageTableModel

  def editorFolder: EditorFolder

  def statusBar: StatusBar

  private val guiThreadId = Thread.currentThread.getId

  private val addMessageSignal = new Signal1[Message]
  addMessageSignal connect(this, "addMessage(Message)", QueuedConnection)

  private val addConnectionSignal = new Signal1[Connection]
  addConnectionSignal connect(this, "addConnection(Connection)", QueuedConnection)

  private val changeConnectionStatusSignal = new Signal2[String, Connection.Status]
  changeConnectionStatusSignal connect(this, "changeConnectionStatus(String, Connection$Status)", QueuedConnection)

  private val setStatusListeningSignal = new Signal1[Integer]
  setStatusListeningSignal connect(this, "setStatusListening(int)", QueuedConnection)

  def addMessage(msg: Message) = {
    if (Thread.currentThread.getId != guiThreadId)
      addMessageSignal emit msg

    else {
      val connection = connectionTableModel get msg.cid

      if (true)
        aggregateChunks(connection, msg)
      else
        messageTableModel insertItem msg

      statusBar.setMessagesCount(messageTableModel.rowCount)
    }
  }

  def aggregateChunks(connection: Connection, msg: Message) = {
    val cid = connection.id

    if (connection.chunk.isDefined && (msg.isChunk || msg.isLastChunk)) {
      val previousMsg = messageTableModel get connection.chunk.get
      val reassembledData = previousMsg.data ++ msg.data
      messageTableModel updateItem(previousMsg.id, _.setData(reassembledData))

      if (msg.isLastChunk) {
        if (msg.hasInfo)
          messageTableModel updateItem(previousMsg.id, _.setInfo(msg.info))
        if (msg.hasDissection)
          messageTableModel updateItem(previousMsg.id, _.setDissection(msg.dissection))
        connectionTableModel updateItem(cid, _.copy(chunk = None))
      }

      val editedMsg = editorFolder.getMsg
      if (editedMsg.isDefined && editedMsg.get.id == previousMsg.id)
        editorFolder.notifyNewChunk(msg)
    }
    else {
      messageTableModel insertItem msg

      if (msg.isChunk)
        connectionTableModel updateItem(cid, _.copy(chunk = Some(msg.id)))
      else
        connectionTableModel updateItem(cid, _.copy(chunk = None))
    }
  }

  def addConnection(conn: Connection) = {
    if (Thread.currentThread.getId != guiThreadId)
      addConnectionSignal emit conn
    else {
      connectionTableModel.insertItem(conn)
      statusBar.setConnectionsCount(connectionTableModel.rowCount)
    }
  }

  def changeConnectionStatus(id: String, status: Connection.Status) = {
    if (Thread.currentThread.getId != guiThreadId)
      changeConnectionStatusSignal emit(id, status)
    else
      connectionTableModel.changeStatus(id, status)
  }

  def setStatusListening(port: Int) = {
    if (Thread.currentThread.getId != guiThreadId)
      setStatusListeningSignal emit port
    else
      statusBar.setListening(port)
  }
}
