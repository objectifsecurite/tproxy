package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.{
  QWidget,
  QIcon,
  QVBoxLayout,
  QTabWidget
}

trait MainFolder extends GuiComponent {
  lazy val widget = new QTabWidget
  lazy val connectionTable = new ConnectionTable with Context

  val connectionLayout = new QVBoxLayout
  connectionLayout addWidget connectionTable.view

  val connectionWindow = new QWidget
  connectionWindow setLayout connectionLayout

  lazy val messageWindow = new MessageWindow with Context

  widget.addTab(connectionWindow, new QIcon("classpath:images/flash-2x.png"), "Connections")
  widget.addTab(messageWindow.widget, new QIcon("classpath:images/chat-2x.png"), "Messages")
}
