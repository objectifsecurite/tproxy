package ch.os.tproxy.gui.component

import com.trolltech.qt.gui.QItemSelection

trait SelectConnectionAction {
  def connectionTable: ConnectionTable

  def messageTable: MessageTable

  val selectionModel = messageTable.view.selectionModel()
  selectionModel.selectionChanged.connect(this, "changeConnectionSelection(QItemSelection, QItemSelection)")

  def changeConnectionSelection(selected: QItemSelection, deselected: QItemSelection) = {
    if (!selected.isEmpty) {
      val row = selectionModel.selectedRows.get(0).row
      val msg = messageTable.proxyModel.itemAt(row)
      val connectionRow = connectionTable.model.getRow(msg.cid)
      connectionTable.view.selectRow(connectionRow)
    }
  }
}
