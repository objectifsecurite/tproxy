package ch.os.tproxy.gui.component

import ch.os.tproxy.core.filter.{Filter, FilterParser}
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.gui.component.FilterType._

trait FilterViewAction {
  def filterField: FilterField

  def messageView: MessageView

  def messageTable: MessageTable

  def filterView: FilterView

  val interceptFilterView = filterView.interceptFilterView
  val scriptInterceptFilterView = filterView.scriptInterceptFilterView
  val captureFilterView = filterView.captureFilterView

  def addInterceptFilter(filter: Filter) = {}

  def addScriptInterceptFilter(filter: Filter) = {}

  def removeInterceptFilter(filter: Filter) = {}

  def removeScriptInterceptFilter(filter: Filter) = {}

  def forwardMessage(msg: Message) = {}

  def setEditable(_editable: Boolean) = {}

  def clear() = {}

  addFilter(Capture, FilterParser("").get)

  def addFilterFromField(filterType: FilterType) = {
    val text = filterField.text()
    val filter = FilterParser(text)
    if (filter != None) addFilter(filterType, filter.get)
  }

  def addFilter(filterType: FilterType, filter: Filter) = {
    val filterText = filter.string.get
    val interceptFilters = interceptFilterView.model.filters.map(_.string.get)
    val scriptInterceptFilters = scriptInterceptFilterView.model.filters.map(_.string.get)
    val captureFilters = captureFilterView.model.filters.map(_.string.get)

    if ((!interceptFilters.contains(filterText) &&
      (!scriptInterceptFilters.contains(filterText)) &&
      (!captureFilters.contains(filterText)))) {
      if (filterType == Intercept) {
        interceptFilterView.addSubFilter(filter)
        addInterceptFilter(filter)
      } else if (filterType == Script) {
        scriptInterceptFilterView.addSubFilter(filter)
        addScriptInterceptFilter(filter)
      } else
        captureFilterView.addSubFilter(filter)
    }
    captureFilterView.view.reset()
    interceptFilterView.view.reset()
    scriptInterceptFilterView.view.reset()
  }

  def removeFilters(filterType: FilterType, filters: Vector[Filter]) = {
    if ((filterType == Intercept) || (filterType == Script)) {
      // forward all waiting messages with these filters
      val model = messageView.messageTable.proxyModel
      val rows = 0 to model.rowCount() - 1
      val messages = rows.map(x => model.itemAt(x))
      messages foreach forwardMessage

      if (filterType == Intercept)
        filters.foreach(removeInterceptFilter)
      else
        filters.foreach(removeScriptInterceptFilter)
    }
  }

  def changeFilterSelection(filterType: FilterType, filter: Filter) = {
    if (filterType == Intercept) {
      captureFilterView.view.clearSelection()
      scriptInterceptFilterView.view.clearSelection()
    }
    else if (filterType == Script) {
      captureFilterView.view.clearSelection()
      interceptFilterView.view.clearSelection()
    } else {
      interceptFilterView.view.clearSelection()
      scriptInterceptFilterView.view.clearSelection()
    }
    messageTable.setFilter(filter, filterType == Intercept)
    filterField.setText(filter.toString)
    setEditable(filterType == Intercept)
  }

  def swapFilter(from: FilterType, to: FilterType) {
    val fromView = if (from == Capture) captureFilterView else if (from == Intercept) interceptFilterView else scriptInterceptFilterView
    val toView = if (to == Capture) captureFilterView else if (to == Intercept) interceptFilterView else scriptInterceptFilterView

    val rows = fromView.selectedRows
    val selectedFilters = fromView.model.filters.zipWithIndex.filter(x => rows.contains(x._2)).map(_._1)
    toView.model.filters = toView.model.filters ++ selectedFilters
    fromView.model.filters = fromView.model.filters.zipWithIndex.filterNot(x => rows.contains(x._2)).map(_._1)
    captureFilterView.view.reset()
    interceptFilterView.view.reset()
    scriptInterceptFilterView.view.reset()

    if (to == Intercept)
      selectedFilters.foreach(addInterceptFilter)
    else if (to == Script)
      selectedFilters.foreach(addScriptInterceptFilter)
    else
      removeFilters(from, selectedFilters)
  }
}
