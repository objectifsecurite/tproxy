package ch.os.tproxy.gui.component

import ch.os.tproxy.common.message.Message
import com.trolltech.qt.QSignalEmitter
import akka.util.ByteString

trait Viewer extends QSignalEmitter with GuiComponent {
  def clear()

  def displayMsg(msg: Option[Message], refresh: Boolean)

  def selectPosition(start: Int, stop: Int)

  val selectedPositionChanged = new Signal2[Integer, Integer]
}

trait Editor extends Viewer {
  def getData(): ByteString
}
