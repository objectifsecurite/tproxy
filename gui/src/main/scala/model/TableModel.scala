package ch.os.tproxy.gui.model

import com.trolltech.qt.core.QModelIndex
import com.trolltech.qt.core.Qt.ItemDataRole.DisplayRole
import com.trolltech.qt.core.Qt.ConnectionType.QueuedConnection
import com.trolltech.qt.core.Qt.Orientation
import com.trolltech.qt.core.Qt.Orientation.Horizontal
import com.trolltech.qt.gui.QAbstractTableModel
import ch.os.tproxy.util.SkipList
import scala.reflect.ClassTag

abstract class TableModel[I, K: Ordering, T: ClassTag] extends QAbstractTableModel {

  def getId(item: T): I

  def getKey(item: T): K

  private val guiThreadId = Thread.currentThread.getId

  private val shown = new SkipList[K, T](getKey)
  private var map = Map.empty[I, T]

  private var pendingInserts = Vector.empty[Int]
  private var pendingRemoves = Vector.empty[Int]
  private var pendingUpdates = Vector.empty[Int]

  def headers: Seq[String]

  def columnCount(index: QModelIndex) = headers.length

  def rowCount(index: QModelIndex) = shown.length

  // override def flags(index: com.trolltech.qt.core.QModelIndex): Qt.ItemFlags = {
  //   new Qt.ItemFlags(Qt.ItemFlag.ItemIsEnabled)
  // }

  def data(index: QModelIndex, role: Int) = {
    if (index.row() > shown.length - 1) null
    else role match {
      case DisplayRole => displayItem(index.row, index.column)
      //case Qt.ItemDataRole.TextAlignmentRole => Qt.AlignmentFlag.AlignCenter
      case _ => null
    }
  }

  def insertItem(item: T) = {
    require(Thread.currentThread.getId == guiThreadId, s"insertItem called from another thread")

    pendingInserts = pendingInserts :+ shown.length
    shown insert item
    map = map + (getId(item) -> item)

    if (pendingInserts.length == 1) firstPendingInsert.emit
  }

  private val firstPendingInsert = new Signal0
  firstPendingInsert connect(this, "flushPendingInserts()", QueuedConnection)

  protected def flushPendingInserts() = {
    val start = pendingInserts(0)
    val p = pendingInserts.length
    val stop = start + p - 1

    beginInsertRows(null, start, stop)
    insertRows(start, p)
    endInsertRows()

    pendingInserts = Vector.empty
  }

  def removeItem(item: T) = {
    require(Thread.currentThread.getId == guiThreadId, s"removeItem called from another thread")

    val id = getId(item)
    val mapItem = map.get(id)
    require(mapItem.isDefined, s"updateItem: No item found with id $id")
    val key = getKey(item)
    val idx = shown.indexMap(key)
    shown.itemMap remove key
    map = map - id

    pendingRemoves = pendingRemoves :+ idx
    if (pendingRemoves.length == 1) firstPendingRemove.emit
  }

  private val firstPendingRemove = new Signal0
  firstPendingRemove connect(this, "flushPendingRemoves()", QueuedConnection)

  protected def flushPendingRemoves() = {
    val ranges = aggregateRanges(pendingRemoves)
    for (range <- ranges) {
      val length = range._2 - range._1
      beginRemoveRows(null, range._1, range._2)
      removeRows(range._1, length)
      endRemoveRows()
    }

    pendingRemoves = Vector.empty
  }

  def updateItem(id: I, func: T => T) = {
    require(Thread.currentThread.getId == guiThreadId, s"updateItem called from another thread")

    val mapItem = map.get(id)
    require(mapItem.isDefined, s"updateItem: No item found with id $id")
    val idx = shown.indexMap(getKey(mapItem.get))
    val item = shown(idx)
    require(mapItem.get == item, s"Items in Map $mapItem.get and SkipList $item are not the same")
    val newItem = func(item)
    shown(idx) = newItem
    map = map updated(id, newItem)

    pendingUpdates = pendingUpdates :+ idx
    if (pendingUpdates.length == 1) firstPendingUpdate.emit
    newItem
  }

  private val firstPendingUpdate = new Signal0
  firstPendingUpdate connect(this, "flushPendingUpdates()", QueuedConnection)

  protected def flushPendingUpdates() = {
    val ranges = aggregateRanges(pendingUpdates)
    for (range <- ranges)
      dataChangedDelegate.emit(range._1, range._2)

    pendingUpdates = Vector.empty
  }


  private val dataChangedDelegate = new Signal2[Integer, Integer]
  dataChangedDelegate connect(this, "flushDataChanged(int, int)", QueuedConnection)

  def flushDataChanged(minRow: Int, maxRow: Int) = dataChanged.emit(index(minRow, 0), index(maxRow, headers.length - 1))

  override def headerData(section: Int, orientation: Orientation, role: Int) = {
    if (orientation == Horizontal && role == DisplayRole) headers(section)
    else null
  }

  def itemAt(row: Int) = {
    require(Thread.currentThread.getId == guiThreadId, s"insertItem called from another thread")
    shown(row)
  }

  def get(id: I) = {
    require(Thread.currentThread.getId == guiThreadId, s"insertItem called from another thread")
    val item = map.get(id)
    require(item.isDefined, s"get: No item found with id $id")
    item.get
  }

  def getRow(id: I) = {
    require(Thread.currentThread.getId == guiThreadId, s"insertItem called from another thread")
    val item = map.get(id)
    require(item.isDefined, s"getRow: No item found with id $id")
    shown.indexMap(getKey(item.get))
  }

  def displayItem(row: Int, col: Int): String

  def aggregateRanges(vec: Vector[Int]) = {
    var ranges = Array((vec(0), vec(0)))
    var lastIndex = 0

    for (i <- 1 to vec.length - 1) {
      if (vec(i) - ranges(lastIndex)._2 == 1)
        ranges.update(lastIndex, (ranges(lastIndex)._1, vec(i)))
      else {
        lastIndex += 1
        ranges = ranges :+ ((vec(i), vec(i)))
      }
    }
    ranges
  }
}
