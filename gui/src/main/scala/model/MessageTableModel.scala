package ch.os.tproxy.gui.model

import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.Message
import java.util.UUID

class MessageTableModel extends TableModel[UUID, (Long, UUID), Message] {

  val headers = Seq("Time", "Clt Address", "Clt Port", "", "Srv Address", "Srv Port", "Proto", "Info")

  def getId(m: Message) = m.id

  def getKey(m: Message) = (m.time, m.id)

  def displayItem(row: Int, col: Int): String = {
    val msg = itemAt(row)
    msg.displayItem(col)
  }
}
