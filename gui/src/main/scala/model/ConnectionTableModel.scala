package ch.os.tproxy.gui.model

import java.text.SimpleDateFormat
import java.util.Date
import ch.os.tproxy.gui.connection.Connection

class ConnectionTableModel extends TableModel[String, (Long, String), Connection] {

  val headers = Seq("Time",
    "Clt Address",
    "Clt Port",
    "Srv Address",
    "Srv Port",
    "Status")

  def getId(c: Connection) = c.id

  def getKey(c: Connection) = (c.time, c.id)

  def displayItem(row: Int, col: Int): String = {
    val conn = itemAt(row)

    col match {
      case 0 => new SimpleDateFormat("H:mm:ss").format(new Date(conn.time))
      case 1 => conn.cltAddr.getAddress.getHostAddress
      case 2 => conn.cltAddr.getPort.toString
      case 3 => conn.srvAddr.getAddress.getHostAddress
      case 4 => conn.srvAddr.getPort.toString
      case 5 => conn.status.toString
    }
  }

  def changeStatus(id: String, newStatus: Connection.Status) = updateItem(id, _.copy(status = newStatus))

}
