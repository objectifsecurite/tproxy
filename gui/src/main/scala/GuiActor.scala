package ch.os.tproxy.gui

import java.net.{InetSocketAddress => Addr}
import scala.concurrent.{Await, Promise}
import scala.concurrent.duration._
import akka.actor.{Props, Terminated}
import ch.os.tproxy.cli.CliActor
import ch.os.tproxy.gui.connection.Connection
import ch.os.tproxy.core.TProxy
import ch.os.tproxy.common.message.{ClientAccepted, ClientClosed, ClientConnected, ClientRefused, Message}
import ch.os.tproxy.common.behaviour.OnEvent
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.log.{LogReceive, Logging}

object GuiActor {

  case object WindowClosed

  def apply(address: Addr)(implicit logging: Logging, logReceive: LogReceive) = {
    Props(classOf[GuiActor], address, logging, logReceive)
  }
}

class GuiActor(address: Addr, _logging: Logging, _logReceive: LogReceive) extends CliActor(address, _logging, _logReceive) {

  import GuiActor._

  override implicit lazy val logging = _logging forObject this

  val guiStarted = Promise[MainWindow]

  val guiThread = new Thread {
    override def run() = {
      require(filterActor.isDefined)
      val window = new MainWindow(self, filterActor.get)
      window start guiStarted
    }
  }
  guiThread.start

  val window = Await result(guiStarted.future, 3.seconds)

  override def onStart = super.onStart <-> Behaviour

  private object Behaviour extends OnEvent {
    def onEvent = {
      case ClientAccepted(id, cltAddr, srvAddr) =>
        val connection = Connection(id, cltAddr, srvAddr, sender)
        window.mainFolder addConnection connection
        this

      case ClientConnected(id) =>
        window.mainFolder changeConnectionStatus(id, Connection.Connected)
        this

      case ClientRefused(id) =>
        window.mainFolder changeConnectionStatus(id, Connection.Refused)
        this

      case ClientClosed(id) =>
        window.mainFolder changeConnectionStatus(id, Connection.Closed)
        this

      case msg: Message =>
        window.mainFolder addMessage msg
        this

      case TProxy.Bound(addr) =>
        window.mainFolder setStatusListening addr.getPort
        this

      case WindowClosed =>
        system.terminate()
        this

      case Terminated(ref) =>
        require(ref == proxy)
        system.terminate()
        this
    }
  }

}
