package ch.os.tproxy.gui.message

import ch.os.tproxy.common.message.FieldMap
import akka.util.ByteString

object implicits {
  private val modifiedKey = "modified"

  implicit class Modified[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasModified = msg has modifiedKey

    def setModified = msg set (modifiedKey -> true)

    def remModified = msg rem modifiedKey
  }

  private val modifiedDataKey = "modifiedData"

  implicit class ModifiedData[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasModifiedData = msg has modifiedDataKey

    def setModifiedData(value: ByteString) =
      msg.set(modifiedDataKey -> value).setModified

    def remModifiedData = msg rem modifiedDataKey

    def modifiedData = msg.get[ByteString](modifiedDataKey)
  }

}
