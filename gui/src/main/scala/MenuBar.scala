package ch.os.tproxy.gui

import com.trolltech.qt.gui.QApplication

class MenuBar(main: MainWindow) extends component.MenuBar {
  autoScrollAct.triggered.connect(main.mainFolder.messageWindow.messageView.messageTable, "autoScroll(boolean)")
  quitAct.triggered.connect(QApplication.instance(), "quit()")
}
