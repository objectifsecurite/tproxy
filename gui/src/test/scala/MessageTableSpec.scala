package ch.os.tproxy.test

import java.util.Random
import com.trolltech.qt.gui.{QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QWidget}
import ch.os.tproxy.gui.component.MessageTable

import scala.concurrent.duration._
import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props}
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.{Request, Response}

object MessageTableSpec {
  def time(prefix: String)(f: => Unit) = {
    val t1 = System.currentTimeMillis
    f
    val t2 = System.currentTimeMillis
    val dt = (t2 - t1) / 1000.0
    println(s"$prefix took $dt seconds")
  }

  class MainWindow(val guiActor: ActorRef)
                  (implicit val logging: Logging) extends TestMainWindow[MainWindow] {
    val app = new QApplication(Array())
    val parent = new QWidget()
    parent.resize(1090, 620)
    val layout = new QVBoxLayout()

    val buttonBar = new QWidget()
    val buttonLayout = new QHBoxLayout()
    val addDirectButton = new QPushButton("Add (direct)")
    val addIndirectButton = new QPushButton("Add (indirect)")
    val populateDirectButton = new QPushButton("Populate (direct)")
    val populateIndirectButton = new QPushButton("Populate (indirect)")
    val filterRequestButton = new QPushButton("Filter only requests")
    val resetFilterButton = new QPushButton("Reset filter")
    val autoScrollButton = new QPushButton("Auto scroll")
    autoScrollButton.setCheckable(true)
    buttonLayout.addWidget(addDirectButton)
    buttonLayout.addWidget(addIndirectButton)
    buttonLayout.addWidget(populateDirectButton)
    buttonLayout.addWidget(populateIndirectButton)
    // buttonLayout.addWidget(filterRequestButton)
    buttonLayout.addWidget(resetFilterButton)
    buttonLayout.addWidget(autoScrollButton)
    buttonBar.setLayout(buttonLayout)

    val messageTable = new MessageTable with Context
    layout.addWidget(buttonBar)
    layout.addWidget(messageTable.view)

    addDirectButton.clicked.connect(this, "addMessageDirect()")
    addIndirectButton.clicked connect(this, "addMessageIndirect()")
    populateDirectButton.clicked.connect(this, "populateDirect()")
    populateIndirectButton.clicked.connect(this, "populateIndirect()")
    // filterRequestButton.clicked.connect(messageTable, "filterRequest()")
    resetFilterButton.clicked.connect(messageTable, "resetFilter()")
    autoScrollButton.toggled.connect(messageTable, "autoScroll(boolean)")
    parent.setLayout(layout)
    parent.show()

    def addMessageDirect() = time("addMessageDirect") {
      val msg = randomMessage
      messageTable.model.insertItem(msg)
    }

    def addMessageIndirect() = guiActor ! AddMessage

    def populateDirect() = time("populate") {
      (1 to 15000).map(x => {
        val msg = randomMessage
        messageTable.model.insertItem(msg)
      })
      println(messageTable.model.rowCount())
    }

    def populateIndirect() = guiActor ! Populate

    private val rand = new Random

    def randomAddr = {
      val host = (1 to 4) map (_ => rand nextInt 256) mkString "."
      val port = rand nextInt 65536
      new Addr(host, port)
    }

    def randomRequest = {
      val src = randomAddr
      val dst = randomAddr
      Request("cid", src) setSrvAddr dst
    }

    def randomResponse = {
      val src = randomAddr
      val dst = randomAddr
      Response("cid", dst) setCltAddr src
    }

    def randomMessage = (rand nextInt 2) match {
      case 0 => randomRequest
      case 1 => randomResponse
    }

    def randomChunk = {
      randomResponse setIsChunk true setIsLastChunk false
    }
  }

  case object AddMessage

  case object Populate

  class GuiActor extends TestGuiActor[MainWindow] {
    def createMainWindow = new MainWindow(self)

    override def receive = {
      case AddMessage => mainWindow addMessageDirect()
      case Populate => mainWindow populateDirect()
      case x => super.receive(x)
    }
  }

}

class MessageTableSpec extends TestSpec("MessageTableSpec") {

  import MessageTableSpec._

  "The MessageTable" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props(classOf[GuiActor])
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
