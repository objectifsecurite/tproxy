package ch.os.tproxy.test

import akka.actor.{Actor, Props}
import ch.os.tproxy.common.log.AkkaLogging

import scala.concurrent.{Await, Promise}
import scala.concurrent.duration._
import ch.os.tproxy.core.filter.FilterActor
import ch.os.tproxy.core.log.TProxyLogReceive
import ch.os.tproxy.gui.MainWindow
import ch.os.tproxy.gui.GuiActor.WindowClosed

object GuiSpec {

  class GuiActor extends Actor {
    implicit lazy val logging = new AkkaLogging(this)
    implicit val logReceive = new TProxyLogReceive()
    lazy val filterActor = Some(FilterActor.getActor(self)(context, logging, logReceive))

    val guiStarted = Promise[MainWindow]

    val guiThread = new Thread {
      override def run() = {
        require(filterActor.isDefined)
        val window = new MainWindow(self, filterActor.get)
        window start guiStarted
      }
    }
    guiThread.start


    val mainWindow = Await result(guiStarted.future, 3.seconds)

    def receive = {
      case WindowClosed => context stop self
      //      case _: Int => mainWindow.mainFolder.connectionWindow addConnection ()
    }
  }

}

class GuiSpec extends TestSpec("GuiSpec") {

  import GuiSpec._

  "The GUI" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props[GuiActor]
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
