package ch.os.tproxy.test

import com.trolltech.qt.gui.{QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QWidget}
import com.trolltech.qt.gui.QSizePolicy
import com.trolltech.qt.gui.QSizePolicy.Policy
import ch.os.tproxy.gui.component.{EditAction, EntryPointsAction, ForwardAction, MessageView, StatusBar}
import ch.os.tproxy.gui.model.ConnectionTableModel
import ch.os.tproxy.gui.connection.Connection
import ch.os.tproxy.core.filter.FilterParser

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props}
import akka.util.ByteString
import ch.os.tproxy.common.log.Logging

import scala.concurrent.duration._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.{Message, Request, Response}
import com.trolltech.qt.QSignalEmitter

object ChunkAggregationSpec {

  def time(prefix: String)(f: => Unit) = {
    val t1 = System.currentTimeMillis
    f
    val t2 = System.currentTimeMillis
    val dt = (t2 - t1) / 1000.0
    println(s"$prefix took $dt seconds")
  }

  abstract class TestMessageView extends QSignalEmitter
    with MessageView
    with EntryPointsAction
    with ForwardAction
    with EditAction {
    lazy val connectionTableModel = new ConnectionTableModel
    lazy val messageTableModel = this.messageTable.model
    lazy val statusBar = new StatusBar

    override def forwardMessage(msg: Message) =
      messageTable.model.updateItem(msg.id, _.remIntercepted)
  }

  class MainWindow(val guiActor: ActorRef)
                  (implicit val logging: Logging) extends TestMainWindow[MainWindow] {
    val app = new QApplication(Array())
    val parent = new QWidget()
    parent.resize(1090, 620)
    val layout = new QVBoxLayout()

    val buttonBar = new QWidget()
    val buttonLayout = new QHBoxLayout()
    val add1ChunkButton = new QPushButton("Add 1 chunk")
    val add2ChunksButton = new QPushButton("Add 2 chunks")
    val addLastChunkButton = new QPushButton("Add last chunk")
    val addMsgButton = new QPushButton("Add new message")
    val editButton = new QPushButton("Enable editing")
    val interceptButton = new QPushButton("Enable interception")
    editButton.setCheckable(true)
    interceptButton.setCheckable(true)
    buttonLayout.addWidget(add1ChunkButton)
    buttonLayout.addWidget(add2ChunksButton)
    buttonLayout.addWidget(addLastChunkButton)
    buttonLayout.addWidget(addMsgButton)
    buttonLayout.addWidget(editButton)
    buttonLayout.addWidget(interceptButton)
    buttonBar.setLayout(buttonLayout)

    val sizePolicy = new QSizePolicy(Policy.Preferred, Policy.Fixed)
    buttonBar setSizePolicy sizePolicy

    val threadId = Thread.currentThread.getId
    val messageView = new TestMessageView with Context
    layout.addWidget(buttonBar)
    layout.addWidget(messageView.widget)
    layout.addWidget(messageView.statusBar.widget)

    add1ChunkButton.clicked.connect(this, "add1Chunk()")
    add2ChunksButton.clicked.connect(this, "add2Chunks()")
    addLastChunkButton.clicked.connect(this, "addLastChunk()")
    addMsgButton.clicked.connect(this, "addMsg()")
    editButton.toggled.connect(this, "setEditable(boolean)")
    interceptButton.toggled.connect(this, "setIntercept(boolean)")
    parent.setLayout(layout)
    parent.show()

    private val rand = new scala.util.Random
    var intercept = false

    val conn = randomConnection
    addConnection(conn)
    val request = Request(conn.id, conn.cltAddr) setSrvAddr conn.srvAddr
    messageView addMessage request

    private def randomAddr = {
      val host = (1 to 4) map (_ => rand nextInt 256) mkString "."
      val port = rand nextInt 65536
      new Addr(host, port)
    }

    private def randomConnection: Connection = {
      Connection(
        (1 to 10).map { _ => rand.alphanumeric(10) }.mkString(""),
        randomAddr,
        randomAddr,
        guiActor
      )
    }

    private def randomMsg(conn: Connection) = {
      val msg = Response(conn.id, conn.srvAddr) setCltAddr conn.cltAddr setData ByteString((1 to 200).map { _ => rand.alphanumeric(1) }.mkString("") + "\r\n")
      if (intercept)
        msg.setIntercepted
      else
        msg
    }

    def addConnection(conn: Connection) = {
      messageView.connectionTableModel.insertItem(conn)
    }

    def addChunk(num: Int) = {
      (1 to num).map(x => {
        messageView.addMessage(randomMsg(conn) setIsChunk true)
      })
    }

    def add1Chunk = addChunk(1)

    def add2Chunks = addChunk(2)

    def addLastChunk() = {
      val msg = randomMsg(conn) setIsChunk true setIsLastChunk true
      messageView.addMessage(msg)
    }

    def addMsg() = messageView.addMessage(randomMsg(conn))

    def setEditable(_editable: Boolean) = messageView.setEditable(_editable)

    def setIntercept(_intercept: Boolean) = {
      intercept = _intercept
      messageView.messageTable.setFilter(FilterParser("").get, _intercept)
    }
  }

  class GuiActor extends TestGuiActor[MainWindow] {
    def createMainWindow = new MainWindow(self)

    override def receive = {
      case x => super.receive(x)
    }
  }

}

class ChunkAggregationSpec extends TestSpec("ChunkAggregationSpec") {

  import ChunkAggregationSpec._

  "The ChunkAggregation" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props(classOf[GuiActor])
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
