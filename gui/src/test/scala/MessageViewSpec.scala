package ch.os.tproxy.test

import com.trolltech.qt.gui.{QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QWidget}
import com.trolltech.qt.gui.QSizePolicy
import com.trolltech.qt.gui.QSizePolicy.Policy
import ch.os.tproxy.gui.component.{EditAction, FilterFieldAction, MessageView}

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.util.ByteString
import ch.os.tproxy.common.log.{LogReceive, Logging}

import scala.concurrent.duration._
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.{Request, Response}
import ch.os.tproxy.core.pcap.PcapSim


object MessageViewSpec {

  def time(prefix: String)(f: => Unit) = {
    val t1 = System.currentTimeMillis
    f
    val t2 = System.currentTimeMillis
    val dt = (t2 - t1) / 1000.0
    println(s"$prefix took $dt seconds")
  }

  class MainWindow(val guiActor: ActorRef)
                  (implicit val logging: Logging, implicit val factory: ActorRefFactory, val logReceive: LogReceive)
    extends HasDissectionClient(logging, logReceive)(ActorSystem("MessageViewSpec")) with TestMainWindow[MainWindow] {
    val app = new QApplication(Array())
    val parent = new QWidget()
    parent.resize(1090, 620)
    val layout = new QVBoxLayout()

    val buttonBar = new QWidget()
    val buttonLayout = new QHBoxLayout()
    val addButton = new QPushButton("Add empty message")
    val addHttpConversationButton = new QPushButton("Add http conversation")
    val addSslConversationButton = new QPushButton("Add SSL conversation")
    buttonLayout.addWidget(addButton)
    buttonLayout.addWidget(addHttpConversationButton)
    buttonLayout.addWidget(addSslConversationButton)
    buttonBar.setLayout(buttonLayout)

    val sizePolicy = new QSizePolicy(Policy.Preferred, Policy.Fixed)
    buttonBar setSizePolicy sizePolicy

    val messageView = new MessageView with FilterFieldAction with EditAction {
      lazy val logging = MainWindow.this.logging
    }
    layout.addWidget(buttonBar)
    layout.addWidget(messageView.widget)

    addButton.clicked.connect(this, "addEmptyMessage()")
    addHttpConversationButton.clicked.connect(this, "addHttpConversation()")
    addSslConversationButton.clicked.connect(this, "addSslConversation()")
    parent.setLayout(layout)
    parent.show()

    def hex2bytestring(hex: String) =
      ByteString(hex.sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte))

    private val rand = new scala.util.Random

    private def randomAddr = {
      val host = (1 to 4) map (_ => rand nextInt 256) mkString "."
      val port = rand nextInt 65536
      new Addr(host, port)
    }

    private def randomRequest = {
      val src = randomAddr
      val dst = randomAddr
      Request("cid", src) setSrvAddr dst
    }

    private def randomResponse = {
      val src = randomAddr
      val dst = randomAddr
      Response("cid", dst) setCltAddr src
    }

    private def randomMessage = (rand nextInt 2) % 2 match {
      case 0 => randomRequest
      case 1 => randomResponse
    }

    def addEmptyMessage() = {
      val msg = randomMessage
      messageView.messageTable.model.insertItem(msg)
    }

    def addHttpConversation() = {
      val cltAddr = randomAddr
      val srvAddr = new Addr("80.74.147.72", 80)
      val pcapSim = new PcapSim(cltAddr, srvAddr)
      pcapSim.open.map(dissectFrame)

      val req = Request("http", cltAddr) setSrvAddr srvAddr

      val reqData = ByteString(
        """GET / HTTP/1.1
Host: www.objectif-securite.ch
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: keep-alive

""".replace("\n", "\r\n"))
      val reqDissect = pcapSim.request(reqData.toArray).map(dissectFrame)
      val req1 = req setData (reqData) setDissection (reqDissect(0))
      messageView.messageTable.model.insertItem(req1)

      val respData = ByteString(
        """HTTP/1.1 200 OK
Date: Thu, 03 Dec 2015 12:33:04 GMT
Server: Apache
X-Powered-By: PleskLine
Keep-Alive: timeout=5, max=10
Connection: Keep-Alive
Transfer-Encoding: chunked
Content-Type: text/html

<!DOCTYPE html>
éà£

""".replace("\n", "\r\n"))

      val respDissect = pcapSim.request(respData.toArray).map(dissectFrame)
      val resp1 = req setData (respData) setDissection (respDissect(0))
      messageView.messageTable.model.insertItem(resp1)

      pcapSim.closeRequest.map(dissectFrame)
      pcapSim.closeResponse.map(dissectFrame)
    }

    def addSslConversation() = {
      val cltAddr = randomAddr
      val srvAddr = new Addr("80.74.147.72", 443)
      val pcapSim = new PcapSim(cltAddr, srvAddr)
      pcapSim.open.map(dissectFrame)

      val req = Request("https", cltAddr) setSrvAddr srvAddr
      val resp = Response("https", srvAddr) setCltAddr cltAddr
      val client_hello = hex2bytestring("16030100c1010000bd0303e28215296b3fe398b65f79d901d277b49f8f64cdff0df517d5beca0c490e638f000016c02bc02fc00ac009c013c01400330039002f0035000a0100007e0000001d001b0000187777772e6f626a65637469662d73656375726974652e6368ff01000100000a00080006001700180019000b00020100002300003374000000100017001502683208737064792f332e3108687474702f312e31000500050100000000000d001600140401050106010201040305030603020304020202")
      val server_hello = hex2bytestring("16030300410200003d03035661c036c3177c516861742d17d633804ac35a0c9b942c41b7f54860833a0b1c00c02f00001500000000ff01000100000b0004030001020023000016030315210b00151d00151a000767308207633082064ba00302010202070541fe2deef1d1300d06092a864886f70d01010b050030818c310b300906035504061302494c31163014060355040a130d5374617274436f6d204c74642e312b3029060355040b1322536563757265204469676974616c204365727469666963617465205369676e696e67313830360603550403132f5374617274436f6d20436c6173732031205072696d61727920496e7465726d65646961746520536572766572204341301e170d3135303232323132323030395a170d3136303232333038343634395a3060310b30090603550406130243483121301f060355040313187777772e6f626a65637469662d73656375726974652e6368312e302c06092a864886f70d010901161f706f73746d6173746572406f626a65637469662d73656375726974652e636830820222300d06092a864886f70d01010105000382020f003082020a0282020100a5dc7e093295aa84b66f969a0d09b3bf5354dad2f6efea2246ede21170932bfaed6b9ac11e817a5372665cddd5f55981b1aedf563e1c75540ec58ff728b0f8febefb33aa2d050041de0296345f06f131b3501f3e96d228b58e64c380b94f6439206d9e3a25d39b929c39b91468e2531de5d4d72652d17572cdf2715c21c4eef6a813272718f024e03755de858f5f7e673df965fcebb154258ec333af05793f899301ab18c88000279ded24c960d6742b6c4ccff93edbabd95e13617f12e44c50e0acd8338312a17d2d3ebcb79390ab3769e8d50708c3a09ce3a2a84e1927117c4994cc14b45512305982498ce4e470dea289bbe11c9ab5560636bdec7942fde2b6bb188986ec4823c0e658214468fd6aaf371fda2255e3fd7fdb8d94c84ede4be2d6e3d99702ec9d35612b36754345e4ffb3c4cf53e51bd91ee2230670fede7112f026be13c5de23079053b13e9ec06213488e9601e5e008124df5ff8494938bfae301a3c40da1de9fcc0dde4e8cd892f630b291b57c2960e0a81d7ef084b7f9fb204a32859fbfd44352327236f3b52e44477af85bff4b5f4393a49a0def6c882ad747516b73ccf6eb650105a5bebde5019e3c04fc0dbcb2001e08bf2a24100a59c5e39b67674f1bc1d8f56d2703af3da3d87a07e9bde1e75f969d05c843c707420258b65187cda14bdca6191add0d7742249a7c181dcdfd829c1609356193bb0203010001a38202f3308202ef30090603551d1304023000300b0603551d0f0404030203a830130603551d25040c300a06082b06010505070301301d0603551d0e041604140558806f3289ecb0baa57d7c025b6c6253e1a255301f0603551d23041830168014eb4234d098b0ab9ff41b6b08f7cc642eef0e2c4530390603551d110432303082187777772e6f626a65637469662d73656375726974652e636882146f626a65637469662d73656375726974652e6368308201560603551d200482014d308201493008060667810c0102013082013b060b2b0601040181b5370102033082012a302e06082b060105050702011622687474703a2f2f7777772e737461727473736c2e636f6d2f706f6c6963792e7064663081f706082b060105050702023081ea302716205374617274436f6d2043657274696669636174696f6e20417574686f7269747930030201011a81be546869732063657274696669636174652077617320697373756564206163636f7264696e6720746f2074686520436c61737320312056616c69646174696f6e20726571756972656d656e7473206f6620746865205374617274436f6d20434120706f6c6963792c2072656c69616e6365206f6e6c7920666f722074686520696e74656e64656420707572706f736520696e20636f6d706c69616e6365206f66207468652072656c79696e67")

      val reqDissect = pcapSim.request(client_hello.toArray).map(dissectFrame)
      val req1 = req setData (client_hello) setDissection (reqDissect(0))
      messageView.messageTable.model.insertItem(req1)

      val respDissect = pcapSim.request(server_hello.toArray).map(dissectFrame)
      val resp1 = resp setData (server_hello) setDissection (respDissect(0))
      messageView.messageTable.model.insertItem(resp1)

      pcapSim.closeRequest.map(dissectFrame)
      pcapSim.closeResponse.map(dissectFrame)
    }
  }

  class GuiActor extends TestGuiActor[MainWindow] {

    def createMainWindow = new MainWindow(self)

    override def receive = {
      case x => super.receive(x)
    }
  }

}

class MessageViewSpec extends TestSpec("MessageViewSpec") with HasRunningDissector {

  import MessageViewSpec._

  "The MessageView" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props(classOf[GuiActor])
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
