package ch.os.tproxy.test

import scala.concurrent.{Await, Promise}
import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef, Props}
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.log.TProxyLogReceive
import com.trolltech.qt.gui.QApplication
import ch.os.tproxy.gui.component.GuiComponent
import ch.os.tproxy.gui.GuiActor.WindowClosed

trait TestMainWindow[T <: TestMainWindow[T]] extends GuiComponent {
  this: T =>
  def app: QApplication

  def guiActor: ActorRef

  def start(guiStarted: Promise[T]) = {
    guiStarted success this
    app exec()
    guiActor ! WindowClosed
  }
}

object TestGuiActor {
  def apply[T <: TestMainWindow[T]](construct: ActorRef => T) =
    Props(classOf[TestGuiActor[T]], construct)
}

trait TestGuiActor[T <: TestMainWindow[T]] extends Actor {
  implicit lazy val logging = new AkkaLogging(this)
  implicit val logReceive = new TProxyLogReceive()

  def createMainWindow: T

  val guiStarted = Promise[T]

  val guiThread = new Thread {
    override def run() = {
      val window = createMainWindow
      window start guiStarted
    }
  }
  guiThread.start

  val mainWindow = Await result(guiStarted.future, 3.seconds)

  def receive = {
    case WindowClosed => context stop self
  }
}
