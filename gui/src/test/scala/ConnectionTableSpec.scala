package ch.os.tproxy.test

import com.trolltech.qt.gui.{QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QWidget}
import ch.os.tproxy.gui.component.ConnectionTable

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props}
import ch.os.tproxy.common.log.Logging

import scala.concurrent.duration._
import ch.os.tproxy.gui.connection.Connection

object ConnectionTableSpec {

  def time(prefix: String)(f: => Unit) = {
    val t1 = System.currentTimeMillis
    f
    val t2 = System.currentTimeMillis
    val dt = (t2 - t1) / 1000.0
    println(s"$prefix took $dt seconds")
  }

  class MainWindow(val guiActor: ActorRef)
                  (implicit val logging: Logging) extends TestMainWindow[MainWindow] {
    val app = new QApplication(Array())
    val parent = new QWidget()
    parent.resize(1090, 620)
    val layout = new QVBoxLayout()

    val buttonBar = new QWidget()
    val buttonLayout = new QHBoxLayout()
    val addButton = new QPushButton("Add data")
    val closeButton = new QPushButton("Close first connection")
    val populateButton = new QPushButton("Populate")
    buttonLayout.addWidget(addButton)
    buttonLayout.addWidget(closeButton)
    buttonLayout.addWidget(populateButton)
    buttonBar.setLayout(buttonLayout)

    val connectionTable = new ConnectionTable with Context
    layout.addWidget(buttonBar)
    layout.addWidget(connectionTable.view)

    addButton.clicked.connect(this, "addRandomConnection()")
    closeButton.clicked.connect(this, "changeConnectionStatus()")
    populateButton.clicked.connect(this, "populate()")
    parent.setLayout(layout)
    parent.show()

    def addRandomConnection() = time("add_connection") {
      addConnection(randomConnection)
    }

    def addConnection(conn: Connection) = {
      connectionTable.model.insertItem(conn)
    }

    def changeConnectionStatus() = time("change_status") {
      val connectionModel = connectionTable.model
      val id = connectionTable.model.itemAt(0).id
      connectionModel.changeStatus(id, Connection.Closed)
    }

    def populate() = time("populate") {
      (1 to 15000).map(x => {
        val conn = randomConnection
        addConnection(conn)
      })
      println(connectionTable.model.rowCount())
    }

    private def randomConnection: Connection = {
      val random = scala.util.Random
      Connection(
        (1 to 10).map { _ => random.alphanumeric(1) }.mkString(""),
        new Addr((random.nextInt(223) + 1) + "." + (1 to 3).map { _ => random.nextInt(255) }.mkString("."), random.nextInt(65000)),
        new Addr((random.nextInt(223) + 1) + "." + (1 to 3).map { _ => random.nextInt(255) }.mkString("."), random.nextInt(65000)),
        guiActor
      )
    }
  }

  class GuiActor extends TestGuiActor[MainWindow] {
    def createMainWindow = new MainWindow(self)

    override def receive = {
      case x => super.receive(x)
    }
  }

}

class ConnectionTableSpec extends TestSpec("ConnectionTableSpec") {

  import ConnectionTableSpec._

  "The ConnectionTable" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props(classOf[GuiActor])
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
