package ch.os.tproxy.test

import com.trolltech.qt.gui.{QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QWidget}
import com.trolltech.qt.gui.QSizePolicy
import com.trolltech.qt.gui.QSizePolicy.Policy
import ch.os.tproxy.gui.component.{FilterFieldAction, FilterView, FilterViewAction, MessageWindow}
import ch.os.tproxy.gui.component.FilterType._
import ch.os.tproxy.core.filter.FilterParser

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props}
import ch.os.tproxy.common.log.{LogReceive, Logging}

import scala.concurrent.duration._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.{Request, Response}
import ch.os.tproxy.core.filter.Filter

object MessageWindowSpec {

  def time(prefix: String)(f: => Unit) = {
    val t1 = System.currentTimeMillis
    f
    val t2 = System.currentTimeMillis
    val dt = (t2 - t1) / 1000.0
    println(s"$prefix took $dt seconds")
  }

  class TestMessageWindow(main: MainWindow)
                         (implicit val logging: Logging)
    extends MessageWindow with FilterViewAction with FilterFieldAction {

    lazy val filterField = messageView.filterField
    lazy val messageTable = messageView.messageTable

    override lazy val filterView = new FilterView with Context {
      override def addFilterFromField(filterType: FilterType) = TestMessageWindow.this.addFilterFromField(filterType)

      override def removeFilters(filterType: FilterType, filters: Vector[Filter]) = TestMessageWindow.this.removeFilters(filterType, filters)

      override def changeFilterSelection(filterType: FilterType, filter: Filter) = TestMessageWindow.this.changeFilterSelection(filterType, filter)

      override def swapFilter(from: FilterType, to: FilterType) = TestMessageWindow.this.swapFilter(from, to)
    }
  }

  class MainWindow(val guiActor: ActorRef)
                  (implicit val logging: Logging, implicit val logReceive: LogReceive)
    extends TestMainWindow[MainWindow] {

    val app = new QApplication(Array())
    val parent = new QWidget()
    parent.resize(1090, 620)
    val layout = new QVBoxLayout()

    val buttonBar = new QWidget()
    val buttonLayout = new QHBoxLayout()
    val addButton = new QPushButton("Add data")
    val populateButton = new QPushButton("Populate")
    val addFilterButton = new QPushButton("Add random capture filter")
    buttonLayout.addWidget(addButton)
    buttonLayout.addWidget(populateButton)
    buttonLayout.addWidget(addFilterButton)
    buttonBar.setLayout(buttonLayout)

    val sizePolicy = new QSizePolicy(Policy.Preferred, Policy.Fixed)
    buttonBar setSizePolicy sizePolicy

    val messageWindow = new TestMessageWindow(this)
    layout.addWidget(buttonBar)
    layout.addWidget(messageWindow.widget)

    addButton.clicked.connect(this, "addMessage()")
    populateButton.clicked.connect(this, "populate()")
    addFilterButton.clicked.connect(this, "addFilter()")
    parent.setLayout(layout)
    parent.show()

    private val rand = new scala.util.Random

    private def randomAddr = {
      val host = (1 to 4) map (_ => rand nextInt 256) mkString "."
      val port = rand nextInt 65536
      new Addr(host, port)
    }

    private def randomRequest = {
      val src = randomAddr
      val dst = randomAddr
      Request("cid", src) setSrvAddr dst
    }

    private def randomResponse = {
      val src = randomAddr
      val dst = randomAddr
      Response("cid", dst) setCltAddr src
    }

    private def randomMessage = (rand nextInt 2) % 2 match {
      case 0 => randomRequest
      case 1 => randomResponse
    }

    def addMessage() = time("add_message") {
      val msg = randomMessage
      messageWindow.messageView.messageTable.model.insertItem(msg)
    }

    def populate() = time("populate") {
      (1 to 15000).map(x => {
        val msg = randomMessage
        messageWindow.messageView.messageTable.model.insertItem(msg)
      })
      println(messageWindow.messageView.messageTable.model.rowCount())
    }

    def addFilter(): Unit = {
      messageWindow.addFilter(Capture, FilterParser("clt.ip == " + randomAddr.getAddress.getHostAddress).get)
    }

  }

  class GuiActor extends TestGuiActor[MainWindow] {
    def createMainWindow = new MainWindow(self)

    override def receive = {
      case x => super.receive(x)
    }
  }

}

class MessageWindowSpec extends TestSpec("MessageWindowSpec") {

  import MessageWindowSpec._

  "The MessageWindow" must {
    "work correctly" taggedAs GuiTest in {
      val guiActor = system actorOf Props(classOf[GuiActor])
      watch(guiActor)
      expectTerminated(guiActor, 5.minutes)
    }
  }
}
