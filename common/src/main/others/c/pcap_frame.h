#include <stdint.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <linux/if_ether.h>

struct pcaphdr {
    uint32_t ts_sec;         /* timestamp seconds */
    uint32_t ts_usec;        /* timestamp microseconds (nsecs for PCAP_NSEC_MAGIC) */
    uint32_t incl_len;       /* number of octets of packet saved in file */
    uint32_t orig_len;       /* actual length of packet */
};

typedef struct {
  char *data;
  uint32_t size;
  struct pcaphdr *pcap_hdr;
  struct ethhdr *eth_hdr;
  struct iphdr *ip_hdr;
  struct tcphdr *tcp_hdr;
} pcap_frame_t;