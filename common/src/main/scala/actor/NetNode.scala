package ch.os.tproxy.common.actor

import akka.actor.ActorRef
import akka.io.Tcp
import ch.os.tproxy.common.behaviour.{Behaviour, OnCommand}
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.behaviour.actor.SendCmdTo
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.tcp.TcpBackpressure
import ch.os.tproxy.common.behaviour.implicits._

abstract class NetNode extends ActorNode {
  lazy val Backpressure = {
    val kbyte = 1 << 10
    TcpBackpressure(100 * kbyte, 1000 * kbyte, 5000 * kbyte)
  }


  object SendNetCmdTo {
    def apply(connection: ActorRef, name: String) =
      SendCmdTo({
        case cmd: Tcp.Command =>
          SendTo(connection, cmd, false, name)
      }: PartialFunction[Any, SendTo])
  }
}
