package ch.os.tproxy.common.message

import java.net.{InetSocketAddress => Addr}
import java.util.UUID
import implicits._

trait FieldMap[+A <: FieldMap[A]] {
  this: A =>
  def has(keys: String*) = keys forall (fields contains _)

  def set(kvs: (String, Any)*) = update(fields ++ kvs)

  def rem(keys: String*) = update(fields -- keys)

  def getOption[B](key: String) = (fields get key).asInstanceOf[Option[B]]

  def get[B](key: String) = getOption[B](key).get

  private[message] def fields: Map[String, Any]

  private[message] def update(fields: Map[String, Any]): A
}

sealed abstract class Message extends FieldMap[Message] {
  def id: UUID

  def cid: String

  def time: Long

  override def equals(o: Any) = o match {
    case that: Message =>
      (that canEqual this) &&
        that.id == this.id &&
        that.cid == this.cid &&
        that.time == this.time &&
        that.fields == this.fields
    case _ => false
  }

  override def hashCode =
    41 * (
      41 * (
        41 * (
          41 * (
            41 + id.hashCode
            ) + cid.hashCode
          ) + time.hashCode
        ) + fields.hashCode
      )

  def canEqual(that: Any) = that.isInstanceOf[Message]
}

object Request {
  def apply(msg: Message) = new Request(cid = msg.cid, fields = msg.fields)

  def apply(cid: String, cltAddr: Addr) = new Request(cid) setCltAddr (cltAddr)
}

class Request(
               val cid: String,
               val id: UUID = UUID.randomUUID,
               val time: Long = System.currentTimeMillis,
               private[message] val fields: Map[String, Any] = Map.empty
             )
  extends Message with FieldMap[Request] {

  private[message] def update(fields: Map[String, Any]) =
    new Request(cid, id, time, fields)

  override def canEqual(that: Any) = that.isInstanceOf[Request]
}

object Response {
  def apply(msg: Message) = new Response(cid = msg.cid, fields = msg.fields)

  def apply(cid: String, srvAddr: Addr) = new Response(cid) setSrvAddr (srvAddr)
}

class Response(
                val cid: String,
                val id: UUID = UUID.randomUUID,
                val time: Long = System.currentTimeMillis,
                private[message] val fields: Map[String, Any] = Map.empty
              )
  extends Message with FieldMap[Response] {

  private[message] def update(fields: Map[String, Any]) =
    new Response(cid, id, time, fields)

  override def canEqual(that: Any) = that.isInstanceOf[Response]
}

case class ClientAccepted(id: String, src: Addr, dst: Addr)

case class ClientConnected(id: String)

case class ClientRefused(id: String)

case class ClientClosed(id: String)
