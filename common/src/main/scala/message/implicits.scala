package ch.os.tproxy.common.message

import java.net.{InetSocketAddress => Addr}
import java.text.SimpleDateFormat
import java.util.Date

import scala.reflect.ClassTag
import akka.actor.ActorRef
import akka.util.ByteString
import akka.io.Tcp


import java.text.SimpleDateFormat


object implicits {
  private val cltAddrKey = "src"
  private val srvAddrKey = "dst"

  implicit class Addresses[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasCltAddr = msg has cltAddrKey

    def hasSrvAddr = msg has srvAddrKey

    def hasSrcAddr = msg has srcAddrKey

    def hasDstAddr = msg has dstAddrKey

    def hasCltSrvAddr = hasCltAddr && hasSrvAddr

    def hasSrcDstAddr = hasSrcAddr && hasDstAddr

    def setCltAddr(addr: Addr) = msg set (cltAddrKey -> addr)

    def setSrvAddr(addr: Addr) = msg set (srvAddrKey -> addr)

    def setSrcAddr(addr: Addr) = msg set (srcAddrKey -> addr)

    def setDstAddr(addr: Addr) = msg set (dstAddrKey -> addr)

    def setCltSrvAddr(cltAddr: Addr, srvAddr: Addr) =
      setCltAddr(cltAddr) setSrvAddr (srvAddr)

    def setSrcDstAddr(srcAddr: Addr, dstAddr: Addr) =
      setSrcAddr(srcAddr) setDstAddr (dstAddr)

    def cltAddr = msg get[Addr] cltAddrKey

    def srvAddr = msg get[Addr] srvAddrKey

    def srcAddr = msg get[Addr] srcAddrKey

    def dstAddr = msg get[Addr] dstAddrKey

    private def srcAddrKey = msg match {
      case _: Request => cltAddrKey
      case _: Response => srvAddrKey
    }

    private def dstAddrKey = msg match {
      case _: Request => srvAddrKey
      case _: Response => cltAddrKey
    }
  }

  implicit class MsgType[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def isRequest = matchType[Request]

    def isResponse = matchType[Response]

    private def matchType[T: ClassTag] = msg match {
      case _: T => true
      case _ => false
    }
  }

  private val closeKey = "closeEvt"

  implicit class Close[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasClose = msg has closeKey

    def setClose(value: Tcp.ConnectionClosed) = msg set (closeKey -> value)

    def remClose = msg rem closeKey

    def close = msg.get[Tcp.ConnectionClosed](closeKey)
  }

  private val dataKey = "data"

  implicit class Data[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasData = msg has dataKey

    def setData(value: ByteString) = msg set (dataKey -> value)

    def remData = msg rem dataKey

    def data = msg.get[ByteString](dataKey)
  }

  private val infoKey = "info"

  implicit class Info[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasInfo = msg has infoKey

    def setInfo(info: String) = msg set (infoKey -> info)

    def info = msg get[String] infoKey
  }

  private val protoKey = "key"

  implicit class Proto[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasProto = msg has protoKey

    def setProto(proto: String) = msg set (protoKey -> proto)

    def proto = msg get[String] protoKey
  }

  private val chunkTestKey = "chunkTests"

  implicit class ChunkTest[A <: FieldMap[A]](val msg: A) {

    private case class Tests(isChunk: Boolean,
                             isLastChunk: Boolean)

    def isChunk = value map (_.isChunk) getOrElse false

    def isLastChunk = value map (_.isLastChunk) getOrElse false

    def setIsChunk(x: Boolean) = msg set (chunkTestKey -> Tests(x, isLastChunk))

    def setIsLastChunk(x: Boolean) = msg set (chunkTestKey -> Tests(isChunk, x))

    private def value = msg getOption[Tests] chunkTestKey
  }

  private val proxyKey = "proxy"

  implicit class Proxy[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasProxy = msg has proxyKey

    def setProxy(value: ActorRef) = msg set (proxyKey -> value)

    def remProxy = msg rem proxyKey

    def proxy = msg.get[ActorRef](proxyKey)
  }

  private val interceptedKey = "intercepted"

  implicit class Intercepted[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasIntercepted = msg has interceptedKey

    def setIntercepted = msg set (interceptedKey -> true)

    def remIntercepted = msg rem interceptedKey
  }

  private val scriptInterceptedKey = "scriptIntercepted"

  implicit class ScriptIntercepted[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasScriptIntercepted = msg has scriptInterceptedKey

    def setScriptIntercepted = msg set (scriptInterceptedKey -> true)

    def remScriptIntercepted = msg rem scriptInterceptedKey
  }
}
