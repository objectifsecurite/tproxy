package ch.os.tproxy.common.json

import JsonDissectTreeNode.ValueType

import upickle.default.{macroRW, write, ReadWriter => RW}

object JsonDissectTreeNode {
  implicit val rw: RW[JsonDissectTreeNode] = macroRW
  implicit val valueTypeRW: RW[ValueType] = macroRW

  sealed trait ValueType

  case object BYTES extends ValueType

  case object TEXT extends ValueType

  case object NULL extends ValueType

  case object OTHER extends ValueType

}

case class JsonDissectTreeNode(length: Int, position: Int, abbrev: String, name: String, var value: Option[String], valueType: ValueType,
                               pendingNextNode: Boolean = false, var next: JsonDissectTreeNode = null, var firstChild: JsonDissectTreeNode = null) {
  def toJson: String = {
    write[JsonDissectTreeNode](this)
  }

  def getValue: String = {
    value.orNull
  }
}