package ch.os.tproxy.common.json

import ch.os.tproxy.common.json.JsonDissectTree.{DissectionResultType, TREE}
import upickle.default.{macroRW, write, ReadWriter => RW}

object JsonDissectTree {
  implicit val rw: RW[JsonDissectTree] = macroRW
  implicit val resTypeRW: RW[DissectionResultType] = macroRW

  sealed trait DissectionResultType

  case object TREE extends DissectionResultType

  case object ACK extends DissectionResultType

  case object ERROR extends DissectionResultType

  object JsonDissectionError extends JsonDissectTree("", "", -1, null, -1, -1, ERROR)

  object JsonDissectionAck extends JsonDissectTree("", "", -1, null, -1, -1, ACK)
}

case class JsonDissectTree(protocol: String, info: String, frameNumber: Int, root: JsonDissectTreeNode, offset: Int,
                           splitDepth: Int, resType: DissectionResultType = TREE) {

  import JsonDissectTreeNode.rw

  def toJson: String = {
    write[JsonDissectTree](this)
  }
}

