package ch.os.tproxy.common.json

import upickle.default.{macroRW, read, write, ReadWriter => RW}

object DissectionRequest {
  implicit val rw: RW[DissectionRequest] = macroRW

  def apply(data: String): DissectionRequest = read[DissectionRequest](data)
}

case class DissectionRequest(pktData: Array[Byte], tcpData: Array[Byte], proto: String) {
  def toJson: String = {
    write[DissectionRequest](this)
  }
}
