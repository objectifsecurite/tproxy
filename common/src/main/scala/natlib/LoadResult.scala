package ch.os.tproxy.common.natlib

import java.io.File

sealed abstract class LoadResult(val success: Boolean)

case class PropNotDefined(prop: String) extends LoadResult(false)

case class NotFound(path: File) extends LoadResult(false)

case class NotAFile(path: File) extends LoadResult(false)

case class LoadFailed(path: File, exception: Throwable) extends LoadResult(false)

case class Loaded(path: File) extends LoadResult(true)

case class AlreadyLoaded(path: File) extends LoadResult(true)
