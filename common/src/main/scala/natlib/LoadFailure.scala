package ch.os.tproxy.common.natlib

object LoadFailure {
  def apply(libName: String, results: LoadResults) = {
    val lines = Seq(
      s"error: native library $libName could not be loaded ",
      "because the following errors occurred\n"
    ) ++ (results map (r => s"  ${r.message}\n"))
    val message = lines.mkString
    new LoadFailure(libName, results, message)
  }
}

class LoadFailure(val libName: String, val results: LoadResults, message: String)
  extends IllegalArgumentException(message)
