package ch.os.tproxy.common.natlib

object `package` {
  type LoadResults = Vector[LoadResult]

  implicit class Message(val result: LoadResult) extends AnyVal {
    def message = result match {
      case PropNotDefined(prop) => s"property $prop not defined"
      case NotFound(p) => s"file ${p.getName} not found in ${p.getParent}"
      case NotAFile(p) => s"file ${p.getName} in ${p.getParent} is not a file"
      case LoadFailed(p, e) =>
        s"file ${p.getName} in ${p.getParent} cannot be loaded (${e.getMessage})"
      case Loaded(p) => s"file ${p.getName} successfully loaded from ${p.getParent}"
      case AlreadyLoaded(p) => s"file ${p.getName} already loaded from ${p.getParent}"
    }
  }

}
