package ch.os.tproxy.common.natlib

import java.io.File
import scala.annotation.tailrec
import scala.util.{Try, Success, Failure}
import scala.collection.mutable.Map

object NatlibLoader {
  lazy val natlibLoader: NatlibLoader = new NatlibLoader

  def load(libName: String, fatal: Boolean = true) = {
    val results = natlibLoader loadFromProp libName
    if (fatal && !results.last.success) throw LoadFailure(libName, results)
    !results.last.success
  }
}

class NatlibLoader {
  private val loadedLibs = Map.empty[String, File]

  def pathsOfProp(prop: String) = {
    val pathStr = Option(System getProperty prop)
    val paths = pathStr map (_ split File.pathSeparator) getOrElse Array.empty
    paths map (new File(_).getAbsoluteFile)
  }

  def loadFromProps(libName: String, props: TraversableOnce[String]) = {
    val iter = props.toIterator

    @tailrec
    def loop(results: LoadResults): LoadResults = iter.hasNext match {
      case true =>
        val prop = iter.next

        Option(System getProperty prop) match {
          case None => loop(results :+ PropNotDefined(prop))
          case Some(str) =>
            val paths = (str split File.pathSeparator) map (new File(_))
            loop(results ++ loadFromPaths(libName, paths))
        }

      case false => results
    }

    loop(Vector.empty)
  }

  def loadFromProp(libName: String, prop: String = "java.library.path") =
    loadFromProps(libName, Seq(prop))

  def loadFromPaths(libName: String, paths: TraversableOnce[File]) = {
    val iter = paths.toIterator

    @tailrec
    def loop(results: LoadResults): LoadResults = iter.hasNext match {
      case true =>
        val result = loadFromPath(libName, iter.next)
        if (result.success) results :+ result
        else loop(results :+ result)

      case false => results
    }

    loop(Vector.empty)
  }

  def loadFromPath(libName: String, path: File) = loadedLibs synchronized {
    val libFileName = System mapLibraryName libName
    val libFile = new File(path, libFileName).getAbsoluteFile

    (loadedLibs get libName) match {
      case Some(libFile) => AlreadyLoaded(libFile)
      case None =>
        if (!libFile.exists) NotFound(libFile)
        else if (!libFile.isFile) NotAFile(libFile)
        else
          Try(System load libFile.getPath) match {
            case Failure(e) => LoadFailed(libFile, e)
            case Success(_) =>
              loadedLibs += libName -> libFile
              Loaded(libFile)
          }
    }
  }
}
