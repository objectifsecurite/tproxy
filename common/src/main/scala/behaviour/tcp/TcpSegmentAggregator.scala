package ch.os.tproxy.common.behaviour.tcp

import akka.actor.{ActorContext, ActorRef}
import akka.io.Tcp
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.implicits._

import java.nio.ByteOrder
import scala.annotation.tailrec


object TcpSegmentAggregator {
  /*
   * Packet :
   *  | Data Length (4 bytes) | Data |
   */

  implicit val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
  val headerLength = 4 // 1 integer => 4 bytes


  @tailrec
  def aggregate(newData: ByteString, bufferedData: Map[ActorRef, ByteString], sender: ActorRef,
                aggregatedMessages: Vector[ByteString] = Vector.empty): (Seq[ByteString], Map[ActorRef, ByteString]) = {
    val data = bufferedData.getOrElse(sender, ByteString()) ++ newData
    if (data.length > headerLength) {
      val dataIt = data.iterator
      val totalLength = dataIt.getInt
      val msgData = dataIt.getByteString(totalLength.min(dataIt.len))
      if (msgData.length < totalLength) {
        (aggregatedMessages, bufferedData + (sender -> data))
      } else
        aggregate(dataIt.toByteString, bufferedData - sender, sender, aggregatedMessages :+ msgData)
    }
    else {
      val newBuff = if (data.nonEmpty) bufferedData + (sender -> data) else bufferedData
      (aggregatedMessages, newBuff)
    }
  }

  def buildSegmentWithHeader(data: ByteString): ByteString =
    ByteString.newBuilder.putInt(data.length).append(data).result()
}

case class TcpSegmentAggregator(bufferedData: Map[ActorRef, ByteString] = Map.empty)(implicit context: ActorContext) extends Behaviour {

  import TcpSegmentAggregator._

  def onEvent: Behaviour.Receive = {
    case Tcp.Received(data) =>
      val (aggregatedMessages, newBuff) = aggregate(data, bufferedData, context.sender)
      copy(bufferedData = newBuff) withEvents aggregatedMessages
  }

  def onCommand: Behaviour.Receive = {
    case data: ByteString =>
      val segment = buildSegmentWithHeader(data)
      this withCommand Tcp.Write(segment)
  }
}
