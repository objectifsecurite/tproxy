package ch.os.tproxy.common.behaviour.tcp

import akka.io.Tcp
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.OnCommandOrEvent

object TcpBackpressure {

  trait HighWatermarkReached extends Tcp.Event

  case object HighWatermarkReached extends HighWatermarkReached

  trait LowWatermarkReached extends Tcp.Event

  case object LowWatermarkReached extends LowWatermarkReached

  private case class Ack(num: Int, ack: Tcp.Event) extends Tcp.Event

}

case class TcpBackpressure(lowBytes: Long, highBytes: Long, maxBytes: Long)
  extends OnCommandOrEvent {

  import TcpBackpressure._
  import Behaviour.Result

  require(lowBytes >= 0, "lowBytes needs to be non-negative")
  require(highBytes >= lowBytes, "highBytes needs to be at least as large as lowWatermark")
  require(maxBytes >= highBytes, "maxBytes needs to be at least as large as highWatermark")

  def onCommandOrEvent = Writing().onCommandOrEvent

  private case class Storage(
                              offset: Int = 0,
                              stored: Long = 0L,
                              private val content: Vector[Tcp.Write] = Vector.empty
                            ) {

    def apply(i: Int) = content(i)

    def add(w: Tcp.Write) = copy(content = content :+ w,
      stored = stored + w.data.size)

    def dropFirst = {
      require(content.nonEmpty, "storage should not be empty")
      val size = content(0).data.size
      copy(offset = offset + 1,
        stored = stored - size,
        content = content drop 1)
    }

    def toList = content.toList

    def isEmpty = content.isEmpty

    def nonEmpty = content.nonEmpty

    def size = content.size
  }

  private trait Buffer[+T <: Buffer[T] with Behaviour] {
    this: T =>
    val storage: Storage
    val suspended: Boolean
    val doWrite: Boolean

    def updateStorage(storage: Storage): T

    def suspend: T

    def currentOffset = storage.offset + storage.size

    def buffer(w: Tcp.Write): Result = {
      val cmd = Tcp.Write(w.data, Ack(currentOffset, w.ack))
      updateStorage(storage add cmd) nextAfterWrite cmd
    }

    def nextAfterWrite(cmd: Tcp.Write): Result = {
      if (storage.stored > maxBytes) Finished withCommand Tcp.Abort

      else if (storage.stored > highBytes && !suspended) {
        val result = suspend withEvent HighWatermarkReached
        if (doWrite) (result withCommand cmd) else result
      }

      else if (doWrite) this withCommand cmd
      else this
    }
  }

  private trait Acknowledge[+T <: Acknowledge[T] with Behaviour] {
    this: T =>
    val storage: Storage
    val suspended: Boolean

    def activate: T

    def updateStorage(storage: Storage): T

    def acknowledge(seq: Int, ack: Tcp.Event) = {
      val (b, evts) = updateForAck(seq, ack)
      b withEvents evts
    }

    def updateForAck(seq: Int, ack: Tcp.Event) = {
      require(seq == storage.offset, s"received ack $seq at ${storage.offset}")
      require(storage.nonEmpty, s"storage was empty at ack $seq")
      updateStorage(storage.dropFirst) nextAfterAck(seq, ack)
    }

    def nextAfterAck(seq: Int, ack: Tcp.Event): (T, List[Tcp.Event]) = {
      if (suspended && storage.stored < lowBytes)
        if (ack == Tcp.NoAck) (activate, LowWatermarkReached :: Nil)
        else (activate, ack :: LowWatermarkReached :: Nil)

      else if (ack == Tcp.NoAck) (this, Nil)
      else (this, ack :: Nil)
    }
  }

  private trait Resume[+T <: Resume[T] with Behaviour] extends Acknowledge[T] {
    this: T =>
    val storage: Storage
    val suspended: Boolean
    val toAck: Int
    val closed: Option[Tcp.CloseCommand]

    def updateStorage(storage: Storage): T

    def updateToAck(toAck: Int): T

    def resume(seq: Int, ack: Tcp.Event) = {
      val (b, evts) = updateForAck(seq, ack)
      b resumeAfterAck evts
    }

    def resumeAfterAck(evts: List[Any]): Result = {
      if (storage.nonEmpty) {
        if (toAck > 0)
          updateToAck(toAck - 1) withEvents evts withCommand storage(0)
        else {
          val b: Behaviour = closed match {
            case Some(cmd) => Closing(storage, suspended, cmd)
            case None => Writing(storage, suspended)
          }
          b withEvents evts withCommands storage.toList
        }
      } else
        Writing(storage, suspended) withEvents evts
    }
  }

  private case class Writing(
                              storage: Storage = Storage(),
                              suspended: Boolean = false
                            ) extends OnCommandOrEvent with Buffer[Writing] with Acknowledge[Writing] {

    val doWrite = true

    def updateStorage(storage: Storage) = copy(storage = storage)

    def suspend = copy(suspended = true)

    def activate = copy(suspended = false)

    def onCommandOrEvent = {
      case w: Tcp.Write => buffer(w)

      case Tcp.CommandFailed(Tcp.Write(_, Ack(offset, _))) =>
        Buffering(storage, suspended, offset) withCommand Tcp.ResumeWriting

      case cmd: Tcp.CloseCommand if storage.isEmpty => Finished withCommand cmd
      case Tcp.Abort => Finished withCommand Tcp.Abort
      case cmd: Tcp.CloseCommand => Closing(storage, suspended, cmd)
      case Ack(seq, ack) => acknowledge(seq, ack)
    }
  }

  private case class Buffering(
                                storage: Storage,
                                suspended: Boolean,
                                nack: Int,
                                toAck: Int = 10,
                                closed: Option[Tcp.CloseCommand] = None
                              ) extends OnCommandOrEvent with Buffer[Buffering] with Acknowledge[Buffering]
    with Resume[Buffering] {

    val doWrite = false

    def updateStorage(storage: Storage) = copy(storage = storage)

    def suspend = copy(suspended = true)

    def activate = copy(suspended = false)

    def updateToAck(toAck: Int) = copy(toAck = toAck)

    def updateClosed(closed: Tcp.CloseCommand) = copy(closed = Some(closed))

    def onCommandOrEvent = {
      case w: Tcp.Write => buffer(w)
      case Tcp.WritingResumed => this withCommand storage(0)
      case Tcp.Abort => Finished withCommand Tcp.Abort
      case cmd: Tcp.CloseCommand => this updateClosed (cmd)
      case Ack(seq, ack) if seq < nack => acknowledge(seq, ack)
      case Ack(seq, ack) => resume(seq, ack)
      case Tcp.CommandFailed(_: Tcp.Write) => this
    }
  }

  private case class Closing(
                              storage: Storage,
                              suspended: Boolean,
                              closeCmd: Tcp.CloseCommand
                            ) extends OnCommandOrEvent with Acknowledge[Closing] {

    def updateStorage(storage: Storage) = copy(storage = storage)

    def activate = copy(suspended = false)

    def onCommandOrEvent = {
      case Ack(seq, ack) => acknowledge(seq, ack)

      case Tcp.CommandFailed(_: Tcp.Write) =>
        ClosingAfterFailed(storage) withCommand Tcp.ResumeWriting
    }

    override def acknowledge(seq: Int, ack: Tcp.Event) = {
      val (b, evts) = updateForAck(seq, ack)
      if (b.storage.isEmpty) Finished withEvents evts withCommand closeCmd
      else b withEvents evts
    }
  }

  private case class ClosingAfterFailed(storage: Storage) extends OnCommandOrEvent {
    def onCommandOrEvent = {
      case Tcp.WritingResumed => this withCommands storage.toList
      case Tcp.CommandFailed(_: Tcp.Write) => this
    }
  }

  private case object Finished extends OnCommandOrEvent {
    def onCommandOrEvent = {
      case _: Tcp.Write => this
      case Tcp.CommandFailed(_: Tcp.Write) => this
    }
  }

}
