package ch.os.tproxy.common.behaviour

import scala.annotation.tailrec
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import ch.os.tproxy.common.behaviour.implicits._

object Behaviour {
  type Receive = PartialFunction[Any, Result]
  val emptyReceive = PartialFunction.empty[Any, Result]

  def onEvent(receive: Receive) = new Behaviour {
    def onCommand = emptyReceive

    def onEvent = receive
  }

  def onCommand(receive: Receive) = new Behaviour {
    def onCommand = receive

    def onEvent = emptyReceive
  }

  private val Vil = Vector()

  case class Result(behaviours: Vector[Behaviour] = Vil,
                    commands: Vector[Any] = Vil,
                    events: Vector[Any] = Vil) {

    def withBehaviour(beh: Behaviour) = copy(behaviours = Vector(beh))

    def withCommand(cmd: Any) = copy(commands = Vector(cmd))

    def withEvent(evt: Any) = copy(events = Vector(evt))

    def withBehaviours(behs: Traversable[Behaviour]) = copy(behaviours = behs.toVector)

    def withCommands(cmds: Traversable[Any]) = copy(commands = cmds.toVector)

    def withEvents(evts: Traversable[Any]) = copy(events = evts.toVector)

    def addCommand(cmd: Any) = copy(commands = commands :+ cmd)

    def addEvent(evt: Any) = copy(events = events :+ evt)

    def addCommands(cmds: Traversable[Any]) = copy(commands = commands ++ cmds)

    def addEvents(evts: Traversable[Any]) = copy(events = events ++ evts)

    def >>:(cmds: Traversable[Any]) = {
      val r = propagate(cmds, behaviours, Vil)
      Result(r.behaviours, commands ++ r.commands, events ++ r.events)
    }

    def :<<(evts: Traversable[Any]) = {
      val r = propagate(Vil, behaviours, evts)
      Result(r.behaviours, commands ++ r.commands, events ++ r.events)
    }

    def >>:(cmd: Any): Result = >>:(cmd +: Vil)

    def :<<(evt: Any): Result = :<<(evt +: Vil)

    def <->(r: Result) = propagate(this, r)
  }

  object propagate {

    private abstract class Applies[T, A] {
      def apply(xs: Vector[A]): T

      def apply(xs: Traversable[A]): T = apply(xs.toVector)
    }

    private object Cmds extends Applies[Cmds, Any]

    private object Behs extends Applies[Behs, Behaviour]

    private object Evts extends Applies[Evts, Any]

    private abstract class Factor

    private case class Cmds(cmds: Vector[Any]) extends Factor {
      require(cmds.nonEmpty, "there must be at least one command")
    }

    private case class Behs(behs: Vector[Behaviour]) extends Factor {
      require(behs.nonEmpty, "there must be at least one behaviour")
    }

    private case class Evts(evts: Vector[Any]) extends Factor {
      require(evts.nonEmpty, "there must be at least one event")
    }

    private abstract class Action {
      val before: Vector[Factor]
      val after: Vector[Factor]
    }

    private case class PropCmds(before: Vector[Factor],
                                after: Vector[Factor]) extends Action

    private case class PropEvts(before: Vector[Factor],
                                after: Vector[Factor]) extends Action

    def apply(cmds: Traversable[Any],
              behs: Traversable[Behaviour],
              evts: Traversable[Any]) = {

      if (behs.isEmpty)
        Result(Vil, cmds.toVector, evts.toVector)

      else if (cmds.isEmpty && evts.isEmpty)
        Result(behs.toVector)

      else {
        val action = (cmds, behs, evts) match {
          case (cs, bs, Vil) => PropCmds(Vil, Cmds(cs) +: Behs(bs) +: Vil)
          case (Vil, bs, es) => PropEvts(Vil, Evts(es) +: Behs(bs) +: Vil)
          case (cs, bs, es) => PropCmds(Vil, Cmds(cs) +: Behs(bs) +: Evts(es) +: Vil)
        }
        propagate(action +: Vil)
      }
    }

    def apply(results: Result*) = {
      val actions = (results foldLeft Vector.empty[PropCmds]) {
        (a, r) => {
          val evts = if (r.events.isEmpty) None else Some(Evts(r.events))
          val behs = if (r.behaviours.isEmpty) None else Some(Behs(r.behaviours))
          val cmds = if (r.commands.isEmpty) None else Some(Cmds(r.commands))
          a :+ PropCmds(Vil, (evts +: behs +: cmds +: Vil).flatten)
        }
      }
      propagate(actions.toVector)
    }

    @tailrec
    private def propagate(actions: Vector[Action]): Result = {
      //      println(s"actions: $actions")
      actions match {
        case PropCmds(Vil, Vil) +: Vil => Result(Vil, Vil, Vil)
        case PropEvts(Vil, Vil) +: Vil => Result(Vil, Vil, Vil)

        case PropCmds(Vil, Behs(behs) +: Vil) +: Vil => Result(behs, Vil, Vil)
        case PropEvts(Vil, Behs(behs) +: Vil) +: Vil => Result(behs, Vil, Vil)

        case PropCmds(Vil, Cmds(cmds) +: Vil) +: Vil => Result(Vil, cmds, Vil)
        case PropEvts(Vil, Cmds(cmds) +: Vil) +: Vil => Result(Vil, cmds, Vil)

        case PropCmds(Vil, Evts(evts) +: Vil) +: Vil => Result(Vil, Vil, evts)
        case PropEvts(Vil, Evts(evts) +: Vil) +: Vil => Result(Vil, Vil, evts)

        case PropCmds(Vil, Behs(behs) +: Cmds(cmds) +: Vil) +: Vil => Result(behs, cmds, Vil)
        case PropEvts(Vil, Behs(behs) +: Evts(evts) +: Vil) +: Vil => Result(behs, Vil, evts)

        case PropCmds(Vil, Evts(evts) +: Behs(behs) +: Vil) +: Vil => Result(behs, Vil, evts)
        case PropEvts(Vil, Cmds(cmds) +: Behs(behs) +: Vil) +: Vil => Result(behs, cmds, Vil)

        case PropCmds(Vil, Evts(evts) +: Cmds(cmds) +: Vil) +: Vil => Result(Vil, cmds, evts)
        case PropEvts(Vil, Cmds(cmds) +: Evts(evts) +: Vil) +: Vil => Result(Vil, cmds, evts)

        case PropCmds(Vil, Evts(evts) +: Behs(behs) +: Cmds(cmds) +: Vil) +: Vil =>
          Result(behs, cmds, evts)

        case PropEvts(Vil, Cmds(cmds) +: Behs(behs) +: Evts(evts) +: Vil) +: Vil =>
          Result(behs, cmds, evts)

        case PropCmds(Vil, Vil) +: others => propagate(others)
        case PropEvts(Vil, Vil) +: others => propagate(others)

        case PropCmds(Vil, a1) +: PropCmds(Vil, a2) +: others =>
          propagate(PropCmds(Vil, a1 ++ a2) +: others)

        case PropEvts(Vil, a1) +: PropEvts(Vil, a2) +: others =>
          propagate(PropEvts(Vil, a1 ++ a2) +: others)

        case PropCmds(before, after) +: others =>
          propagate(propagateCmds(before, after) ++ others)

        case PropEvts(before, after) +: others =>
          propagate(propagateEvts(before, after) ++ others)
      }
    }

    @tailrec
    private def propagateCmds(before: Vector[Factor], after: Vector[Factor]): Vector[Action] = {
      after match {
        case Cmds(cmd +: Vil) +: Behs(b +: Vil) +: others => {
          //	  println("C B -> Es Bs Cs")
          val r =
            if (b.onCommand isDefinedAt cmd) {
              Try(b onCommand cmd) match {
                case Success(result) => result
                case f: Failure[_] => b withEvent f
              }
            } else b withCommand cmd

          val evts = if (r.events.isEmpty) Vil else Evts(r.events) +: Vil
          val behs = if (r.behaviours.isEmpty) Vil else Behs(r.behaviours) +: Vil
          val cmds = if (r.commands.isEmpty) Vil else Cmds(r.commands) +: Vil

          PropEvts(Vil, evts ++ before) +: PropCmds(Vil, behs ++ cmds ++ others) +: Vil
        }

        case Cmds(cmd +: Vil) +: Behs(b +: behs) +: others =>
          //	  println("C Bs -> C B Bs")
          propagateCmds(before, Cmds(cmd +: Vil) +: Behs(b +: Vil) +: Behs(behs) +: others)

        case Cmds(cmd +: cmds) +: Behs(behs) +: others =>
          //	  println("Cs Bs -> Cs C Bs")
          propagateCmds(Cmds(cmds) +: before, Cmds(cmd +: Vil) +: Behs(behs) +: others)

        case Cmds(cmds1) +: Cmds(cmds2) +: others =>
          //	  println("Cs Cs -> Cs")
          propagateCmds(before, Cmds(cmds2 ++ cmds1) +: others)

        case Behs(behs1) +: Behs(behs2) +: others =>
          //	  println("Bs Bs -> Bs")
          propagateCmds(Behs(behs1 ++ behs2) +: before, others)

        case (c: Cmds) +: (e: Evts) +: others =>
          //	  println("Cs Es -> Es Cs")
          propagateCmds(e +: before, c +: others)

        case x +: others =>
          //	  println(s"X -> X ($x)")
          propagateCmds(x +: before, others)

        case Vil if before == Vil => Vil
        case Vil => PropEvts(Vil, before) +: Vil
      }
    }

    @tailrec
    private def propagateEvts(before: Vector[Factor], after: Vector[Factor]): Vector[Action] = {
      after match {
        case Evts(evt +: Vil) +: Behs(b +: Vil) +: others => {
          //	  println("E B -> Cs Bs Es")
          val r =
            if (b.onEvent isDefinedAt evt) {
              Try(b onEvent evt) match {
                case Success(result) => result
                case f: Failure[_] => b withCommand f
              }
            } else b withEvent evt

          val cmds = if (r.commands.isEmpty) Vil else Cmds(r.commands) +: Vil
          val behs = if (r.behaviours.isEmpty) Vil else Behs(r.behaviours) +: Vil
          val evts = if (r.events.isEmpty) Vil else Evts(r.events) +: Vil

          PropCmds(Vil, cmds ++ before) +: PropEvts(Vil, behs ++ evts ++ others) +: Vil
        }

        case Evts(evt +: Vil) +: Behs(behs :+ b) +: others =>
          //	  println("E Bs -> E B Bs")
          propagateEvts(before, Evts(evt +: Vil) +: Behs(b +: Vil) +: Behs(behs) +: others)

        case Evts(evt +: evts) +: Behs(behs) +: others =>
          //	  println("Es Bs -> Es E Bs")
          propagateEvts(Evts(evts) +: before, Evts(evt +: Vil) +: Behs(behs) +: others)

        case Evts(evts1) +: Evts(evts2) +: others =>
          //	  println("Es Es -> Es")
          propagateEvts(before, Evts(evts2 ++ evts1) +: others)

        case Behs(behs1) +: Behs(behs2) +: others =>
          //	  println("Bs Bs -> Bs")
          propagateEvts(Behs(behs2 ++ behs1) +: before, others)

        case (e: Evts) +: (c: Cmds) +: others =>
          //	  println("Es Cs -> Cs Es")
          propagateEvts(c +: before, e +: others)

        case x +: others =>
          //	  println(s"Y -> Y ($x)")
          propagateEvts(x +: before, others)

        case Vil if before == Vil => Vil
        case Vil => PropCmds(Vil, before) +: Vil
      }
    }
  }

}

trait Behaviour {
  def onCommand: Behaviour.Receive

  def onEvent: Behaviour.Receive
}

trait OnCommandOrEvent extends Behaviour {
  def onCommand = onCommandOrEvent

  def onEvent = onCommandOrEvent

  def onCommandOrEvent: Behaviour.Receive
}

trait OnCommand extends Behaviour {
  def onEvent = Behaviour.emptyReceive
}

trait OnEvent extends Behaviour {
  def onCommand = Behaviour.emptyReceive
}
