package ch.os.tproxy.common.behaviour.actor.message

import akka.actor.ActorRef

sealed abstract class Message {
  def needAck: Boolean
}

case class Command(msg: Any, needAck: Boolean = true) extends Message

case class Event(msg: Any, needAck: Boolean = true) extends Message

case object Ack extends Message {
  val needAck = false
}

case object Stop

case class SentBy(src: ActorRef, msg: Any)

object SendTo {
  def apply(dest: ActorRef, msg: Any, needWrap: Boolean = true): SendTo =
    SendTo(dest, msg, needWrap, None)

  def apply(dest: ActorRef, msg: Any, name: String): SendTo =
    SendTo(dest, msg, true, Some(name))

  def apply(dest: ActorRef, msg: Any, needWrap: Boolean, name: String): SendTo =
    SendTo(dest, msg, needWrap, Some(name))
}

case class SendTo(dest: ActorRef, msg: Any,
                  needWrap: Boolean,
                  private val _name: Option[String]) {

  lazy val name = _name getOrElse dest.toString

  lazy val msgToSend = msg match {
    case _: Message if needWrap => msg
    case Command(cmd, _) => cmd
    case Event(evt, _) => evt
    case ack if ack == Ack => ack
    case _ => msg
  }
}
