package ch.os.tproxy.common.behaviour.actor

import scala.util.Failure
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.message._

case class LogUnhandled(msgType: (Any, Boolean) => Message)
                       (implicit logging: Logging)
  extends Behaviour {

  lazy val log = logging(this)

  lazy val onCommand = msgType match {
    case Command => handleMsg
    case _ => Behaviour.emptyReceive
  }

  lazy val onEvent = msgType match {
    case Event => handleMsg
    case _ => Behaviour.emptyReceive
  }

  // private type MsgType = (Any, Boolean) => Message

  private val direction = msgType match {
    case Command => "outgoing"
    case Event => "incoming"
  }

  private val handleMsg: Behaviour.Receive = {
    case msg@Command(Failure(e), _) => unhandledFailure(msg, e); this
    case msg@Event(Failure(e), _) => unhandledFailure(msg, e); this
    case msg => unhandledMessage(msg); this
  }

  private def unhandledFailure(msg: Any, e: Throwable) = {
    val strace = akka.event.Logging stackTraceFor e
    log warning s"unhandled $direction message $msg"
    log debug s"unhandled $direction message $msg caused by $strace"
  }

  private def unhandledMessage(msg: Any) =
    log warning s"unhandled $direction message $msg"
}
