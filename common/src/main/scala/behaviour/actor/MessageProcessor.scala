package ch.os.tproxy.common.behaviour.actor

import scala.annotation.tailrec
import ch.os.tproxy.common.behaviour.OnEvent
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.message._

object MessageProcessor {
  def apply(result: Result) = {
    def cmds = result.commands map wrapMsg(Command)

    def evts = result.events map wrapMsg(Event)

    new MessageProcessor(result.behaviours) withCommands mix(cmds, evts)
  }

  private def wrapMsg(wrap: (Any, Boolean) => Message)(msg: Any) = {
    msg match {
      case send: SendTo => send.msg match {
        case _: Message => send
        case _ => send copy (msg = wrap(send.msg, true))
      }
      case Stop => wrap(Stop, false)
      case _ => wrap(msg, true)
    }
  }

  private def mix(cmds: Vector[Any], evts: Vector[Any]) = {
    type State = (Vector[Any], Vector[Any], Vector[Any], Boolean)

    @tailrec
    def loop(state: State): Vector[Any] = state match {
      case (Vector(), Vector(), out, _) => out
      case (cmds, Vector(), out, _) => out ++ cmds
      case (Vector(), evts, out, _) => out ++ evts
      case (cmd +: others, evts, out, true) => loop((others, evts, out :+ cmd, false))
      case (cmds, evt +: others, out, false) => loop((cmds, others, out :+ evt, true))
    }

    loop((cmds, evts, Vector.empty, true))
  }
}

class MessageProcessor(behaviours: Behaviours) extends OnEvent {

  def onEvent = {
    case SentBy(src, msg: Message) => {
      val result: Result = msg match {
        case Command(c, _) => c >>: behaviours
        case Event(e, _) => behaviours :<< e
        case Ack => behaviours
      }

      msg.needAck match {
        case false => MessageProcessor(result)
        case true => {
          val cmds = SendTo(src, Ack) +: result.commands
          MessageProcessor(result withCommands cmds)
        }
      }
    }
  }
}
