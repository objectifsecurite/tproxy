package ch.os.tproxy.common.behaviour.actor

import akka.actor.ActorContext
import ch.os.tproxy.common.log.{Logging, LogReceive}
import ch.os.tproxy.common.behaviour.{Behaviour, OnCommand, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.message._

object `package` {
  type Behaviours = Vector[Behaviour]
}

case class Receiver(context: ActorContext) extends OnEvent {
  def onEvent = {
    case evt: Message => this withEvent SentBy(context.sender, evt)
    case Stop => this withEvent SentBy(context.sender, Command(Stop, false))
    case evt => this withEvent SentBy(context.sender, Event(evt, false))
  }
}

case class Sender(context: ActorContext)
                 (implicit logging: Logging, logReceive: LogReceive) extends OnCommand {

  lazy val log = logging(this)
  implicit val self = context.self

  def onCommand = {
    case send: SendTo => {
      send.dest ! send.msgToSend
      log debug s"${logReceive shortenMsg send.msgToSend} sent to ${send.name}"
      this
    }
    case Command(Stop, _) => {
      log debug "stopping actor"
      context stop self
      this
    }
  }
}
