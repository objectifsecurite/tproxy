package ch.os.tproxy.common.behaviour.actor

import akka.actor.{Props, Actor}
import ch.os.tproxy.common.log.{Logging, LogReceive}
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.message._

object ActorNode {
  def apply(result: Result)(implicit _logging: Logging, _logReceive: LogReceive) =
    Props(new ActorNode {
      implicit lazy val logging = _logging
      implicit val logReceive = _logReceive
      implicit val onStart = result
    })
}

abstract class ActorNode extends Actor {

  def onStart: Result

  def onRestart = onStart

  implicit val system = context.system

  implicit def logging: Logging

  implicit val logReceive: LogReceive

  implicit lazy val log = logging(this)

  def initialize(initialResult: Result): Result = {
    LogUnhandled(Event) <->
      MessageProcessor(initialResult) <->
      Sender(context) <->
      LogUnhandled(Command) <->
      Receiver(context)
  }

  override def preStart() = {
    val result = initialize(onStart)
    context become processing(result.behaviours)
  }

  override def postRestart(reason: Throwable) = {
    val result = initialize(onRestart)
    context become processing(result.behaviours)
  }

  // if we do not want to stop the children
  // override def preRestart(reason: Throwable, message: Option[Any]) = {
  //   postStop()
  // }

  override def postStop() = log debug s"stopped $self"

  def receive = {
    val result = initialize(None)
    processing(result.behaviours)
  }

  protected def processing(behaviours: Behaviours): Receive = {
    case msg =>
      val shortMsg = logReceive shortenMsg msg
      log debug s"received message $shortMsg from ${context.sender}"
      val result = behaviours :<< msg
      context become processing(result.behaviours)
  }
}
