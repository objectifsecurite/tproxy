package ch.os.tproxy.common.behaviour.actor

import akka.actor.ActorRef
import ch.os.tproxy.common.behaviour.{OnCommand, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.message._

trait SendToCommon[T] {
  def apply(f: Any => SendTo): T

  def apply(dest: ActorRef, needWrap: Boolean = true): T =
    apply(SendTo(dest, _, needWrap))

  def apply(dest: ActorRef, name: String): T =
    apply(SendTo(dest, _, name))

  def apply(dest: ActorRef, needWrap: Boolean, name: String): T =
    apply(SendTo(dest, _, needWrap, name))
}

object SendCmdTo extends SendToCommon[SendCmdTo] {
  def apply(f: Any => SendTo) = new SendCmdTo({ case msg => f(msg) })
}

object SendEvtTo extends SendToCommon[SendEvtTo] {
  def apply(f: Any => SendTo) = new SendEvtTo({ case msg => f(msg) })
}

case class SendCmdTo(f: PartialFunction[Any, SendTo]) extends OnCommand {
  def onCommand = {
    case msg: SendTo => this withCommand msg
    case msg if f isDefinedAt msg => this withCommand f(msg)
  }
}

case class SendEvtTo(f: PartialFunction[Any, SendTo]) extends OnEvent {
  def onEvent = {
    case msg: SendTo => this withEvent msg
    case msg if f isDefinedAt msg => this withEvent f(msg)
  }
}
