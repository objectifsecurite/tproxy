package ch.os.tproxy.common.behaviour

import scala.language.implicitConversions
import ch.os.tproxy.common.behaviour.Behaviour.Result

object implicits {
  implicit def resultFromBehaviour(b: Behaviour) = Result(Vector(b))

  implicit def resultFromIterable(bs: Iterable[Behaviour]) = Result(bs.toVector)

  implicit def resultFromOption(opt: Option[Behaviour]) = Result(opt.toVector)
}
