package ch.os.tproxy.common

import java.io.File

object Properties {
  def prependPaths(dstProp: String, srcProp: String, unique: Boolean = true) = {
    def emptyIfNull(prop: String) = {
      val x = System getProperty prop
      if (x == null) "" else x
    }

    val dst = emptyIfNull(dstProp)
    val src = emptyIfNull(srcProp)
    val pathSep = File.pathSeparator

    val paths = unique match {
      case false => Array(src, dst) filter (!_.isEmpty)
      case true =>
        val dstSeq = dst.split(pathSep)
        val srcSeq = src.split(pathSep)
        val dstSet = Set(dstSeq: _*)
        srcSeq.filter(!dstSet.contains(_)) ++ dstSeq
    }

    val nlp = paths.mkString(pathSep)
    System.setProperty(dstProp, nlp)

    nlp
  }
}
