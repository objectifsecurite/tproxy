package ch.os.tproxy.common.log

import akka.actor.{ActorSystem, ExtendedActorSystem, ActorRef, Actor}
import akka.event.{LogSource, LoggingAdapter, BusLogging}

object `package` {
  type MDC = Map[String, Any]
}

trait Logging {
  def apply[T <: AnyRef](source: T): Logger

  def forObject[T <: AnyRef](obj: T): Logging

  def addMDC(kvs: (String, Any)*): Logging
}

trait Logger {
  def error(msg: String): Unit

  def warning(msg: String): Unit

  def info(msg: String): Unit

  def debug(msg: String): Unit
}

class AkkaLogging(system: ActorSystem,
                  actorRef: Option[ActorRef] = None,
                  mdc: MDC = Map.empty)
  extends Logging {

  def this(actor: Actor) =
    this(actor.context.system, Some(actor.context.self))

  def this(system: ActorSystem, ref: ActorRef) = this(system, Some(ref))

  def apply[T <: AnyRef](obj: T) = {
    val str = actorRef match {
      case Some(ref) => LogSource.fromActorRef genString(ref, system)
      case None => system.toString
    }

    val clazz = obj.getClass
    val filter = system.asInstanceOf[ExtendedActorSystem].logFilter

    val adapter = new BusLogging(system.eventStream, str, clazz, filter) {
      override val mdc = AkkaLogging.this.mdc
    }

    new AkkaLogger(adapter)
  }

  def forObject[T <: AnyRef](obj: T) = obj match {
    case actor: Actor =>
      new AkkaLogging(actor.context.system, Some(actor.context.self), mdc)
    case _ => this
  }

  def addMDC(kvs: (String, Any)*) =
    new AkkaLogging(system, actorRef, mdc ++ kvs)
}

class AkkaLogger(log: LoggingAdapter) extends Logger {
  def error(msg: String) = log error msg

  def warning(msg: String) = log warning msg

  def info(msg: String) = log info msg

  def debug(msg: String) = log debug msg
}
