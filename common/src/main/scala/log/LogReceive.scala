package ch.os.tproxy.common.log

import akka.io.Tcp
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}
import ch.os.tproxy.common.message.{Request, Response, Message}
import ch.os.tproxy.common.message.implicits._

class LogReceive {

  def shortenMsg(msg: Any): Any = msg match {
    case Command(cmd, nw) => s"Command(${shortenMsg(cmd)}, $nw)"
    case Event(evt, nw) => s"Event(${shortenMsg(evt)}, $nw)"
    case m: Message => {
      val pre = m match {
        case _: Request => "Request"
        case _: Response => "Response"
        case _ => "Message"
      }
      val post = (if (m.hasCltAddr) s"(${m.cltAddr} " else "(") +
        (if (m.hasSrvAddr) s"${m.srvAddr})" else ")") +
        (if (m.hasData) s" with Data(${m.data.length})" else "") +
        (if (m.hasClose) s" with Close(${m.close})" else "")
      s"$pre(${m.cid})$post"
    }
    case Tcp.Received(data) => s"Tcp.Received(${data.length})"
    case Tcp.Write(data, ack) => s"Tcp.Write(${data.length}, $ack)"
    case Tcp.CommandFailed(Tcp.Write(data, ack)) => s"Tcp.CommandFailed(Tcp.Write(${data.length}, $ack))"
    case _ => msg
  }
}
