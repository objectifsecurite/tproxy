#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

if [ $(id -u) == 0 ]; then
    ./iptables.sh port dst 54819 enable
fi

ARG=${1:-""}
TESTSUITE="-l GuiTest"

if [ ! -z "$ARG" ]; then
    TESTSUITE="-s $ARG"
fi

declare -r app_home="$(dirname "$(readlink -f "$0")")"
declare -r app_natlibpath="[NATLIB_PATH]"
declare -r app_mainjar="[MAIN_JAR_PATH]"
declare -r app_core_test_classes="[CORE_TEST_PATH]" 
declare -r app_gui_test_classes="[GUI_TEST_PATH]" 
declare -r java_opts="-Dapp.home=$app_home -Dnative.library.path=$app_natlibpath"
declare -r app_extlibpath=""

if [ ! -z "$app_extlibpath" ]; then
    if [ -z "$LD_LIBRARY_PATH" ]; then
	export LD_LIBRARY_PATH="$app_extlibpath"
    else
	export LD_LIBRARY_PATH="$app_extlibpath:$LD_LIBRARY_PATH"
    fi
fi

xprop_kfs=$(xprop -root KDE_FULL_SESSION | grep = || echo "")
xprop_ksv=$(xprop -root KDE_SESSION_VERSION | grep = || echo "")
xprop_ns=$(xprop -root _NET_SUPPORTED | grep = || echo "")

if [ ! -z "$xprop_kfs" ]; then
    export KDE_FULL_SESSION=$(echo $xprop_kfs | cut -d \" -f 2)
    export KDE_SESSION_VERSION=$(echo $xprop_ksv | cut -d ' ' -f 3)
elif [ ! -z "$(echo $xprop_ns | grep -i gtk)" ]; then
    export DESKTOP_SESSION=gnome
fi

java -Dlogback.configurationFile=$app_home/config/tproxy/logback-test.xml -ea $java_opts -jar $app_mainjar -o -R $app_core_test_classes [GUI_TESTS] $TESTSUITE

if [ $(id -u) == 0 ]; then
    ./iptables.sh port dst 54819 disable
fi
