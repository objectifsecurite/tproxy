#!/usr/bin/env bash
trap : SIGTERM SIGINT
# set -o nounset
# set -o errexit
# set -o pipefail

declare -r app_home="$(dirname "$(readlink -f "$0")")"
declare -r app_natlibpath="[NATLIB_PATH]"
declare -r app_mainjar="[MAIN_JAR_PATH]"
declare -r java_opts="-Dapp.home=$app_home -Dnative.library.path=$app_natlibpath"
declare -r app_extlibpath=""

if [ ! -z "$app_extlibpath" ]; then
    if [ -z "$LD_LIBRARY_PATH" ]; then
	export LD_LIBRARY_PATH="$app_extlibpath"
    else
	export LD_LIBRARY_PATH="$app_extlibpath:$LD_LIBRARY_PATH"
    fi
fi

java -Dlogback.configurationFile=$app_home/config/wiresharkDissection/logback.xml -ea -Xmx200m $java_opts -jar $app_mainjar $@ &

# kill subprocess
PID=$!
wait $PID

if [[ $? -gt 128 ]]
then
    kill $PID
fi
