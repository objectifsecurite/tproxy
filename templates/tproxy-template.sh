#!/usr/bin/env bash

# set -o nounset
# set -o errexit
# set -o pipefail

declare -r app_home="$(dirname "$(readlink -f "$0")")"
declare -r app_natlibpath="[NATLIB_PATH]"
declare -r app_mainjar="[MAIN_JAR_PATH]"
declare -r java_opts="-Dapp.home=$app_home -Dnative.library.path=$app_natlibpath"
declare -r app_extlibpath=""

if [ ! -z "$app_extlibpath" ]; then
    if [ -z "$LD_LIBRARY_PATH" ]; then
	export LD_LIBRARY_PATH="$app_extlibpath"
    else
	export LD_LIBRARY_PATH="$app_extlibpath:$LD_LIBRARY_PATH"
    fi
fi

# Qt4 looks at the following variables to determine the desktop
# environment in use. The file in the source code of Qt4 where the
# checks are performed is src/gui/kernel/qapplication_x11.cpp.

# export KDE_FULL_SESSION=true
# export KDE_SESSION_VERSION=5
# export GNOME_DESKTOP_SESSION_ID=this-is-deprecated
# export DESKTOP_SESSION=gnome

# xprop -root KDE_FULL_SESSION returns KDE_FULL_SESSION(STRING) = "true"
# xprop -root KDE_SESSION_VERSION returns KDE_SESSION_VERSION(CARDINAL) = 5

xprop_kfs=$(xprop -root KDE_FULL_SESSION | grep =)
xprop_ksv=$(xprop -root KDE_SESSION_VERSION | grep =)
xprop_ns=$(xprop -root _NET_SUPPORTED | grep =)

if [ ! -z "$xprop_kfs" ]; then
    export KDE_FULL_SESSION=$(echo $xprop_kfs | cut -d \" -f 2)
    export KDE_SESSION_VERSION=$(echo $xprop_ksv | cut -d ' ' -f 3)
elif [ ! -z "$(echo $xprop_ns | grep -i gtk)" ]; then
    export DESKTOP_SESSION=gnome
fi

java -Dlogback.configurationFile=$app_home/config/tproxy/logback.xml -ea -Xmx200m $java_opts -jar $app_mainjar $@
