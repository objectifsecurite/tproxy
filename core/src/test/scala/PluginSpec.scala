package ch.os.tproxy.test

import java.net.InetSocketAddress
import scala.util.Random
import akka.util.ByteString
import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import ch.os.tproxy.common.message.{Message, Request}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.core.behaviour.Plugin._
import ch.os.tproxy.core.dissection.Dissector
import ch.os.tproxy.common.behaviour.actor.{ActorNode, SendEvtTo}
import ch.os.tproxy.common.behaviour.actor.message.Event
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.behaviour.PcapBehaviour.{Dissect, Sim}
import ch.os.tproxy.core.log.TProxyLogReceive

object PluginSpec {

  class DissectHandler(probe: TestProbe) extends ActorNode {
    implicit lazy val logging = new AkkaLogging(this)
    implicit val logReceive = new TProxyLogReceive()

    def onStart = {
      SendEvtTo(probe.ref, false) <->
        Dissect(Dissector.getActor) <->
        Sim.Waiting()
    }
  }

}

class PluginSpec extends TestSpec("PluginSpec") {

  trait Send1EventExpect1EventTest {
    val test: Any
    val plugin: Result
    val expect: Any
    val result = plugin :<< test
    result.commands.length shouldBe 0
    result.events.length shouldBe 1
    result.events(0) shouldBe expect
  }

  object FilterEvtSpec {

    case object Add1 extends Behaviour {
      def onCommand = Behaviour.emptyReceive

      def onEvent = {
        case i: Int => this withEvent i + 1
      }
    }

    case object Remove1 extends Behaviour {
      def onCommand = Behaviour.emptyReceive

      def onEvent = {
        case i: Int => this withEvent i - 1
      }
    }

    abstract class HasFilterPlugin {
      lazy val plugin = FilterEvt((i: Integer) => i >= 0, Add1, Remove1)
    }

    class CommandTest extends HasFilterPlugin {
      val test = 42
      val result = test >>: plugin
      result.commands.length shouldBe 1
      result.commands(0) shouldBe test
      result.events.length shouldBe 0
    }

    class FilterPositiveTest extends HasFilterPlugin with Send1EventExpect1EventTest {
      lazy val test = 2
      lazy val expect = test + 1
    }

    class FilterNegativeTest extends HasFilterPlugin with Send1EventExpect1EventTest {
      lazy val test = -2
      lazy val expect = test - 1
    }

  }

  object BufferEvtSpec {

    abstract class HasBufferPlugin {
      def stop(value: Integer) = value > 10

      def update(buffer: Vector[Integer], value: Integer) = buffer :+ value

      def onStop(buffer: Vector[Integer], value: Integer) = {
        (buffer, buffer.fold(Integer.valueOf(0)) {
          _ + _
        })
      }

      lazy val plugin: Result = BufferEvtUntil(Vector.empty, stop _, update _, onStop _)
    }

    class CommandTest extends HasBufferPlugin {
      val test = Vector(1, 100)
      val result = test >>: plugin
      result.commands.length shouldBe 2
      result.commands shouldBe test
      result.events.length shouldBe 0
    }

    class BufferZeroTest extends HasBufferPlugin with Send1EventExpect1EventTest {
      lazy val test = 100
      lazy val expect = (Vector.empty, 0)
    }

    class BufferOneTest extends HasBufferPlugin {
      val test = Vector(5, 100)
      val result = plugin :<< test
      result.commands.length shouldBe 0
      result.events.length shouldBe 1
      result.events(0) shouldBe ((test.dropRight(1), 5))
    }

    class BufferManyTest extends HasBufferPlugin {
      val test = Vector(5, 8, 4, 2, 3, 9, 4, 100)
      val result = plugin :<< test
      result.commands.length shouldBe 0
      result.events.length shouldBe 1
      result.events(0) shouldBe ((test.dropRight(1), 35))
    }

  }

  object TcpAggregateEvtSpec {
    def req = {
      val cltAddress = new InetSocketAddress("127.0.0.1", Random.nextInt(100) + 10000)
      val srvAddress = new InetSocketAddress("1.2.3.4", Random.nextInt(100) + 10101)
      Request("id", cltAddress).setSrvAddr(srvAddress)
    }

    abstract class HasTcpAggregateEvt {
      lazy val plugin = TcpAggregateEvt()
    }

    class NotMessageTest extends HasTcpAggregateEvt with Send1EventExpect1EventTest {
      lazy val test = "TEST"
      lazy val expect = test
    }

    class NoDataTest extends HasTcpAggregateEvt with Send1EventExpect1EventTest {
      lazy val test = req
      lazy val expect = test
    }

    class NoDissectionTest extends HasTcpAggregateEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("TEST")
      lazy val expect = test
    }

    abstract class ProbeAndDissector extends HasRunningDissector {

      import PluginSpec.DissectHandler

      lazy val probe = new TestProbe(system)

      lazy val dissector = {
        val props = Props(classOf[DissectHandler], probe)
        val name = randomString("DissectHandler")
        TestActorRef[DissectHandler](props, probe.ref, name)
      }

      probe send(dissector, Event(req, false))
      probe.expectMsgType[Request]
    }

    class NonSegmentedTest extends ProbeAndDissector {
      val r = req setData ByteString("GET / HTTP/1.1\r\n\r\n")
      probe send(dissector, Event(r, false))

      val message = probe.expectMsgType[Request]
      val result = TcpAggregateEvt() :<< message

      result.commands.length shouldBe 0
      result.events.length shouldBe 1
      result.events(0) shouldBe message
    }

    class TcpAggregationTest extends ProbeAndDissector {
      val r = req
      val r0 = r setData ByteString("GET / HTTP/1.1\r\n")
      val r1 = r setData ByteString("\r\n")

      probe send(dissector, Event(r0, false))
      probe send(dissector, Event(r1, false))

      val messages = for (_ <- 1 to 2) yield probe.expectMsgType[Request]

      val plugin = TcpAggregateEvt()
      val result = plugin :<< messages

      result.commands.length shouldBe 0
      result.events.length shouldBe 1

      val evt = result.events(0)

      evt shouldBe a[Message]
      evt.asInstanceOf[Message].data.utf8String shouldBe "GET / HTTP/1.1\r\n\r\n"
    }

  }

  object SearchAndReplaceEvtSpec {
    val req = Request("id", new InetSocketAddress(0))

    abstract class HasSearchAndReplaceEvt {
      lazy val plugin: Result = SearchAndReplaceEvt(ByteString("a"), ByteString("AA"))
    }

    class NoReplacement extends HasSearchAndReplaceEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("qwertzuiop")
      lazy val expect = test
    }

    class OneReplacement extends HasSearchAndReplaceEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("asdfghjkl")
      lazy val expect = req setData ByteString("AAsdfghjkl")
    }

    class ManyReplacement extends HasSearchAndReplaceEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("a la peche aux moules")
      lazy val expect = req setData ByteString("AA lAA peche AAux moules")
    }

  }

  object ContainsEvtSpec {
    val req = Request("id", new InetSocketAddress(0))

    case object SetSeen extends Behaviour {
      def onCommand = Behaviour.emptyReceive

      def onEvent = {
        case m: Message => this withEvent m.set("seen" -> true)
        case _ => this withEvent "ERROR"
      }
    }

    abstract class HasContainsEvt {
      lazy val plugin = ContainsEvt(ByteString("string"), SetSeen)
    }

    class TestNonMessage extends HasContainsEvt with Send1EventExpect1EventTest {
      lazy val test = "TEST"
      lazy val expect = test
    }

    class TestNoData extends HasContainsEvt with Send1EventExpect1EventTest {
      lazy val test = req
      lazy val expect = req
    }

    class TestContains extends HasContainsEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("This contains string !")
      lazy val expect = test set ("seen" -> true)
    }

    class TestContainsNot extends HasContainsEvt with Send1EventExpect1EventTest {
      lazy val test = req setData ByteString("This doesn't contain strring !")
      lazy val expect = test
    }

  }

  object InterceptEvtSpec {
    val req = Request("id", new InetSocketAddress(0))

    abstract class HasInterceptEvt {
      lazy val plugin: Result = InterceptEvt
    }

    class TestNonMessage extends HasInterceptEvt with Send1EventExpect1EventTest {
      lazy val test = "TEST"
      lazy val expect = test
    }

    class TestAlreadyIntercepted extends HasInterceptEvt with Send1EventExpect1EventTest {
      lazy val test = req.setIntercepted
      lazy val expect = test
    }

    class TestIntercept extends HasInterceptEvt with Send1EventExpect1EventTest {
      lazy val test = req
      lazy val expect = req.setIntercepted
    }

  }


  "Plugins" when {
    "FilterEvt" must {
      import FilterEvtSpec._
      "let command pass" in new CommandTest
      "work with event that match the filter" in new FilterPositiveTest
      "work with event that doesn't match the filter" in new FilterNegativeTest
    }
    "BufferEvtUntil" must {
      import BufferEvtSpec._
      "let command pass" in new CommandTest
      "buffer zero events correctly" in new BufferZeroTest
      "buffer one event correctly" in new BufferOneTest
      "buffer many events correctly" in new BufferManyTest
    }
    "TcpAggregateEvt" must {
      import TcpAggregateEvtSpec._
      "let non-message events pass" in new NotMessageTest
      "let messages without data pass" in new NoDataTest
      "let messages without dissection pass" in new NoDissectionTest
      "let messages not segmented pass" in new NonSegmentedTest
      "aggregate TCP segments" in new TcpAggregationTest
    }
    "SearchAndReplaceEvt" must {
      import SearchAndReplaceEvtSpec._
      "not change messages unnecessarily" in new NoReplacement
      "perform a replacement when necessary" in new OneReplacement
      "perform multiple replacements" in new ManyReplacement
    }
    "ContainsEvt" must {
      import ContainsEvtSpec._
      "filter out non-messages" in new TestNonMessage
      "filter out messages without data" in new TestNoData
      "filter a message that contains the string" in new TestContains
      "filter out a message that doesn't contain the sting" in new TestContainsNot
    }
    "InterceptEvt" must {
      import InterceptEvtSpec._
      "not change non-message" in new TestNonMessage
      "not change already tagged message" in new TestAlreadyIntercepted
      "set message tag intercepted" in new TestIntercept
    }
  }
}
