package ch.os.tproxy.test

import scala.util.Failure
import akka.actor.{ActorRef, Props}
import akka.io.Tcp
import akka.util.ByteString
import akka.testkit.{TestActorRef, TestProbe}
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.tcp.TcpBackpressure
import ch.os.tproxy.common.behaviour.actor.{ActorNode, SendCmdTo, SendEvtTo}
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.log.TProxyLogReceive

object TcpBackpressureSpec {

  case class MsgTag(t: Int) extends Tcp.Event

  case object NoWrite extends Tcp.Event

  class Handler(client: ActorRef, server: ActorRef) extends ActorNode {
    implicit lazy val logging = new AkkaLogging(this)
    implicit val logReceive = new TProxyLogReceive()

    def onStart = {
      SendEvtTo(client, false, "client") <->
        SendNoWrite(TcpBackpressure(5, 10, 15)) <->
        SendCmdTo(server, false, "server")
    }

    override def initialize(init: Behaviour.Result) = {
      super.initialize(init) <-> new OnEvent {
        def onEvent = {
          case cmd: Tcp.Command => this withEvent Command(cmd)
          case evt => this withEvent Event(evt)
        }
      }
    }

    private case class SendNoWrite(behaviours: Behaviour*) extends Behaviour {
      def onCommand = {
        case cmd => {
          val r = cmd >>: behaviours
          val next = r withBehaviour SendNoWrite(r.behaviours: _*)
          cmd match {
            case _: Tcp.Write if r.commands.isEmpty => next withCommand NoWrite
            case _ => next
          }
        }
      }

      def onEvent = {
        case evt => {
          val r = behaviours :<< evt
          r withBehaviour SendNoWrite(r.behaviours: _*)
        }
      }
    }

  }

}

class TcpBackpressureSpec extends TestSpec("TcpBackpressureSpec") {

  import TcpBackpressureSpec._

  abstract class ClientServerHandler {
    val client = new TestProbe(system)
    val server = new TestProbe(system)
    val props = Props(classOf[Handler], client.ref, server.ref)
    val name = randomString("Handler")
    val handler = TestActorRef[Handler](props, self, name)
  }

  object Writing {

    class TestWrite3Bytes extends ClientServerHandler {
      val msg0 = ByteString(Array[Byte](1, 2, 3))
      handler ! Tcp.Write(msg0, MsgTag(0))
      val Tcp.Write(data0, ack0) = server.expectMsgType[Tcp.Write]
      data0 shouldBe msg0
    }

    class TestAck1Byte extends TestWrite3Bytes {
      handler ! ack0
      client.expectMsgType[Tcp.Event]
    }

    class TestHighWatermark extends TestAck1Byte {
      val msg123 = Array(ByteString(Array[Byte](1, 2)),
        ByteString(Array[Byte](3, 4, 5, 6, 7)),
        ByteString(Array[Byte](8, 9, 10, 11)))

      val write123 = for ((msg, t) <- msg123 zip (1 to 3)) yield {
        handler ! Tcp.Write(msg, MsgTag(t))
        server.expectMsgType[Tcp.Write]
      }

      val data123 = write123 map (_.data)
      for ((data, msg) <- data123 zip msg123) data shouldBe msg

      client.expectMsg(TcpBackpressure.HighWatermarkReached)
    }

    class TestLowWatermark extends TestHighWatermark {
      val ack123 = write123 map (_.ack)
      handler ! ack123(0)
      handler ! ack123(1)
      client expectMsg MsgTag(1)
      client expectMsg MsgTag(2)
      client expectMsg TcpBackpressure.LowWatermarkReached
    }

    class TestClose extends TestHighWatermark {
      val msg4 = ByteString(Array[Byte](12, 13, 14, 15, 16, 17))
      handler ! Tcp.Write(msg4, MsgTag(4))
      server expectMsg Tcp.Abort
    }

    class TestUnorderedAck extends ClientServerHandler {
      val msg0 = ByteString(Array[Byte](1, 2, 3))
      val msg1 = ByteString(Array[Byte](4, 5, 6))
      handler ! Tcp.Write(msg0, MsgTag(0))
      val Tcp.Write(data0, ack0) = server.expectMsgType[Tcp.Write]
      handler ! Tcp.Write(msg1, MsgTag(1))
      val Tcp.Write(data1, ack1) = server.expectMsgType[Tcp.Write]

      handler ! ack1
      val e = server.expectMsgType[Failure[_]].exception
      e.getMessage should startWith("requirement failed: received ack")
    }

    class TestNonTcpMessage extends ClientServerHandler {

      case class TestObject()

      handler ! TestObject()
      client.expectMsgType[TestObject]
    }

  }

  object Buffering {

    class TestCommandFailed extends Writing.TestAck1Byte {
      val msg1 = ByteString(Array[Byte](4, 5))
      handler ! Tcp.Write(msg1, MsgTag(1))
      val write1 = server.expectMsgType[Tcp.Write]

      val msg2 = ByteString(Array[Byte](6, 7))
      handler ! Tcp.Write(msg2, MsgTag(2))
      val write2 = server.expectMsgType[Tcp.Write]

      handler ! Tcp.CommandFailed(write2)
      val resumeMsg = server.expectMsgType[Tcp.Command]
      resumeMsg shouldBe Tcp.ResumeWriting
    }

    class TestBufferWrite extends TestCommandFailed {
      val msg3 = ByteString(Array[Byte](8, 9))
      handler ! Tcp.Write(msg3, MsgTag(3))
      server expectMsg NoWrite
    }

    class TestAck1Message extends TestBufferWrite {
      handler ! write1.ack
      val ack1 = client.expectMsgType[Tcp.Event]
      ack1 shouldBe MsgTag(1)
    }

    class TestWritingResumed extends TestAck1Message {
      handler ! Tcp.WritingResumed
      val write2bis = server.expectMsgType[Tcp.Write]
      write2bis.data shouldBe msg2
    }

    class TestAckFailedCommand extends TestWritingResumed {
      handler ! write2.ack
      val write3 = server.expectMsgType[Tcp.Write]
      write3.data shouldBe msg3
      val ack2 = client.expectMsgType[Tcp.Event]
      ack2 shouldBe MsgTag(2)
    }

    class Test10WritesAfterFail extends TestAckFailedCommand {
      val msgs = Array(ByteString(Array[Byte](10)),
        ByteString(Array[Byte](11)),
        ByteString(Array[Byte](12)),
        ByteString(Array[Byte](13)),
        ByteString(Array[Byte](14)),
        ByteString(Array[Byte](15)),
        ByteString(Array[Byte](16)),
        ByteString(Array[Byte](17)),
        ByteString(Array[Byte](18)),
        ByteString(Array[Byte](19)))

      handler ! Tcp.Write(msgs(0), MsgTag(4))
      server expectMsg NoWrite

      var previousAck = write3.ack
      var previousData = msgs(0)

      for ((msg, tag) <- msgs.tail zip (5 to 13)) {
        handler ! Tcp.Write(msg, MsgTag(tag))
        server expectMsg NoWrite

        handler ! previousAck
        val write = server.expectMsgType[Tcp.Write]
        write.data shouldBe previousData
        val ack = client.expectMsgType[Tcp.Event]
        ack shouldBe MsgTag(tag - 2)

        previousData = msg
        previousAck = write.ack
      }
    }

    class Test11thWrite extends Test10WritesAfterFail {
      val msg14 = ByteString(Array[Byte](20))
      handler ! Tcp.Write(msg14, MsgTag(14))
      server expectMsg NoWrite

      handler ! previousAck
      var write13 = server.expectMsgType[Tcp.Write]
      write13.data shouldBe previousData
      var write14 = server.expectMsgType[Tcp.Write]
      write14.data shouldBe msg14
      val ack = client.expectMsgType[Tcp.Event]
      ack shouldBe MsgTag(12)
    }

  }

  object Closing {

    class TestCloseBeforeAck extends ClientServerHandler {
      val msg0 = ByteString("hello")
      val msg1 = ByteString("world")

      handler ! Tcp.Write(msg0, MsgTag(0))
      handler ! Tcp.Write(msg1, MsgTag(1))

      val Tcp.Write(data0, ack0) = server.expectMsgType[Tcp.Write]
      val Tcp.Write(data1, ack1) = server.expectMsgType[Tcp.Write]

      data0 shouldBe msg0
      data1 shouldBe msg1

      handler ! Tcp.Close
      handler ! ack0
      client expectMsg MsgTag(0)
    }

    class TestCloseAfterAck extends TestCloseBeforeAck {
      handler ! ack1
      client expectMsg MsgTag(1)
      server.expectMsgType[Tcp.CloseCommand]
    }

  }

  "TcpBackpressure" when {
    "in state writing" must {
      import Writing._
      "transmit Tcp.Write commands" in new TestWrite3Bytes
      "return ack event after acknowledgement" in new TestAck1Byte
      "warn when high water mark is reached" in new TestHighWatermark
      "warn when low water mark is reached" in new TestLowWatermark
      "close the connection when buffer capacity is exceeded" in new TestClose
      "fail if an ack is received out of order" in new TestUnorderedAck
      "forward seamlessly non-tcp messages" in new TestNonTcpMessage
    }
    "in state buffering" must {
      import Buffering._
      "send Tcp.ResumeWriting after receiving Tcp.CommandFailed" in new TestCommandFailed
      "store Tcp.Write commands" in new TestBufferWrite
      "return Ack event after acknowledgment" in new TestAck1Message
      "resend the failed command when receiving Tcp.WritingResumed" in new TestWritingResumed
      "return ack event and one Tcp.Write ack of failed command" in new TestAckFailedCommand
      "transmit only one message before 10 acks" in new Test10WritesAfterFail
      "go back to writing mode after 10 successful acks" in new Test11thWrite
    }
    "in state closing" must {
      import Closing._
      "not transmit Tcp.Close before the last ack" in new TestCloseBeforeAck
      "transmit Tcp.Close after the last ack" in new TestCloseAfterAck
    }
  }
}
