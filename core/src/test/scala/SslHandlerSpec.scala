package ch.os.tproxy.test

import java.net.InetSocketAddress
import java.util.Date
import java.security.cert.Certificate
import sun.security.tools.keytool.CertAndKeyGen
import sun.security.x509.X500Name
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.message.{Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.behaviour.ssl.SslHandler
import ch.os.tproxy.core.behaviour.ssl.SslHandler.implicits._
import ch.os.tproxy.core.behaviour.ssl.ClientHelloParser
import ch.os.tproxy.core.log.TProxyLogReceive

class SslHandlerSpec extends TestSpec("SslHandlerSpec") {

  trait Send1EventExpect1EventTest {
    val send: Any
    val handler: Result
    val expect: Any
    val result = handler :<< send
    result.commands.length shouldBe 0
    result.events.length shouldBe 1
    result.events(0) shouldBe expect
  }

  trait Send1CommandExpect1CommandTest {
    val send: Any
    val handler: Result
    val expect: Any
    val result = send >>: handler
    result.events.length shouldBe 0
    result.commands.length shouldBe 1
    result.commands(0) shouldBe expect
  }

  abstract class HasSslHandler {
    implicit val logReceive = new TProxyLogReceive()
    implicit lazy val logging = SslHandlerSpec.this.logging
    val handler: Result = SslHandler()
  }

  val req = new Request("id")
  val resp = new Response("id")
  val addr = new InetSocketAddress(0)

  def hex2bytestring(hex: String) =
    ByteString(hex.sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte))

  // SNI = carol.sni.velox.ch
  val client_hello_sni = hex2bytestring("16030100b2010000ae030353a812aac6ae085e684849080c941671e43e7b1d4eba7884839ed41f3200a01b207a5221425f684f4b418b40c11db82d3e87181628917f85aaa6a0406c0423d07c0038c00ac0140035c005c00f00390038c009c013002fc004c00e00330032c007c0110005c002c00cc008c012000ac003c00d00160013000400ff0100002d000a00080006001700180019000b000201000000001700150000126361726f6c2e736e692e76656c6f782e6368")

  val client_hello = hex2bytestring("160301007701000073030353a83ecf1bdfc0a332ba61050474203537b008cc0dc788cce74b67ae1a7fc4ce000038c00ac0140035c005c00f00390038c009c013002fc004c00e00330032c007c0110005c002c00cc008c012000ac003c00d00160013000400ff01000012000a00080006001700180019000b00020100")

  private val certificates = Array[Certificate] {
    val x500Name = new X500Name("test", "unknown", "unknown",
      "unkown", "unkown", "unknown")
    val keypair = new CertAndKeyGen("RSA", "SHA1WithRSA", null)
    keypair generate 1024
    keypair getSelfCertificate(x500Name, new Date, 10 * 60)
  }

  object ClientSide {

    class TestNonHelloRequest extends HasSslHandler with Send1EventExpect1EventTest {
      lazy val send = req setData ByteString("Some non ssl data")
      lazy val expect = send
    }

    class TestAnyResponse extends HasSslHandler with Send1CommandExpect1CommandTest {
      lazy val send = resp setData ByteString("Some other non ssl data")
      lazy val expect = send
    }

    class TestClientHello extends HasSslHandler with Send1EventExpect1EventTest {
      lazy val send = req setData client_hello
      lazy val expect = req.remData.setSsl
    }

    class TestSni extends HasSslHandler with Send1EventExpect1EventTest {
      lazy val send = req setData client_hello_sni
      lazy val expect = req.setSsl setSni "carol.sni.velox.ch"
    }

    class TestServerHello extends TestClientHello {
      val newsend = resp setSslCertificates certificates
      val newresult = newsend >>: result.behaviours
      newresult.events.length shouldBe 0
      newresult.commands.length shouldBe 1
      val serverhello = newresult.commands(0).asInstanceOf[Message]
      serverhello.hasData shouldBe true
      ClientHelloParser.isSslServerHello(serverhello.data) shouldBe true
    }

  }

  object ServerSide {

    class TestNonSslRequest extends HasSslHandler with Send1CommandExpect1CommandTest {
      lazy val send = req setData ByteString("Some non ssl data request")
      lazy val expect = send
    }

    class TestAnyResponse extends HasSslHandler with Send1EventExpect1EventTest {
      lazy val send = resp setData ByteString("Some non ssl data response")
      lazy val expect = send
    }

    class TestSslStart extends HasSslHandler {
      val send = req.setSsl
      val result = send >>: handler
      result.events.length shouldBe 0
      result.commands.length shouldBe 1
      val clienthello = result.commands(0).asInstanceOf[Message]
      clienthello.hasData shouldBe true
      ClientHelloParser.isSslClientHello(clienthello.data) shouldBe true
      (ClientHelloParser extractSni clienthello.data).isEmpty shouldBe true
    }

    class TestSslStartSni extends HasSslHandler {
      val testsni = "some.test.com"
      val send = req.setSrvAddr(addr).setSsl.setSni(testsni)
      val result = send >>: handler
      result.events.length shouldBe 0
      result.commands.length shouldBe 1
      val clienthello = result.commands(0).asInstanceOf[Message]
      clienthello.hasData shouldBe true
      ClientHelloParser.isSslClientHello(clienthello.data) shouldBe true
      val snis = ClientHelloParser extractSni clienthello.data
      snis.length shouldBe 1
      snis(0).utf8String shouldBe testsni
    }

    class TestCertificates extends TestSslStartSni {
      val certif = resp setSslCertificates certificates
      val (server, serverhello) = {
        val sslserver = certif >>: (SslHandler() :<< clienthello)
        sslserver.events.length shouldBe 1
        sslserver.commands.length shouldBe 1
        val hello = sslserver.commands(0).asInstanceOf[Response]
        hello.hasData shouldBe true
        ClientHelloParser.isSslServerHello(hello.data) shouldBe true
        (sslserver, hello)
      }
      val newresult = result.behaviours :<< serverhello
      newresult.commands.length shouldBe 1
      val cipherspec = newresult.commands(0)

      val servercertifs = {
        val serverresult = server.behaviours :<< cipherspec
        serverresult.events.length shouldBe 0
        serverresult.commands.length shouldBe 1
        serverresult.commands(0)
      }

      val finalresult = newresult.behaviours :<< servercertifs
      finalresult.events.length shouldBe 1
      val certif_resp: Response = finalresult.events(0).asInstanceOf[Response]
      certif_resp.hasSslCertificates shouldBe true
    }

  }

  "SslHandler" when {
    "on the client side" must {
      import ClientSide._
      "let non client_hello requests" in new TestNonHelloRequest
      "let responses pass" in new TestAnyResponse
      "recognise SSL client_hello without SNI" in new TestClientHello
      "recognise SSL client_hello with SNI" in new TestSni
      "send server hello when it receives certificates" in new TestServerHello
    }
    "on the server side" must {
      import ServerSide._
      "let non ssl request pass" in new TestNonSslRequest
      "let responses pass" in new TestAnyResponse
      "send client_hello after having received a ssl msg" in new TestSslStart
      "send client_hello after having received a ssl msg with sni" in new TestSslStartSni
      "forward certificates" in new TestCertificates
    }
  }
}
