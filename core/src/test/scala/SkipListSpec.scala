package ch.os.tproxy.test

import java.util.UUID
import scala.util.{Random, Sorting, Try, Failure}
import scala.math.Ordering
import ch.os.tproxy.util.SkipList

class SkipListSpec extends TestSpec("SkipListSpec") {

  case class Item(key: Int, value: UUID = UUID.randomUUID)

  implicit val itemOrdering = Ordering by[Item, Int] (_.key)

  abstract class InsertRandomItems {
    val keys = (0 until 1000 by 2).toArray

    for (i <- 0 until keys.size) {
      val j = Random nextInt keys.size
      val k = keys(i)
      keys(i) = keys(j)
      keys(j) = k
    }

    val insertedItems = keys map (Item(_))
    val skipList = new SkipList[Int, Item](_.key)

    for (item <- insertedItems) skipList insert item
  }

  class CorrectSize extends InsertRandomItems {
    skipList.size shouldBe insertedItems.size
  }

  class StoreItemsSorted extends CorrectSize {
    Sorting quickSort insertedItems
    skipList.toArray shouldBe insertedItems
  }

  class AccessByIndex extends InsertRandomItems {
    Sorting quickSort insertedItems

    for (i <- 0 until insertedItems.size)
      skipList(i) shouldBe insertedItems(i)
  }

  class AccessByKey extends InsertRandomItems {
    Sorting quickSort insertedItems

    for ((item, i) <- insertedItems.zipWithIndex) {
      skipList.itemMap(item.key) shouldBe item
      skipList.indexMap(item.key) shouldBe i
    }
  }

  class StackIdentical extends InsertRandomItems {
    val idx = Random nextInt skipList.size
    val idxes = List(idx, idx + 1)

    val item = skipList(idx)
    val items = List(Item(item.key), item)

    skipList.insert(items.head) shouldBe idx
    skipList.itemMultiMap(item.key) shouldBe items
    skipList.indexMultiMap(item.key) shouldBe idxes
  }

  /** We force the caching of the item at index idx+1 and check that
   * when we search for all items matching key item.key, the item at
   * index idx (which, by construction has the same key) is also
   * returned. It would be wrong to start the search at the index of
   * the cached item.
   */
  class FirstItemMatchingKey extends StackIdentical {
    skipList(idx + 1) shouldBe item
    skipList.itemMultiMap(item.key) shouldBe items
  }

  class FirstIndexMatchingKey extends StackIdentical {
    skipList(idx + 1) shouldBe item
    skipList.indexMultiMap(item.key) shouldBe idxes
  }

  class UpdateByIndex extends InsertRandomItems {
    val idx = Random nextInt skipList.size
    val item0 = skipList(idx)
    val item1 = Item(item0.key + 1)

    skipList(idx) = item1

    skipList(idx) shouldBe item1
    skipList.itemMap(item1.key) shouldBe item1
    skipList.indexMap(item1.key) shouldBe idx
  }

  class KeyOrderUpdByIdx extends InsertRandomItems {
    val prevIdx = 1 + Random.nextInt(skipList.size - 3)
    val currIdx = prevIdx + 1
    val nextIdx = currIdx + 1

    val prevItem = skipList(prevIdx)
    val currItem = skipList(currIdx)
    val nextItem = skipList(nextIdx)

    for (key <- prevItem.key to nextItem.key)
      skipList(currIdx) = Item(key)

    val gteqFailure = Try(skipList(currIdx) = Item(prevItem.key - 1))
    gteqFailure shouldBe a[Failure[_]]

    val lteqFailure = Try(skipList(currIdx) = Item(nextItem.key + 1))
    lteqFailure shouldBe a[Failure[_]]
  }

  class RemoveByIndex extends InsertRandomItems {
    val size = skipList.size
    val idx = Random nextInt (size - 1)

    val currItem = skipList(idx)
    val nextItem = skipList(idx + 1)
    val remItem = skipList remove idx

    skipList.size shouldBe size - 1
    remItem shouldBe currItem
    skipList(idx) shouldBe nextItem
  }

  class RemoveByKey extends InsertRandomItems {
    val size = skipList.size
    val idx = Random nextInt (size - 1)

    val currItem = skipList(idx)
    val nextItem = skipList(idx + 1)
    val remItem = skipList.itemMap remove currItem.key

    skipList.size shouldBe size - 1
    remItem shouldBe Some(currItem)
    skipList(idx) shouldBe nextItem
    skipList.indexMap(nextItem.key) shouldBe idx
  }

  "A SkipList" must {
    "have the correct size" in new CorrectSize
    "store items sorted" in new StoreItemsSorted
    "allow access to items by key" in new AccessByKey
    "act as a stack for identical items" in new StackIdentical
  }

  it when {
    "used as an indexed list" must {
      "allow access to items by index" in new AccessByIndex
      "allow update of items by index" in new UpdateByIndex
      "ensure key order consistency" in new KeyOrderUpdByIdx
      "allow items to be removed" in new RemoveByIndex
    }
  }

  it when {
    "used as a map" must {
      "return the first item matching a key" in new FirstItemMatchingKey
      "return the first index matching a key" in new FirstIndexMatchingKey
      "allow items to be removed" in new RemoveByKey
    }
  }
}
