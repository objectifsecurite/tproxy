package ch.os.tproxy.test

import org.scalatest.tools.Runner
import ch.os.tproxy.core.Native
import ch.os.tproxy.common.Properties
import ch.os.tproxy.common.natlib.NatlibLoader

object TestRunner {
  val runAsRootProp = "run.as.root"
  val runAsRootPropVal = "true"

  def main(args: Array[String]) = {
    Properties prependPaths("java.library.path", "native.library.path")

    NatlibLoader load "tproxy_core"
    NatlibLoader load "tproxy_core_jni"

    if (Native.getUID == 0)
      System setProperty(runAsRootProp, runAsRootPropVal)

    Native.setCapsAndCurrentUser()
    Runner.main(args)
  }
}
