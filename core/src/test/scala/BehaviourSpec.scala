package ch.os.tproxy.test

import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import scala.util.{Try, Success, Failure}

object BehaviourSpec {

  case class SumCmdEmitCmd(n: Int = 0) extends Behaviour {
    def onCommand = {
      case cmd: Int => copy(n = n + cmd) withCommand n
    }

    def onEvent = {
      case _: Int => None
    }
  }

  case class SumCmdEmitEvt(n: Int = 0) extends Behaviour {
    def onCommand = {
      case cmd: Int => copy(n = n + cmd) withEvent n
    }

    def onEvent = {
      case _: Int => None
    }
  }

  case class SumEvtEmitEvt(n: Int = 0) extends Behaviour {
    def onCommand = {
      case _: Int => this
    }

    def onEvent = {
      case evt: Int => copy(n = n + evt) withEvent n
    }
  }

  case class SumEvtEmitCmd(n: Int = 0) extends Behaviour {
    def onCommand = {
      case _: Int => this
    }

    def onEvent = {
      case evt: Int => copy(n = n + evt) withCommand n
    }
  }

  case class ExceptionOnEvenCmd(n: Int = 0) extends Behaviour {
    def onCommand = {
      case i: Int => i / (i % 2); copy(n = n + i) withCommand i
    }

    def onEvent = {
      case evt: Int => this
    }
  }

  case class FailOnEvenCmd(n: Int = 0) extends Behaviour {
    def onCommand = {
      case i: Int => Try(i / (i % 2)) match {
        case Success(r) => copy(n = n + i) withCommand i
        case f: Failure[_] => this withEvent f
      }
    }

    def onEvent = {
      case evt: Int => this
    }
  }

}

class BehaviourSpec extends TestSpec("BehaviourSpec") {

  import BehaviourSpec._
  import Behaviour.propagate

  "Propagate" when {
    "only commands are given" must {
      "work when a behaviour emits a command" in {
        val result = List(1, 2, 3) >>: SumCmdEmitCmd(0)
        result.events shouldBe Nil
        result.commands shouldBe List(0, 1, 3)
        result.behaviours shouldBe SumCmdEmitCmd(6) :: Nil
      }

      "work when a behaviour emits an event" in {
        val result = List(1, 2, 3) >>: SumCmdEmitEvt(0)
        result.events shouldBe List(0, 1, 3)
        result.commands shouldBe Nil
        result.behaviours shouldBe SumCmdEmitEvt(6) :: Nil
      }

      "work when a behaviour emits a command and an event" in {
        val b = new Behaviour {
          def onCommand = {
            case cmd: Int => this withCommand (cmd + 1) withEvent (cmd - 1)
          }

          def onEvent = Behaviour.emptyReceive
        }

        val result = 1 >>: b

        result.events shouldBe 0 :: Nil
        result.commands shouldBe 2 :: Nil
        result.behaviours shouldBe b :: Nil
      }

      "work when a behaviour disappears without emitting a command" in {
        val b = new Behaviour {
          def onCommand = {
            case _ => None
          }

          def onEvent = {
            case _ => None
          }
        }

        val result = List(1, 2, 3) >>: b

        result.events shouldBe Nil
        result.commands shouldBe List(2, 3)
        result.behaviours shouldBe Nil
      }

      "work when a behaviour disappears after emitting a command" in {
        val b = new Behaviour {
          def onCommand = {
            case cmd => None withCommand cmd
          }

          def onEvent = {
            case _ => None
          }
        }

        val result = List(1, 2, 3) >>: b

        result.events shouldBe Nil
        result.commands shouldBe List(1, 2, 3)
        result.behaviours shouldBe Nil
      }

      "work when a behaviour disappears after emitting a command and an event" in {
        val b = new Behaviour {
          def onCommand = {
            case cmd: Int => None withCommand (cmd + 1) withEvent (cmd - 1)
          }

          def onEvent = Behaviour.emptyReceive
        }

        val result = 1 >>: b

        result.events shouldBe 0 :: Nil
        result.commands shouldBe 2 :: Nil
        result.behaviours shouldBe Nil
      }

      "work when a behaviour disappears and nothing remains" in {
        val b = new Behaviour {
          def onCommand = {
            case _ => None
          }

          def onEvent = {
            case _ => None
          }
        }

        val result = 1 >>: b

        result.events shouldBe Nil
        result.commands shouldBe Nil
        result.behaviours shouldBe Nil
      }

      "work with a sequence of behaviours" in {
        val result = List(1, 2, 3) >>: (SumCmdEmitCmd(0) <-> SumCmdEmitCmd(1))
        result.events shouldBe Nil
        result.commands shouldBe List(1, 1, 2)
        result.behaviours shouldBe SumCmdEmitCmd(6) :: SumCmdEmitCmd(5) :: Nil
      }

      "work when a behaviour throws an exception" in {
        val result = List(1, 2, 3) >>: ExceptionOnEvenCmd(0)
        result.events should have size 1
        result.events.head shouldBe a[Failure[_]]
        result.commands shouldBe List(1, 3)
        result.behaviours shouldBe ExceptionOnEvenCmd(4) :: Nil
      }

      "work when a behaviour fails explicitly" in {
        val result = List(1, 2, 3) >>: FailOnEvenCmd(0)
        result.events should have size 1
        result.events.head shouldBe a[Failure[_]]
        result.commands shouldBe List(1, 3)
        result.behaviours shouldBe FailOnEvenCmd(4) :: Nil
      }

      "work when a behaviour cannot handle a command" in {
        val result = List(1, "a", 3) >>: SumCmdEmitCmd(0)
        result.events shouldBe Nil
        result.commands shouldBe List(0, "a", 1)
        result.behaviours shouldBe SumCmdEmitCmd(4) :: Nil
      }
    }

    "only events are given" must {
      "work when a behaviour emits an event" in {
        val result = SumEvtEmitEvt(0) :<< List(1, 2, 3)
        result.events shouldBe List(0, 1, 3)
        result.commands shouldBe Nil
        result.behaviours shouldBe SumEvtEmitEvt(6) :: Nil
      }

      "work when a behaviour emits a command" in {
        val result = SumEvtEmitCmd(0) :<< List(1, 2, 3)
        result.events shouldBe Nil
        result.commands shouldBe List(0, 1, 3)
        result.behaviours shouldBe SumEvtEmitCmd(6) :: Nil
      }

      "work when a behaviour disappears and nothing remains" in {
        val b = new Behaviour {
          def onCommand = {
            case _ => None
          }

          def onEvent = {
            case _ => None
          }
        }

        val result = b :<< 1

        result.events shouldBe Nil
        result.commands shouldBe Nil
        result.behaviours shouldBe Nil
      }

      "work with a sequence of behaviours" in {
        val result = (SumEvtEmitEvt(1) <-> SumEvtEmitEvt(2)) :<< List(1, 2, 3)
        result.events shouldBe List(1, 3, 6)
        result.commands shouldBe Nil
        result.behaviours shouldBe SumEvtEmitEvt(11) :: SumEvtEmitEvt(8) :: Nil
      }
    }

    "both commands and events are given" must {
      "work when a behaviour does not emit commands nor events" in {
        case class TestBehaviour(c: Int = 0) extends Behaviour {
          def onCommand = {
            case cmd: Int => copy(c = c + cmd)
          }

          def onEvent = {
            case evt: Int => copy(c = c * evt)
          }
        }

        val result = propagate(List(1, 2, 3), TestBehaviour(0) :: Nil, List(4, 5, 6))

        result.events shouldBe Nil
        result.commands shouldBe Nil
        result.behaviours shouldBe TestBehaviour(720) :: Nil
      }

      "work when a behaviour does emit only commands" in {
        case class TestBehaviour(c: Int = 0) extends Behaviour {
          def onCommand = {
            case cmd: Int => copy(c = c + cmd) withCommand c
          }

          def onEvent = {
            case evt: Int => copy(c = c * evt)
          }
        }

        val result = propagate(List(1, 2, 3), TestBehaviour(0) :: Nil, List(4, 5, 6))

        result.events shouldBe Nil
        result.commands shouldBe List(0, 1, 3)
        result.behaviours shouldBe TestBehaviour(720) :: Nil
      }

      "work when a behaviour does emit only events" in {
        case class AddMultiply(c: Int = 0) extends Behaviour {
          def onCommand = {
            case cmd: Int => copy(c = c + cmd)
          }

          def onEvent = {
            case evt: Int => copy(c = c * evt) withEvent c
          }
        }

        val result = propagate(List(1, 2, 3), AddMultiply(0) :: Nil, List(4, 5, 6))

        result.events shouldBe List(6, 24, 120)
        result.commands shouldBe Nil
        result.behaviours shouldBe AddMultiply(720) :: Nil
      }

      "work when commands and events are propagated through a sequence of behaviours" in {
        case class AddMulFwd(c: Int = 0) extends Behaviour {
          def onCommand = {
            case cmd: Int => copy(c = c + cmd) withCommand c
          }

          def onEvent = {
            case evt: Int => copy(c = c * evt) withEvent c
          }
        }

        val behaviours = AddMulFwd(0) :: AddMulFwd(1) :: AddMulFwd(2) :: Nil
        val afterCmds = List(1, 2, 3) >>: behaviours

        afterCmds.events shouldBe Nil
        afterCmds.commands shouldBe List(2, 3, 4)
        afterCmds.behaviours shouldBe (List(6, 5, 6) map AddMulFwd)

        val afterEvts = afterCmds :<< List(4, 5, 6)

        afterEvts.events shouldBe List(6, 30, 900)
        afterEvts.commands shouldBe afterCmds.commands
        afterEvts.behaviours shouldBe (List(648000, 86400, 720) map AddMulFwd)

        val result = propagate(List(1, 2, 3), behaviours, List(4, 5, 6))
        result shouldBe afterEvts
      }
    }
  }

  "Result" must {
    "propagate commands when a behaviour is added" in {
      val r0 = List(1, 2, 3) >>: SumCmdEmitCmd(0)
      val r1 = r0 <-> SumCmdEmitCmd(1)

      r1.events shouldBe Nil
      r1.commands shouldBe List(1, 1, 2)
      r1.behaviours shouldBe SumCmdEmitCmd(6) :: SumCmdEmitCmd(5) :: Nil
    }

    "propagate events when a behaviour is added" in {
      val r0 = SumEvtEmitEvt(0) :<< List(1, 2, 3)
      val r1 = SumEvtEmitEvt(1) <-> r0

      r1.events shouldBe List(1, 1, 2)
      r1.commands shouldBe Nil
      r1.behaviours shouldBe SumEvtEmitEvt(5) :: SumEvtEmitEvt(6) :: Nil
    }
  }
}
