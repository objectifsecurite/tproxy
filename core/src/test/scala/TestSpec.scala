package ch.os.tproxy.test

import java.net.InetSocketAddress
import java.util.concurrent.atomic.AtomicLong
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.actor.{Actor, ActorSystem}
import akka.io.{IO, Tcp}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{GivenWhenThen, WordSpecLike}
import org.scalatest.{Matchers, Tag}
import com.typesafe.config.ConfigFactory
import ch.os.tproxy.common.Properties
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.log.TProxyLogReceive

object TestSpec {
  // This lazy val will be initialised only once per JVM instance.
  lazy val setNatlibPath =
    Properties prependPaths("java.library.path", "native.library.path")
}

abstract class TestSpec(system: ActorSystem) extends TestKit(system)
  with WordSpecLike
  with Matchers
  with ImplicitSender
  with GivenWhenThen {

  import system.dispatcher

  implicit lazy val logging = new AkkaLogging(system, self)
  implicit val logReceive = new TProxyLogReceive()
  lazy val log = logging(this)

  // This is necessary to make sure that native libraries are found.
  TestSpec.setNatlibPath

  // The following tag can be useful during debugging to select tests
  // to be run. "taggedAs ToTest" must be added before the 'in'
  // keyword. Tagged tests can then be run in sbt with the command
  // 'testOnly XyzSpec -- -n ToTest' for example.
  //
  case object ToTest extends Tag("ToTest")

  case object GuiTest extends Tag("GuiTest")

  val asRoot = {
    val x = System getProperty TestRunner.runAsRootProp
    x != null && x == TestRunner.runAsRootPropVal
  }

  def this(actorSystemName: String) =
    this(ActorSystem(actorSystemName, ConfigFactory.defaultReference))

  def succeedIfRootOrFailIn(test: => Unit) = {
    if (asRoot) "succeed" else "fail"
  } in test

  implicit class IfRootWrapper(str: String) {
    def inIfRootOrIgnore(test: => Unit) =
      if (asRoot) str in test else str ignore test
  }

  private val number = new AtomicLong

  def randomString(prefix: String = "") = {
    val l = number.getAndIncrement()
    s"$prefix$$" + akka.util.Helpers.base64(l)
  }

  def afterAll() = shutdown()

  private case object StopAutoReceive

  private case object AutoReceiveStopped

  implicit class ExtendedTestProbe(probe: TestProbe) {
    def bind() = {
      val addr = new InetSocketAddress("localhost", 0)
      probe send(IO(Tcp)(system), Tcp.Bind(probe.ref, addr))
      (probe.expectMsgType[Tcp.Bound].localAddress, probe.lastSender)
    }

    def connect(serverAddress: InetSocketAddress) = {
      probe send(IO(Tcp)(system), Tcp.Connect(serverAddress))
      probe.expectMsgType[Tcp.Connected].localAddress
    }

    def register() = {
      probe.lastSender ! Tcp.Register(probe.ref)
      probe.lastSender
    }

    def autoReceive(receive: Actor.Receive) = Future {
      (probe fishForMessage 3.seconds) {
        case StopAutoReceive => probe.lastSender ! AutoReceiveStopped; true
        case x if receive isDefinedAt x => receive(x); false
        case x => log warning s"AutoReceive: unexpected message $x"; false
      }
    }

    def stopAutoReceive(): Unit = {
      val inner = new TestProbe(system)
      inner send(probe.ref, StopAutoReceive)
      inner expectMsg AutoReceiveStopped
    }
  }

}
