package ch.os.tproxy.test

import akka.actor.Props
import akka.testkit.TestActorRef
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.log.TProxyLogReceive

object ActorNodeSpec {

  object TestActorNode {
    def apply(result: Result) = Props(classOf[TestActorNode], result)
  }

  class TestActorNode(result: Result) extends ActorNode {
    lazy val logging = new AkkaLogging(this)
    implicit val logReceive = new TProxyLogReceive()
    val onStart = result
  }

}

class ActorNodeSpec extends TestSpec("ActorNodeSpec") {

  import ActorNodeSpec._
  import ch.os.tproxy.common.behaviour.actor.{SendCmdTo, SendEvtTo}

  "ActorNode" when {
    "created" must {
      "process onStart result" in {
        object AddOne extends Behaviour {
          def onCommand = {
            case x: Int => this withCommand (x + 1)
          }

          def onEvent = Behaviour.emptyReceive
        }

        val behaviours = {
          SendEvtTo(self, false, "TestSpec") <->
            AddOne <->
            SendCmdTo(self, false, "TestSpec")
        }

        val result = (1 >>: behaviours) :<< 3
        val props = TestActorNode(result)
        val name = randomString("ActorNode")
        TestActorRef[ActorNode](props, self, name)

        expectMsgType[Int] shouldBe 2
        expectMsgType[Int] shouldBe 3
      }
    }
  }
}
