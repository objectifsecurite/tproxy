package ch.os.tproxy.test

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, PoisonPill, Props}
import akka.io.Tcp.Unbound
import akka.io.{IO, Tcp}
import akka.testkit.TestProbe
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}
import ch.os.tproxy.core.dissection.Dissector
import ch.os.tproxy.core.dissection.Dissector.{DissectorDo, DissectorDone}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.Request
import ch.os.tproxy.core.pcap.{PcapFrame, PcapSim}
import ch.os.tproxy.common.json.DissectionRequest
import ch.os.tproxy.common.json.JsonDissectTree.JsonDissectionAck

import scala.concurrent.duration.DurationInt

class DissectorSpec extends TestSpec("DissectorSpec") {

  class HasRunningDissectionClientServer {
    val dissectionServerPort = 8888
    val cltAddr = new Addr("localhost", 1234)
    val srvAddr = new Addr("localhost", 4567)

    val proxyActor = new TestProbe(system, "proxy")
    var dissectionServer = new TestProbe(system, "dissectionServer")

    var socketActor: Option[ActorRef] = None

    dissectionServerBind()

    val dissectorActor: ActorRef = {
      val props = Props(classOf[Dissector], logging, dissectionServerPort, logReceive)
      system.actorOf(props, "Dissector")
    }
    val pcapSim = new PcapSim(cltAddr, srvAddr)

    dissectionServer.expectMsgType[Tcp.Connected]
    val props = Props(classOf[TcpSegmentAggregatorSpec.AggregatingForwarder], logging, dissectionServer.ref, dissectionServer.lastSender)
    val tcpSegmentAggregator: ActorRef = system.actorOf(props, "TestAggregator")
    dissectionServer.send(dissectionServer.lastSender, Tcp.Register(tcpSegmentAggregator))

    def dissectionServerBind(): Unit = {
      dissectionServer.send(IO(Tcp), Tcp.Bind(dissectionServer.ref, new Addr("localhost", dissectionServerPort)))
      dissectionServer.expectMsgType[Tcp.Bound]
      socketActor = Some(dissectionServer.lastSender)
    }

    val unbindingProbe = new TestProbe(system)
    def stopDissectionServer(): Unit = {
      if (socketActor.isDefined) {
        unbindingProbe.send(socketActor.get, Tcp.Unbind)
        unbindingProbe.expectMsgType[Unbound]
        socketActor.get ! PoisonPill
        socketActor = None
      }
      system.stop(dissectionServer.ref)
    }

    def stopActors(): Unit = {
      stopDissectionServer()
      system.stop(proxyActor.ref)
      system.stop(dissectorActor)
      system.stop(tcpSegmentAggregator)
    }

    def sendSimpleRequest(data: Option[ByteString] = None): Array[PcapFrame] = {
      val httpGet = "GET / HTTP/1.1\r\n\r\n"
      val frames = pcapSim.request(ByteString(httpGet).toArray)
      frames.length shouldBe 2

      val cid = randomString("cid")
      val req = Request(cid, cltAddr).setSrvAddr(srvAddr).setFrames(frames)
      val cmd = if (data.isDefined) DissectorDo(req.setData(data.get)) else DissectorDo(req)
      proxyActor.send(dissectorActor, Command(cmd, needAck = false))
      cmd.msg.frames
    }
  }

  class TestSimpleRequestWithoutData extends HasRunningDissectionClientServer {
    val frames = sendSimpleRequest()

    frames foreach { frame =>
      val dissectionRequest = {

        val data = dissectionServer.expectMsgType[ByteString]
        DissectionRequest(data.utf8String)
      }
      dissectionRequest.pktData shouldBe Array.empty
      dissectionRequest.tcpData shouldBe frame.getBytes
      dissectionRequest.proto shouldBe ""
      dissectionServer.send(dissectionServer.lastSender, Command(ByteString(JsonDissectionAck.toJson), needAck = false))
    }


    stopActors()
  }

  class TestSimpleRequestWithData(stop: Boolean = true) extends HasRunningDissectionClientServer {
    val frames = sendSimpleRequest(Some(ByteString("sample data")))

    val dissectionRequest = {
      val data = dissectionServer.expectMsgType[ByteString]
      DissectionRequest(data.utf8String)
    }
    dissectionRequest.pktData shouldBe frames(0).getBytes
    dissectionRequest.tcpData shouldBe frames(1).getBytes
    dissectionRequest.proto shouldBe ""

    if (stop)
      stopActors()
  }

  class TestDissectionTimeout(stop: Boolean = true) extends TestSimpleRequestWithData(stop = false) {
    val msg = {
      val evt = proxyActor.expectMsgType[Event]
      evt.msg shouldBe a[DissectorDone]
      val resp = evt.msg.asInstanceOf[DissectorDone]
      resp.msg
    }
    msg.hasDissection shouldBe false
    if (stop)
      stopActors()
  }

  class TestReconnect extends TestDissectionTimeout(stop = false) {
    dissectionServer.expectMsgType[Tcp.Connected](4.seconds)
    stopActors()
  }

  "Dissector" when {
    "receiving a message without any data" must {
      "send a DissectionRequest for each frame to dissect to the dissection server" in new TestSimpleRequestWithoutData
    }
    "receiving a message with data" must {
      "send a DissectionRequest for the first frame and wait for a response" in new TestSimpleRequestWithData
      "timeout when no response is received and send the original message without dissection to the proxy" in new TestDissectionTimeout
      "try to reconnect to the dissection server after a timeout" in new TestReconnect
    }

  }
}
