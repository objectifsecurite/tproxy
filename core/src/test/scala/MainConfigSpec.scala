package ch.os.tproxy.test

import ch.os.tproxy.config.MainConfig

import scala.util.Failure

class MainConfigSpec extends TestSpec("MainConfigSpec") {
  "MainConfig" when {
    "a config file is provided via the cmd line" must {
      "fail if the file does not exist" in {
        val fname = randomString()
        val config = MainConfig load s"-c $fname"
        config shouldBe a[Failure[_]]
      }
    }
  }
}
