package ch.os.tproxy.test

import akka.util.ByteString
import ch.os.tproxy.core.behaviour.ssl.ClientHelloParser

class ClientHelloParserSpec extends TestSpec("ClientHelloParserSpec") {
  def hex2bytestring(hex: String) = {
    val hexNoSpace = hex replace(" ", "")
    ByteString(hexNoSpace.sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte))
  }

  val clientHello = hex2bytestring {
    "16 03 01 00 77 01 00 00 73 03 01 53 a8 3e cf 1b" +
      "df c0 a3 32 ba 61 05 04 74 20 35 37 b0 08 cc 0d" +
      "c7 88 cc e7 4b 67 ae 1a 7f c4 ce 00 00 38 c0 0a" +
      "c0 14 00 35 c0 05 c0 0f 00 39 00 38 c0 09 c0 13" +
      "00 2f c0 04 c0 0e 00 33 00 32 c0 07 c0 11 00 05" +
      "c0 02 c0 0c c0 08 c0 12 00 0a c0 03 c0 0d 00 16" +
      "00 13 00 04 00 ff 01 00 00 12 00 0a 00 08 00 06" +
      "00 17 00 18 00 19 00 0b 00 02 01 00"
  }

  // SNI = carol.sni.velox.ch
  val clientHelloSni = hex2bytestring {
    "16 03 01 00 b2 01 00 00 ae 03 01 53 a8 12 aa c6" +
      "ae 08 5e 68 48 49 08 0c 94 16 71 e4 3e 7b 1d 4e" +
      "ba 78 84 83 9e d4 1f 32 00 a0 1b 20 7a 52 21 42" +
      "5f 68 4f 4b 41 8b 40 c1 1d b8 2d 3e 87 18 16 28" +
      "91 7f 85 aa a6 a0 40 6c 04 23 d0 7c 00 38 c0 0a" +
      "c0 14 00 35 c0 05 c0 0f 00 39 00 38 c0 09 c0 13" +
      "00 2f c0 04 c0 0e 00 33 00 32 c0 07 c0 11 00 05" +
      "c0 02 c0 0c c0 08 c0 12 00 0a c0 03 c0 0d 00 16" +
      "00 13 00 04 00 ff 01 00 00 2d 00 0a 00 08 00 06" +
      "00 17 00 18 00 19 00 0b 00 02 01 00 00 00 00 17" +
      "00 15 00 00 12 63 61 72 6f 6c 2e 73 6e 69 2e 76" +
      "65 6c 6f 78 2e 63 68"
  }

  val clientHelloSimple = hex2bytestring {
    "16 03 01 00 41 01 00 00 3d 03 01 54 61 df 06 17" +
      "b2 fe aa 7f 4f 85 f3 ce 5d da 22 f7 d2 83 cc 84" +
      "ee 02 e8 1f c1 b5 8f 73 0d 76 14 00 00 16 00 33" +
      "00 39 00 2f 00 35 00 32 00 38 00 16 00 0a 00 13" +
      "00 05 00 66 01 00"
  }

  val clientHelloSslv2 = hex2bytestring {
    "80 46 01 03 01 00 2d 00 00 00 10 00 00 04 00 00" +
      "05 00 00 0a 01 00 80 07 00 c0 03 00 80 00 00 09" +
      "06 00 40 00 00 64 00 00 62 00 00 03 00 00 06 02" +
      "00 80 04 00 80 00 00 ff 77 30 d4 0c 06 5f b6 c6" +
      "88 a0 19 63 4b a5 b2 26"
  }

  "ClientHelloParser" must {
    "correctly identify TLS client_hello" in {
      (ClientHelloParser isSslClientHello clientHello) shouldBe true
      (ClientHelloParser isSslClientHello clientHelloSni) shouldBe true
      (ClientHelloParser isSslClientHello clientHelloSimple) shouldBe true
    }

    "correctly identify SSLv2 client_hello" in {
      (ClientHelloParser isSslClientHello clientHelloSslv2) shouldBe true
    }

    "be able to extract the server name" when {
      "the SNI extension is provided" in {
        val snis = ClientHelloParser extractSni clientHelloSni
        val hnames = snis map (_.utf8String)
        hnames shouldBe "carol.sni.velox.ch" :: Nil
      }
    }

    "correclty handle the absence of server name" when {
      "no SNI extension is provided" in {
        (ClientHelloParser extractSni clientHelloSimple) shouldBe Nil
      }
      "a SSLv2 client_hello is provided" in {
        (ClientHelloParser extractSni clientHelloSslv2) shouldBe Nil
      }
    }
  }
}
