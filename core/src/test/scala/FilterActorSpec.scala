package ch.os.tproxy.test

import java.net.{InetSocketAddress => Addr}
import akka.actor.Props
import akka.testkit.{TestProbe, TestActorRef}
import ch.os.tproxy.core.filter.FilterActor
import ch.os.tproxy.core.filter.FilterParser
import ch.os.tproxy.common.message.Response
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}

class FilterActorSpec extends TestSpec("FilterActorSpec") {

  abstract class HasFilterActor {
    val masterActor = new TestProbe(system)
    val filterActor = {
      val props = Props(classOf[FilterActor], masterActor.ref, logging, logReceive)
      val name = randomString("FilterActor")
      TestActorRef[FilterActor](props, name)
    }
    val proxyActor = new TestProbe(system)
  }

  class TestSimpleMessage extends HasFilterActor {
    val tointercept = Response("intercepted", new Addr("localhost", 4567))
    proxyActor send(filterActor, Event(tointercept, false))
    proxyActor.expectMsgType[Command].msg shouldBe tointercept
    masterActor.expectMsgType[Event].msg shouldBe tointercept
  }

  class TestAddFilter extends TestSimpleMessage {
    val testFilter = FilterParser("srv.port == 4567").get

    masterActor send(filterActor, {
      val cmd = FilterActor.AddFilter(testFilter, script = false)
      Command(cmd, false)
    })
  }

  class TestPass extends TestAddFilter {
    val pass = Response("pass", new Addr("localhost", 1234))
    proxyActor send(filterActor, Event(pass, false))
    proxyActor.expectMsgType[Command].msg shouldBe pass
    masterActor.expectMsgType[Event].msg shouldBe pass
  }

  class TestIntercept extends TestPass {
    proxyActor send(filterActor, Event(tointercept, false))

    val event = masterActor.expectMsgType[Event]
    event.msg shouldBe a[Response]

    val intercepted = event.msg.asInstanceOf[Response]

    intercepted.cid shouldBe tointercept.cid
    intercepted.hasIntercepted shouldBe true
  }

  class TestRemoveFilter extends TestIntercept {
    masterActor send(filterActor, {
      val cmd = FilterActor.RemoveFilter(testFilter, script = false)
      Command(cmd, false)
    })
  }

  class TestPassAfterRemove extends TestRemoveFilter {
    proxyActor send(filterActor, Event(tointercept, false))
    proxyActor.expectMsgType[Command].msg shouldBe tointercept
    masterActor.expectMsgType[Event].msg shouldBe tointercept
  }

  "FilterActor" must {
    "By default, forward messages to both master and proxy" in new TestSimpleMessage
    "Accept AddFilter command" in new TestAddFilter
    "Let go messages not to be intercepted" in new TestPass
    "Filter and tag messages to be intercepted" in new TestIntercept
    "Accept RemoveFilter command" in new TestRemoveFilter
    "Actually remove filters" in new TestPassAfterRemove
  }
}
