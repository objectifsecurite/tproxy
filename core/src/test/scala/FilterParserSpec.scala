package ch.os.tproxy.test

import ch.os.tproxy.core.filter.FilterParser
import ch.os.tproxy.common.message.{Message, Request, Response}
import ch.os.tproxy.common.message.implicits._

import java.net.InetSocketAddress

class FilterParserSpec extends TestSpec("FilterParserSpec") {
  val msg = new Response("someId")

  trait TestFilter {
    val filterString: String
    val pass: Message
    val fail: Message

    val result = FilterParser(filterString)
    result.isDefined shouldBe true
    val filter = result.get
    filter(pass) shouldBe true
    filter(fail) shouldBe false
  }

  class TestEmpty {
    val result = FilterParser("")
    result.isDefined shouldBe true
    val filter = result.get
    filter(msg) shouldBe true
  }

  class TestCltPort extends TestFilter {
    lazy val filterString = "clt.port == 1234"
    lazy val pass = msg.setCltAddr(new InetSocketAddress("localhost", 1234))
    lazy val fail = msg.setCltAddr(new InetSocketAddress("localhost", 4321))
  }

  class TestSrvPort extends TestFilter {
    lazy val filterString = "srv.port != 5555"
    lazy val pass = msg.setSrvAddr(new InetSocketAddress("localhost", 6666))
    lazy val fail = msg.setSrvAddr(new InetSocketAddress("localhost", 5555))
  }

  class TestCltIp extends TestFilter {
    lazy val filterString = "clt.ip == \"192.168.0.4\""
    lazy val pass = msg.setCltAddr(new InetSocketAddress("192.168.0.4", 1234))
    lazy val fail = msg.setCltAddr(new InetSocketAddress("212.165.1.8", 1234))
  }

  class TestSrvIp extends TestFilter {
    lazy val filterString = "srv.ip != 127.0.0.1"
    lazy val pass = msg.setSrvAddr(new InetSocketAddress("1.2.3.4", 5555))
    lazy val fail = msg.setSrvAddr(new InetSocketAddress("127.0.0.1", 5555))
  }

  class TestId extends TestFilter {
    lazy val filterString = "cid == \"test id\""
    lazy val pass = new Response("test id")
    lazy val fail = new Response("other id")
  }

  class TestClose extends TestFilter {
    lazy val filterString = "close"
    lazy val pass = msg.setClose(akka.io.Tcp.Closed)
    lazy val fail = msg
  }

  class TestData extends TestFilter {
    lazy val filterString = "!data"
    lazy val pass = msg
    lazy val fail = msg.setData(akka.util.ByteString("some data"))
  }

  class TestRequest extends TestFilter {
    lazy val filterString = "request"
    lazy val pass = new Request("id")
    lazy val fail = new Response("id")
  }

  class TestResponse extends TestFilter {
    lazy val filterString = "!response"
    lazy val pass = new Request("id")
    lazy val fail = new Response("id")
  }

  class TestAnd extends TestFilter {
    lazy val filterString = "data & cid == \"" + msg.cid + "\""
    lazy val pass = msg.setData(akka.util.ByteString("some data"))
    lazy val fail = msg
  }

  class TestOr extends TestFilter {
    lazy val filterString = "request | close"
    lazy val pass = msg.setClose(akka.io.Tcp.Closed)
    lazy val fail = msg
  }

  class TestAndParenthesis extends TestFilter {
    lazy val filterString = "(data & cid == \"" + msg.cid + "\")"
    lazy val pass = msg.setData(akka.util.ByteString("some data"))
    lazy val fail = msg
  }

  class TestOrParenthesis extends TestFilter {
    lazy val filterString = "(request | close)"
    lazy val pass = msg.setClose(akka.io.Tcp.Closed)
    lazy val fail = msg
  }

  class TestNot extends TestFilter {
    lazy val filterString = "!(request | close)"
    lazy val pass = msg
    lazy val fail = msg.setClose(akka.io.Tcp.Closed)
  }

  "FilterParser" must {
    "filter empty string correctly" in new TestEmpty
    "filter clt.port == correctly" in new TestCltPort
    "filter srv.port != correctly" in new TestSrvPort
    "filter clt.ip == \"\" correctly" in new TestCltIp
    "filter srv.ip != correctly" in new TestSrvIp
    "filter close correctly" in new TestClose
    "filter !data correctly" in new TestData
    "filter request correctly" in new TestRequest
    "filter !response correctly" in new TestResponse
    "filter & correctly" in new TestAnd
    "filter | correctly" in new TestOr
    "filter ( & ) correctly" in new TestAndParenthesis
    "filter ( | ) correctly" in new TestOrParenthesis
    "filter !( ) correctly" in new TestNot
  }
}
