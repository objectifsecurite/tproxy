package ch.os.tproxy.test

import java.net.InetSocketAddress
import scala.concurrent.duration._
import akka.io.{IO, Tcp}
import akka.actor.PoisonPill
import akka.testkit.{TestActorRef, TestProbe}
import ch.os.tproxy.core.TProxy
import ch.os.tproxy.common.message.{ClientAccepted, Request}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.{Event, Stop}
import ch.os.tproxy.core.log.TProxyLogReceive

class TProxySpec extends TestSpec("TProxySpec") {

  private val proxyAddr = new InetSocketAddress("localhost", 9080)
  private val serverAddr = new InetSocketAddress("localhost", 54819)


  class TestCreate {
    val bindOpts = List(TProxy.TranspSockOpt)
    val probe = new TestProbe(system)

    val bindMsg = Tcp.Bind(self, proxyAddr, options = bindOpts)
    probe.send(IO(Tcp), bindMsg)


    if (asRoot) {
      val addr = probe.expectMsgType[Tcp.Bound].localAddress
      val listener = probe.lastSender

      addr.getAddress shouldBe proxyAddr.getAddress
      addr.getPort shouldBe proxyAddr.getPort
      IO(Tcp) ! Tcp.Connect(proxyAddr)

      probe.send(listener, Tcp.Unbind)
      probe.expectMsgType[Tcp.Unbound]

      probe watch listener
      listener ! PoisonPill
      probe expectTerminated listener

    } else
      probe expectMsg Tcp.CommandFailed(bindMsg)
  }

  abstract class ProxyCreation {

    val master = new TestProbe(system, randomString("master"))
    implicit val config = new TProxy.Config {
      val logging = TProxySpec.this.logging
      val logReceive = new TProxyLogReceive()
      val master = ProxyCreation.this.master.ref
      val filter = None
      val dissector = None
      val pcapDumper = None
    }

    val proxy = TestActorRef[TProxy](TProxy(proxyAddr), self, randomString("TProxy"))
  }

  class TestStartSucceed extends ProxyCreation {
    val client = new TestProbe(system, randomString("client"))
    val addr = master.expectMsgType[TProxy.Bound].address

    addr.getAddress shouldBe proxyAddr.getAddress
    addr.getPort shouldBe proxyAddr.getPort

    client send(IO(Tcp), Tcp.Connect(serverAddr))
    val clientConnected = client.expectMsgType[Tcp.Connected]
    val clientConnection = client.lastSender
  }

  class TestStartFail extends ProxyCreation {
    val addr = master.expectMsgType[TProxy.BindFailed].address
    addr.getAddress shouldBe proxyAddr.getAddress
    addr.getPort shouldBe proxyAddr.getPort
  }

  trait StopProxy extends ProxyCreation {
    master watch proxy
    master send(proxy, Stop)

    {
      val addr = master.expectMsgType[TProxy.Unbound].address
      addr.getAddress shouldBe proxyAddr.getAddress
      addr.getPort shouldBe proxyAddr.getPort
    }

    (master fishForMessage 3.seconds) {
      case akka.actor.Terminated(`proxy`) => true
      case _ => false
    }

    system.stop(master.ref)
    system.stop(proxy)

  }

  class TestInterception extends TestStartSucceed {
    // The client believes it is connected to the server, but it is
    // indeed connected to the proxy.
    clientConnected.remoteAddress.getAddress shouldBe serverAddr.getAddress
    clientConnected.remoteAddress.getPort shouldBe serverAddr.getPort

    // ProxyActor notifies the master that a new client connection has
    // been intercepted.
    val accepted = master.expectMsgType[ClientAccepted]
    accepted.src shouldBe clientConnected.localAddress
    accepted.dst shouldBe clientConnected.remoteAddress

    // ProxyActor sends an empty request when it starts intercepting a
    // connection. The master will see this request since all messages
    // are forwarded to the master (normally a CLI or a GUI).

    val emptyRequest = {
      val event = master.expectMsgType[Event]
      event.msg shouldBe a[Request]
      event.msg.asInstanceOf[Request]
    }
    emptyRequest.hasData shouldBe false
  }

  "TProxy" when {
    val allowOrForbid = if (asRoot) "allow" else "forbid"
    val rights = if (asRoot) "sufficient" else "insufficient"

    s"the user has $rights rights" must {
      s"$allowOrForbid the creation of a transparent socket" in new TestCreate
      s"$allowOrForbid interception" in {
        asRoot match {
          case true => new TestStartSucceed with StopProxy
          case false => new TestStartFail
        }
      }
      "intercept connections and notify its master" inIfRootOrIgnore {
        new TestInterception with StopProxy
      }
    }
  }
}
