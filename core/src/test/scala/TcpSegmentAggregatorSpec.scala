package ch.os.tproxy.test

import java.util.Random
import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.io.Tcp.{ErrorClosed, Unbound}
import akka.testkit.TestProbe
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.tcp.TcpSegmentAggregator.buildSegmentWithHeader
import ch.os.tproxy.common.behaviour.tcp.TcpSegmentAggregator
import TcpSegmentAggregatorSpec.{AggregatingForwarder, largeMsg}
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.actor.message.Command
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.core.log.TProxyLogReceive

import scala.concurrent.duration.DurationInt

object TcpSegmentAggregatorSpec {

  class AggregatingForwarder(_logging: Logging, var msgHandler: ActorRef, connexion: ActorRef) extends NetNode {
    implicit lazy val logging = _logging forObject this
    implicit val logReceive = new TProxyLogReceive()

    def onStart: Behaviour.Result = Forwarder() <-> TcpSegmentAggregator() <-> SendNetCmdTo(connexion, "testConnexion")

    private case class Forwarder() extends OnEvent {
      def onEvent: Behaviour.Receive = {
        case data: ByteString =>
          msgHandler ! data
          this
        case _: ErrorClosed => this
      }
    }

  }

  lazy val largeMsg = {
    val bytes = new Array[Byte](80 * 1024)
    new Random nextBytes bytes
    ByteString(bytes)
  }
}

class TcpSegmentAggregatorSpec extends TestSpec("TcpSegmentAggregator") {

  class HasClientServerWithTcpAggregation {
    val client = new TestProbe(system, randomString("client"))


    val srvAddr = new Addr("localhost", 1234)
    val server = new TestProbe(system, randomString("server"))


    server.send(IO(Tcp), Tcp.Bind(server.ref, srvAddr))
    server.expectMsgType[Tcp.Bound]
    val socketActor = server.lastSender

    client.send(IO(Tcp), Tcp.Connect(srvAddr))

    server.expectMsgType[Tcp.Connected]
    val serverConnection = server.lastSender
    val serverProps = Props(classOf[AggregatingForwarder], logging, server.ref, serverConnection)
    val serverAggregator = system.actorOf(serverProps, randomString("serverAggregator"))
    server.send(server.lastSender, Tcp.Register(serverAggregator))


    client.expectMsgType[Tcp.Connected]
    val clientConnection = client.lastSender
    val clientProps = Props(classOf[AggregatingForwarder], logging, client.ref, clientConnection)
    val clientAggregator = system.actorOf(clientProps, randomString("clientAggregator"))
    client.send(client.lastSender, Tcp.Register(clientAggregator))

    lazy val unbindingProbe = new TestProbe(system)
    def stopActors() = {
      unbindingProbe.send(socketActor, Tcp.Unbind)
      unbindingProbe.expectMsgType[Unbound]
      system.stop(socketActor)
      system.stop(server.ref)
      system.stop(serverAggregator)
      system.stop(client.ref)
      system.stop(clientAggregator)
    }

  }

  class TestSingleMessage(simpleMsg: => ByteString) extends HasClientServerWithTcpAggregation {
    client.send(clientAggregator, Command(simpleMsg))
    val data = server.expectMsgType[ByteString]
    data shouldBe simpleMsg

    stopActors()
  }

  class TestSmallMessage extends TestSingleMessage(ByteString("small message"))

  class TestLargeMessage extends TestSingleMessage(largeMsg)

  class TestIncompleteMessage extends HasClientServerWithTcpAggregation {

    import TcpSegmentAggregator.byteOrder

    val data = ByteString("sample data")
    val incompleteSegment = ByteString.newBuilder.putInt(data.length + 5).append(data).result() // length announced in headers > data length
    clientConnection ! Tcp.Write(incompleteSegment)
    server.expectNoMessage(3.seconds)
    stopActors()
  }

  class TestIncompleteHeaders extends HasClientServerWithTcpAggregation {
    val data = ByteString("sample message")
    val segment = buildSegmentWithHeader(data)

    clientConnection ! Tcp.Write(segment.slice(0, TcpSegmentAggregator.headerLength - 1))
    server.expectNoMessage(2.seconds)
    clientConnection ! Tcp.Write(segment.slice(TcpSegmentAggregator.headerLength - 1, segment.length))
    val receivedData = server.expectMsgType[ByteString]
    receivedData shouldBe data
    stopActors()
  }

  "TcpAggregator" must {
    "correctly retrieve a small message sent over one TCP segment" in new TestSmallMessage()
    "correctly retrieve a large message sent over multiple TCP segments" in new TestLargeMessage()
    "wait for remaining data when an incomplete message is received" in new TestIncompleteMessage()
    "correctly retrieve a message that was split in its headers" in new TestIncompleteHeaders()
  }

}
