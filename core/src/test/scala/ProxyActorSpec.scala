package ch.os.tproxy.test

import java.net.InetSocketAddress
import java.util.Random
import scala.annotation.tailrec
import akka.actor.ActorRef
import akka.io.{IO, Tcp}
import akka.util.ByteString
import akka.testkit.{TestActorRef, TestProbe}
import ch.os.tproxy.core.dissection.Dissector
import ch.os.tproxy.core.actor.ProxyActor
import ch.os.tproxy.common.message.{ClientAccepted, ClientClosed, ClientConnected, ClientRefused, Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.Event
import ch.os.tproxy.common.log.{LogReceive, Logging}

class ProxyActorSpec extends TestSpec("ProxyActorSpec") {

  val helloMsg = ByteString("hello")
  val worldMsg = ByteString("world")

  abstract class ImplicitConfig {
    val master = new TestProbe(system)

    implicit val config = new ProxyActor.Config {
      implicit val logging: Logging = ProxyActorSpec.this.logging
      implicit val logReceive = new LogReceive()

      val master: ActorRef = ImplicitConfig.this.master.ref
      val filter = None
      val dissector = Some(Dissector.getActor)
      val pcapDumper = None
    }
  }

  abstract class ProxyClientServer extends ImplicitConfig {
    val proxy = new TestProbe(system)
    val client = new TestProbe(system)
    val server = new TestProbe(system)

    val (proxyAddr, proxyListener) = proxy.bind

    def serverAddr: InetSocketAddress
  }

  object Common {

    trait TestConnect extends ProxyClientServer {
      val clientAddr = client.connect(proxyAddr)
      val clientConnection = client.register

      val proxyConnection = {
        proxy.expectMsgType[Tcp.Connected]
        proxy.lastSender
      }

      val connectionId = "someId"

      val proxyActor = {
        val name = randomString("ProxyActor")
        val props = ProxyActor(connectionId, clientAddr, serverAddr, proxyConnection)
        TestActorRef[ProxyActor](props, name)
      }
    }

    trait TestClientAccepted extends TestConnect {
      val accepted = master.expectMsgType[ClientAccepted]
      accepted.id shouldBe connectionId
      accepted.src shouldBe clientAddr
      accepted.dst shouldBe serverAddr
    }

    trait TestEmptyRequest extends ImplicitConfig {
      val emptyRequest = {
        val event = master.expectMsgType[Event]
        event.msg shouldBe a[Request]
        event.msg.asInstanceOf[Request]
      }
      emptyRequest.hasData shouldBe false
    }

  }

  object Failure {

    class TestConnect extends ProxyClientServer with Common.TestConnect {
      lazy val serverAddr = new InetSocketAddress("127.0.0.1", 0)
    }

    class TestClientAccepted extends TestConnect with Common.TestClientAccepted

    class TestEmptyRequest extends TestClientAccepted with Common.TestEmptyRequest

  }

  object Success {

    class TestConnect extends ProxyClientServer with Common.TestConnect {
      lazy val serverAddr = {
        val addr = new InetSocketAddress("127.0.0.1", 0)
        server send(IO(Tcp), Tcp.Bind(server.ref, addr))
        server.expectMsgType[Tcp.Bound].localAddress
      }
      val serverListener = server.lastSender

      server.expectMsgType[Tcp.Connected]
      val serverConnection = server.register
    }

    class TestClientAccepted extends TestConnect with Common.TestClientAccepted

    class TestEmptyRequest extends TestClientAccepted with Common.TestEmptyRequest

  }

  class TestClientRefused extends Failure.TestEmptyRequest {
    val refused = master.expectMsgType[ClientRefused]
    refused.id shouldBe connectionId
  }

  class TestClientConnected extends Success.TestEmptyRequest {
    val connected = master.expectMsgType[ClientConnected]
    connected.id shouldBe connectionId
  }

  class TestHello extends TestClientConnected {
    client send(clientConnection, Tcp.Write(helloMsg))
    server expectMsg Tcp.Received(helloMsg)
  }

  class TestHelloMasterNotified extends TestHello {
    val request = {
      val event = master.expectMsgType[Event]
      event.msg shouldBe a[Request]
      event.msg.asInstanceOf[Request]
    }
    request.hasData shouldBe true
    request.data shouldBe helloMsg
  }

  class TestWorld extends TestHelloMasterNotified {
    server send(server.lastSender, Tcp.Write(worldMsg))
    client expectMsg Tcp.Received(worldMsg)
  }

  class TestWorldMasterNotified extends TestWorld {
    val response = {
      val event = master.expectMsgType[Event]
      event.msg shouldBe a[Response]
      event.msg.asInstanceOf[Response]
    }
    response.hasData shouldBe true
    response.data shouldBe worldMsg
  }

  trait ActorToWatch {
    lazy val actorToWatch = Option.empty[ActorRef]
    actorToWatch foreach watch
  }

  trait TestClose {
    def src: TestProbe

    def srcConnection: ActorRef

    def dst: TestProbe

    src send(srcConnection, Tcp.Close)
    src expectMsg Tcp.Closed
    dst expectMsg Tcp.PeerClosed
  }

  trait TestCloseMasterNotified {
    def master: TestProbe

    def connectionId: String

    val firstCloseMessage = {
      val event = master.expectMsgType[Event]
      event.msg shouldBe a[Message]
      event.msg.asInstanceOf[Message]
    }

    firstCloseMessage.cid shouldBe connectionId
    firstCloseMessage.hasClose shouldBe true

    val secondCloseMessage = {
      val event = master.expectMsgType[Event]
      event.msg shouldBe a[Message]
      event.msg.asInstanceOf[Message]
    }

    secondCloseMessage.cid shouldBe connectionId
    secondCloseMessage.hasClose shouldBe true

    val closedNotification = master.expectMsgType[ClientClosed]
    closedNotification.id shouldBe connectionId
  }

  class TestClientClose extends TestWorldMasterNotified with TestClose {
    lazy val src = client
    lazy val srcConnection = clientConnection
    lazy val dst = server
  }

  class TestServerClose extends TestWorldMasterNotified with TestClose {
    lazy val src = server
    lazy val srcConnection = serverConnection
    lazy val dst = client
  }

  trait TestTerminate extends TestWorldMasterNotified with ActorToWatch {
    override lazy val actorToWatch = Some(proxyActor)

    expectTerminated(proxyActor)

    serverListener ! Tcp.Unbind
    expectMsg(Tcp.Unbound)

    proxyListener ! Tcp.Unbind
    expectMsg(Tcp.Unbound)
  }

  class TestMaxPacketSize extends Success.TestConnect {
    val bigPacket = {
      val bytes = new Array[Byte](70 * 1024)
      new Random nextBytes bytes
      ByteString(bytes)
    }
    client send(clientConnection, Tcp.Write(bigPacket))

    @tailrec
    private def loop(packet: ByteString): Unit = if (packet.size > 0) {
      val data = server.expectMsgType[Tcp.Received].data
      val (p1, p2) = packet splitAt data.size
      data.size should be < 65535
      data shouldBe p1
      loop(p2)
    }

    loop(bigPacket)
  }

  "ProxyActor" when {
    "the server is not available" must {
      import Failure._
      "fail to connect to the server when a client connects" in new TestConnect
      "send the ClientAccepted message to master" in new TestClientAccepted
      "send the first empty request to master" in new TestEmptyRequest
      "send message ClientRefused to master" in new TestClientRefused
    }
    "the server is available" must {
      import Success._
      "connect to the server when a client connects" in new TestConnect
      "send message ClientAccepted to master" in new TestClientAccepted
      "send the first empty request to master" in new TestEmptyRequest
      "send message ClientConnected to master" in new TestClientConnected
      "transmit client requests to the server" in new TestHello
      "transmit client requests to the master" in new TestHelloMasterNotified
      "transmit server responses to the client" in new TestWorld
      "transmit server responses to the master" in new TestWorldMasterNotified
      "close server connection when client closes" in new TestClientClose
      "close client connection when server closes" in new TestServerClose
      "notify master when client closes" in new TestClientClose with TestCloseMasterNotified
      "notify master when server closes" in new TestServerClose with TestCloseMasterNotified
      "terminate after client has been closed" in new TestClientClose with TestTerminate
      "terminate after server has been closed" in new TestServerClose with TestTerminate
      "split packets bigger than 65,535 bytes" in new TestMaxPacketSize
    }
  }
}
