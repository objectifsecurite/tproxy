package ch.os.tproxy.test

import akka.actor.{ActorSystem, Props}
import akka.io.{IO, Tcp}
import akka.testkit.TestProbe
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}
import ch.os.tproxy.common.json.DissectionRequest
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.core.dissection.DissectionClient.DissectionResponse
import ch.os.tproxy.core.dissection.{DissectionClient, Dissector}
import ch.os.tproxy.core.pcap.PcapFrame

import java.net.InetSocketAddress
import java.util.concurrent.ThreadLocalRandom

trait HasRunningDissector {
  Dissector.startWiresharkDissector(wait = true)
}

class HasDissectionClient(logging: Logging, logReceive: LogReceive)(implicit system: ActorSystem) extends HasRunningDissector {
  val dissectorProbe = TestProbe("dissectorProbe")
  val addr = new InetSocketAddress("localhost", 9082)
  dissectorProbe.send(IO(Tcp), Tcp.Connect(addr))
  dissectorProbe.expectMsgType[Tcp.Connected]


  val props = Props(classOf[DissectionClient], logging, logReceive, dissectorProbe.lastSender, dissectorProbe.ref)
  val cid = "%08x" format ThreadLocalRandom.current.nextInt
  val dissectionClient = system actorOf(props, s"TestDissectionClient-$cid")

  def dissectFrameAs(frame: PcapFrame, proto: String) = {
    val req = new DissectionRequest(frame.getBytes, Array.empty, proto)
    dissectionClient ! Command(req, needAck = false)
    val evt = dissectorProbe.expectMsgType[Event]
    val dissection = evt.msg.asInstanceOf[DissectionResponse].dissection
    if (dissection.isDefined) dissection.get else null
  }

  def dissectFrame(frame: PcapFrame) = dissectFrameAs(frame, "")
}