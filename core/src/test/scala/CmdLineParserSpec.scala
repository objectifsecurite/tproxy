package ch.os.tproxy.test

import ch.os.tproxy.cli.CmdLineParser

import scala.util.{Success, Failure}
import com.typesafe.config.ConfigFactory

class CmdLineParserSpec extends TestSpec("CmdLineParserSpec") {
  "CmdLineParser" must {
    "fail if an unknown option is provided" in {
      val config = new CmdLineParser parse "-x"
      config shouldBe a[Failure[_]]
    }

    "succeed when parsing known options" in {
      val config = new CmdLineParser parse "-d dump.pcap -c tproxy.conf -D"
      config shouldBe a[Success[_]]

      for (c <- config) {
        (c getString "tproxy.dump-file") shouldBe "dump.pcap"
        (c getString "tproxy.config-file") shouldBe "tproxy.conf"
        (c getBoolean "tproxy.disable-dump") shouldBe true
      }
    }

    "support the -h and --help options" in {
      val parser = new CmdLineParser
      val configs = List("-h", "--help") map (parser parse _)

      for (config <- configs) {
        config shouldBe a[Success[_]]
        for (c <- config) (c getString "tproxy.usage-text") shouldBe parser.usage
      }
    }

    "display default values in usage message" in {
      val dumpFile = "dump.pcap"
      val configFile = "tproxy.conf"
      val disableDump = "true"

      val defaultConfig = ConfigFactory.parseString(
        s"""tproxy {
	      dump-file: $dumpFile
	      config-file: $configFile
	      disable-dump: $disableDump
	    }"""
      )

      val usage = new CmdLineParser(defaultConfig).usage

      (usage contains s"default: $dumpFile") shouldBe true
      (usage contains s"default: $configFile") shouldBe true
      (usage contains s"default: $disableDump") shouldBe true
    }
  }
}
