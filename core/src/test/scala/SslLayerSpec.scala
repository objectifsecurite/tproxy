package ch.os.tproxy.test

import javax.net.ssl.SSLEngine
import java.net.InetSocketAddress
import java.security.cert.X509Certificate
import scala.annotation.tailrec
import akka.actor.{ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import akka.testkit.{TestActorRef, TestProbe}
import ch.os.tproxy.core.behaviour.ssl.SslLayer
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.core.ssl.{SSLEngineFactory, SslContext, SslSocket}
import ch.os.tproxy.test.ssl.SslServerSocket
import ch.os.tproxy.common.behaviour.actor.{ActorNode, SendCmdTo, SendEvtTo}
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.log.TProxyLogReceive
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder
import org.bouncycastle.asn1.x500.style.{BCStyle, IETFUtils}

import java.io.File
import java.security.KeyStore

object SslLayerSpec {
  val supportedProtocols = Array("TLSv1.2")

  class ClientSslActor(connection: ActorRef, engine: SSLEngine)
    extends SslActor(connection, engine, true)

  class ServerSslActor(connection: ActorRef, engine: SSLEngine)
    extends SslActor(connection, engine, false)

  class SslActor(connection: ActorRef, engine: SSLEngine, client: Boolean)
    extends ActorNode {

    implicit lazy val logging = new AkkaLogging(this)
    implicit val logReceive = new TProxyLogReceive()

    def onStart = {
      connection ! Tcp.Register(self)

      SendEvtTo(context.parent, false, "parent") <->
        SslLayer(engine, client) <->
        SendCmdTo(connection, false, "connection")
    }

    override def initialize(init: Behaviour.Result) = {
      super.initialize(init) <-> new OnEvent {
        def onEvent = {
          case cmd: Tcp.Command => this withEvent Command(cmd, false)
          case evt: Tcp.Event => this withEvent Event(evt, false)
          case _ => require(false, "This is unexpected"); this
        }
      }
    }
  }

}

class SslLayerSpec extends TestSpec("SslLayerSpec") {

  import SslLayerSpec._
  import SslLayer.CertificateReceived

  implicit class ProxyProbe(probe: TestProbe) {
    def autoForward(cmdActor: ActorRef, evtActor: ActorRef) = probe autoReceive {
      case cmd: Tcp.Command => cmdActor ! cmd
      case evt: Tcp.Event => evtActor ! evt
    }

    def stopAutoForward() = probe.stopAutoReceive
  }

  object ServerSide {

    abstract class ClientServer {
      val server = new TestProbe(system, randomString("server"))
      system actorOf SslServerSocket(server.ref)

      val serverAddress = server.expectMsgType[Tcp.Bound].localAddress
      val client = new TestProbe(system, randomString("client"))
      val clientConnection = {
        client send(IO(Tcp), Tcp.Connect(serverAddress))
        client.expectMsgType[Tcp.Connected]
        client.lastSender
      }
    }

    abstract class HasClientSslActor extends ClientServer {
      val connection: ActorRef

      val sslactor = {
        val engine = SslContext.createEngine
        engine.setEnabledProtocols(supportedProtocols)
        val props = Props(classOf[ClientSslActor], connection, engine)
        val name = randomString("ClientSslActor")
        TestActorRef[ClientSslActor](props, client.ref, name)
      }

      val serverConnection = {
        server.expectMsgType[Tcp.Connected]
        server.lastSender
      }

      serverConnection ! Tcp.Register(server.ref)
    }

    class TestConnectToServer extends HasClientSslActor {
      lazy val connection = clientConnection
      val certificates = client.expectMsgType[CertificateReceived].certificates
    }

    class TestHello extends TestConnectToServer {
      val helloString = ByteString("Hello")
      client send(sslactor, Tcp.Write(helloString))

      val helloReceived = server.expectMsgType[Tcp.Received]
      helloReceived.data shouldBe helloString
    }

    class TestWorld extends TestHello {
      val worldString = ByteString("World")
      serverConnection ! Tcp.Write(worldString)

      val worldReceived = client.expectMsgType[Tcp.Received]
      worldReceived.data shouldBe worldString
    }

    class TestClientClose extends TestWorld {
      client send(sslactor, Tcp.Close)
      client.expectMsgType[Tcp.ConnectionClosed]
      server.expectMsgType[Tcp.ConnectionClosed]
    }

    /* Simulate a connection gracefully closed by the server, i.e., a
     * close_notify is sent by the server before closing the socket.
     */
    class TestServerClose extends TestWorld {
      server send(serverConnection, Tcp.Close)
      client.expectMsgType[Tcp.ConnectionClosed]
      server.expectMsgType[Tcp.ConnectionClosed]
    }

    /* Simulate a connection abruptly closed by the server, i.e., no
     * close_notify is sent by the server before closing the socket.
     */
    class TestServerClosed extends TestWorld {
      sslactor ! Tcp.PeerClosed
      client.expectMsgType[Tcp.ConnectionClosed]
    }

  }

  object Handshaking {

    abstract class PauseHandshake extends ServerSide.HasClientSslActor {
      lazy val proxy = new TestProbe(system, randomString("proxy"))
      lazy val connection = proxy.ref

      // register the proxy as the handler of the connection
      proxy.expectMsgType[Tcp.Register]
      proxy send(clientConnection, Tcp.Register(proxy.ref))

      // forward client_hello to the server
      proxy send(clientConnection, proxy.expectMsgType[Tcp.Write])


      // forward server_hello + certificate to the sslactor (client)
      proxy send(sslactor, proxy.expectMsgType[Tcp.Received])

      // forward server_key_exchange + server_hello_done to the sslactor (client)
      proxy send(sslactor, proxy.expectMsgType[Tcp.Received])

      // forward 2nd handshake message to the server
      proxy send(clientConnection, proxy.expectMsgType[Tcp.Write])

      // catch the 2nd handshake message sent by the server
      val sndHandshakeMsg = proxy.expectMsgType[Tcp.Received]
    }

    /* Simulate the case where a client starts a handshake with the
     * server and closes the connection before the handshake is over.
     */
    class TestCloseWithoutData extends PauseHandshake {
      // the client wants to close the connection
      client send(sslactor, Tcp.Close)

      // send the 2nd handshake message to the sslactor
      proxy send(sslactor, sndHandshakeMsg)

      // forward messages exchanged between sslactor and server until
      // handshake if over
      proxy autoForward(clientConnection, sslactor)
      client.expectMsgType[CertificateReceived]
      proxy.stopAutoForward

      // both side expect the connection to be closed
      client.expectMsgType[Tcp.ConnectionClosed]
      server.expectMsgType[Tcp.ConnectionClosed]
    }

    /* Simulate the case where a client send data during the handshake
     * and closes the connection while the handshake is still ongoing.
     */
    class TestCloseWithData extends PauseHandshake {
      // the client send data and close the connection
      val helloMsg = ByteString("Hello")
      client send(sslactor, Tcp.Write(helloMsg))
      client send(sslactor, Tcp.Close)

      // resume handshake
      proxy send(sslactor, sndHandshakeMsg)

      // forward messages exchanged between sslactor and server until
      // handshake is over
      proxy autoForward(clientConnection, sslactor)
      client.expectMsgType[CertificateReceived]
      proxy.stopAutoForward

      // the server expects to receive the data
      server.expectMsgType[Tcp.Received]

      // both side expect the connection to be closed
      server.expectMsgType[Tcp.ConnectionClosed]
      client.expectMsgType[Tcp.ConnectionClosed]
    }

  }

  object ClientSide {

    class TestConnectToClient {
      val server = new TestProbe(system, randomString("server"))
      val addr = {
        val addr = new InetSocketAddress("localhost", 0)
        server send(IO(Tcp), Tcp.Bind(server.ref, addr))
        server.expectMsgType[Tcp.Bound].localAddress
      }

      val client = new TestProbe(system, randomString("client"))
      system actorOf SslSocket(client.ref, addr)

      val clientConnection = {
        client.expectMsgType[Tcp.Connected]
        client.lastSender ! Tcp.Register(client.ref)
        client.lastSender
      }

      val connection = {
        server.expectMsgType[Tcp.Connected]
        server.lastSender
      }

      val sslactor = {
        val engine = SslServerSocket.sslContext.createSSLEngine
        engine.setEnabledProtocols(supportedProtocols)
        val props = Props(classOf[ServerSslActor], connection, engine)
        val name = randomString("ServerSslActor")
        TestActorRef[ServerSslActor](props, server.ref, name)
      }
    }

    class TestHello extends TestConnectToClient {
      val helloMsg = ByteString("Hello hello my friend")
      clientConnection ! Tcp.Write(helloMsg)
      server.expectMsgType[Tcp.Received].data shouldBe helloMsg
    }

    class TestWorld extends TestHello {
      val worldMsg = ByteString("World world world")
      server send(sslactor, Tcp.Write(worldMsg))
      client.expectMsgType[Tcp.Received].data shouldBe worldMsg
    }

    class TestServerClose extends TestWorld {
      server send(sslactor, Tcp.Close)
      server expectMsg Tcp.Closed
      client expectMsg Tcp.PeerClosed
    }

    class TestClientClose extends TestWorld {
      clientConnection ! Tcp.Close
      server.expectMsgType[Tcp.ConnectionClosed]
    }

  }

  object SpecialCase {

    trait ReceiveData {
      val dest: TestProbe

      @tailrec
      protected final def receiveData(data: ByteString): ByteString = {
        dest.expectMsgType[Tcp.Event] match {
          case Tcp.Received(bytes) => receiveData(data ++ bytes)
          case _: Tcp.ConnectionClosed => data
        }
      }
    }

    class TestBigDataToServer extends ClientSide.TestConnectToClient with ReceiveData {
      val dest = server
      val helloMsg = ByteString("Hello hello my friend" * 1000)
      clientConnection ! Tcp.Write(helloMsg)
      clientConnection ! Tcp.Close
      receiveData(ByteString.empty) shouldBe helloMsg
    }

    class TestBigDataToClient extends ClientSide.TestConnectToClient with ReceiveData {
      val dest = client
      val hugeMsg = ByteString("This is a huge data!!!" * 1000)
      sslactor ! Tcp.Write(hugeMsg)

      // data received by the client means that the handshake is done
      val data = client.expectMsgType[Tcp.Received].data

      // handshake done and data sent, the connection can be closed
      sslactor ! Tcp.Close
      receiveData(data) shouldBe hugeMsg
    }

    class HasProxy {
      val server = new TestProbe(system)
      system actorOf SslServerSocket(server.ref)

      val srvAddr = server.expectMsgType[Tcp.Bound].localAddress

      val client = new TestProbe(system)
      val connection = {
        client send(IO(Tcp), Tcp.Connect(srvAddr))
        client.expectMsgType[Tcp.Connected]
        client.lastSender
      }

      val serverConnection = {
        server.expectMsgType[Tcp.Connected]
        server.lastSender ! Tcp.Register(server.ref)
        server.lastSender
      }

      val proxy = new TestProbe(system)
      connection ! Tcp.Register(proxy.ref)

      val sslactor = {
        val engine = SslContext.createEngine
        engine.setEnabledProtocols(supportedProtocols)
        val props = Props(classOf[ClientSslActor], proxy.ref, engine)
        val name = randomString("ClientSslActor")

        TestActorRef[ClientSslActor](props, client.ref, name)
      }

      proxy.expectMsgType[Tcp.Register]
    }

    class TestWriteWhileHandshaking extends HasProxy {
      connection ! proxy.expectMsgType[Tcp.Write]

      val (first, second) = {
        val recv = proxy.expectMsgType[Tcp.Received].data
        recv.splitAt(recv.length / 2)
      }

      val testData = ByteString("This is a test hello hi")

      sslactor ! Tcp.Received(first)
      sslactor ! Tcp.Write(testData)
      sslactor ! Tcp.Received(second)

      proxy autoForward(connection, sslactor)
      client.expectMsgType[CertificateReceived]
      proxy.stopAutoForward

      server.expectMsgType[Tcp.Received].data shouldBe testData
      serverConnection ! Tcp.Write(testData)
      sslactor ! proxy.expectMsgType[Tcp.Received]

      client.expectMsgType[Tcp.Received].data shouldBe testData
    }

    trait IsHandshaking {
      val client = new TestProbe(system)
      val server = new TestProbe(system)
      val proxy = new TestProbe(system)

      val cltSslactor = {
        val engine = SslContext.createEngine
        engine.setEnabledProtocols(supportedProtocols)
        val props = Props(classOf[ClientSslActor], proxy.ref, engine)
        val name = randomString("ClientSslActor")

        TestActorRef[ClientSslActor](props, client.ref, name)
      }

      val srvSslactor = {
        val engine = SslServerSocket.sslContext.createSSLEngine
        engine.setEnabledProtocols(supportedProtocols)
        engine.setEnabledProtocols(Array("TLSv1.2"))
        val props = Props(classOf[ServerSslActor], proxy.ref, engine)
        val name = randomString("ServerSslActor")

        TestActorRef[ServerSslActor](props, server.ref, name)
      }

      proxy.expectMsgType[Tcp.Register]
      srvSslactor ! Tcp.Received(proxy.expectMsgType[Tcp.Write].data) // Client Hello
      proxy.expectMsgType[Tcp.Register]
      cltSslactor ! Tcp.Received(proxy.expectMsgType[Tcp.Write].data) // Server Hello, Certificate, Server Key Exchange, Server Hello Done
      srvSslactor ! Tcp.Received(proxy.expectMsgType[Tcp.Write].data) // Client Key Exchange, Change Cipher Spec, Encrypted Handshake Message

      val last = proxy.expectMsgType[Tcp.Write] //Change Cipher Spec,  Encrypted Handshake Message
    }

    class TestReceiveWhileHandshaking extends IsHandshaking {
      srvSslactor ! Tcp.Write(ByteString("coucou"))
      var msg = proxy.expectMsgType[Tcp.Write]

      cltSslactor ! Tcp.Received(last.data ++ msg.data)

      client.expectMsgType[CertificateReceived]
      client.expectMsgType[Tcp.Received]
    }

    abstract class HasCustomKeystore {
      val password = "password".toCharArray
      val keystore = {
        val ks = KeyStore.getInstance("PKCS12")
        val inStream = classOf[HasCustomKeystore] getResourceAsStream  "/keystore.p12"
        ks.load(inStream, password)
        inStream.close
        ks
      }
      val expectedCerts = keystore.getCertificateChain("ca")
      val expectedIssuer = "CN=TestCustomKeystore, OU=TestCustomKeystore, O=TestCustomKeystore, L=TestCustomKeystore, ST=TestCustomKeystore, C=CH"
      val file = {
        val filename = "customkeystore.p12"
        Option(System getProperty "app.home") match {
          case None => new File(filename)
          case Some(dir) => new File(dir, filename)
        }
      }
      SSLEngineFactory.saveKeystore(file, keystore, password)
    }

    class TestCustomKeystore extends HasCustomKeystore with IsHandshaking {
      cltSslactor ! Tcp.Received(last.data)

      val receivedCerts = client.expectMsgType[CertificateReceived].certificates
      receivedCerts shouldEqual expectedCerts
      receivedCerts(0).asInstanceOf[X509Certificate].getIssuerDN.getName shouldEqual expectedIssuer
    }

    class TestCloseNotifyAfterCloseCommand extends HasProxy {
      proxy autoForward(connection, sslactor)
      client.expectMsgType[CertificateReceived]
      proxy.stopAutoForward

      sslactor ! Tcp.Close
      connection ! proxy.expectMsgType[Tcp.Write] // Close Notify

      val closeCmd = proxy.expectMsgType[Tcp.CloseCommand]

      sslactor ! proxy.expectMsgType[Tcp.Received] // Close Notify response
      connection ! closeCmd

      sslactor ! proxy.expectMsgType[Tcp.ConnectionClosed]
      client.expectMsgType[Tcp.ConnectionClosed] // Forwarded by sslactor
    }

    class TestCloseNotifyAfterCloseNotify extends HasProxy {
      proxy autoForward(connection, sslactor)
      client.expectMsgType[CertificateReceived]
      proxy.stopAutoForward

      val stopMsg = ByteString("stop")

      sslactor ! Tcp.Write(stopMsg)
      connection ! proxy.expectMsgType[Tcp.Write]

      server.expectMsgType[Tcp.Received].data shouldBe stopMsg

      serverConnection ! Tcp.Close
      sslactor ! proxy.expectMsgType[Tcp.Received] // Server's close_notify

      // Order varies ...
      proxy.expectMsgType[Tcp.Message] match {
        case cmd: Tcp.Write => { // SslActor's close_notify
          connection ! cmd
          sslactor ! proxy.expectMsgType[Tcp.ConnectionClosed] // PeerClosed
        }
        case evt: Tcp.ConnectionClosed => { // PeerClosed
          sslactor ! evt
          connection ! proxy.expectMsgType[Tcp.Write] // SslActor's close_notify
        }
        case _ => require(false, s"Tcp.Write or Tcp.ConnectionClosed expected")
      }

      client.expectMsgType[Tcp.ConnectionClosed] // Forwarded by sslactor
    }

  }

  object FakeCert {

    class TestFakeCert {
      val certificates = new ServerSide.TestConnectToServer().certificates

      val (cert, host) = {
        require(certificates.length == 1, "only one certificate is expected")
        val certificate = certificates.head.asInstanceOf[X509Certificate]

        val x500name = new JcaX509CertificateHolder(certificate).getSubject
        val cn = x500name.getRDNs(BCStyle.CN).head
        (certificate, IETFUtils.valueToString(cn.getFirst.getValue))
      }

      val server = new TestProbe(system, randomString("server"))
      val addr = {
        val addr = new InetSocketAddress("localhost", 0)
        server send(IO(Tcp), Tcp.Bind(server.ref, addr))
        server.expectMsgType[Tcp.Bound].localAddress
      }

      val client = new TestProbe(system, randomString("client"))
      system actorOf SslSocket(client.ref, addr)

      val clientConnection = {
        client.expectMsgType[Tcp.Connected]
        client.lastSender ! Tcp.Register(client.ref)
        client.lastSender
      }

      val serverConnection = {
        server.expectMsgType[Tcp.Connected]
        server.lastSender
      }

      val srvSslActor = {
        val sslfakeengine = SSLEngineFactory.getEngine(host, cert)
        sslfakeengine.setEnabledProtocols(supportedProtocols)
        val srvProps = Props(classOf[ServerSslActor], serverConnection, sslfakeengine)
        val srvName = randomString("ServerSslActor")
        TestActorRef[ServerSslActor](srvProps, server.ref, srvName)
      }

      val testMsg = ByteString("Testing fake certificate")
      clientConnection ! Tcp.Write(testMsg)

      server.expectMsgType[Tcp.Received].data shouldBe testMsg
    }

  }

  "A SSL Behaviour" when {
    "acting as a client, talking to a server" must {
      import ServerSide._
      "connect to the server and forward the certificate" in new TestConnectToServer
      "forward queries once connected" in new TestHello
      "forward responses as well" in new TestWorld
      "handle a Tcp.Close from client correctly" in new TestClientClose
      "handle a proper server close correctly" in new TestServerClose
      "handle a Tcp.Closed from server correctly" in new TestServerClosed
    }
    "acting as a server, talking to a client" must {
      import ClientSide._
      "allow the client to connect" in new TestConnectToClient
      "forward queries once connected" in new TestHello
      "forward responses once connected" in new TestWorld
      "handle Tcp.Close from server correctly" in new TestServerClose
      "hande a client close correctly" in new TestClientClose
    }
    "handshaking" when {
      import Handshaking._
      "a Tcp.Close command is received" must {
        "finish handshake before closing" in new TestCloseWithoutData
        "send data before closing" in new TestCloseWithData
      }
    }
    "receiving special data" must {
      import SpecialCase._
      "forward large amount of data to server" in new TestBigDataToServer
      "forward large amount of data to client" in new TestBigDataToClient
      "buffer writes arriving during handshake" in new TestWriteWhileHandshaking
      "handle receives directly after handshake" in new TestReceiveWhileHandshaking
      "use the provided custom PKCS12 keystore as it is" in new TestCustomKeystore
      "send close_notify when receiving a Tcp.Close" in new TestCloseNotifyAfterCloseCommand
      "send close_notify when receiving a close notify" in new TestCloseNotifyAfterCloseNotify
    }
    "using a fake certificate" must {
      import FakeCert._
      "use it" in new TestFakeCert
    }
  }
}
