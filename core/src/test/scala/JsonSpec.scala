package ch.os.tproxy.test

import java.net.{InetSocketAddress => Addr}
import akka.util.ByteString
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.json._
import ch.os.tproxy.common.message.{Request, Response}
import upickle.default._

object JsonSpec {

  val cltAddr = new Addr("192.168.5.100", 55555)
  val srvAddr = new Addr("80.74.147.72", 80)
  val tempReq = Request("abcd1234", cltAddr) setSrvAddr srvAddr
  val reqData = ByteString(
    """GET / HTTP/1.1
Host: www.objectif-securite.ch
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Connection: keep-alive

""".replace("\n", "\r\n"))
  val respData = ByteString(
    """HTTP/1.1 200 OK
Date: Thu, 03 Dec 2015 12:33:04 GMT
Server: Apache
X-Powered-By: PleskLine
Keep-Alive: timeout=5, max=10
Connection: Keep-Alive
Transfer-Encoding: chunked
Content-Type: text/html

<!DOCTYPE html>
éà£

""".replace("\n", "\r\n"))

  val req = tempReq setData (reqData)
  val resp = Response("abcd1234", srvAddr) setCltAddr cltAddr setData respData
  val close = tempReq setClose akka.io.Tcp.Closed

  val serialisedReq = """{"cid":"abcd1234","msg_type":"request","cltAddr":{"ip":"192.168.5.100","port":55555},"srvAddr":{"ip":"80.74.147.72","port":80},"close":false,"data":"R0VUIC8gSFRUUC8xLjENCkhvc3Q6IHd3dy5vYmplY3RpZi1zZWN1cml0ZS5jaA0KVXNlci1BZ2VudDogTW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0OyBydjo0Mi4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94LzQyLjANCkFjY2VwdDogdGV4dC9odG1sLGFwcGxpY2F0aW9uL3hodG1sK3htbCxhcHBsaWNhdGlvbi94bWw7cT0wLjksKi8qO3E9MC44DQpBY2NlcHQtTGFuZ3VhZ2U6IGVuLVVTLGVuO3E9MC41DQpBY2NlcHQtRW5jb2Rpbmc6IGd6aXAsIGRlZmxhdGUNCkROVDogMQ0KQ29ubmVjdGlvbjoga2VlcC1hbGl2ZQ0KDQo="}"""
  val serialisedResp = """{"cid":"abcd1234","msg_type":"response","cltAddr":{"ip":"192.168.5.100","port":55555},"srvAddr":{"ip":"80.74.147.72","port":80},"close":false,"data":"SFRUUC8xLjEgMjAwIE9LDQpEYXRlOiBUaHUsIDAzIERlYyAyMDE1IDEyOjMzOjA0IEdNVA0KU2VydmVyOiBBcGFjaGUNClgtUG93ZXJlZC1CeTogUGxlc2tMaW5lDQpLZWVwLUFsaXZlOiB0aW1lb3V0PTUsIG1heD0xMA0KQ29ubmVjdGlvbjogS2VlcC1BbGl2ZQ0KVHJhbnNmZXItRW5jb2Rpbmc6IGNodW5rZWQNCkNvbnRlbnQtVHlwZTogdGV4dC9odG1sDQoNCjwhRE9DVFlQRSBodG1sPg0Kw6nDoMKjDQoNCg=="}"""
  val serialisedClose = """{"cid":"abcd1234","msg_type":"request","cltAddr":{"ip":"192.168.5.100","port":55555},"srvAddr":{"ip":"80.74.147.72","port":80},"close":true,"data":""}"""
}

class JsonSpec extends TestSpec("JsonSpec") {

  import JsonSpec._

  "The JSON serialiser" must {
    "serialise correctly a Request" in {
      write(MessageJson(req)) shouldBe serialisedReq
    }
    "serialise correctly a Response" in {
      write(MessageJson(resp)) shouldBe serialisedResp
    }
    "deserialise correctly a Request" in {
      val newReq = read[MessageJson](serialisedReq).toReq
      newReq.data shouldBe req.data
      newReq.cid shouldBe req.cid
      newReq.cltAddr shouldBe req.cltAddr
      newReq.srvAddr shouldBe req.srvAddr
    }
    "deserialise correctly a Response" in {
      val newResp = read[MessageJson](serialisedResp).toReq
      newResp.data shouldBe resp.data
      newResp.cid shouldBe resp.cid
      newResp.cltAddr shouldBe resp.cltAddr
      newResp.srvAddr shouldBe resp.srvAddr
    }
    "deserialise correctly a CloseRequest" in {
      val newClose = read[MessageJson](serialisedClose).toReq
      newClose.close shouldBe close.close
      newClose.cid shouldBe close.cid
      newClose.cltAddr shouldBe close.cltAddr
      newClose.srvAddr shouldBe close.srvAddr
    }
  }
}
