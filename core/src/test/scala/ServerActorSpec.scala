package ch.os.tproxy.test

import java.net.InetSocketAddress
import akka.io.Tcp
import akka.util.ByteString
import akka.testkit.{TestProbe, TestActorRef}
import ch.os.tproxy.core.actor.ServerActor
import ch.os.tproxy.common.message.{Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}

class ServerActorSpec extends TestSpec("ServerActorSpec") {

  abstract class ServerProbe {
    val server = new TestProbe(system)

    def serverAddr: InetSocketAddress
  }

  trait TestConnect extends ServerProbe {
    val proxy = new TestProbe(system)

    val serverActor = {
      val props = ServerActor("SomeId", serverAddr)
      val name = randomString("ServerActor")
      TestActorRef[ServerActor](props, proxy.ref, name)
    }
  }

  class TestConnectUnbound extends ServerProbe with TestConnect {
    lazy val serverAddr = new InetSocketAddress("127.0.0.1", 0)

    val connectResponse = {
      val event = proxy.expectMsgType[Event]
      event.msg shouldBe a[Response]
      event.msg.asInstanceOf[Response]
    }
    connectResponse.hasClose shouldBe true
    connectResponse.close shouldBe Tcp.Aborted
  }

  class TestConnectBound extends ServerProbe with TestConnect {
    lazy val (serverAddr, _) = server.bind

    server.expectMsgType[Tcp.Connected].localAddress shouldBe serverAddr
    server.register

    val connectResponse = {
      val event = proxy.expectMsgType[Event]
      event.msg shouldBe a[Tcp.Connected]
      event.msg.asInstanceOf[Tcp.Connected]
    }

    connectResponse.remoteAddress shouldBe serverAddr
  }

  class TestServerData extends TestConnectBound {
    val dataMsg = ByteString("hello")
    server send(serverActor, Tcp.Received(dataMsg))

    val dataResponse = {
      val event = proxy.expectMsgType[Event]
      event.msg shouldBe a[Response]
      event.msg.asInstanceOf[Response]
    }

    dataResponse.hasSrvAddr shouldBe true
    dataResponse.srvAddr shouldBe serverAddr
    dataResponse.hasCltAddr shouldBe false
    dataResponse.hasData shouldBe true
    dataResponse.data shouldBe dataMsg
  }

  class TestClientRequest extends TestServerData {
    val worldMsg = ByteString("WORLD")
    val clientAddr = new InetSocketAddress("127.0.0.1", 54530)
    val worldRequest = new Request("id").setCltSrvAddr(clientAddr, serverAddr)
      .setData(worldMsg)

    proxy send(serverActor, Command(worldRequest, false))

    val worldReceived = server.expectMsgType[Tcp.Received]
    worldReceived.data shouldBe worldMsg
  }

  class TestServerClose extends TestClientRequest {
    val closeEvent = Tcp.PeerClosed
    server send(serverActor, closeEvent)

    val closeResponse = {
      val event = proxy.expectMsgType[Event]
      event.msg shouldBe a[Response]
      event.msg.asInstanceOf[Response]
    }

    closeResponse.hasSrvAddr shouldBe true
    closeResponse.srvAddr shouldBe serverAddr
    closeResponse.hasCltAddr shouldBe false
    closeResponse.hasClose shouldBe true
    closeResponse.close shouldBe closeEvent
  }

  class TestCloseRequest extends TestClientRequest {
    val closeEvent = Tcp.PeerClosed
    val closeRequest = new Request("id") setCltSrvAddr(clientAddr, serverAddr) setClose closeEvent

    proxy send(serverActor, Command(closeRequest, false))

    server.expectMsgType[Tcp.ConnectionClosed]
  }

  "ServerActor" when {
    "the server is not available" must {
      "fail to connect" in new TestConnectUnbound
    }
    "the server is avaible" must {
      "connect to an available server" in new TestConnectBound
      "transmit server data as a response" in new TestServerData
      "transmit client request to the socket" in new TestClientRequest
      "transmit a close response when receiving a close event" in new TestServerClose
      "transmit a close event when receiving a close request" in new TestCloseRequest
    }
  }
}
