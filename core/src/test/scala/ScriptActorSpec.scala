package ch.os.tproxy.test

import java.net.{InetSocketAddress => Addr}
import java.util.Random
import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.io.Tcp.{Bind, Register}
import akka.io.{IO, Tcp}
import akka.testkit.{TestActorRef, TestProbe}
import akka.util.ByteString
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.core.actor.ScriptActor
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event, Message}
import ch.os.tproxy.common.message.Request
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.json.MessageJson
import ch.os.tproxy.test.ScriptActorSpec.LargeMessageModifier.largeMsg
import ch.os.tproxy.test.ScriptActorSpec.SimpleMessageModifier.simpleMsg
import ch.os.tproxy.test.TcpSegmentAggregatorSpec.AggregatingForwarder
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import upickle.default.{read, write}

import scala.reflect.ClassTag


object ScriptActorSpec {

  trait Script extends Actor {
    implicit def logging: Logging

    val addr = new Addr("localhost", 9081)
    IO(Tcp)(context.system) ! Bind(self, addr)
    var socketActor: Option[ActorRef] = None

    var aggregator: Option[ActorRef] = None
    override def receive: Receive = {
      case _: Tcp.CommandFailed =>
        context.stop(self)
      case _: Tcp.Bound =>
        socketActor = Some(sender)
      case _: Tcp.Connected =>
        aggregator = Some(context.actorOf(Props(classOf[AggregatingForwarder], logging, self, sender), "TestAggregator"))
        sender ! Register(aggregator.get)
      case Tcp.Unbind =>
        aggregator.foreach(context.stop)
        val unbindingProbe = new TestProbe(context.system)
        socketActor.foreach(actor => {
          unbindingProbe.send(actor, Tcp.Unbind)
          unbindingProbe.expectMsgType[Tcp.Unbound]
        })
        context.stop(unbindingProbe.ref)
    }
  }

  class BlockingScript(implicit _logging: Logging) extends Script {
    def logging = _logging
  }

  class ForwardingScript(implicit _logging: Logging) extends Script {
    def logging = _logging

    override def receive: Receive = super.receive orElse {
      case data: ByteString if aggregator.isDefined =>
        aggregator.get ! Command(data)
    }
  }


  class ModifyingScript(newMsg: => ByteString)(implicit _logging: Logging) extends Script {
    def logging = _logging

    override def receive: Receive = super.receive orElse {
      case data: ByteString if aggregator.isDefined =>
        val msg = read[MessageJson](data.utf8String).toReq.set(("data", newMsg))
        val json = ByteString(write(MessageJson(msg)))
        aggregator.get ! Command(json)
    }
  }

  object SimpleMessageModifier {
    val simpleMsg = ByteString("modified message")
  }

  class SimpleMessageModifier(implicit _logging: Logging) extends ModifyingScript(simpleMsg)

  object LargeMessageModifier {
    lazy val largeMsg = {
      val bytes = new Array[Byte](70 * 1024)
      new Random nextBytes bytes
      ByteString(bytes)
    }
  }

  class LargeMessageModifier(implicit _logging: Logging) extends ModifyingScript(largeMsg)

}

class ScriptActorSpec extends TestSpec("ScriptActorSpec") {

  import ScriptActorSpec._

  class TestSimpleMessageScript[A <: Script](implicit c: ClassTag[A]) {
    val masterActor = new TestProbe(system)
    val proxyActor = new TestProbe(system)
    val filterActor = new TestProbe(system)

    val cltAddr = new Addr("localhost", 1234)
    val srvAddr = new Addr("localhost", 4567)

    val scriptActor: ActorRef = {
      val props = Props(classOf[ScriptActor], masterActor.ref, logging, logReceive)
      system.actorOf(props)
    }

    val runningScript: TestActorRef[A] = TestActorRef(Props(c.runtimeClass, logging), randomString("RunningScript"))

    val data = ByteString("sample request")
    val cid = randomString("cid")
    val req = Request(cid, cltAddr).set(("data", data)).setSrvAddr(srvAddr)
    val msg: Message = Command(req.setProxy(proxyActor.ref).setScriptIntercepted, needAck = false)

    filterActor.send(scriptActor, msg)

    def checkReceivedRequest(): Request = {
      val request: Request = {
        val event = masterActor.expectMsgType[Event]
        event.msg shouldBe a[Request]
        event.msg.asInstanceOf[Request]
      }
      request.hasData shouldBe true
      request.cid shouldBe cid
      request
    }

    def stopActors(): Unit = {
      runningScript ! Tcp.Unbind
      runningScript ! PoisonPill
      system.stop(scriptActor)
      system.stop(masterActor.ref)
      system.stop(proxyActor.ref)
      system.stop(filterActor.ref)
    }
  }

  class TestMessageForwarder extends TestSimpleMessageScript[ForwardingScript] {
    val request: Request = checkReceivedRequest()
    request.data shouldBe data
    stopActors()
  }

  class TestSimpleMessageModifier extends TestSimpleMessageScript[SimpleMessageModifier] {
    val request: Request = checkReceivedRequest()
    request.data shouldBe simpleMsg
    stopActors()
  }

  class TestLargeMessageModifier extends TestSimpleMessageScript[LargeMessageModifier] {
    val request: Request = checkReceivedRequest()
    request.data shouldBe largeMsg
    stopActors()
  }

  class TestMessageBlocker extends TestSimpleMessageScript[BlockingScript] {
    masterActor.expectNoMessage(200.millis)
    stopActors()
  }

  "ScriptActor" when {
    "a script that forwards all received packets is running" must {
      "send a request to master containing the same data it has received from the proxy" in new TestMessageForwarder
    }
    "a script that blocks all received packets is running" must {
      "not send anything to master" in new TestMessageBlocker
    }
    "a script that modifies all received packets with a fixed value is running" must {
      "send a packet containing the modified value to master" in new TestSimpleMessageModifier
      "aggregate large messages sent over multiple TCP segments" in new TestLargeMessageModifier
    }
  }
}
