package ch.os.tproxy.test

import java.net.InetSocketAddress
import akka.util.ByteString
import ch.os.tproxy.core.pcap.{PcapFrame, PcapSim}
import ch.os.tproxy.core.dissection.DissectTree

class DissectSpec extends TestSpec("DissectSpec") {

  trait HasPcapSim {
    val clientAddr = new InetSocketAddress(1234)
    val serverAddr = new InetSocketAddress(4321)
    val pcapSim = new PcapSim(clientAddr, serverAddr)
  }


  class TestOpen extends HasDissectionClient(logging, logReceive) with HasPcapSim {

    val open = pcapSim.open.map(dissectFrame)
    open.length shouldBe 3 // SYN, SYNACK, ACK
  }

  class TestRequest extends TestOpen {
    val httpGet = "GET / HTTP/1.1\r\n\r\n"
    val req = pcapSim.request(ByteString(httpGet).toArray).map(dissectFrame)
    req.length shouldBe 2 // REQUEST, ACK
  }

  class TestResponse extends TestRequest {
    val httpOk = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n"
    val res = pcapSim.response(ByteString(httpOk).toArray).map(dissectFrame)
    res.length shouldBe 2 // RESPONSE, ACK
  }

  class TestCloseRequest extends TestResponse {
    val close = pcapSim.closeRequest.map(dissectFrame)
    close.length shouldBe 3 // FINACK, FINACK, ACK
  }

  class TestCloseResponse extends TestResponse {
    val close = pcapSim.closeResponse.map(dissectFrame)
    close.length shouldBe 3 // FINACK, FINACK, ACK
  }

  "PcapSim" must {
    "generate the correct open frames" in new TestOpen
    "generate the correct request frames" in new TestRequest
    "generate the correct response frames" in new TestResponse
    "generate the correct close request frames" in new TestCloseRequest
    "generate the correct close response frames" in new TestCloseResponse
  }

  class TestPorts extends TestOpen {
    val tree = open(1).getRoot()
    tree.getTag("tcp") should not be null
    tree.getTag("tcp.srcport").toInt shouldBe serverAddr.getPort
    tree.getTag("tcp.dstport").toInt shouldBe clientAddr.getPort
  }

  class TestRequestResponse extends TestResponse {
    val reqtree = req(0).getRoot()
    reqtree.getTag("http") should not be null
    reqtree.getTag("http.request.uri") shouldBe "/"
    val restree = res(0).getRoot()
    restree.getTag("http") should not be null
    restree.getTag("http.response.code").toInt shouldBe 200
  }

  abstract trait HasPcapSim53 {
    def dissectFrame(frame: PcapFrame): DissectTree

    val pcapSim = {
      val clientAddr = new InetSocketAddress(1234)
      val serverAddr = new InetSocketAddress(53)
      new PcapSim(clientAddr, serverAddr)
    }

    pcapSim.open.foreach(dissectFrame)
    val httpGet = "GET /test HTTP/1.1\r\n\r\n"
    val req = pcapSim.request(ByteString(httpGet).toArray)
  }

  class TestDecodeDefault extends HasDissectionClient(logging, logReceive) with HasPcapSim53 {
    val httpDecoded = req.map(dissectFrame)
    httpDecoded(0).getRoot().getTag("http") shouldBe null
  }

  class TestDecodeAs extends HasDissectionClient(logging, logReceive) with HasPcapSim53 {
    val httpDecoded = req.map(dissectFrameAs(_, "http"))
    httpDecoded(0).getRoot().getTag("http") should not be null
    httpDecoded(0).getRoot().getTag("http.request.uri") shouldBe "/test"
  }

  "Dissect" when {
    "using default protocol" must {
      "retrieve TCP ports correctly" in new TestPorts
      "retrieve HTTP fields correctly" in new TestRequestResponse
    }
    "decoding as some non-default protocol" must {
      "not decode http on port 53" in new TestDecodeDefault
      "decode http on port 53 if specified" in new TestDecodeAs
    }
  }
}
