package ch.os.tproxy.test

import akka.testkit.TestProbe
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.{SendCmdTo, SendEvtTo}
import ch.os.tproxy.common.behaviour.actor.message._

class SendToSpec extends TestSpec("SendToSpec") {
  type MsgType = (Any, Boolean) => Message

  abstract class Check {
    def msgType: MsgType

    def behaviour: Behaviour

    def input: Traversable[Any]

    def output: Traversable[Any]

    def ignored: Traversable[Any] = Nil

    lazy val probe = new TestProbe(system)

    lazy val result = msgType match {
      case Command => (input >>: behaviour) :<< ignored
      case Event => ignored >>: (behaviour :<< input)
    }

    (msgType match {
      case Command => result.commands
      case Event => result.events
    }) shouldBe output

    (msgType match {
      case Command => result.events
      case Event => result.commands
    }) shouldBe ignored
  }

  abstract class CheckWithWrap extends Check {
    def needWrap: Boolean

    lazy val behaviour = msgType match {
      case Command => SendCmdTo(probe.ref, needWrap = needWrap)
      case Event => SendEvtTo(probe.ref, needWrap = needWrap)
    }
  }

  class CheckDefault(val msgType: MsgType) extends CheckWithWrap {
    lazy val needWrap = true
    lazy val input = List(1)
    lazy val output = List(SendTo(probe.ref, 1, true))
  }

  class CheckUnwrapped(val msgType: MsgType) extends CheckWithWrap {
    lazy val needWrap = false
    lazy val input = List(1)
    lazy val output = List(SendTo(probe.ref, 1, false))
  }

  abstract class CheckWithPF extends Check {
    def f: PartialFunction[Any, SendTo]

    lazy val behaviour = msgType match {
      case Command => SendCmdTo(f)
      case Event => SendEvtTo(f)
    }
  }

  class CheckMatch(val msgType: MsgType) extends CheckWithPF {
    lazy val f: PartialFunction[Any, SendTo] = {
      case msg: Int => SendTo(probe.ref, msg, true)
    }
    lazy val input = List[Any](1, "a")
    lazy val output = List(SendTo(probe.ref, 1, true), "a")
  }

  class CheckSendTo(val msgType: MsgType) extends CheckWithPF {
    lazy val f: PartialFunction[Any, SendTo] = {
      case msg: Int => SendTo(probe.ref, msg, true)
    }
    lazy val otherProbe = new TestProbe(system)
    lazy val input = List[Any](1, SendTo(otherProbe.ref, 2), "a")
    lazy val output = List(SendTo(probe.ref, 1, true),
      SendTo(otherProbe.ref, 2, true),
      "a")
  }

  class CheckIgnored(val msgType: MsgType) extends CheckWithWrap {
    lazy val needWrap = true
    lazy val input = Nil
    lazy val output = Nil
    override lazy val ignored = List(1)
  }

  "SendCmdTo" must {
    "send by default a wrapped command" in new CheckDefault(Command)
    "send an unwrapped command if so specified" in new CheckUnwrapped(Command)
    "send only matching commands" in new CheckMatch(Command)
    "let SendTo commands go through unaltered" in new CheckSendTo(Command)
    "let events go through unaltered" in new CheckIgnored(Command)
  }
  "SendEvtTo" must {
    "send by default a wrapped event" in new CheckDefault(Event)
    "send an unwrapped event if so specified" in new CheckUnwrapped(Event)
    "send only matching events" in new CheckMatch(Event)
    "let SendTo events go through unaltered" in new CheckSendTo(Event)
    "let commands go through unaltered" in new CheckIgnored(Event)
  }
}
