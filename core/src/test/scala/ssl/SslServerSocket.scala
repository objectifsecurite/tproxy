package ch.os.tproxy.test.ssl

import ch.os.tproxy.core.ssl.SslSocket
import java.security.KeyStore
import java.net.InetSocketAddress
import javax.net.ssl.{
  KeyManagerFactory,
  SSLServerSocket
}
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.io.Tcp

object SslServerSocket {
  private val defaultAddress = new InetSocketAddress("", 0)

  val sslContext = {
    val keystore = KeyStore.getInstance("PKCS12")
    val password = "password"
    val inStream = classOf[SslServerSocket] getResourceAsStream "/keystore.p12"

    keystore.load(inStream, password.toCharArray)
    inStream.close

    val keyManFactory = KeyManagerFactory.getInstance("SunX509")
    keyManFactory init(keystore, password.toCharArray)

    val sslCtx = javax.net.ssl.SSLContext.getInstance("TLS")
    sslCtx init(keyManFactory.getKeyManagers, null, null)

    sslCtx
  }

  def apply(handler: ActorRef, address: InetSocketAddress = defaultAddress) = {
    val factory = sslContext.getServerSocketFactory
    val socket = factory.createServerSocket.asInstanceOf[SSLServerSocket]

    socket setNeedClientAuth false
    socket setUseClientMode false

    address.getPort match {
      case 0 => socket bind null
      case _ => socket bind address
    }

    Props(classOf[SslServerSocket], handler, socket)
  }
}

class SslServerSocket(handler: ActorRef, serverSocket: SSLServerSocket) extends Actor {
  override def preStart() = Listener.start

  override def postRestart(reason: Throwable) = {}

  def receive = {
    case Tcp.Unbind => serverSocket.close; context become closing(sender)
  }

  def closing(actor: ActorRef): Actor.Receive = {
    case evt if evt == Tcp.Unbound && sender == self => context stop self; actor ! evt
  }

  private object Listener extends Thread {
    override def run() = {
      val localAddr = {
        val host = serverSocket.getInetAddress
        val port = serverSocket.getLocalPort
        new InetSocketAddress(host, port)
      }

      handler ! Tcp.Bound(localAddr)

      try while (true) {
        val socket = serverSocket.accept
        val props = Props(classOf[SslSocket], handler, socket)
        context actorOf props
      }
      catch {
        case _: Throwable => ()
      } finally self ! Tcp.Unbound
    }
  }

}
