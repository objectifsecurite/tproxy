package ch.os.tproxy.test

import ch.os.tproxy.core.ssl.SslSocket
import ch.os.tproxy.test.ssl.SslServerSocket
import akka.actor.ActorRef
import akka.io.Tcp
import akka.util.ByteString
import akka.testkit.TestProbe

class SslSocketSpec extends TestSpec("SslSocketSpec") {
  lazy val serverSocket = {
    val probe = new TestProbe(system)
    system actorOf SslServerSocket(probe.ref)
    probe
  }

  lazy val (serverListener, serverAddr) = {
    val addr = serverSocket.expectMsgType[Tcp.Bound].localAddress
    (serverSocket.lastSender, addr)
  }

  lazy val clientSockets = for (_ <- 1 to 3) yield {
    val probe = new TestProbe(system)
    system actorOf SslSocket(probe.ref, serverAddr)
    probe
  }

  case class Endpoint(conn: ActorRef) extends TestProbe(system) {
    this watch conn
    this send Tcp.Register(this.ref)

    def send(msg: Any): Unit = this send(conn, msg)
  }

  case class Link(client: Endpoint, server: Endpoint)

  lazy val links = for (clientSocket <- clientSockets) yield {
    val Tcp.Connected(cra, cla) = clientSocket.expectMsgType[Tcp.Connected]
    val Tcp.Connected(sra, sla) = serverSocket.expectMsgType[Tcp.Connected]

    cra.getPort shouldBe serverAddr.getPort
    sla.getPort shouldBe serverAddr.getPort
    sra shouldBe cla

    val client = Endpoint(clientSocket.lastSender)
    val server = Endpoint(serverSocket.lastSender)

    Link(client, server)
  }

  "SslServerSocket" must {
    "be able to bind to a random port" in serverListener
    "accept multiple client connections" in links
  }

  "SslSocket" must {
    "allow data to be exchanged via the network" in {
      val helloMsg = ByteString("hello")
      val worldMsg = ByteString("world")

      for (link <- links) {
        link.client send Tcp.Write(helloMsg)
        link.server.expectMsgType[Tcp.Received].data shouldBe helloMsg

        link.server send Tcp.Write(worldMsg)
        link.client.expectMsgType[Tcp.Received].data shouldBe worldMsg
      }
    }

    "be informed when the server closes the link" in {
      for (link <- links take links.size - 1) {
        link.server send Tcp.Close
        link.client expectMsg Tcp.PeerClosed
        link.server expectMsg Tcp.Closed
        link.server expectTerminated link.server.conn
      }
    }

    "terminate after having been closed" in {
      for (link <- links take links.size - 1)
        link.client expectTerminated link.client.conn
    }
  }

  "SslServerSocket" must {
    "be informed when a client closes the link" in {
      val link = links.last
      link.client send Tcp.Close
      link.server expectMsg Tcp.PeerClosed
      link.server expectTerminated link.server.conn
      link.client expectMsg Tcp.Closed
      link.client expectTerminated link.client.conn
    }

    "be able to unbind" in {
      serverSocket watch serverListener
      serverSocket send(serverListener, Tcp.Unbind)
      serverSocket expectMsg Tcp.Unbound
    }

    "terminate after having been unbound" in {
      serverSocket expectTerminated serverListener
    }
  }
}
