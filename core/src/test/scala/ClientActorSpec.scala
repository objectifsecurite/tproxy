package ch.os.tproxy.test

import java.net.InetSocketAddress
import akka.io.Tcp
import akka.util.ByteString
import akka.testkit.{TestProbe, TestActorRef}
import ch.os.tproxy.core.actor.ClientActor
import ch.os.tproxy.common.message.{Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.{Command, Event}

class ClientActorSpec extends TestSpec("ClientActorSpec") {

  abstract class HasClientActor {
    val clientAddr = new InetSocketAddress("127.0.0.1", 54530)
    val clientConnection = new TestProbe(system)

    val proxy = new TestProbe(system)

    val clientActor = {
      val props = ClientActor("someId", clientAddr, clientConnection.ref)
      val name = randomString("ClientActor")
      TestActorRef[ClientActor](props, proxy.ref, name)
    }

    clientConnection.expectMsgType[Tcp.Register]
    clientConnection.lastSender shouldBe clientActor
  }

  class TestEmptyRequest extends HasClientActor {
    val emptyRequest = proxy.expectMsgType[Event].msg.asInstanceOf[Request]

    emptyRequest.hasCltAddr shouldBe true
    emptyRequest.cltAddr shouldBe clientAddr
    emptyRequest.hasSrvAddr shouldBe false
    emptyRequest.hasData shouldBe false
  }

  class TestClientData extends TestEmptyRequest {
    val dataMsg = ByteString("hello")
    clientConnection send(clientActor, Tcp.Received(dataMsg))

    val dataRequest = proxy.expectMsgType[Event].msg.asInstanceOf[Request]

    dataRequest.hasCltAddr shouldBe true
    dataRequest.cltAddr shouldBe clientAddr
    dataRequest.hasSrvAddr shouldBe false
    dataRequest.hasData shouldBe true
    dataRequest.data shouldBe dataMsg
  }

  class TestServerResponse extends TestClientData {
    val worldMsg = ByteString("WORLD")
    val serverAddr = new InetSocketAddress(0)
    val worldResponse = new Response("id") setCltSrvAddr(clientAddr, serverAddr) setData worldMsg

    proxy send(clientActor, Command(worldResponse, false))

    val worldWrite = clientConnection.expectMsgType[Tcp.Write]
    worldWrite.data shouldBe worldMsg
    clientConnection send(clientActor, worldWrite.ack)
  }

  class TestClientClose extends TestServerResponse {
    val closeEvent = Tcp.PeerClosed
    clientConnection send(clientActor, closeEvent)

    val closeRequest = proxy.expectMsgType[Event].msg.asInstanceOf[Request]

    closeRequest.hasCltAddr shouldBe true
    closeRequest.cltAddr shouldBe clientAddr
    closeRequest.hasSrvAddr shouldBe false
    closeRequest.hasClose shouldBe true
    closeRequest.close shouldBe closeEvent
  }

  class TestCloseResponse extends TestServerResponse {
    val closeEvent = Tcp.PeerClosed
    val closeResponse = new Response("id") setCltSrvAddr(clientAddr, serverAddr) setClose closeEvent

    proxy send(clientActor, Command(closeResponse, false))

    clientConnection.expectMsgType[Tcp.CloseCommand]
  }

  "ClientActor" must {
    "begin by transmitting an empty request" in new TestEmptyRequest
    "transmit client data as a request" in new TestClientData
    "transmit server response to the socket" in new TestServerResponse
    "transmit a close request when receiving a close event" in new TestClientClose
    "transmit a close event when receiving a close response" in new TestCloseResponse
  }
}
