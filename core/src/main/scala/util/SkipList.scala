package ch.os.tproxy.util

import java.util.{NoSuchElementException => NseException}
import scala.reflect.ClassTag
import scala.math.{Ordering, log}
import scala.util.Random

class SkipList[K, T: ClassTag](keyOf: T => K)
                              (implicit ordering: Ordering[K]) {
  protected val p = 0.5
  protected val sizeHint = Int.MaxValue
  protected val maxLevel = (log(sizeHint) / log(1 / p)).ceil.toInt - 1
  protected var listLevel = 0
  protected var listSize = 0

  protected class Node(val level: Int, var v: T = null.asInstanceOf[T]) {
    val fwd = new Array[Node](level + 1)
    val dist = new Array[Int](level + 1)

    def k = keyOf(v)
  }

  protected val head = new Node(maxLevel)
  protected val tail = new Node(-1)

  for (i <- head.level to(0, -1)) {
    head.fwd(i) = tail
    head.dist(i) = 1
  }

  protected val prevNode = (Array fill (maxLevel + 1)) (head)
  protected val prevPos = (Array fill (maxLevel + 1)) (-1)

  protected trait SearchConds[U] {
    def equiv(node: Node, pos: Int, u: U): Boolean

    def lt(node: Node, pos: Int, u: U): Boolean

    def cacheHit(u: U) = {
      val next = prevNode(0).fwd(0)
      val pos = prevPos(0) + 1
      next != tail && equiv(next, pos, u)
    }

    def ltNotHead(node: Node, pos: Int, u: U) = node != head && lt(node, pos, u)

    def ltNotTail(node: Node, pos: Int, u: U) = node != tail && lt(node, pos, u)
  }

  protected object KeySearchConds extends SearchConds[K] {
    override def cacheHit(k: K) = {
      val prev = prevNode(0)
      (prev == head || !ordering.equiv(prev.k, k)) && super.cacheHit(k)
    }

    def equiv(node: Node, pos: Int, k: K) = ordering.equiv(node.k, k)

    def lt(node: Node, pos: Int, k: K) = ordering.lt(node.k, k)
  }

  protected object IdxSearchConds extends SearchConds[Int] {
    def equiv(node: Node, pos: Int, idx: Int) = pos == idx

    def lt(node: Node, pos: Int, idx: Int) = pos < idx
  }

  protected def search[U](u: U, conds: SearchConds[U]) = conds.cacheHit(u) match {
    case true => true
    case false => {
      val node = prevNode(listLevel)
      val pos = prevPos(listLevel)

      conds.ltNotHead(node, pos, u) match {
        case true => searchFrom(node, u, pos, conds)
        case false => searchFrom(head, u, -1, conds)
      }
    }
  }

  protected def searchFrom[U](node: Node, u: U, idx: Int, conds: SearchConds[U]) = {
    var x = node
    var pos = idx

    def isNextLtNotTail(i: Int) = {
      val nextNode = x.fwd(i)
      val nextPos = pos + x.dist(i)
      conds.ltNotTail(nextNode, nextPos, u)
    }

    for (i <- listLevel to 0 by -1) {
      while (isNextLtNotTail(i)) {
        pos = pos + x.dist(i)
        x = x.fwd(i)
      }
      prevNode(i) = x
      prevPos(i) = pos
    }

    x.fwd(0) != tail && conds.equiv(x.fwd(0), pos + 1, u)
  }

  protected def searchKey(k: K) = search(k, KeySearchConds)

  protected def searchIdx(idx: Int) = {
    require(idx >= 0, "Index must be >= 0")
    require(idx < listSize, "Index must be < #elements")
    search(idx, IdxSearchConds)
  }

  def insert(item: T) = {
    searchKey(keyOf(item))

    val newLevel = randomLevel
    if (newLevel > listLevel) listLevel = newLevel

    val newNode = new Node(newLevel, item)
    val newPos = prevPos(0) + 1

    for (i <- 0 to newLevel) {
      newNode.fwd(i) = prevNode(i).fwd(i)
      prevNode(i).fwd(i) = newNode
      newNode.dist(i) = prevPos(i) + prevNode(i).dist(i) - newPos + 1
      prevNode(i).dist(i) = newPos - prevPos(i)
    }

    for (i <- newLevel + 1 to maxLevel)
      prevNode(i).dist(i) = prevNode(i).dist(i) + 1

    listSize = listSize + 1
    newPos
  }

  def apply(idx: Int) = {
    searchIdx(idx)
    prevNode(0).fwd(0).v
  }

  def update(idx: Int, item: T) = {
    searchIdx(idx)

    val key = keyOf(item)
    val prev = prevNode(0)
    val curr = prev.fwd(0)
    val next = curr.fwd(0)

    val isHead = prev == head
    val isTail = next == tail

    val isKeyGteq = if (isHead) true else ordering.gteq(key, prev.k)
    val isKeyLteq = if (isTail) true else ordering.lteq(key, next.k)

    def errmsg(n: Node) = {
      val what = if (n == prev) ">=" else "<="
      val which = if (n == prev) "previous" else "next"
      s"key of updated item ($key) must be $what the key of the $which item (${n.k})"
    }

    require(isKeyGteq, errmsg(prev))
    require(isKeyLteq, errmsg(next))

    curr.v = item
  }

  protected def removeCachedNode(): Unit = {
    val curr = prevNode(0).fwd(0)

    for (i <- 0 to listLevel)
      if (prevNode(i).fwd(i) == curr) {
        prevNode(i).fwd(i) = curr.fwd(i)
        prevNode(i).dist(i) = prevNode(i).dist(i) + curr.dist(i) - 1
      }
      else
        prevNode(i).dist(i) = prevNode(i).dist(i) - 1

    listSize = listSize - 1

    while (head.fwd(listLevel) != tail && listLevel > 0)
      listLevel = listLevel - 1
  }

  def remove(idx: Int) = {
    searchIdx(idx)
    val node = prevNode(0).fwd(0)
    removeCachedNode()
    node.v
  }

  protected trait Map[U] {
    def value(node: Node, pos: Int): U

    def apply(key: K) = get(key) match {
      case Some(x) => x
      case None => throw new NseException(s"key not found: $key")
    }

    def get(key: K) = searchKey(key) match {
      case false => None
      case true =>
        val node = prevNode(0).fwd(0)
        val pos = prevPos(0) + 1
        Some(value(node, pos))
    }

    def remove(key: K) = searchKey(key) match {
      case false => None
      case true =>
        val node = prevNode(0).fwd(0)
        val pos = prevPos(0) + 1
        removeCachedNode()
        Some(value(node, pos))
    }
  }

  protected trait MultiMap[U] {
    def value(node: Node, pos: Int): U

    def apply(key: K) = searchKey(key) match {
      case false => Vector.empty[U]
      case true => {
        var items = Vector.empty[U]
        var x = prevNode(0).fwd(0)
        var pos = prevPos(0) + 1

        while (x != tail && (ordering equiv(x.k, key))) {
          items = items :+ value(x, pos)
          x = x.fwd(0)
          pos = pos + 1
        }

        items
      }
    }

    def get(key: K) = apply(key)
  }

  protected trait ItemValue {
    def value(node: Node, pos: Int) = node.v
  }

  protected trait IndexValue {
    def value(node: Node, pos: Int) = pos
  }

  object itemMap extends Map[T] with ItemValue

  object itemMultiMap extends MultiMap[T] with ItemValue

  object indexMap extends Map[Int] with IndexValue

  object indexMultiMap extends MultiMap[Int] with IndexValue

  def size = listSize

  def length = listSize

  def toArray = {
    val result = new Array[T](listSize)
    var x = head

    for (i <- 0 until listSize) {
      x = x.fwd(0)
      result(i) = x.v
    }

    result
  }

  protected def randomLevel = {
    var l = 0
    while (Random.nextDouble < p && l < maxLevel) l = l + 1
    l
  }
}
