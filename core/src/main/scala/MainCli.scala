package ch.os.tproxy

import java.net.InetSocketAddress
import scala.util.{Failure, Success}
import akka.actor.ActorSystem
import ch.os.tproxy.config.MainConfig
import ch.os.tproxy.cli.CliActor
import ch.os.tproxy.common.log.AkkaLogging
import ch.os.tproxy.core.Native
import ch.os.tproxy.common.Properties
import ch.os.tproxy.common.natlib.NatlibLoader
import ch.os.tproxy.cli.CliActor
import ch.os.tproxy.core.log.TProxyLogReceive
import ch.os.tproxy.common.log.AkkaLogging

import scala.util.{Failure, Success}

object MainCli {
  def setup(args: Array[String]) = {
    Properties prependPaths("java.library.path", "native.library.path")

    NatlibLoader load "tproxy_core"
    NatlibLoader load "tproxy_core_jni"

    Native.setCapsAndCurrentUser()

    val config = (MainConfig load args) match {
      case Success(c) => c
      case Failure(e) =>
        Console.err println e.getMessage
        sys exit 1
    }

    val system = ActorSystem("tproxy", config)


    if (config hasPath "tproxy.usage-text") {
      Console.out println (config getString "tproxy.usage-text")
      sys exit 0
    }

    val address = new InetSocketAddress(9080)
    (system, config, address)
  }

  def main(args: Array[String]): Unit = {

    val (system, _, address) = setup(args)
    implicit val logging = new AkkaLogging(system)
    implicit val logReceive = new TProxyLogReceive()

    system actorOf(CliActor(address), "CliActor")
  }
}
