package ch.os.tproxy.core.log

import ch.os.tproxy.common.log.LogReceive
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.behaviour.ssl.SslHandler.implicits._

class TProxyLogReceive() extends LogReceive {
  override def shortenMsg(msg: Any): Any = {
    val prefix = super.shortenMsg(msg)
    msg match {
      case m: Message =>
        val post = (if (m.hasSsl) s" with Ssl" else "") +
          (if (m.hasSni) s"(${m.sni})" else "") +
          (if (m.hasFrames) s" with Pcap" else "") +
          (if (m.hasDissection) s" with Dissected" else "")
        prefix + post
      case _ => prefix
    }
  }
}
