package ch.os.tproxy.core.filter

import akka.actor.{ActorRef, ActorRefFactory, Props}
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.core.actor.ScriptActor
import ch.os.tproxy.common.log.{LogReceive, Logging}

object FilterActor {

  case class AddFilter(filter: Filter, script: Boolean)

  case class RemoveFilter(filter: Filter, script: Boolean)

  private var actor: Option[ActorRef] = None;

  def getActor(master: ActorRef)
              (implicit factory: ActorRefFactory, logging: Logging, logReceive: LogReceive) = {
    actor match {
      case Some(a) => a
      case None => {
        val props = Props(classOf[FilterActor], master, logging, logReceive)
        val a = factory actorOf(props, "FilterActor")
        actor = Some(a)
        a
      }
    }
  }
}

class FilterActor(master: ActorRef, _logging: Logging, _logReceive: LogReceive) extends ActorNode {

  import FilterActor._

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive

  lazy val scriptActor = Some(ScriptActor.getActor(self)(context, logging, logReceive))

  def onStart = MessageFilter(Vector.empty, Vector.empty)

  private type Filters = Vector[Filter]

  private case class MessageFilter(filters: Filters, scriptFilters: Filters) extends Behaviour {
    def onCommand = {
      case AddFilter(filter: Filter, script: Boolean) =>
        if (script)
          MessageFilter(filters, scriptFilters :+ filter)
        else
          MessageFilter(filters :+ filter, scriptFilters)
      case RemoveFilter(filter: Filter, script: Boolean) =>
        if (script)
          MessageFilter(filters, scriptFilters diff Vector(filter))
        else
          MessageFilter(filters diff Vector(filter), scriptFilters)

    }

    def onEvent = {
      case msg: Message if intercept(msg) =>
        this withEvent SendTo(master, msg.setIntercepted)

      case msg: Message if scriptIntercept(msg) =>
        this withCommand SendTo(scriptActor.get, msg.setScriptIntercepted.setProxy(sender))

      case msg: Message =>
        this withEvent SendTo(master, msg) withCommand SendTo(sender, msg)
    }

    private def intercept(msg: Message) =
      msg.hasIntercepted || (filters exists (f => f(msg)))

    private def scriptIntercept(msg: Message) =
      msg.hasScriptIntercepted || (scriptFilters exists (f => f(msg)))
  }

}
