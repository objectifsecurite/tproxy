package ch.os.tproxy.core.filter

import ch.os.tproxy.common.message.{Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.behaviour.ssl.SslHandler.implicits._

import scala.util.parsing.combinator.RegexParsers

object FilterParser extends RegexParsers {

  import Filter._

  type Identifier[T] = Message => T
  type Test[T] = (T, T) => Boolean

  override def skipWhitespace = false

  lazy val intIdentifiers: PartialFunction[String, Identifier[Int]] = {
    case "clt.port" => ((m: Message) => m.cltAddr.getPort)
    case "srv.port" => ((m: Message) => m.srvAddr.getPort)
  }

  lazy val stringIdentifiers: PartialFunction[String, Identifier[String]] = {
    case "clt.ip" => ((m: Message) => m.cltAddr.getAddress.getHostAddress)
    case "srv.ip" => ((m: Message) => m.srvAddr.getAddress.getHostAddress)
    case "cid" => ((m: Message) => m.cid)
  }

  lazy val booleanIdentifiers: PartialFunction[String, Identifier[Boolean]] = {
    case "close" => ((m: Message) => m.hasClose)
    case "data" => ((m: Message) => m.hasData)
    case "request" => ((m: Message) => m.isInstanceOf[Request])
    case "response" => ((m: Message) => m.isInstanceOf[Response])
    case "ssl" => ((m: Message) => m.hasSsl)
  }

  lazy val intTests: PartialFunction[String, Test[Int]] = {
    case "==" => ((a: Int, b: Int) => a == b)
    case "!=" => ((a: Int, b: Int) => a != b)
  }

  lazy val stringTests: PartialFunction[String, Test[String]] = {
    case "==" => ((a: String, b: String) => a == b)
    case "!=" => ((a: String, b: String) => a != b)
  }

  lazy val intId = (" ".? ~> """((clt|srv)\.port)""".r <~ " ".?) ^^ intIdentifiers
  lazy val stringId = (" ".? ~> """(((clt|srv)\.ip)|cid)""".r <~ " ".?) ^^ stringIdentifiers
  lazy val booleanId = (" ".? ~> """(close|data|request|response|ssl)""".r <~ " ".?) ^^ booleanIdentifiers

  lazy val stringTest = (" ".? ~> """(==|!=)""".r <~ " ".?) ^^ stringTests
  lazy val intTest = (" ".? ~> """(==|!=)""".r <~ " ".?) ^^ intTests

  lazy val integer = (" ".? ~> """(0|[1-9]\d*)""".r <~ " ".?) ^^ {
    _.toInt
  }
  lazy val string = " ".? ~> (("\"" ~> """([^\"\)]*)""".r <~ "\"") | ("""([^\s\)]*)""".r)) <~ " ".?

  lazy val intFilter: Parser[TypedFilter[Int]] = intId ~ intTest ~ integer ^^ {
    case id ~ test ~ int => TypedFilter[Int](id, test, int)
  }
  lazy val stringFilter: Parser[TypedFilter[String]] = stringId ~ stringTest ~ string ^^ {
    case id ~ test ~ str => TypedFilter[String](id, test, str)
  }
  lazy val booleanFilter: Parser[TypedFilter[Boolean]] = booleanId ^^ {
    case id => BooleanFilter(id)
  }
  lazy val dissectionFilter: Parser[TypedFilter[Boolean]] = "hasTag" ~> string ^^ {
    case tag => TagFilter(tag)
  }

  lazy val expression: Parser[Filter] = factor ~ (("&" | "|") ~ expression).? ^^ {
    case filter ~ Some("&" ~ expr) => AndFilter(filter, expr)
    case filter ~ Some("|" ~ expr) => OrFilter(filter, expr)
    case filter ~ None => filter
    case _ => throw new RuntimeException("Unexpected filter expression")
  }
  lazy val factor: Parser[Filter] = " ".? ~> (intFilter | stringFilter | booleanFilter | dissectionFilter | group | not) <~ " ".?
  lazy val group = "(" ~> expression <~ ")"
  lazy val not = "!" ~> (group | booleanFilter) ^^ {
    case filter => NotFilter(filter)
  }

  lazy val filter: Parser[Filter] = expression.? ^^ {
    case Some(filter) => filter
    case None => Filter.DefaultFilter
  }

  def apply(input: String) = parseAll(filter, input) match {
    case Success(result, _) => Some(result.withString(input))
    case x => None
  }
}
