package ch.os.tproxy.core.filter

import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.message.implicits.Dissection

object Filter {
  val defaultFilter = DefaultFilter

  object DefaultFilter extends Filter {
    string = Some("")
    val filter = (msg: Message) => true
  }

  object BooleanFilter {
    def apply(value: Message => Boolean) =
      TypedFilter[Boolean](value, (a: Boolean, b: Boolean) => a == b, true)
  }

  object TagFilter {
    def hasTag(msg: Message, tag: String) =
      msg.hasDissection && msg.dissection.getRoot.hasTag(tag)

    def apply(tag: String) = BooleanFilter(hasTag(_, tag))
  }

  case class TypedFilter[T](value: Message => T,
                            test: (T, T) => Boolean,
                            reference: T) extends Filter {
    val filter = (msg: Message) => test(value(msg), reference)
  }

  case class AndFilter(filters: Filter*) extends Filter {
    val filter = (msg: Message) => filters forall (f => f(msg))
  }

  case class OrFilter(filters: Filter*) extends Filter {
    val filter = (msg: Message) => filters exists (f => f(msg))
  }

  case class NotFilter(fil: Filter) extends Filter {
    val filter = (msg: Message) => !fil(msg)
  }

}

abstract class Filter {
  var string: Option[String] = None

  def withString(value: String) = {
    string = Some(value);
    this
  }

  val filter: Message => Boolean

  def apply(msg: Message) = filter(msg)

  override def toString = {
    require(string.isDefined, "Filter must have its string defined")
    string.get
  }
}
