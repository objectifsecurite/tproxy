package ch.os.tproxy.core

import java.net.{InetSocketAddress, ServerSocket}
import java.util.concurrent.ThreadLocalRandom
import akka.actor.{ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.io.Inet.SocketOption
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.behaviour.actor.message.{SendTo, Stop}
import ch.os.tproxy.core.actor.ProxyActor

object TProxy {

  trait Config extends ProxyActor.Config {
    def logging: Logging

    def logReceive: LogReceive

    def master: ActorRef

    def filter: Option[ActorRef]

    def dissector: Option[ActorRef]

    def pcapDumper: Option[ActorRef]
  }

  case class Bound(address: InetSocketAddress)

  case class BindFailed(address: InetSocketAddress)

  case class Unbound(address: InetSocketAddress)

  def apply(address: InetSocketAddress)(implicit config: Config) =
    Props(classOf[TProxy], address, config)

  case object TranspSockOpt extends SocketOption {
    def setTransparent(ss: ServerSocket) =
      Native setTransparent ss.getChannel

    override def beforeServerSocketBind(ss: ServerSocket) = {
      var isTransp = setTransparent(ss)

      if (!isTransp) {
        val canSetCap = Native.setCapForCurrentUser == 0
        isTransp = canSetCap && setTransparent(ss)
      }

      require(isTransp, s"Cannot set transparent socket option on $ss")
    }
  }

}

class TProxy(address: InetSocketAddress)(implicit config: TProxy.Config)
  extends ActorNode {

  import TProxy._
  import config._

  implicit def logging = config.logging forObject this

  implicit val logReceive = config.logReceive

  def onStart = {
    val bindCmd = Tcp.Bind(self, address, options = (TranspSockOpt :: Nil))
    IO(Tcp) ! bindCmd
    Binding(bindCmd)
  }

  private case class Binding(bindCmd: Tcp.Bind) extends Behaviour {
    lazy val log = logging(this)

    def onEvent = {
      case Tcp.Bound(addr) => {
        log info s"${TProxy.this} listening on $addr"
        val cmd = SendTo(master, Bound(addr), needWrap = false)
        Intercepting(addr, sender) withCommand cmd
      }


      case Tcp.CommandFailed(bindCmd) => {
        log info s"Bind to $address failed"
        val cmd = SendTo(master, BindFailed(address), needWrap = false)
        Stop >>: (Stopping(address) withCommand cmd)
      }
    }

    def onCommand = {
      case Stop => Stopping(address)
    }
  }

  private case class Intercepting(address: InetSocketAddress,
                                  listener: ActorRef)
    extends Behaviour {
    lazy val log = logging(this)

    def onEvent = {
      case Tcp.Connected(src, dst) => {
        log debug s"Intercepted connection $src --> $dst"
        val cid = "%08x" format ThreadLocalRandom.current.nextInt
        context actorOf(ProxyActor(cid, src, dst, sender), s"ProxyActor-$cid")
        this
      }
    }

    def onCommand = {
      case Stop => {
        val cmd = SendTo(listener, Tcp.Unbind, needWrap = false)
        Stopping(address) withCommand cmd
      }
    }
  }

  private case class Stopping(address: InetSocketAddress) extends OnEvent {
    lazy val log = logging(this)

    def onEvent = {
      case Tcp.Unbound => {
        log info s"Stop listening on $address"
        val cmd = SendTo(master, Unbound(address), needWrap = false)
        Stop >>: (this withCommand cmd)
      }

      case _: Tcp.CommandFailed => {
        log info s"Bind to $address failed"
        val cmd = SendTo(master, BindFailed(address), needWrap = false)
        Stop >>: (this withCommand cmd)
      }

      case Tcp.Bound(addr) => {
        log info s"Start listening on $addr"
        val cmds = Vector(SendTo(master, Bound(addr), needWrap = false),
          SendTo(sender, Tcp.Unbind, needWrap = false))
        Stopping(addr) withCommands cmds
      }

      case Tcp.Connected(src, dst) => {
        log debug s"Ignore intercepted connection $src --> $dst"
        this
      }
    }
  }

  override def toString = s"${this.getClass.getName}($address)"
}
