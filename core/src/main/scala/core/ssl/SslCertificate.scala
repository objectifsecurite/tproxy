package ch.os.tproxy.core.ssl

import com.karasiq.tls.internal.BCConversions.JavaKeyOps
import com.karasiq.tls.x509.CertExtension
import com.karasiq.tls.x509.CertExtension.{extendedKeyUsage, keyUsage}
import org.bouncycastle.asn1.x509.{Extension, KeyPurposeId, KeyUsage}
import org.bouncycastle.cert.jcajce.{JcaX509CertificateHolder, JcaX509ExtensionUtils, JcaX509v3CertificateBuilder}
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder

import java.io.ByteArrayInputStream
import java.math.BigInteger
import java.security.cert.{CertificateFactory, X509Certificate}
import java.security.{KeyPair, PrivateKey, PublicKey}
import java.util.Date
import javax.security.auth.x500.X500Principal

object SslCertificate {
  private val SIGALG = "SHA256withRSA"

  def generateCA(subject: X500Principal, caKeyPair: KeyPair, start: Date, end: Date) = {
    val (caPubKey, caKey) = (caKeyPair.getPublic, caKeyPair.getPrivate)
    val subjectKeyId = CertExtension.subjectKeyId(caPubKey.toSubjectPublicKeyInfo)
    val extensions = CertExtension.certificationAuthorityExtensions(pathLenConstraint = 5) ++ Set(subjectKeyId)
    signCertificate(subject, caPubKey, subject, caKey, start, end, BigInteger.ONE, extensions)
  }

  def generateCertificate(pubKey: PublicKey,
                          issuer: X500Principal, issuerPubKey: PublicKey, issuerKey: PrivateKey,
                          start: Date, end: Date, serialNo: BigInteger, baseCrt: X509Certificate) = {
    val subjKeyId = CertExtension.subjectKeyId(pubKey.toSubjectPublicKeyInfo)

    val jxeu = new JcaX509ExtensionUtils
    val authorityKeyIdentifier = jxeu.createAuthorityKeyIdentifier(issuerPubKey)
    val authKeyIdExt = CertExtension(Extension.authorityKeyIdentifier, authorityKeyIdentifier, critical = false)
    val keyUsageExt = keyUsage(KeyUsage.keyEncipherment | KeyUsage.digitalSignature)
    val extendedKeyUsageExt = extendedKeyUsage(KeyPurposeId.id_kp_clientAuth, KeyPurposeId.id_kp_serverAuth)
    val subjAltName = new JcaX509CertificateHolder(baseCrt).getExtension(Extension.subjectAlternativeName)
    val subjAltNameExt =  if(subjAltName != null)
      Set(CertExtension(subjAltName.getExtnId, subjAltName.getParsedValue, subjAltName.isCritical)) else Set()

    val extensions: Set[CertExtension] = Set(CertExtension.basicConstraints(ca = false),
      keyUsageExt, extendedKeyUsageExt, subjKeyId, authKeyIdExt) ++ subjAltNameExt

    val certSubject = baseCrt.getSubjectX500Principal
    signCertificate(certSubject, pubKey, issuer, issuerKey, start, end, serialNo, extensions)
  }


  private def signCertificate(subject: X500Principal, pubKey: PublicKey,
                    issuer: X500Principal, issuerKey: PrivateKey,
                          start: Date, end: Date, serialNo: BigInteger, extensions: Set[CertExtension]) = {
    val certificateBuilder = new JcaX509v3CertificateBuilder(issuer, serialNo, start, end, subject, pubKey)
    extensions.foreach(ext => certificateBuilder.addExtension(ext.id, ext.critical , ext.value))

    val signerBuilder = new JcaContentSignerBuilder(SIGALG)
    val holder = certificateBuilder.build(signerBuilder.build(issuerKey))
    val certificateFactory = CertificateFactory.getInstance("X.509")
    certificateFactory.generateCertificate(new ByteArrayInputStream(holder.getEncoded)).asInstanceOf[X509Certificate]
  }
}
