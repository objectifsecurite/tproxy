package ch.os.tproxy.core.ssl

import java.io.{File, FileInputStream, FileOutputStream, IOException}
import java.math.BigInteger
import java.net.{Socket, InetAddress}
import java.security.{
  GeneralSecurityException, KeyPair, KeyPairGenerator, KeyStore,
  Principal, PrivateKey
}
import java.security.cert.{Certificate, X509Certificate}
import java.util.Date

import javax.net.ssl.{KeyManager, SSLContext, SSLEngine, X509ExtendedKeyManager}
import javax.security.auth.x500.X500Principal

import scala.collection.mutable.{HashMap, HashSet}

object SSLEngineFactory {
  private val default_age = 30L * 24L * 60L * 60L * 1000L
  private val default_validity = 10L * 365L * 24L * 60L * 60L * 1000L

  private val ca_alias = "CA"
  private val ca_name = {
    try {
      new X500Principal("cn=Custom CA for "
        + InetAddress.getLocalHost.getHostName
        + " at " + new Date
        + ",ou=Custom CA,o=UNDEF,l=UNDEF,st=UNDEF,c=UNDEF");
    } catch {
      case ioe: IOException => ioe.printStackTrace; null
    }
  }
  private val password = "password".toCharArray()

  private val keystore = KeyStore.getInstance("PKCS12")

  private val file = {
    val filename = "customkeystore.p12"
    val appHome = Option(System getProperty "app.home")
    appHome match {
      case None => new File(filename)
      case Some(dir) => new File(dir, filename)
    }
  }

  private val contextCache = new HashMap[String, SSLContext]
  private val serials = new HashSet[BigInteger]

  private val (ca_key, ca_certs): (PrivateKey, Array[X509Certificate]) = {
    if (file != null && file.exists) {
      val is = new FileInputStream(file)
      keystore.load(is, password)
      initSerials
    } else keystore.load(null, password);

    val key = Option(keystore getKey(ca_alias, password))
    val chain = Option(keystore getCertificateChain ca_alias)

    if (!key.isDefined || !chain.isDefined)
      generateCA(ca_name)

    ((keystore getKey(ca_alias, password)).asInstanceOf[PrivateKey],
      (keystore getCertificateChain ca_alias map (_.asInstanceOf[X509Certificate])))
  }

  def getEngine(host: String, baseCrt: X509Certificate) = synchronized {
    val sslcontext = contextCache.getOrElse(host, {
      val km = {
        if (!keystore.containsAlias(host)) createKeyMaterial(host, baseCrt)
        else loadKeyMaterial(host)
      }
      val context = SSLContext getInstance "TLS"
      context init(Array[KeyManager](km), null, null)
      contextCache put(host, context)
      context
    })
    sslcontext.createSSLEngine
  }

  private def loadKeyMaterial(host: String) = {
    val certs = keystore.getCertificateChain(host).map(_.asInstanceOf[X509Certificate])
    if (certs != null) {
      val pk = keystore.getKey(host, password).asInstanceOf[PrivateKey]
      if (pk != null) {
        new HostKeyManager(host, pk, certs)
      } else throw new GeneralSecurityException("Internal error: private key for "
        + host + " not found!");
    } else throw new GeneralSecurityException("Internal error: certificate chain for "
      + host + " not found!");
  }

  def saveKeystore(file: File = file, keystore: KeyStore = keystore, password: Array[Char] = password) = {
    try {
      val out = new FileOutputStream(file)
      keystore store(out, password)
      out.close
    } catch {
      case e: Throwable => e.printStackTrace
    }
  }

  private def generateCA(caName: X500Principal): (PrivateKey, Array[X509Certificate]) = {
    val keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen initialize 2048
    val caPair = keyGen.generateKeyPair
    val start = new Date(System.currentTimeMillis - default_age)
    val end = new Date(start.getTime + default_validity)

    val cert = SslCertificate.generateCA(caName, caPair, start, end)

    val caCerts = Array[X509Certificate](cert)
    val caKey = caPair.getPrivate

    keystore setKeyEntry(ca_alias, caKey, password,
      caCerts.asInstanceOf[Array[Certificate]])
    saveKeystore()
    (caKey, caCerts)
  }

  private def initSerials() = {
    val e = keystore.aliases
    while (e.hasMoreElements) {
      val cert = keystore.getCertificate(e.nextElement).asInstanceOf[X509Certificate]
      serials add cert.getSerialNumber
    }
  }

  private def getNextSerialNo() = {
    val serial = BigInteger.valueOf(System.currentTimeMillis)
    while (serials contains serial) {
      serial add BigInteger.ONE
    }
    serials add serial
    serial
  }

  private def createKeyMaterial(host: String, baseCrt: X509Certificate) = {
    val ca_root = ca_certs(0)
    val keyPair = new KeyPair(ca_root.getPublicKey, ca_key)
    val start = new Date(System.currentTimeMillis - default_age)
    val end = new Date(start.getTime() + default_validity)

    val cert = SslCertificate.generateCertificate(keyPair.getPublic, ca_root.getSubjectX500Principal,
      ca_root.getPublicKey, ca_key, start, end, getNextSerialNo, baseCrt)

    val chain = cert +: ca_certs
    val pk = keyPair.getPrivate
    keystore setKeyEntry(host, pk, password, chain.asInstanceOf[Array[Certificate]])
    saveKeystore()
    new HostKeyManager(host, pk, chain)
  }

  class HostKeyManager(host: String, pk: PrivateKey,
                       certs: Array[X509Certificate]) extends X509ExtendedKeyManager {
    def chooseClientAlias(keyType: Array[String],
                          issuers: Array[Principal],
                          socket: Socket) = {
      throw new UnsupportedOperationException("Not implemented")
    }

    def chooseServerAlias(keyType: String,
                          issuers: Array[Principal],
                          socket: Socket) = host

    def getCertificateChain(alias: String) = certs

    def getClientAliases(keyType: String, issuers: Array[Principal]) = {
      throw new UnsupportedOperationException("Not implemented");
    }

    def getPrivateKey(alias: String) = pk

    def getServerAliases(keyType: String,
                         issuers: Array[Principal]) = Array[String](host)

    override def chooseEngineClientAlias(keyType: Array[String],
                                         issuers: Array[Principal],
                                         engine: SSLEngine) = {
      throw new UnsupportedOperationException("Not implemented")
    }

    override def chooseEngineServerAlias(keyType: String,
                                         issuers: Array[Principal],
                                         engine: SSLEngine) = host
  }

}
