package ch.os.tproxy.core.ssl

import java.security.cert.X509Certificate
import javax.net.ssl.{TrustManager, X509TrustManager}

final object SslContext {

  class TrustAllCerts extends X509TrustManager {
    def getAcceptedIssuers: Array[X509Certificate] = {
      null
    }

    def checkClientTrusted(certs: Array[X509Certificate], authType: String) = {}

    def checkServerTrusted(certs: Array[X509Certificate], authType: String) = {}
  }

  private val sslContext = {
    val sslCtx = javax.net.ssl.SSLContext.getInstance("TLSv1.2")
    val trustManagers = Array[TrustManager](new TrustAllCerts)
    sslCtx.init(null, trustManagers, null)
    sslCtx
  }

  def createEngine(sni: String, port: Int) = sslContext.createSSLEngine(sni, port)

  def createEngine = sslContext.createSSLEngine

  def getServerSocketFactory = sslContext.getServerSocketFactory

  def getSocketFactory = sslContext.getSocketFactory
}
