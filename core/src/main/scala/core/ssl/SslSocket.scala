package ch.os.tproxy.core.ssl

import java.io.InputStream
import java.net.InetSocketAddress

import scala.annotation.tailrec
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.io.Tcp
import akka.util.ByteString
import javax.net.ssl.SSLSocket

object SslSocket {
  def apply(handler: ActorRef, addr: InetSocketAddress) = {
    val (host, port) = (addr.getAddress, addr.getPort)
    val factory = SslContext.getSocketFactory
    val socket = factory.createSocket(host, port).asInstanceOf[SSLSocket]

    socket setNeedClientAuth false
    socket setUseClientMode true

    Props(classOf[SslSocket], handler, socket)
  }
}

class SslSocket(firstHandler: ActorRef, socket: SSLSocket) extends Actor {
  override def preStart() = {
    val remoteAddr = new InetSocketAddress(socket.getInetAddress, socket.getPort)
    val localAddr = new InetSocketAddress(socket.getLocalAddress, socket.getLocalPort)
    firstHandler ! Tcp.Connected(remoteAddr, localAddr)
  }

  override def postRestart(reason: Throwable) = {}

  def receive = {
    case cmd: Tcp.Register => {
      new Listener(cmd.handler, socket.getInputStream).start
      context become waiting(cmd.handler)
    }
  }

  def waiting(handler: ActorRef): Actor.Receive = {
    case Tcp.Write(data, _) => output write data.toArray; output.flush
    case cmd: Tcp.CloseCommand => socket.close; context become closing(handler, cmd)
    case evt: Tcp.ConnectionClosed => socket.close; handler ! evt; context stop self
  }

  def closing(handler: ActorRef, cmd: Tcp.CloseCommand): Actor.Receive = {
    case evt: Tcp.ConnectionClosed if sender == self => handler ! cmd.event; context stop self
  }

  private lazy val output = socket.getOutputStream

  private class Listener(handler: ActorRef, input: InputStream) extends Thread {
    override def run() = {
      val bytes = new Array[Byte](1024)

      @tailrec
      def loop(buffer: ByteString): Unit = {
        if (buffer.nonEmpty && input.available == 0) {
          handler ! Tcp.Received(buffer)
          loop(ByteString.empty)
        }
        else Try(input read bytes) match {
          case Success(-1) => self ! Tcp.PeerClosed
          case Success(nbytes) => {
            val data = ByteString fromArray(bytes, 0, nbytes)
            loop(buffer ++ data)
          }
          case Failure(e) => self ! Tcp.ErrorClosed(e.getMessage)
        }
      }

      Try(socket.startHandshake)
      loop(ByteString.empty)
    }
  }

}
