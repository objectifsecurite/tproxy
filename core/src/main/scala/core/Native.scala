package ch.os.tproxy.core

import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel
import ch.os.tproxy.common.natlib.NatlibLoader

object Native {
  NatlibLoader load "tproxy_core"
  NatlibLoader load "tproxy_core_jni"

  def setLoggedAsCurrentUser() = {
    val r = loggedAsCurrentUser()
    val homeDir = getEnv("HOME")
    System setProperty("user.home", homeDir)
    r
  }

  def setCapsAndCurrentUser() = {
    if (getUID == 0) {
      val canSetCap = setCapForCurrentUser == 0
      require(canSetCap, "cannot set capabilities of current thread")

      val canSetUser = setLoggedAsCurrentUser == 0
      require(canSetUser, "cannot set logged user as current user")
    }
  }

  @native def setTransparent(channel: ServerSocketChannel): Boolean

  @native def getLocalPort(channel: SocketChannel): Int

  @native def setTos(channel: SocketChannel, tos: Int): Boolean

  @native def getUID: Int

  @native def getEnv(name: String): String

  @native def setCapForCurrentUser(): Int

  @native def loggedAsCurrentUser(): Int

  @native def gLazyModuleOpen(libFilePath: String): Unit
}
