package ch.os.tproxy.core.dissection

import akka.actor.TypedActor.context
import akka.actor.{ActorRef, Timers}
import akka.io.Tcp
import akka.util.ByteString
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.tcp.TcpSegmentAggregator
import ch.os.tproxy.common.json.JsonDissectTree.{ACK, ERROR}
import ch.os.tproxy.common.json.{DissectionRequest, JsonDissectTree, JsonDissectTreeNode}
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.core.dissection.Dissector.Reconnect
import upickle.default.read

import scala.annotation.tailrec
import scala.concurrent.duration.DurationInt

object DissectionClient {
  private object DissectionTimeout

  case class DissectionResponse(dissection: Option[DissectTree])

}

case class DissectionClient(_logging: Logging, _logReceive: LogReceive, connection: ActorRef, dissector: ActorRef = context.parent) extends NetNode with Timers {

  import DissectionClient._

  def onStart: Behaviour.Result =
    (Connected()
      <-> TcpSegmentAggregator()
      <-> Backpressure
      <-> SendNetCmdTo(connection, "dissectionServer")) withCommand SendTo(connection, Tcp.Register(self), needWrap = false)

  implicit lazy val logging: Logging = _logging

  implicit val logReceive: LogReceive = _logReceive


  case class Connected(waitingForResponse: Boolean = false, pendingCmds: Vector[Any] = Vector.empty,
                       incompleteTree: Option[DissectTree] = None, splitDepth: Option[Int] = None,
                       currentDepth: Option[Int] = None, incompleteNodes: Vector[IncompleteDissectTreeNode] = Vector.empty)
    extends Behaviour {
    override def onCommand: Behaviour.Receive = {
      case cmd if waitingForResponse =>
        copy(pendingCmds = pendingCmds :+ cmd)
      case req: DissectionRequest =>
        sendTimeout()
        copy(waitingForResponse = true).withCommand(ByteString(req.toJson))
    }

    override def onEvent: Behaviour.Receive = {
      case _: Tcp.ConnectionClosed =>
        this withEvent SendTo(dissector, Reconnect)
      case DissectionTimeout if waitingForResponse =>
        val nextState = copy(waitingForResponse = false, pendingCmds = Vector.empty, incompleteTree = None,
          currentDepth = None, splitDepth = None, incompleteNodes = Vector.empty)
        val evts = Vector(SendTo(dissector, DissectionResponse(None)), SendTo(dissector, Reconnect))
        nextState.withEvents(evts)
      case DissectionTimeout => this

      // Receive a (potentially incomplete) JsonDissectTree
      case data: ByteString if waitingForResponse && incompleteTree.isEmpty =>
        val jsonDissectTree = read[JsonDissectTree](data.utf8String)
        val nextState = copy(waitingForResponse = false, pendingCmds = Vector.empty, incompleteTree = None,
          currentDepth = None, splitDepth = None, incompleteNodes = Vector.empty)

        jsonDissectTree.resType match {
          case ACK =>
            pendingCmds >>: nextState
          case ERROR =>
            pendingCmds >>: nextState.withEvent(SendTo(dissector, DissectionResponse(None)))
          case _ =>
            val dissectTree = toDissectTree(jsonDissectTree)
            val incompleteNodes = getIncompleteNodes(jsonDissectTree.root, 0, jsonDissectTree.splitDepth, isSibling = false)
            // the received tree is complete
            if (dissectTree.getRoot == null || incompleteNodes.isEmpty) {
              pendingCmds >>: nextState.withEvent(SendTo(dissector, DissectionResponse(Some(dissectTree))))
            }
            // the received tree is incomplete -> waiting for subtrees
            else {
              sendTimeout()
              copy(incompleteTree = Some(dissectTree), incompleteNodes = incompleteNodes, currentDepth = Some(incompleteNodes(0).depth),
                splitDepth = Some(jsonDissectTree.splitDepth))
            }
        }

      // reconstruct the incomplete dissection tree with incoming subtrees
      case data: ByteString if waitingForResponse && incompleteNodes.nonEmpty =>
        val jsonDissectTreeNode = read[JsonDissectTreeNode](data.utf8String)
        val newIncompleteNodes = getIncompleteNodes(jsonDissectTreeNode, currentDepth.get, splitDepth.get)
        // reconstruct a subtree
        if (newIncompleteNodes.isEmpty) {
          val (completeJsonNode, updatedIncompleteNodes) = reconstructTree(jsonDissectTreeNode)
          // the whole tree is complete
          if (updatedIncompleteNodes.isEmpty) {
            val completeRoot = new DissectTreeNode(completeJsonNode)
            val dissectTree = incompleteTree.get.withRoot(completeRoot)
            pendingCmds >>: copy(waitingForResponse = false, pendingCmds = Vector.empty,
              incompleteTree = None, splitDepth = None, currentDepth = None, incompleteNodes = Vector.empty)
              .withEvent(SendTo(dissector, DissectionResponse(Some(dissectTree))))
          }
          else {
            sendTimeout()
            copy(currentDepth = Some(updatedIncompleteNodes(0).depth), incompleteNodes = updatedIncompleteNodes)
          }
        }
        else {
          sendTimeout()
          copy(currentDepth = Some(newIncompleteNodes(0).depth), incompleteNodes = newIncompleteNodes ++ incompleteNodes)
        }
    }

    private def sendTimeout() = timers.startSingleTimer(DissectionTimeout.toString, DissectionTimeout, 1.seconds)

    private def reconstructTree(jsonDissectTreeNode: JsonDissectTreeNode): (JsonDissectTreeNode, Vector[IncompleteDissectTreeNode]) = {
      val (completeJsonNode, cnt, _, _) = incompleteNodes.foldLeft((jsonDissectTreeNode, 0, false, true))((acc, currentNode) => {
        val (prevCompleteNode, cnt, done, prevWasSibling) = acc
        if (done)
          (prevCompleteNode, cnt, done, prevWasSibling)
        else {
          if (prevWasSibling) {
            require(currentNode.jsonDissectTreeNode.pendingNextNode)
            currentNode.jsonDissectTreeNode.next = prevCompleteNode
          }
          val isDone = done || (!prevWasSibling && currentNode.jsonDissectTreeNode.pendingNextNode)

          val newCnt = if (isDone) cnt else cnt + 1
          (currentNode.jsonDissectTreeNode, newCnt, isDone, currentNode.isSibling)
        }
      })

      (completeJsonNode, incompleteNodes.drop(cnt))
    }

    private def getIncompleteNodes(node: JsonDissectTreeNode, depth: Int, splitDepth: Int, isSibling: Boolean = true) = {

      @tailrec
      def getIncompleteChildren(node: JsonDissectTreeNode, depth: Int, splitDepth: Int,
                                incompleteChildren: Vector[IncompleteDissectTreeNode] = Vector.empty): Vector[IncompleteDissectTreeNode] = {
        if (node == null || depth > splitDepth)
          incompleteChildren
        else {
          val incompleteChild = if (node.pendingNextNode)
            Vector(IncompleteDissectTreeNode(node, isSibling = false, depth)) else Vector.empty
          getIncompleteChildren(node.firstChild, depth + 1, splitDepth,
            incompleteChildren = incompleteChild ++ incompleteChildren)
        }
      }

      val incompleteChildren = if (node == null) Vector.empty else getIncompleteChildren(node.firstChild, depth + 1, splitDepth)
      lazy val incompleteParent = Vector(IncompleteDissectTreeNode(node, isSibling, depth))
      if (incompleteChildren.isEmpty && !node.pendingNextNode) Vector.empty else incompleteChildren ++ incompleteParent

    }

    private def toDissectTree(jsonDissectTree: JsonDissectTree): DissectTree = {
      val root = if (jsonDissectTree.root == null) null else new DissectTreeNode(jsonDissectTree.root)
      new DissectTree(jsonDissectTree.protocol, jsonDissectTree.info, jsonDissectTree.frameNumber, root, jsonDissectTree.offset)
    }
  }

  case class IncompleteDissectTreeNode(jsonDissectTreeNode: JsonDissectTreeNode, isSibling: Boolean, depth: Int)

}
