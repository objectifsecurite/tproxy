package ch.os.tproxy.core.dissection

import akka.actor.{ActorRef, ActorRefFactory, PoisonPill, Props, Timers}
import akka.io.{IO, Tcp}
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.json.DissectionRequest
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.dissection.DissectionClient.DissectionResponse
import ch.os.tproxy.core.message.implicits._

import java.io.IOException
import java.net.{InetSocketAddress, ServerSocket}
import scala.concurrent.duration.DurationInt
import scala.sys.process.{ProcessLogger, _}

object Dissector {

  case class DissectorDo(msg: Message)

  case class DissectorDone(msg: Message)

  object Reconnect

  val dissectionServerPort = 9082
  private var actor: Option[ActorRef] = None;

  def getActor(implicit factory: ActorRefFactory, logging: Logging, logReceive: LogReceive) = {
    actor match {
      case Some(a) => a
      case None =>
        val props = Props(classOf[Dissector], logging, dissectionServerPort, logReceive)
        val a = factory actorOf(props, "Dissector")
        actor = Some(a)
        a
    }
  }

  def startWiresharkDissector(wait: Boolean = false) =
    if (dissectionPortAvailable()) {
      println(s"Starting the wireshark dissector on port ${dissectionServerPort}")
      val proc = "./wiresharkDissection".run(ProcessLogger(_ => (), Console.err.println))
      Runtime.getRuntime.addShutdownHook(new Thread {
        override def run() = proc.destroy()
      })

      // wait for the wireshark dissector to be listening on port 9082 (used in tests to avoid timeouts)
      if (wait)
        Thread.sleep(2000)
    }

  private def dissectionPortAvailable(port: Int = dissectionServerPort): Boolean = {
    try {
      val serverSocket = new ServerSocket(port)
      serverSocket.close()
      true
    }
    catch {
      case _: IOException => false
    }
  }

}

class Dissector(_logging: Logging, dissectionServerPort: Int, _logReceive: LogReceive) extends NetNode with Timers {

  import Dissector._

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive


  def onStart = {
    val addr = new InetSocketAddress("localhost", dissectionServerPort)
    val tcpConnectCmd = Tcp.Connect(addr)
    val connectCmd = SendTo(IO(Tcp), tcpConnectCmd, needWrap = false)
    Connecting(connectCmd) withCommand (connectCmd)
  }

  case class Connecting(connectCmd: SendTo, pendingCmds: Vector[Any] = Vector.empty, connectionFailed: Boolean = false)
    extends Behaviour {
    def onCommand = {
      case DissectorDo(m) if !connectionFailed && m.hasFrames =>
        val newMsg = m.setProxy(sender)
        copy(pendingCmds = pendingCmds :+ DissectorDo(newMsg))
      case DissectorDo(m) if m.hasFrames =>
        // Do not wait for reconnection if previous connection failed
        val dst = if (m.hasProxy) m.proxy else sender
        this withEvent SendTo(dst, DissectorDone(m))
    }

    def onEvent = {
      case _: Tcp.Connected =>
        val props = Props(classOf[DissectionClient], logging, logReceive, sender, self)
        val dissectionClient = context actorOf props
        pendingCmds >>: Connected(dissectionClient, connectCmd)

      case Tcp.CommandFailed(_: Tcp.Connect) =>
        timers.startSingleTimer(Reconnect.toString, Reconnect, 3.seconds)
        this

      case Reconnect =>
        this withCommand connectCmd
    }
  }

  private case class Connected(dissectionClient: ActorRef, connectCmd: SendTo, pendingDissections: Vector[Message] = Vector.empty)
    extends Behaviour {
    def onCommand = {
      case DissectorDo(m: Message) if m.hasFrames && m.hasData =>
        require(m.frames.length == 2, "Frames should contain messages and ack only.")
        val proto = if (m.srvAddr.getPort == 443) "HTTP" else ""
        val dissectionReq = DissectionRequest(m.frames(0).getBytes, m.frames(1).getBytes, proto)
        val proxy = if (m.hasProxy) m.proxy else sender
        copy(pendingDissections = pendingDissections :+ m.setProxy(proxy)).withCommand(SendTo(dissectionClient, dissectionReq))

      case DissectorDo(m: Message) if m.hasFrames =>
        var cmds = Vector.empty[SendTo]
        m.frames foreach (x => {
          val dissectionReq = new DissectionRequest(Array.empty, x.getBytes, "")
          cmds :+= SendTo(dissectionClient, dissectionReq)
        })
        val proxy = if (m.hasProxy) m.proxy else sender
        this.withEvent(SendTo(proxy, DissectorDone(m))).withCommands(cmds)

    }

    def onEvent = {
      case DissectionResponse(dissection) if pendingDissections.nonEmpty =>
        val originalMsg = pendingDissections.head
        require(originalMsg.hasProxy)
        val resp = if (dissection.isEmpty) DissectorDone(originalMsg)
        else DissectorDone(originalMsg.setDissection(dissection.get))
        copy(pendingDissections = pendingDissections.tail) withEvent SendTo(originalMsg.proxy, resp)

      case DissectionResponse(_) =>
        this

      case Reconnect =>
        dissectionClient ! PoisonPill
        Connecting(connectCmd, connectionFailed = true) withCommand connectCmd
    }
  }
}
