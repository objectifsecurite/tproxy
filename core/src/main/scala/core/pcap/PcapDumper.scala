package ch.os.tproxy.core.pcap

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.io.Tcp

object PcapDumper {

  case class WriteFrames(frames: Array[PcapFrame]) extends Tcp.Command

}

class PcapDumper(dumpfile: String) extends Actor with ActorLogging {

  import PcapDumper._

  private lazy val dumpfd = {
    log debug ("Initializing dump file" + dumpfile + ".")
    val fd = PcapDump.init(dumpfile)
    if (fd < 0) log warning ("Cannot create dump file " + dumpfile + ".")
    fd
  }

  def receive = {
    case WriteFrames(frames) => PcapDump.write(dumpfd, frames)
  }
}
