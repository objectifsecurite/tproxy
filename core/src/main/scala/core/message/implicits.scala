package ch.os.tproxy.core.message

import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.pcap.PcapFrame
import ch.os.tproxy.core.dissection.DissectTree
import ch.os.tproxy.common.message.{FieldMap, Message}
import ch.os.tproxy.core.behaviour.ssl.SslHandler.implicits._

import java.text.SimpleDateFormat
import java.util.Date


object implicits {
  private val framesKey = "frames"

  implicit class Frames[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasFrames = msg has framesKey

    def setFrames(value: Array[PcapFrame]) = msg set (framesKey -> value)

    def remFrames = msg rem framesKey

    def frames = msg.get[Array[PcapFrame]](framesKey)
  }

  private val dissectionKey = "dissection"

  implicit class Dissection[A <: FieldMap[A]](val msg: A) extends AnyVal {
    def hasDissection = msg has dissectionKey

    def setDissection(dissection: DissectTree) = {
      msg.set(dissectionKey -> dissection)
        .setInfo(dissection.getInfo)
        .setProto(dissection.getProtocol)
        .setIsChunk(dissection.getRoot hasTag "tcp.segment_data")
        .setIsLastChunk(dissection.getRoot hasTag "tcp.segments")
    }

    def remDissection = msg rem dissectionKey

    def dissection = msg.get[DissectTree](dissectionKey)
  }

  implicit class Display(val msg: Message) extends AnyVal {
    def displayItem(col: Int): String = col match {
      case 0 => new SimpleDateFormat("H:mm:ss") format new Date(msg.time)
      case 1 => if (msg.hasCltAddr) msg.cltAddr.getAddress.getHostAddress else "?"
      case 2 => if (msg.hasCltAddr) msg.cltAddr.getPort.toString else "?"
      case 3 => if (msg.isRequest) "-->" else "<--"
      case 4 => if (msg.hasSrvAddr) msg.srvAddr.getAddress.getHostAddress else "?"
      case 5 => if (msg.hasSrvAddr) msg.srvAddr.getPort.toString else "?"
      case 6 => if (msg.hasProto) msg.proto else ""
      case 7 => {
        // if (modelItem.parent != Some(model.root)) "Chunk"
        // else if (modelItem.childCount > 0) "Chunked message"
        if (msg.hasInfo) msg.info
        else if (msg.hasClose) "Close message"
        else if (msg.hasSsl) {
          if (msg.hasSslCertificates) "SSL Server certificates"
          else "SSL Start"
        } else if (!msg.hasData) "Connection message"
        else "Unknown message"
      }
    }
  }

}