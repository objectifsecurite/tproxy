package ch.os.tproxy.core.message.json

import java.net.{InetSocketAddress => Addr}
import java.util.Base64
import akka.util.ByteString
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.{Message, Request, Response}
import upickle.default.{macroRW, ReadWriter => RW, readwriter}

object ByteStringRW {
  implicit val byteStringRw: RW[ByteString] = readwriter[String].bimap[akka.util.ByteString](
    x => Base64.getEncoder.encodeToString(x.toArray),
    str => ByteString(Base64.getDecoder.decode(str))
  )
}

object AddrRW {
  implicit val addrRw: RW[java.net.InetSocketAddress] = readwriter[ujson.Value].bimap[java.net.InetSocketAddress](
    x => ujson.Obj("ip" -> ujson.Str(x.getHostString), "port" -> ujson.Num(x.getPort)),
    json => new Addr(json("ip").str, json("port").num.toInt)
  )
}

//object MessageRW {
//import ByteStringRW._
//import AddrRW._
//implicit val messageRw = readwriter[ujson.Value].bimap[Request] (
//x => ujson.Obj("cid" -> x.cltAddr),
//json => Request(json("cid").str, new Addr("1.2.3.4", 80))
//)
//}

case class MessageJson(cid: String, msg_type: String, cltAddr: Addr, srvAddr: Addr, close: Boolean, data: ByteString) {
  def toReq = msg_type match {
    case "request" =>
      val request = Request(cid, cltAddr) setSrvAddr srvAddr
      if (!data.isEmpty)
        request setData data
      else if (close)
        request setClose akka.io.Tcp.Closed
      else
        request
    case "response" =>
      val response = Response(cid, srvAddr) setCltAddr cltAddr
      if (!data.isEmpty)
        response setData data
      else if (close)
        response setClose akka.io.Tcp.Closed
      else
        response
  }
}

object MessageJson {

  import ByteStringRW._
  import AddrRW._

  implicit val rw: RW[MessageJson] = macroRW

  def apply(msg: Message) = {
    val data = if (msg.hasData) msg.data else ByteString("")
    msg match {
      case msg: Request => new MessageJson(msg.cid, "request", msg.cltAddr, msg.srvAddr, msg.hasClose, data)
      case msg: Response => new MessageJson(msg.cid, "response", msg.cltAddr, msg.srvAddr, msg.hasClose, data)
    }
  }
}

