package ch.os.tproxy.core.actor

import java.net.{Socket, InetSocketAddress => Addr}
import akka.io.{IO, Inet, Tcp}
import akka.actor.Props
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.core.Native
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.core.behaviour.ssl.SslHandler
import ch.os.tproxy.common.behaviour.actor.SendEvtTo
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.core.behaviour.Encapsulator
import ch.os.tproxy.common.log.{LogReceive, Logging}

object ServerActor {
  def apply(cid: String, addr: Addr)(implicit logging: Logging, logReceive: LogReceive) =
    Props(classOf[ServerActor], cid, addr, logging, logReceive)

  private case object TosSockOpt extends Inet.SocketOption {
    def setTos(s: Socket) = Native setTos(s.getChannel, 0x20)

    override def beforeConnect(s: Socket) = {
      var isTosSet = setTos(s)

      if (!isTosSet) {
        val canSetCap = Native.setCapForCurrentUser == 0
        isTosSet = canSetCap && setTos(s)
      }

      require(isTosSet, s"Cannot set tos socket option on $s")
    }
  }

}

class ServerActor(cid: String, addr: Addr, _logging: Logging, _logReceive: LogReceive) extends NetNode {

  import ServerActor.TosSockOpt

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive

  def onStart = {
    log debug s"connecting to $addr"

    val tcpConnectCmd = Tcp.Connect(addr, options = List(TosSockOpt))
    val connectCmd = SendTo(IO(Tcp), tcpConnectCmd, needWrap = false)

    (SendEvtTo(context.parent, "proxy") <->
      SslHandler() <->
      Encapsulator(cid, addr, clientMode = false) <->
      Backpressure <->
      Connecting(tcpConnectCmd)) withCommand connectCmd
  }

  private case class Connecting(tcpConnectCmd: Tcp.Connect,
                                pendingCmds: Vector[Any] = Vector.empty)
    extends Behaviour {

    lazy val log = logging(this)

    def onCommand = {
      case cmd => copy(pendingCmds = pendingCmds :+ cmd)
    }

    def onEvent = {
      case Tcp.Connected(remoteAddr, localAddr) => {
        log debug s"connected to $remoteAddr on local port ${localAddr.getPort}"
        log debug s"sending ${pendingCmds.size} buffered commands"

        val connection = context.sender
        val register = SendTo(connection, Tcp.Register(self), needWrap = false)
        val cmds = register +: pendingCmds
        val evt = Tcp.Connected(tcpConnectCmd.remoteAddress, localAddr)

        (cmds >>: SendNetCmdTo(connection, "server")) withEvent evt
      }

      case Tcp.CommandFailed(`tcpConnectCmd`) => this withEvent Tcp.Aborted
    }
  }

}
