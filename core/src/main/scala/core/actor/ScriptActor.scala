package ch.os.tproxy.core.actor

import akka.actor.{ActorRef, ActorRefFactory, PoisonPill, Props, Timers}
import akka.io.{IO, Tcp}

import scala.concurrent.duration._
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.Message

import java.net.{InetSocketAddress => Addr}

object ScriptActor {
  private var actor: Option[ActorRef] = None

  case object Reconnect

  def getActor(master: ActorRef)
              (implicit factory: ActorRefFactory, logging: Logging, logReceive: LogReceive) = {
    actor match {
      case Some(a) => a
      case None => {
        val props = Props(classOf[ScriptActor], master, logging, logReceive)
        val a = factory actorOf(props, "ScriptActor")
        actor = Some(a)
        a
      }
    }
  }
}

class ScriptActor(master: ActorRef, _logging: Logging, _logReceive: LogReceive) extends ActorNode with Timers {

  import ScriptActor.Reconnect

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive

  val addr = new Addr("127.0.0.1", 9081)

  def onStart = {
    log debug s"connecting to $addr"
    val tcpConnectCmd = Tcp.Connect(addr)
    val connectCmd = SendTo(IO(Tcp), tcpConnectCmd, needWrap = false)

    Connecting(tcpConnectCmd) withCommand connectCmd
  }

  private case class Connecting(tcpConnectCmd: Tcp.Connect, pendingCmds: Vector[Any] = Vector.empty) extends Behaviour {
    def onCommand = {
      case cmd =>
        copy(pendingCmds = pendingCmds :+ cmd)
    }

    def onEvent = {
      case Tcp.Connected(remoteAddr, localAddr) => {
        log debug s"connected to $remoteAddr on local port ${localAddr.getPort}"
        log debug s"sending ${pendingCmds.size} buffered commands"
        val props = Props(classOf[ScriptClient], context.sender, master, logging, logReceive)
        val scriptClient = context actorOf props
        pendingCmds >>: Connected(scriptClient, tcpConnectCmd)
      }

      case Tcp.CommandFailed(_: Tcp.Connect) =>
        timers.startSingleTimer("tick", Reconnect, 3.seconds)
        this
      case Reconnect =>
        reconnect(pendingCmds)
    }
  }


  private case class Connected(scriptClient: ActorRef, tcpConnectCmd: Tcp.Connect) extends Behaviour {
    def onCommand = {
      case msg: Message =>
        require(msg.hasProxy, s"msg with no ref to proxy reached scriptActor")
        require(msg.hasScriptIntercepted, s"msg without scriptIntercepted flag reached scriptActor")
        this withCommand SendTo(scriptClient, msg)
    }

    def onEvent = {
      case Reconnect =>
        scriptClient ! PoisonPill
        reconnect(Vector.empty)
    }
  }

  private def reconnect(pendingCmds: Vector[Any]) = {
    val tcpConnectCmd = Tcp.Connect(addr)
    val connectCmd = SendTo(IO(Tcp), tcpConnectCmd, needWrap = false)
    Connecting(tcpConnectCmd, pendingCmds) withCommand connectCmd
  }

}
