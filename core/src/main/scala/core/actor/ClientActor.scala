package ch.os.tproxy.core.actor

import java.net.{InetSocketAddress => Addr}
import akka.io.Tcp
import akka.actor.{ActorRef, Props}
import akka.util.ByteString
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.core.behaviour.ssl.SslHandler
import ch.os.tproxy.core.behaviour.Encapsulator
import ch.os.tproxy.common.behaviour.actor.SendEvtTo
import ch.os.tproxy.common.log.{LogReceive, Logging}

object ClientActor {
  def apply(cid: String, addr: Addr, connection: ActorRef)
           (implicit logging: Logging, logReceive: LogReceive) =
    Props(classOf[ClientActor], cid, addr, connection, logging, logReceive)
}

class ClientActor(cid: String, addr: Addr,
                  connection: ActorRef, _logging: Logging, _logReceive: LogReceive)
  extends NetNode {
  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive
  val emptyReceived = Tcp.Received(ByteString.empty)

  def onStart = {
    connection ! Tcp.Register(self)

    (SendEvtTo(context.parent, "proxy") <->
      SslHandler() <->
      Encapsulator(cid, addr, clientMode = true) <->
      Backpressure <->
      SendNetCmdTo(connection, "client")) :<< emptyReceived
  }
}
