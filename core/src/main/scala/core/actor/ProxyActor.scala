package ch.os.tproxy.core.actor

import java.lang.{IllegalArgumentException => IAE}
import java.net.{InetSocketAddress => Address}
import akka.actor.{ActorRef, Props, Terminated}
import akka.io.Tcp
import ch.os.tproxy.common.behaviour.{Behaviour, OnEvent}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.common.behaviour.actor.message.{SendTo, Stop}
import ch.os.tproxy.core.behaviour.PcapBehaviour.{Dissect, Dump, Sim}
import ch.os.tproxy.common.message.{ClientAccepted, ClientClosed, ClientConnected, ClientRefused, Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.log.{LogReceive, Logging}

object ProxyActor {

  trait Config {
    def logging: Logging

    def logReceive: LogReceive

    def master: ActorRef

    def filter: Option[ActorRef]

    def dissector: Option[ActorRef]

    def pcapDumper: Option[ActorRef]
  }

  def apply(cid: String, cltAddr: Address, srvAddr: Address,
            clientConnection: ActorRef)(implicit config: Config) =
    Props(classOf[ProxyActor], cid, cltAddr, srvAddr, clientConnection, config)
}

class ProxyActor(cid: String, cltAddr: Address, srvAddr: Address,
                 clientConnection: ActorRef, config: ProxyActor.Config)
  extends ActorNode {

  import config._

  implicit lazy val logging = config.logging forObject this addMDC (
    "connectionId" -> cid
    )
  implicit val logReceive = config.logReceive

  def onStart = {
    val clientProps = ClientActor(cid, cltAddr, clientConnection)
    val client = context actorOf(clientProps, s"ClientActor")

    val notification = SendTo(master,
      ClientAccepted(cid, cltAddr, srvAddr),
      needWrap = false)

    (Forwarder <->
      (if (dissector.isDefined) Dissect(dissector.get) else None) <->
      (if (pcapDumper.isDefined) Dump(pcapDumper.get) else None) <->
      Sim.Waiting() <->
      Router(client)) withEvent notification
  }

  private case object Forwarder extends OnEvent {
    def onEvent = {
      case evt: Message if filter.isDefined => this withEvent SendTo(filter.get, evt)
      case evt: Message => this withEvent SendTo(master, evt) withCommand evt
    }
  }

  private object Route {

    object Status extends Enumeration {
      type Enum = Value
      val Connecting, Connected, HalfClosed, Closed = Value
    }

  }

  private case class Route(status: Route.Status.Enum,
                           dstAddr: Address,
                           dst: ActorRef)

  private object Router {
    def apply(client: ActorRef): Router = {
      val route = Route(Route.Status.Connected, cltAddr, client)
      Router(Map(route.dstAddr -> route))
    }
  }

  private case class Router(table: Map[Address, Route]) extends Behaviour {

    import Route.Status._

    def onCommand = {
      case msg: Request if !routeExistsFor(srvAddr) => {
        val server = context actorOf(ServerActor(cid, srvAddr), "ServerActor")
        addRouteTo(server) withCommand SendTo(server, msg)
      }

      case msg: Message if msg.hasClose => {
        val route = routeFor(msg.dstAddr)
        val status = if (route.status == HalfClosed) Closed else HalfClosed

        val result = {
          val cmd = SendTo(route.dst, msg)
          updateStatus(route, status) withCommand cmd
        }

        if (status == Closed) {
          context watch route.dst
          result addCommand SendTo(route.dst, Stop)
        }
        else result
      }

      case msg: Message => {
        val route = routeFor(msg.dstAddr)
        this withCommand SendTo(route.dst, msg)
      }
    }

    def onEvent = {
      case Tcp.Connected(remoteAddr, _) => {
        val route = routeFor(remoteAddr)

        val result = route.status match {
          case Connecting => updateStatus(route, Connected)
          case HalfClosed => this
          case status => throw new IAE(s"Invalid route status $status")
        }

        val notification = SendTo(master, ClientConnected(cid), needWrap = false)
        result withEvent notification
      }

      case msg: Response if isConnecting(srvAddr) => {
        require(msg.hasClose, "The only allowed response is a close")
        val route = (table get srvAddr).get
        val notif = SendTo(master, ClientRefused(cid), needWrap = false)
        val updatedMsg = updateAddr(msg)

        updateStatus(route, HalfClosed) withEvents Vector(notif, updatedMsg)
      }

      case msg: Message if msg.hasClose => {
        val route = routeFor(msg.srcAddr)
        val status = if (route.status == HalfClosed) Closed else HalfClosed
        val result = updateStatus(route, status) withEvent updateAddr(msg)

        if (status == Closed) {
          context watch route.dst
          result addCommand SendTo(route.dst, Stop)
        }
        else result
      }

      case msg: Message => this withEvent updateAddr(msg)

      case Terminated(actor) => {
        val route = routeFor(actor)
        val updatedRouter = Router(table - route.dstAddr)

        if (updatedRouter.table.isEmpty) {
          val notif = SendTo(master, ClientClosed(cid), needWrap = false)
          updatedRouter withCommands Vector(notif, Stop)
        }
        else updatedRouter
      }
    }

    private def addRouteTo(server: ActorRef) = {
      val route = Route(Connecting, srvAddr, server)
      val updatedTable = table + (route.dstAddr -> route)
      Router(updatedTable)
    }

    private def updateStatus(route: Route, status: Route.Status.Enum) = {
      val updatedRoute = route.copy(status = status)
      val updatedTable = table + (route.dstAddr -> updatedRoute)
      Router(updatedTable)
    }

    private def updateAddr(msg: Message) = msg setDstAddr {
      msg match {
        case _: Request => srvAddr
        case _: Response => cltAddr
      }
    }

    private def routeFor(dst: ActorRef) = {
      val routes = table.values filter (_.dst == dst)
      require(routes.size == 1, s"A unique route to actor $dst must exist")
      routes.head
    }

    private def routeFor(dst: Address) = {
      val route = table get dst
      require(route.isDefined, s"A route to address $dst must exist")
      route.get
    }

    private def routeExistsFor(dst: Address) = table contains dst

    private def isConnecting(dst: Address) = routeFor(dst).status == Connecting
  }

  override def postStop() = super.postStop()
}
