package ch.os.tproxy.core.actor

import akka.actor.{ActorRef, Timers}
import akka.io.Tcp
import akka.util.ByteString
import ch.os.tproxy.common.actor.NetNode
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.actor.message.{Command, SendTo}
import ch.os.tproxy.common.behaviour.tcp.TcpSegmentAggregator
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.actor.ScriptActor.Reconnect
import ch.os.tproxy.core.message.json.MessageJson
import upickle.default.{read, write}

class ScriptClient(connection: ActorRef, master: ActorRef, _logging: Logging, _logReceive: LogReceive) extends NetNode with Timers {
  implicit lazy val logging: Logging = _logging

  implicit val logReceive: LogReceive = _logReceive

  def onStart: Behaviour.Result =
    (Connected()
      <-> TcpSegmentAggregator()
      <-> Backpressure
      <-> SendNetCmdTo(connection, "scriptConnection")) withCommand SendTo(connection, Tcp.Register(self), needWrap = false)


  case class Connected(connectionMap: Map[String, ActorRef] = Map.empty) extends Behaviour {
    def onCommand: Behaviour.Receive = {
      case msg: Message =>
        require(msg.hasProxy, s"msg with no ref to proxy reached scriptActor")
        require(msg.hasScriptIntercepted, s"msg without scriptIntercepted flag reached scriptActor")

        // todo: remove from map when connection is closed
        val newConnectionMap = if (!(connectionMap contains msg.cid)) connectionMap + (msg.cid -> msg.proxy) else connectionMap

        if (msg.hasData || msg.hasClose) {
          val json = write(MessageJson(msg))
          copy(connectionMap = newConnectionMap) withCommand ByteString(json)
        }
        else {
          val newMsg = msg.remScriptIntercepted
          val proxy = newConnectionMap get newMsg.cid
          proxy.get ! Command(newMsg)
          copy(connectionMap = newConnectionMap) withEvent SendTo(master, newMsg)
        }
    }

    def onEvent: Behaviour.Receive = {
      case data: ByteString =>
        val newMsg = read[MessageJson](data.utf8String).toReq
        val proxy = connectionMap get newMsg.cid
        proxy.get ! Command(newMsg)
        this withEvent SendTo(master, newMsg)

      case _: Tcp.ConnectionClosed =>
        this withEvent SendTo(context.parent, Reconnect)
    }
  }
}
