package ch.os.tproxy.core.behaviour.ssl

import java.security.cert.Certificate
import java.security.cert.X509Certificate
import akka.io.Tcp
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.OnCommandOrEvent
import ch.os.tproxy.core.behaviour.ssl.ClientHelloParser.{APPLICATION_PROTOCOLS, SNI}
import ch.os.tproxy.core.behaviour.ssl.SslLayer.CertificateReceived
import ch.os.tproxy.common.message.{FieldMap, Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.behaviour.ssl.SslHandler.supportedProtocols
import ch.os.tproxy.core.ssl.SSLEngineFactory
import ch.os.tproxy.core.ssl.SslContext
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder
import org.bouncycastle.asn1.x500.style.BCStyle
import org.bouncycastle.asn1.x500.style.IETFUtils

import javax.net.ssl.{SSLEngine, SSLParameters}

object SslHandler {
  object implicits {
    private val sslCertificatesKey = "sslCertificates"

    implicit class SslCertificates[A <: FieldMap[A]](val msg: A) extends AnyVal {
      def hasSslCertificates = msg has sslCertificatesKey

      def certificates = msg.get[Array[Certificate]](sslCertificatesKey)

      def setSslCertificates(value: Array[Certificate]) = msg set (sslCertificatesKey -> value)
    }

    private val sslKey = "ssl"

    implicit class SslStart[A <: FieldMap[A]](val msg: A) extends AnyVal {
      def hasSsl = msg has sslKey

      def setSsl = msg set (sslKey -> true)
    }

    private val sniKey = "sni"

    implicit class Sni[A <: FieldMap[A]](val msg: A) extends AnyVal {
      def hasSni = msg has sniKey

      def setSni(value: String) = msg set (sniKey -> value)

      def sni = msg.get[String](sniKey)
    }

    private val appProtoKey = "applicationProtocols"

    implicit class ApplicationProtocols[A <: FieldMap[A]](val msg: A) extends AnyVal {
      def hasApplicationProtocols = msg has appProtoKey

      def setApplicationProtocols(value: Array[String]) = msg set (appProtoKey -> value)

      def applicationProtocols: Array[String] = msg.get[Array[String]](appProtoKey)
    }
  }
  val supportedProtocols: Array[String] = Array("TLSv1.2", "TLSv1.1", "TLSv1", "SSLv3")

}

case class SslHandler()(implicit logging: Logging) extends Behaviour {

  import SslHandler.implicits._

  def onEvent = {
    case r: Request if r.hasData && ClientHelloParser.isSslClientHello(r.data) => {
      val extensions = ClientHelloParser extractExtensions r.data
      val snis = extensions.getOrElse(SNI, Vector.empty)
      var sslstart = if (snis.isEmpty) r.remData.setSsl
      else r.remData.setSsl.setSni(snis.head.utf8String)

      val applicationProtocols = extensions.getOrElse(APPLICATION_PROTOCOLS, Vector.empty).map(_.utf8String).toArray
      if (!applicationProtocols.isEmpty)
        sslstart = sslstart.setApplicationProtocols(applicationProtocols)

      WaitingForCertificate(r) :<< sslstart
    }
  }

  def onCommand = {
    case req: Request if req.hasSsl =>
      val engine = if (req.hasSni && req.hasSrvAddr)
        SslContext.createEngine(req.sni, req.srvAddr.getPort)
      else SslContext.createEngine
      setupSslEngine(engine, if(req.hasApplicationProtocols) req.applicationProtocols else Array.empty)

      val result = SslLayer(engine, client = true)
      req >>: SslWrapper(client = true, result)
  }

  case class WaitingForCertificate(hello: Message) extends OnCommandOrEvent {
    def onCommandOrEvent = {
      case r: Response if r.hasSslCertificates => {
        val certificate = r.certificates.head.asInstanceOf[X509Certificate]

        val hostname = {
          val x500name = new JcaX509CertificateHolder(certificate).getSubject
          val rdns = x500name.getRDNs(BCStyle.CN)

          if (rdns.isEmpty) {
            require(r.hasSrvAddr, "A response must come from somewhere")
            r.srvAddr.getHostString
          } else {
            val cn = rdns.head
            IETFUtils.valueToString(cn.getFirst.getValue)
          }
        }

        val engine = SSLEngineFactory.getEngine(hostname, certificate)
        setupSslEngine(engine, if(r.hasApplicationProtocols) r.applicationProtocols else Array.empty)

        val result = SslLayer(engine, client = false)
        SslWrapper(client = false, result) :<< hello
      }
    }
  }

  case class SslWrapper(client: Boolean, ssllayer: Result) extends Behaviour {
    def onCommand = {
      case m: Message if m.hasData => process(m, Tcp.Write(m.data) >>: ssllayer)
      case m: Message if m.hasClose => process(m, Tcp.Close >>: ssllayer)
      case m: Message if m.hasSsl => process(m, ssllayer)
    }

    def onEvent = {
      case m: Message if m.hasData => process(m, ssllayer :<< Tcp.Received(m.data))
      case m: Message if m.hasClose => process(m, ssllayer :<< Tcp.Closed)
    }

    private val msgForCmd: Message => Message =
      if (client) msg => Request(msg) else msg => Response(msg)

    private val msgForEvt: Message => Message =
      if (client) msg => Response(msg) else msg => Request(msg)

    private def process(origMsg: Message, result: Result) = {
      val msg = origMsg.remData.remClose.setSsl

      val commands = result.commands map (_ match {
        case cmd: Tcp.Write => msgForCmd(msg) setData cmd.data
        case cmd: Tcp.CloseCommand => msgForCmd(msg) setClose cmd.event
        case cmd => cmd
      })

      val events = result.events map (_ match {
        case evt: Tcp.Received => msgForEvt(msg) setData evt.data
        case evt: Tcp.ConnectionClosed => msgForEvt(msg) setClose evt
        case evt: CertificateReceived => evt.applicationProtocols match {
          case Some(p) => msgForEvt(msg) setSslCertificates evt.certificates setApplicationProtocols Array(p)
          case None => msgForEvt(msg) setSslCertificates evt.certificates
        }

        case evt => evt
      })

      SslWrapper(client, result.behaviours) withCommands commands withEvents events
    }
  }
  private def setupSslEngine(sslEngine: SSLEngine, applicationProtocols: Array[String]) = {
    if (supportedProtocols.nonEmpty)
      sslEngine.setEnabledProtocols(supportedProtocols)
    if (applicationProtocols.nonEmpty) {
      val sslParams: SSLParameters = sslEngine.getSSLParameters
      sslParams.setApplicationProtocols(applicationProtocols)
      sslEngine.setSSLParameters(sslParams)
    }
  }
}

