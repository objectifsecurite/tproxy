package ch.os.tproxy.core.behaviour.ssl

import java.security.cert.Certificate

import scala.annotation.tailrec
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.io.Tcp
import akka.util.ByteString
import ch.os.tproxy.common.log.Logging
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.OnCommandOrEvent
import javax.net.ssl.SSLEngine
import javax.net.ssl.SSLEngineResult
import javax.net.ssl.SSLEngineResult.HandshakeStatus.FINISHED
import javax.net.ssl.SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING
import javax.net.ssl.SSLEngineResult.Status.CLOSED
import javax.net.ssl.SSLEngineResult.Status.OK
import javax.net.ssl.SSLException

object SslLayer {
  case class CertificateReceived(certificates: Array[Certificate], applicationProtocols: Option[String])

  def apply(sslengine: SSLEngine, client: Boolean)
           (implicit logging: Logging): Result = {

    sslengine.setUseClientMode(client)
    sslengine.setNeedClientAuth(false)

    val ssllayer = SslLayer(sslengine, client, logging)

    if (client) ssllayer.Handshaking().encode()
    else ssllayer.Handshaking().decode().get
  }
}

case class SslLayer(sslengine: SSLEngine, client: Boolean,
                    logging: Logging) {

  import SslLayer._

  val sslproto = new SslProto(sslengine)

  private abstract class State extends OnCommandOrEvent {
    lazy val log = logging(this)

    def encode(data: ByteString = ByteString.empty) = {
      log debug s"Encode started with ${data.size} bytes"
      val (res, cmds, evts) = sslproto encode data
      log debug s"Encode finished with ($res, ${cmds.size} bytes, ${evts.size} bytes)"
      nextState(res.getStatus, generateCommands(cmds), generateEvents(evts), true)
    }

    def decode(data: ByteString = ByteString.empty): Try[Result] = {
      log debug s"Decode started with ${data.size} bytes"
      val result = Try(sslproto decode data)

      log debug (result match {
        case Success((res, cmdBytes, evtBytes)) =>
          s"Decode finished with ($res, ${cmdBytes.size} bytes, ${evtBytes.size} bytes)"
        case Failure(e) =>
          s"Decode finished with ${e.getMessage}"
      })

      for ((res, cmdBytes, evtBytes) <- result) yield {
        val cmds = generateCommands(cmdBytes)
        val evts = generateEvents(evtBytes)
        nextState(res.getStatus, cmds, evts, false)
      }
    }

    def nextState(status: SSLEngineResult.Status, cmds: List[Tcp.Command],
                  evts: List[Tcp.Event], wasEncoding: Boolean): Result = {
      status match {
        case CLOSED if wasEncoding => {
          log debug "SSLEngine outBound is closed, sending Tcp.Close"
          Closed withCommands (cmds :+ Tcp.Close) withEvents evts
        }
        case CLOSED => {
          log debug s"SSLEngine inBound is closed"
          close(cmds, evts, true)
        }
        case _ => this withCommands cmds withEvents evts
      }
    }

    def generateCommands(data: ByteString) = {
      if (data.length <= 0) Nil
      else List(Tcp.Write(data))
    }

    def generateEvents(data: ByteString) = {
      if (data.length <= 0) Nil
      else List(Tcp.Received(data))
    }

    def onCommandOrEvent = {
      case Tcp.Received(data) => decode(data).get
      case Tcp.Write(data, _) => encode(data)
      case cmd: Tcp.CloseCommand => {
        log debug s"Closing engine due to reception of $cmd"
        close()
      }
      case evt: Tcp.ConnectionClosed => {
        log debug s"Closing engine due to reception of $evt (forwarded)"
        try sslengine.closeInbound
        catch {
          case e: SSLException =>
        } // Truncation attack warning
        sslengine.closeOutbound
        val (_, evts) = closeEngine()
        Closed withEvents (evts :+ evt)
      }
    }

    protected def close(commands: List[Any] = Nil,
                        events: List[Any] = Nil,
                        noCommand: Boolean = false) = {
      log debug "Closing SSLEngine outBound and sending Tcp.Close"
      sslengine.closeOutbound

      val (cmds, evts) = {
        val (newCmds, newEvts) = closeEngine()
        (if (noCommand) commands ++ newCmds else commands ++ newCmds :+ Tcp.Close,
          events ++ newEvts)
      }
      log debug s"Going to Closed with ${cmds.length} commands"
      Closed withCommands cmds withEvents evts
    }

    @tailrec
    private def closeEngine(commands: ByteString = ByteString.empty,
                            events: ByteString = ByteString.empty)
    : (List[Tcp.Command], List[Tcp.Event]) = {
      if (!sslengine.isOutboundDone) {
        val (_, cmds, evts) = sslproto.encode()
        closeEngine(commands ++ cmds, events ++ evts)
      } else (generateCommands(commands), generateEvents(events))
    }
  }

  private case class Handshaking(closeCmd: Option[Tcp.CloseCommand] = None) extends State {
    override def onCommandOrEvent =
      decodeTcpDataOrClose orElse super.onCommandOrEvent

    private def decodeTcpDataOrClose: Behaviour.Receive = {
      case Tcp.Received(data) => decode(data) match {
        case Success(result) => result
        case Failure(e: SSLException) => {
          log debug e.getMessage
          e.getMessage match {
            case "Received fatal alert: unknown_ca" => close()
            case "Received fatal alert: bad_certificate" => close()
            case _ => throw e
          }
        }
        case Failure(e) => throw e
      }
      case cmd: Tcp.CloseCommand if cmd != Tcp.Abort => {
        log debug s"Finish handshaking before closing with $cmd"
        Handshaking(closeCmd = Some(cmd))
      }
    }

    override def nextState(status: SSLEngineResult.Status, cmds: List[Tcp.Command],
                           evts: List[Tcp.Event], wasEncoding: Boolean) = {
      require(status != CLOSED, "Oops ... CLOSED Happened while handshaking")
      val hsStatus = sslengine.getHandshakeStatus
      status match {
        case OK if hsStatus == FINISHED || hsStatus == NOT_HANDSHAKING => {
          val events = if (client) {
            val certifs = sslengine.getSession.getPeerCertificates
            val applicationProt = if (sslengine.getApplicationProtocol.isEmpty) None else Option(sslengine.getApplicationProtocol)
            evts :+ CertificateReceived(certifs, applicationProt)
          } else {
            evts
          }

          DoneHandshaking(cmds, events, closeCmd)
        }
        case _ => this withCommands cmds withEvents evts
      }
    }
  }

  private case object DoneHandshaking extends State {
    def apply(commands: List[Tcp.Command], events: List[Any], closeCmd: Option[Tcp.CloseCommand]) = {
      val (encCmds, encEvts) = encodeRemaining()
      val (decCmds, decEvts) = decodeRemaining()

      val cmds = commands ++ encCmds ++ decCmds
      val evts = events ++ encEvts ++ decEvts

      log debug s"Finished handshake with ${cmds.size} commands and ${evts.size} events"

      if (closeCmd.isDefined) close(cmds, evts)
      else Waiting withCommands cmds withEvents evts
    }

    @tailrec
    def encodeRemaining(commands: ByteString = ByteString.empty,
                        events: ByteString = ByteString.empty)
    : (List[Tcp.Command], List[Tcp.Event]) = {
      if (sslproto.needEncoding) {
        val (_, cmds, evts) = sslproto.encode()
        encodeRemaining(commands ++ cmds, events ++ evts)
      } else (generateCommands(commands), generateEvents(events))
    }

    @tailrec
    def decodeRemaining(commands: ByteString = ByteString.empty,
                        events: ByteString = ByteString.empty)
    : (List[Tcp.Command], List[Tcp.Event]) = {
      if (sslproto.needDecoding) {
        val (_, cmds, evts) = sslproto.decode()
        decodeRemaining(commands ++ cmds, events ++ evts)
      } else (generateCommands(commands), generateEvents(events))
    }
  }

  private case object Waiting extends State

  private case object Closed extends State {
    override def nextState(status: SSLEngineResult.Status, cmds: List[Tcp.Command],
                           evts: List[Tcp.Event], wasEncoding: Boolean) = {
      this
    }

    override def onCommandOrEvent = {
      case Tcp.Received(data) => decode(data) match {
        case Success(result) => result
        case Failure(e: SSLException) => this
        case Failure(e) => throw e
      }
      case _: Tcp.Write => log debug "Ignoring incoming Tcp.Write"; this
      case evt: Tcp.ConnectionClosed => {
        log debug s"Forwarding incoming $evt"
        this withEvent evt
      }
      case cmd: Tcp.CloseCommand => log debug s"Ignoring incoming $cmd"; this
    }
  }

}
