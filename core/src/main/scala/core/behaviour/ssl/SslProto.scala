package ch.os.tproxy.core.behaviour.ssl

import java.nio.ByteBuffer

import scala.annotation.tailrec

import akka.util.ByteString
import javax.net.ssl.SSLEngine
import javax.net.ssl.SSLEngineResult
import javax.net.ssl.SSLEngineResult.HandshakeStatus.FINISHED
import javax.net.ssl.SSLEngineResult.HandshakeStatus.NEED_TASK
import javax.net.ssl.SSLEngineResult.HandshakeStatus.NEED_UNWRAP
import javax.net.ssl.SSLEngineResult.HandshakeStatus.NEED_WRAP
import javax.net.ssl.SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING
import javax.net.ssl.SSLEngineResult.Status.BUFFER_OVERFLOW
import javax.net.ssl.SSLEngineResult.Status.BUFFER_UNDERFLOW
import javax.net.ssl.SSLEngineResult.Status.CLOSED
import javax.net.ssl.SSLEngineResult.Status.OK

class SslProto(engine: SSLEngine) {
  private val maxInput = engine.getSession.getApplicationBufferSize + 50
  private val maxOutput = 16665 + 2048

  private var inToEncode = ByteBuffer.allocate(maxInput)
  private var inToDecode = ByteBuffer.allocate(maxInput)
  private val outBuffer = ByteBuffer.allocate(maxOutput)

  def encode(data: ByteString = ByteString.empty) = {
    inToEncode = copyData(inToEncode, data)
    process(Encoding)
  }

  def decode(data: ByteString = ByteString.empty) = {
    inToDecode = copyData(inToDecode, data)
    process(Decoding)
  }

  def needDecoding = {
    inToDecode.position() > 0
  }

  def needEncoding = {
    inToEncode.position() > 0
  }

  private def copyData(buffer: ByteBuffer, data: ByteString) = {
    if (data.length > (buffer.capacity - buffer.position())) {
      buffer.flip
      val newData = ByteString(buffer) ++ data
      val newBuffer = ByteBuffer.allocate(newData.length)
      newData copyToBuffer newBuffer
      newBuffer
    } else {
      data copyToBuffer buffer
      buffer
    }
  }

  @tailrec
  private def process(mode: SslMode,
                      commands: ByteString = ByteString.empty,
                      events: ByteString = ByteString.empty)
  : (SSLEngineResult, ByteString, ByteString) = {
    val buffer = mode match {
      case Encoding => inToEncode
      case Decoding => inToDecode
    }

    buffer.flip
    val (sslresult, newCmds, newEvts) = processInput(mode, commands, events)
    buffer.compact

    sslresult.getStatus match {
      case OK | CLOSED => sslresult.getHandshakeStatus match {
        case NEED_WRAP if mode == Decoding => process(Encoding, newCmds, newEvts)
        case NEED_UNWRAP if mode == Encoding => process(Decoding, newCmds, newEvts)
        case _ => (sslresult, newCmds, newEvts)
      }
      case _ => (sslresult, newCmds, newEvts)
    }
  }

  @tailrec
  private def processInput(mode: SslMode,
                           commands: ByteString,
                           events: ByteString): (SSLEngineResult, ByteString, ByteString) = {
    val result = mode match {
      case Decoding => {
        engine.unwrap(inToDecode, outBuffer)
      }
      case Encoding => {
        engine.wrap(inToEncode, outBuffer)
      }
    }

    val status = result.getStatus
    val hsStatus = result.getHandshakeStatus

    status match {
      case BUFFER_UNDERFLOW => (result, commands, events)
      case BUFFER_OVERFLOW => (result, commands, events)
      case CLOSED if (hsStatus == NEED_UNWRAP) => {
        val (newCmds, newEvts) = processOutput(mode, commands, events)
        (result, newCmds, newEvts)
      }
      case OK | CLOSED => {
        val (newCmds, newEvts) = processOutput(mode, commands, events)
        hsStatus match {
          case NEED_TASK => runTasks; processInput(mode, newCmds, newEvts)
          case FINISHED => (result, newCmds, newEvts)
          case NOT_HANDSHAKING => {
            mode match {
              case Encoding if (inToEncode.hasRemaining) => processInput(mode, newCmds, newEvts)
              case Decoding if (inToDecode.hasRemaining) => processInput(mode, newCmds, newEvts)
              case _ => (result, newCmds, newEvts)
            }
          }
          case NEED_WRAP if mode == Encoding => processInput(Encoding, newCmds, newEvts)
          case NEED_UNWRAP if mode == Decoding => processInput(Decoding, newCmds, newEvts)
          case _ => (result, newCmds, newEvts)
        }
      }
    }
  }

  @tailrec
  private def runTasks(): Unit = {
    //println("RUNNING TASK")
    val task = engine.getDelegatedTask
    if (task != null) {
      task.run
      runTasks
    }
  }

  private def processOutput(mode: SslMode, commands: ByteString, events: ByteString) = {
    outBuffer.flip
    val data = ByteString(outBuffer)
    outBuffer.clear

    mode match {
      case Encoding => (commands ++ data, events)
      case Decoding => (commands, events ++ data)
    }
  }

  private abstract sealed class SslMode

  private case object Encoding extends SslMode

  private case object Decoding extends SslMode

}
