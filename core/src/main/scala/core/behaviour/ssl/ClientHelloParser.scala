package ch.os.tproxy.core.behaviour.ssl


import scala.annotation.tailrec
import akka.util.ByteString

import scala.collection.immutable.HashMap

object ClientHelloParser {
  val SNI = ByteString(0, 0)
  val APPLICATION_PROTOCOLS = ByteString(0, 16)


  def isTlsClientHello(data: ByteString) = {
    data.size >= 6 &&
      (data(0) & 0xff) == 0x16 &&
      (data(1) & 0xff) == 0x03 &&
      (data(5) & 0xff) == 0x01
  }

  def isSslv2ClientHello(data: ByteString) = {
    data.size >= 3 &&
      (data(0) & 0xff) == 0x80 &&
      (data(2) & 0xff) == 0x01
  }

  def isSslClientHello(data: ByteString) = {
    isTlsClientHello(data) || isSslv2ClientHello(data)
  }

  def isSslServerHello(data: ByteString) = {
    data.size >= 6 &&
      (data(0) & 0xff) == 0x16 &&
      (data(1) & 0xff) == 0x03 &&
      (data(5) & 0xff) == 0x02
  }


  private def shortAtPos(data: ByteString, pos: Int) = {
    ((data(pos) & 0xff) << 8) | (data(pos + 1) & 0xff)
  }

  def extractExtensions(data: ByteString): HashMap[ByteString, Vector[ByteString]] =
    try {
      require(isTlsClientHello(data), "Not a TLS client_hello")

      val headerLength = 5
      val length = shortAtPos(data, 3)

      require(data.length == headerLength + length,
        "Incorrect client_hello length")

      val fixedOffset = headerLength
      val fixedLength = 38

      require(data.length >= fixedOffset + fixedLength + 1,
        "Wrong client_hello format (header)")

      val sidOffset = fixedOffset + fixedLength //Constant
      val sidLength = data(sidOffset)

      require(data.length >= sidOffset + sidLength + 2,
        "Wrong client_hello format (session ID)")

      val cipherOffset = sidOffset + 1 + sidLength
      val cipherLength = shortAtPos(data, cipherOffset)

      require(data.length > cipherOffset + cipherLength + 1,
        "Wrong client_hello format (cipher suites)")

      val compOffset = cipherOffset + 2 + cipherLength
      val compLength = data(compOffset)

      require(data.length > compOffset + 2 + compLength,
        "Wrong client_hello format (compression methods)")

      val extOffset = compOffset + 1 + compLength
      val extLength = shortAtPos(data, extOffset)

      require(data.length >= extOffset + 2 + extLength,
        "Wrong client_hello format (extensions)")

      parseExtensions(data drop (extOffset + 2))
    } catch {
      case _: IllegalArgumentException => HashMap.empty
    }

  def extractSni(data: ByteString): Vector[ByteString] = extractExtensions(data).getOrElse(SNI, Vector.empty)

  @tailrec
  private def parseExtensions(data: ByteString, extensions: HashMap[ByteString, Vector[ByteString]] = HashMap.empty):
  HashMap[ByteString, Vector[ByteString]] = {
    if (data.length < 4) {
      extensions
    } else {
      val ext = data slice(0, 2)
      val len = shortAtPos(data, 2)

      require(data.length >= 4 + len, "Wrong client_hello extension format")
      // SNI TLS extension
      if (ext == SNI) {
        val namesData = parseExtensionListHeader(data, len, "server_name")
        parseExtensions(data drop (4 + len), extensions + (SNI -> parseServerNames(namesData)))
      }
      // application_layer_protocol_negotiation TLS extension
      else if (ext == APPLICATION_PROTOCOLS) {
        val protoData = parseExtensionListHeader(data, len, "application_layer_protocol_negotiation")

        val protos = parseStrings(protoData, 1)
        parseExtensions(data drop (4 + len), extensions + (APPLICATION_PROTOCOLS -> protos))
      }
      else parseExtensions(data drop (4 + len), extensions)
    }

  }

  private def parseExtensionListHeader(data: ByteString, len: Int, extName: String): ByteString = {
    val extData = data slice(4, 4 + len)
    require(extData.length >= 2, s"Wrong client_hello $extName list format")

    val listLen = shortAtPos(extData, 0)
    require(extData.length == 2 + listLen, s"Wrong client_hello $extName list format")
    extData drop 2
  }


  private def parseServerNames(data: ByteString): Vector[ByteString] = {
    if (data.isEmpty) Vector.empty
    else {
      require(data.length >= 3, "Wrong client_hello server_name list format")

      val htype = data(0)
      require(htype == 0x00, "Wrong client_hello host_name type")
      parseStrings(data drop 1, 2)
    }
  }

  @tailrec
  private def parseStrings(data: ByteString, byteLen: Int, result: Vector[ByteString] = Vector.empty): Vector[ByteString] = {
    require(byteLen == 1 || byteLen == 2) // byte length of the field length
    if (data.isEmpty)
      result
    else {
      val len = if (byteLen == 2) shortAtPos(data, 0) else data(0)
      require(data.length >= byteLen + len, "wrong length")

      parseStrings(data drop (byteLen + len), byteLen, result :+ (data slice(byteLen, byteLen + len)))
    }
  }
}
