package ch.os.tproxy.core.behaviour

import java.net.{InetSocketAddress => Addr}
import akka.io.Tcp
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.Behaviour
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.message.{Message, Request, Response}
import ch.os.tproxy.common.message.implicits._

case class Encapsulator(cid: String, addr: Addr, clientMode: Boolean)
  extends Behaviour {

  private def newMessage =
    if (clientMode) Request(cid, addr) else Response(cid, addr)

  def onEvent = {
    case Tcp.Received(ByteString.empty) => this withEvent newMessage
    case Tcp.Received(data) => this withEvent newMessage.setData(data)
    case evt: Tcp.ConnectionClosed => this withEvent newMessage.setClose(evt)
  }

  def onCommand = {
    case _: Request if clientMode => {
      require(false, "Request command not expected in client mode")
      this
    }
    case _: Response if !clientMode => {
      require(false, "Response command not expected in server mode")
      this
    }
    case m: Message if m.hasData => this withCommand Tcp.Write(m.data)
    case m: Message if m.hasClose => this withCommand Tcp.Close
    case m: Message => this
  }
}
