package ch.os.tproxy.core.behaviour

import ch.os.tproxy.common.behaviour.Behaviour.Result
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.implicits._
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.Behaviour

import scala.annotation.tailrec
import scala.reflect.ClassTag

object Plugin {

  case class BufferEvtUntil[A: ClassTag, B](buffer: B, stop: A => Boolean,
                                            update: (B, A) => B, onStop: (B, A) => Any)
    extends Behaviour {
    def onCommand = Behaviour.emptyReceive

    def onEvent = {
      case evt: A if stop(evt) => this withEvent onStop(buffer, evt)
      case evt: A => copy(buffer = update(buffer, evt))
      case a => println(a.getClass); this
    }
  }

  object FilterEvt {
    def apply[A: ClassTag](f: A => Boolean, onTrue: Result, onFalse: Result): Result = {
      val cmds = onTrue.commands ++ onFalse.commands
      val evts = onTrue.events ++ onFalse.events
      FilterEvt(f, onTrue.behaviours, onFalse.behaviours) withCommands cmds withEvents evts
    }
  }

  case class FilterEvt[A: ClassTag](f: A => Boolean,
                                    onTrue: Iterable[Behaviour],
                                    onFalse: Iterable[Behaviour]) extends Behaviour {
    def onCommand = Behaviour.emptyReceive

    def onEvent = {
      case evt: A if f(evt) => FilterEvt(f, onTrue :<< evt, onFalse)
      case evt: A => FilterEvt(f, onTrue, onFalse :<< evt)
    }
  }

  object TcpAggregateEvt {
    def test(msg: Message) = {
      msg.hasData && msg.hasDissection &&
        msg.dissection.getRoot.hasTag("tcp.segment_data")
    }

    def stop(msg: Message) = {
      require(msg.hasDissection, "Cannot aggregate non dissected messages")
      msg.dissection.getRoot.hasTag("tcp.segments")
    }

    def update(buffer: ByteString, msg: Message) = {
      require(msg.hasData, "Cannot aggregate non-data messages")
      buffer ++ msg.data
    }

    def onStop(buffer: ByteString, msg: Message) = msg setData update(buffer, msg)

    def apply(): Result = {
      val buffer = BufferEvtUntil(ByteString.empty, stop _, update _, onStop _)
      FilterEvt(test _, buffer, Result())
    }
  }

  case class SearchAndReplaceEvt(search: ByteString, replace: ByteString)
    extends Behaviour {
    def onCommand = Behaviour.emptyReceive

    def onEvent = {
      case msg: Message if msg.hasData => {
        val newData = replaceSlices(msg.data, findSlices(msg.data))
        this withEvent (msg setData newData)
      }
    }

    @tailrec
    private def replaceSlices(data: ByteString, indexes: Vector[Int], offset: Int = 0,
                              result: ByteString = ByteString.empty): ByteString = {
      if (indexes.isEmpty) result ++ data
      else {
        val taken = indexes.head - offset
        val nextoffset = taken + search.length
        replaceSlices(data.drop(nextoffset),
          indexes.tail,
          offset + nextoffset,
          result ++ data.take(taken) ++ replace)
      }
    }

    @tailrec
    private def findSlices(data: ByteString,
                           indexes: Vector[Int] = Vector.empty): Vector[Int] = {
      val start = if (indexes.isEmpty) 0 else indexes.last + search.length
      val index = data.indexOfSlice(search, start)
      if (index == -1) indexes
      else findSlices(data, indexes :+ index)
    }
  }

  object ContainsEvt {
    def contains(data: ByteString, msg: Message) = msg.hasData && (msg.data containsSlice data)

    def apply(data: ByteString, onContains: Result) = {
      FilterEvt(contains(data, _: Message), onContains, Result())
    }
  }

  object InterceptEvt extends Behaviour {
    def onCommand = Behaviour.emptyReceive

    def onEvent = {
      case msg: Message => this withEvent msg.setIntercepted
    }
  }

}
