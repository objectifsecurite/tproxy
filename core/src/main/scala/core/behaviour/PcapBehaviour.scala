package ch.os.tproxy.core.behaviour

import scala.annotation.tailrec
import akka.actor.ActorRef
import akka.util.ByteString
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.message.{Message, Request, Response}
import ch.os.tproxy.common.message.implicits._
import ch.os.tproxy.core.message.implicits._
import ch.os.tproxy.common.behaviour.actor.message.SendTo
import ch.os.tproxy.core.pcap.PcapDumper.WriteFrames
import ch.os.tproxy.core.pcap.PcapSim
import ch.os.tproxy.core.dissection.Dissector.DissectorDo
import ch.os.tproxy.core.dissection.Dissector.DissectorDone
import ch.os.tproxy.common.behaviour.OnEvent

object PcapBehaviour {

  case object Sim {

    case class Waiting(events: Vector[Any] = Vector.empty) extends OnEvent {
      def onEvent = {
        case req: Request if req.hasCltSrvAddr =>
          Simulating(new PcapSim(req.cltAddr, req.srvAddr)) :<< (events :+ req)

        case msg: Message => copy(events = events :+ msg)
      }
    }

    case class Simulating(sim: PcapSim, open: Boolean = false) extends OnEvent {
      def onEvent = {
        case r: Request if r.hasCltSrvAddr && !open => {
          require(!r.hasData && !r.hasClose,
            "The first message need not to have data or close.")
          copy(open = true) withEvent r.setFrames(sim.open)
        }
        case m: Message if m.hasData => {
          val packets = m.data grouped 61440
          val evts = createPcapEvents(packets, m)
          this withEvents evts
        }
        case r: Request if r.hasClose => this withEvent r.setFrames(sim.closeRequest)
        case r: Response if r.hasClose => this withEvent r.setFrames(sim.closeResponse)
      }

      @tailrec
      private def createPcapEvents(packets: Iterator[ByteString], msg: Message,
                                   events: Vector[Any] = Vector.empty): Vector[Any] = {
        if (packets.hasNext) {
          val pckt = packets.next
          val event = msg match {
            case r: Request => r.setData(pckt).setFrames(sim.request(pckt.toArray))
            case r: Response => r.setData(pckt).setFrames(sim.response(pckt.toArray))
          }
          createPcapEvents(packets, msg, events :+ event)
        } else events
      }
    }

  }

  case class Dump(dumper: ActorRef) extends OnEvent {
    def onEvent = {
      case m: Message if m.hasFrames => dumper ! WriteFrames(m.frames); this withEvent m
    }
  }

  case class Dissect(dissector: ActorRef) extends OnEvent {
    def onEvent = {
      case m: Message if m.hasFrames =>
        this withCommand SendTo(dissector, DissectorDo(m), "dissector")

      case DissectorDone(m) =>
        this withEvent m
    }
  }

}

