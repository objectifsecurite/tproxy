package ch.os.tproxy.cli

import scala.util.Try

import com.typesafe.config.Config
import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigValueFactory

import scopt.OptionParser
import scopt.Read

class CmdLineParser(default: Config = ConfigFactory.empty) {
  val parser = new Parser("tproxy")
  val usage = parser.usage

  def parse(args: Seq[String]): Try[Config] = Try {
    parser parse(args, ConfigFactory.empty) match {
      case Some(config) => config
      case None => throw new Exception("Command line parse error")
    }
  }

  def parse(args: String): Try[Config] = parse(args split " ")

  class Parser(programName: String) extends OptionParser[Config](programName) {
    configOpt[String]('c', "config-file", "Configuration file to load")
    configOpt[String]('d', "dump-file", "Name of the pcap dump file")
    configOpt[Unit]('D', "disable-dump", "Disable pcap dump")
    configOpt[Unit]('G', "disable-gui", "Disable gui")
    configOpt[Unit]('W', "disable-dissection", "Disable wireshark dissection")

    opt[Unit]('h', "help") text ("Show this help") action {
      (_, config) =>
        config withValue("tproxy.usage-text",
          ConfigValueFactory fromAnyRef usage)
    }

    override def reportError(msg: String) = throw new Exception(msg)

    override def reportWarning(msg: String) = throw new Exception(msg)

    private def configOpt[A: Read](short: Char, long: String, descr: String) = {
      val path = s"$programName.$long"

      val descrWithDefault =
        try s"$descr (default: ${default getString path})"
        catch {
          case _: Missing => descr
        }

      opt[A](short, long) text (descrWithDefault) action {
        (value, config) =>
          config withValue(path, value match {
            case _: Unit => ConfigValueFactory fromAnyRef true
            case v => ConfigValueFactory fromAnyRef v
          })
      }
    }
  }

}
