package ch.os.tproxy.cli

import java.net.{InetSocketAddress => Addr}
import akka.actor.{ActorRef, Props, Terminated}
import ch.os.tproxy.core.TProxy
import ch.os.tproxy.core.actor.ScriptActor
import ch.os.tproxy.common.log.{LogReceive, Logging}
import ch.os.tproxy.common.message.{ClientAccepted, Message}
import ch.os.tproxy.core.dissection.Dissector
import ch.os.tproxy.core.pcap.PcapDumper
import ch.os.tproxy.common.behaviour.OnEvent
import ch.os.tproxy.common.behaviour.implicits._
import ch.os.tproxy.common.behaviour.actor.ActorNode
import ch.os.tproxy.core.filter.FilterActor
import ch.os.tproxy.cli.interaction.FilterCmdPrompt
import ch.os.tproxy.cli.CliDisplay._
import ch.os.tproxy.core.dissection.Dissector.startWiresharkDissector


object CliActor {

  case class ClearCmdPrompt(withNewLine: Boolean)

  case class PrintLines(lines: String, typedCmd: String)

  def apply(address: Addr)(implicit logging: Logging, logReceive: LogReceive) = {
    Props(classOf[CliActor], address, logging, logReceive)
  }
}

class CliActor(address: Addr, _logging: Logging, _logReceive: LogReceive) extends ActorNode {

  import CliActor._

  implicit lazy val logging = _logging forObject this
  implicit val logReceive = _logReceive
  val config = context.system.settings.config

  val scriptActor =
    Some(ScriptActor.getActor(self)(context, logging, logReceive))
  lazy val filterActor: Option[ActorRef] = Some(FilterActor.getActor(self)(context, logging, logReceive))

  implicit val tproxyConfig = new TProxy.Config {
    val logging = CliActor.this.logging
    val logReceive = CliActor.this.logReceive

    val master = self
    val filter = filterActor

    val dissector = config getBoolean "tproxy.disable-dissection" match {
      case true => None
      case false => Some(Dissector.getActor(context, logging, logReceive))
    }

    val pcapDumper = config getBoolean "tproxy.disable-dump" match {
      case true => None
      case false => {
        val dumpFile = config getString "tproxy.dump-file"
        Some(context actorOf(Props(classOf[PcapDumper], dumpFile), "PcapDumpActor"))
      }
    }
  }

  val proxy = context actorOf(TProxy(address), "TProxyActor")
  context watch proxy

  lazy val cmdPromptThread = new Thread {
    override def run() = {
      require(filterActor.isDefined)
      val cmdPrompt = new FilterCmdPrompt(filterActor.get, self)
      cmdPrompt.run()
    }
  }

  def onStart = {
    if (tproxyConfig.dissector.isDefined)
      startWiresharkDissector()
    Behaviour
  }

  private object Behaviour extends OnEvent {
    var pendingLines, currentCmd = ""

    def onEvent = {
      case TProxy.Bound(addr) =>
        Console.out println s"Ready to intercept traffic on $addr"
        displayHeaders
        cmdPromptThread.start()
        this

      case TProxy.BindFailed(address) =>
        Console.err println s"Cannot bind interceptor to $address"
        Console.err println "Root privileges are required for transparent interception"
        this

      case ClientAccepted(id, srcAddr, dstAddr) =>
        clearPendingLines(pendingLines)
        Console.out.printf(connectionView(id, srcAddr, dstAddr))
        displayPrompt(currentCmd)
        if (!pendingLines.isEmpty)
          Console.out.print(pendingLines)
        this

      case msg: Message =>
        clearPendingLines(pendingLines)
        Console.out.printf(msgView(msg))
        displayPrompt(currentCmd)
        if (!pendingLines.isEmpty)
          Console.out.print(pendingLines)
        this

      case Terminated(ref) =>
        require(ref == proxy)
        context.system.terminate()
        this

      case PrintLines(lines, typedCmd) =>
        pendingLines += lines
        currentCmd = typedCmd
        Console.out.print(lines)
        this

      case ClearCmdPrompt(withNewLine) =>
        clearPendingLines(pendingLines, withNewLine)
        displayPrompt()
        pendingLines = ""
        currentCmd = ""
        this

      case _ => this
    }
  }

}
