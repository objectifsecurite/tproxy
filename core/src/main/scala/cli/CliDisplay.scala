package ch.os.tproxy.cli

import java.net.InetSocketAddress

import ch.os.tproxy.core.filter.Filter
import ch.os.tproxy.common.message.Message
import ch.os.tproxy.core.message.implicits.Display

object CliDisplay {
  private val clear_line = "\u001b[2K"
  private val move_cursor_up = "\u001b[A"
  private val blue = "\u001b[0;34m"
  private val black = "\u001b[0;0m"

  private val headers = Seq("Conn. ID", "Time", "Clt Address", "Clt Port", "", "Srv Address", "Srv Port", "Proto", "Info")
  val headerView = headers.foldLeft("")((acc, hdr) => acc + "%-15s\t".format(hdr))

  def connectionView(id: String, srcAddr: InetSocketAddress, dstAddr: InetSocketAddress) =
    "\r\u001b[0;32m[%-8s] %-10s --> %-10s\u001b[0;0m\n".format(id, srcAddr, dstAddr)

  def msgView(msg: Message) = (msg.cid +: (0 to 7).map(msg.displayItem)).foldLeft("\r")((acc, field) => acc + "%-15s\t".format(field)) + "\n"

  val helpView =
    f"""Usage:
       #\thelp | ?\t\tShow this help
       #\t+ <filter>\t\tAdd new script filter <filter>
       #\t- <filter|filter_idx>\tRemove existent script filter <filter> (or filter at index <filter_index> as displayed by command "show")
       #\tshow\t\t\tShow all script filters in use
       #\texit\t\t\tExit

       #Filter syntax:
       #\tPort: [clt|srv].port [==|!=] port
       #\tIP Address: [clt|srv].ip [==|!=] ip_address
       #\tConnection: cid [==|!=] connection_id
       #\tSpecific packet: [close|data|request|response|ssl]
       #\tBoolean operators: !, &, |""".stripMargin('#')


  def filterView(filters: Vector[Filter]) = {
    if (filters.isEmpty)
      "No script filter in use"
    else
      filters.map(f => s"${filters.indexOf(f) + 1}) ${f.toString}").mkString("\n")
  }

  private def eraseLines(nbLines: Int): Unit = {
    Console.out.printf((clear_line + move_cursor_up) * nbLines + clear_line)
  }

  def clearPendingLines(pendingLines: String, withNewLine: Boolean = false): Unit = {
    val nbLines = if (pendingLines.isEmpty) 0 else pendingLines.split("\n").length
    if (withNewLine)
      CliDisplay.eraseLines(nbLines + 1)
    else
      CliDisplay.eraseLines(nbLines)
  }

  def displayHeaders = Console.out.printf("%-100s\n%100s\n", headerView, "-" * headerView.length)

  def displayPrompt(typedCmd: String = "") = {
    Console.out.printf(blue + "Filter prompt > " + black)
    if (!typedCmd.isEmpty)
      Console.out.println(typedCmd)
  }
}
