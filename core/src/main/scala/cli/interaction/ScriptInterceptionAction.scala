package ch.os.tproxy.cli.interaction

import akka.actor.ActorRef
import ch.os.tproxy.common.behaviour.actor.message.Command
import ch.os.tproxy.core.filter.{Filter, FilterActor}

trait ScriptInterceptionAction {
  def filterActor: ActorRef

  def addScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.AddFilter(filter, true), false)

  def removeScriptInterceptFilter(filter: Filter) = filterActor ! Command(FilterActor.RemoveFilter(filter, true), false)
}
