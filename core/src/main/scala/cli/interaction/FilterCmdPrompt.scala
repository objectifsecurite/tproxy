package ch.os.tproxy.cli.interaction

import akka.actor.ActorRef
import ch.os.tproxy.core.filter.{Filter, FilterParser}
import ch.os.tproxy.cli.CliDisplay

import scala.util.control.Breaks.{break, breakable}

class FilterCmdPrompt(filter: ActorRef, cli: ActorRef) extends ScriptInterceptionAction {

  import ch.os.tproxy.cli.CliActor.{ClearCmdPrompt, PrintLines}

  lazy val filterActor = filter
  lazy val cliActor = cli

  var filters = Vector[Filter]()

  def run(): Unit = {
    var withNewLine = false
    while (true) {
      cliActor ! ClearCmdPrompt(withNewLine)
      val cmd = Console.in.readLine()
      if (cmd == null) // CTRL+D
        sys exit 0
      cmd.trim.split(" ").toList match {
        case cmd_ :: _ if cmd_ == "help" || cmd == "?" =>
          printAndWait(CliDisplay.helpView, cmd)

        case "show" :: _ =>
          printAndWait(CliDisplay.filterView(filters), cmd)

        case "+" :: filterFields if filterFields.nonEmpty =>
          val filter = FilterParser(filterFields.filter(_.nonEmpty).mkString(" "))
          if (filter.isDefined && !filters.contains(filter.get)) {
            addScriptInterceptFilter(filter.get)
            printAndWait(s"Added script filter '${filter.get}'", cmd)
            filters = filters :+ filter.get
          }
          else if (filter.isEmpty)
            printAndWait(s"[ERROR] Unknown filter '${filterFields.mkString(" ")}'", cmd)
          else
            printAndWait(s"Filter '${filter.get}' already in use", cmd)

        case "-" :: filterFields if filterFields.nonEmpty =>
          breakable {
            var filter: Option[Filter] = None
            try {
              val idx = filterFields.head.toInt
              filter = Some(filters(idx - 1))
            }
            catch {
              case _: NumberFormatException => filter = FilterParser(filterFields.filter(_.nonEmpty).mkString(" "))
              case _: IndexOutOfBoundsException =>
                printAndWait("[ERROR] Filter index out of bound", cmd)
                break
            }
            if (filter.isDefined) {
              removeScriptInterceptFilter(filter.get)
              printAndWait(if (filters contains (filter.get)) s"Removed script filter '${filter.get}'" else s"Filter '${filter.get}' is not in use", cmd)
              filters = filters diff Vector(filter.get)

            }
            else
              printAndWait(s"[ERROR] Unknown filter '${filterFields.mkString(" ")}'", cmd)
          }

        case "exit" :: _ => sys exit 0
        case _ if cmd.nonEmpty => printAndWait(s"[ERROR] Command not found. Type 'help' or '?' for usage information.", cmd)
        case _ =>
      }
      withNewLine = true
    }
  }

  private def printAndWait(printedMsg: String, typedCmd: String) = {
    cliActor ! PrintLines(printedMsg + "\n:", typedCmd)
    Console.in.readLine()
  }
}
