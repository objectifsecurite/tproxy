package ch.os.tproxy.config

import java.io.File

import scala.util.Success
import scala.util.Try

import com.typesafe.config.Config
import com.typesafe.config.ConfigException
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigParseOptions

import ch.os.tproxy.cli.CmdLineParser

object MainConfig {
  def load(args: String): Try[Config] = load(args split " ")

  def load(args: Array[String]): Try[Config] = {
    val defaultConfig = Try {
      ConfigFactory.load
    }
    val cmdLineConfig = defaultConfig flatMap (new CmdLineParser(_) parse args)

    val configFileKey = "tproxy.config-file"
    val defaultConfigFile = defaultConfig.toOption flatMap (_ getOptionalString configFileKey)
    val cmdLineConfigFile = cmdLineConfig.toOption flatMap (_ getOptionalString configFileKey)

    val fileConfig = (defaultConfigFile, cmdLineConfigFile) match {
      case (_, Some(fname)) => loadFromFile(fname, mandatory = true) map (Some(_))
      case (Some(fname), None) => loadFromFile(fname) map (Some(_))
      case _ => Success(None)
    }

    for (dc <- defaultConfig; cc <- cmdLineConfig; ofc <- fileConfig) yield ofc match {
      case Some(fc) => cc withFallback (fc withFallback dc)
      case None => cc withFallback dc
    }
  }

  private val DisallowMissing = ConfigParseOptions.defaults setAllowMissing false

  private def loadFromFile(fname: String, mandatory: Boolean = false) = Try {
    mandatory match {
      case true => ConfigFactory parseFile(new File(fname), DisallowMissing)
      case false => ConfigFactory parseFile new File(fname)
    }
  }

  implicit private class RichConfig(val config: Config) extends AnyVal {
    def getOptionalString(path: String) = try {
      Some(config getString path)
    } catch {
      case e: ConfigException.Missing => None
    }
  }

}
