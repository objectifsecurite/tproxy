package ch.os.tproxy.core.dissection;

public class DissectTree {
    private String protocol;
    private String info;
    private int frameNumber;
    private DissectTreeNode root;
    private int offset;

    public DissectTree(String protocol, String info, int frameNumber, DissectTreeNode root, int offset) {
        this.protocol = protocol;
        this.info = info;
        this.frameNumber = frameNumber;
        this.root = root;
        this.offset = offset;
    }

    public DissectTree withRoot(DissectTreeNode root) {
        return new DissectTree(protocol, info, frameNumber, root, offset);
    }


    public String getProtocol() {
        return protocol;
    }

    public String getInfo() {
        return info;
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public int getOffset() {
        return offset;
    }

    public DissectTreeNode getRoot() {
        return root;
    }
}
