package ch.os.tproxy.core.dissection;

import ch.os.tproxy.common.json.JsonDissectTreeNode;
import ch.os.tproxy.common.json.JsonDissectTreeNode.ValueType;

import java.util.ArrayList;

public class DissectTreeNode {
    private DissectTreeNode parent;
    private JsonDissectTreeNode firstChild;
    private JsonDissectTreeNode next;

    private int length, position;
    private String abbrev, name, value;
    private ValueType valueType;

    private boolean root = false;
    private ArrayList<DissectTreeNode> siblings = null;
    private ArrayList<DissectTreeNode> children = null;


    public DissectTreeNode(DissectTreeNode parent, JsonDissectTreeNode firstChild, JsonDissectTreeNode next, int length,
                           int position, String abbrev, String name, String value, ValueType valueType) {

        this.parent = parent;
        this.firstChild = firstChild;
        this.next = next;
        this.length = length;
        this.position = position;
        this.abbrev = abbrev;
        this.name = name;
        this.value = value;
        this.valueType = valueType;
    }

    public DissectTreeNode(DissectTreeNode parent, JsonDissectTreeNode node) {
        this(parent, node.firstChild(), node.next(), node.length(), node.position(), node.abbrev(),
                node.name(), node.getValue(), node.valueType());
    }

    public DissectTreeNode(JsonDissectTreeNode node) {
        this(null, node);
        this.root = true;
    }

    public ArrayList<DissectTreeNode> getSiblings() {
        if (siblings != null) return siblings;
        siblings = new ArrayList<>();
        DissectTreeNode brother = getNext();
        while (brother != null) {
            siblings.add(brother);
            brother = brother.getNext();
        }
        return siblings;
    }

    public ArrayList<DissectTreeNode> getChildren() {
        if (children != null) return children;
        children = new ArrayList<DissectTreeNode>();
        DissectTreeNode child = getFirstChild();
        if (child != null) {
            children.add(child);
            children.addAll(child.getSiblings());
        }
        return children;
    }

    public DissectTreeNode getParent() {
        return parent;
    }

    public String getTag(String tag) {
        DissectTreeNode node = findTag(tag);
        if (node == null) return null;
        return node.getValue();
    }

    public boolean hasTag(String tag) {
        DissectTreeNode node = findTag(tag);
        return node != null;
    }

    public DissectTreeNode findTag(String tag) {
        if (this.getAbbrev().equals(tag)) return this;
        for (DissectTreeNode child : getChildren()) {
            DissectTreeNode grandchild = child.findTag(tag);
            if (grandchild != null) return grandchild;
        }
        if (parent == null && root) { // Check siblings if first generation
            for (DissectTreeNode brother : getSiblings()) {
                DissectTreeNode nephew = brother.findTag(tag);
                if (nephew != null) return nephew;
            }
        }
        return null;
    }

    public DissectTreeNode getFirstChild() {
        return firstChild == null ? null : new DissectTreeNode(this, firstChild);
    }

    public DissectTreeNode getNext() {
        return next == null ? null : new DissectTreeNode(parent, next);
    }

    public int getLength() {
        return length;
    }

    public int getPosition() {
        return position;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ValueType getValueType() {
        return valueType;
    }
}
