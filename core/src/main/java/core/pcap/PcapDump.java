package ch.os.tproxy.core.pcap;

import ch.os.tproxy.core.Native;
import ch.os.tproxy.common.natlib.NatlibLoader;

final class PcapDump {
    static {
        NatlibLoader.load("tproxy_core", true);
        NatlibLoader.load("tproxy_core_jni", true);
    }

    public static native int init(String fileName);

    public static native synchronized void write(int dumpfd, PcapFrame[] frames);
}
