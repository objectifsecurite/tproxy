package ch.os.tproxy.core.pcap;

import java.lang.ref.Cleaner;
import java.net.InetSocketAddress;

import ch.os.tproxy.common.natlib.NatlibLoader;

final public class PcapSim implements AutoCloseable {
    static {
        NatlibLoader.load("tproxy_core", true);
        NatlibLoader.load("tproxy_core_jni", true);
    }

    private final static Cleaner cleaner = Cleaner.create();
    private final Cleaner.Cleanable cleanable;
    private final PtrState ptrState;

    public PcapSim(InetSocketAddress srcAddr, InetSocketAddress dstAddr) {
        long pointer = create(srcAddr.getHostString(), srcAddr.getPort(),
                dstAddr.getHostString(), dstAddr.getPort());

        ptrState = new PtrState(pointer);
        cleanable = cleaner.register(this, ptrState);
    }

    public PcapFrame[] open() {
        return makeFrames(nativeOpen());
    }

    public PcapFrame[] request(byte[] bytes) {
        return makeFrames(nativeRequest(bytes));
    }

    public PcapFrame[] response(byte[] bytes) {
        return makeFrames(nativeResponse(bytes));
    }

    public PcapFrame[] closeRequest() {
        return makeFrames(nativeCloseRequest());
    }

    public PcapFrame[] closeResponse() {
        return makeFrames(nativeCloseResponse());
    }

    private static PcapFrame[] makeFrames(long[] pointers) {
        PcapFrame[] frames = new PcapFrame[pointers.length];

        for (int i = 0; i < pointers.length; ++i)
            frames[i] = new PcapFrame(pointers[i]);

        return frames;
    }

    // called in PcapSim.c
    public long getPointer() {
        return ptrState.pointer;
    }

    @Override
    public void close() {
        cleanable.clean();
    }

    private static class PtrState implements Runnable {
        private long pointer;

        PtrState(long pointer) {
            this.pointer = pointer;
        }

        @Override
        public void run() {
            if (pointer != 0) {
                release(pointer);
                pointer = 0;
            }
        }
    }

    private static native long create(String srcAddr, int srcPort,
                                      String dstAddr, int dstPort);

    private static native void release(long pointer);

    private native long[] nativeOpen();

    private native long[] nativeRequest(byte[] bytes);

    private native long[] nativeResponse(byte[] bytes);

    private native long[] nativeCloseRequest();

    private native long[] nativeCloseResponse();

}
