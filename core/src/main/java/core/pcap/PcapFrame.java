package ch.os.tproxy.core.pcap;

import ch.os.tproxy.common.natlib.NatlibLoader;

import java.lang.ref.Cleaner;

public final class PcapFrame implements AutoCloseable {
    static {
        NatlibLoader.load("tproxy_core", true);
        NatlibLoader.load("tproxy_core_jni", true);
    }

    private final static Cleaner cleaner = Cleaner.create();
    private final Cleaner.Cleanable cleanable;
    private final PtrState ptrState;

    protected PcapFrame(long pointer) {
        ptrState = new PtrState(pointer);
        cleanable = cleaner.register(this, ptrState);
    }

    public byte[] getBytes() {
        return getBytes(ptrState.pointer);
    }

    // called in PcapDump.c
    public long getPointer() {
        return ptrState.pointer;
    }

    @Override
    public void close() {
        cleanable.clean();
    }

    private static class PtrState implements Runnable {
        private long pointer;

        PtrState(long pointer) {
            this.pointer = pointer;
        }

        public void run() {
            if (pointer != 0) {
                release(pointer);
                pointer = 0;
            }
        }
    }

    private static native void release(long pointer);

    private static native byte[] getBytes(long pointer);

}
