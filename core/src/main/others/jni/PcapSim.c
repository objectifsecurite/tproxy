#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <assert.h>

#include "PcapSim.h"
#include "pcap_frame.h"
#include "pcap_sim.h"

JNIEXPORT jlong JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_create(JNIEnv *env, jclass class, 
		    jstring jsrcAddr, jint jsrcPort, 
		    jstring jdstAddr, jint jdstPort)
{
  const char *srcAddr = (*env)->GetStringUTFChars(env, jsrcAddr, 0);
  const char *dstAddr = (*env)->GetStringUTFChars(env, jdstAddr, 0);

  int srcPort = (int)jsrcPort;
  int dstPort = (int)jdstPort;

  pcap_sim_t *sim = pcap_sim_create(srcAddr, srcPort, dstAddr, dstPort);

  (*env)->ReleaseStringUTFChars(env, jsrcAddr, srcAddr);
  (*env)->ReleaseStringUTFChars(env, jdstAddr, dstAddr);

  return (jlong)sim;
}

pcap_sim_t *sim_pointer(JNIEnv *env, jobject jsim)
{
  jclass cls = (*env)->GetObjectClass(env, jsim);

  jmethodID mid_getptr = (*env)->GetMethodID(env, cls, "getPointer", "()J");
  jlong pointer = (jlong) ((*env)->CallLongMethod(env, jsim, mid_getptr));

  return (pcap_sim_t*)pointer;
}

JNIEXPORT jlongArray JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_nativeOpen(JNIEnv *env, jobject jsim)
{
  pcap_sim_t *sim = sim_pointer(env, jsim);
  pcap_frame_t *frames[3] = { 0 };

  pcap_sim_open(sim, frames);

  jlongArray jframes = (*env)->NewLongArray(env, 3);
  (*env)->SetLongArrayRegion(env, jframes, 0, 3, (jlong*)frames);

  return jframes;
}

jlongArray PcapSim_message(JNIEnv *env, jobject jsim, 
			   pcap_frame_type_t type, char *data, uint32_t size)
{
  pcap_sim_t *sim = sim_pointer(env, jsim);
  pcap_frame_t *frames[2] = { 0 };

  pcap_sim_message(sim, type, data, size, frames);

  jlongArray jframes = (*env)->NewLongArray(env, 2);
  (*env)->SetLongArrayRegion(env, jframes, 0, 2, (jlong*)frames);

  return jframes;
}

JNIEXPORT jlongArray JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_nativeRequest(JNIEnv *env, jobject jsim, jbyteArray jbytes)
{
  jbyte *data = (*env)->GetByteArrayElements(env, jbytes, NULL);
  jint size = (*env)->GetArrayLength(env, jbytes);

  jlongArray jframes = PcapSim_message(env, jsim, REQUEST, (char*)data, size);

  (*env)->ReleaseByteArrayElements(env, jbytes, data, JNI_ABORT);

  return jframes;
}

JNIEXPORT jlongArray JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_nativeResponse(JNIEnv *env, jobject jsim, jbyteArray jbytes)
{
  jbyte *data = (*env)->GetByteArrayElements(env, jbytes, NULL);
  jint size = (*env)->GetArrayLength(env, jbytes);

  jlongArray jframes = PcapSim_message(env, jsim, RESPONSE, (char*)data, size);

  (*env)->ReleaseByteArrayElements(env, jbytes, data, JNI_ABORT);

  return jframes;
}

jlongArray PcapSim_close(JNIEnv *env, jobject jsim, pcap_frame_type_t type)
{
  pcap_sim_t *sim = sim_pointer(env, jsim);
  pcap_frame_t *frames[3] = { 0 };

  pcap_sim_close(sim, type, frames);

  jlongArray jframes = (*env)->NewLongArray(env, 3);
  (*env)->SetLongArrayRegion(env, jframes, 0, 3, (jlong*)frames);

  return jframes;
}

JNIEXPORT jlongArray JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_nativeCloseRequest(JNIEnv *env, jobject jsim)
{
  return PcapSim_close(env, jsim, REQUEST);
}

JNIEXPORT jlongArray JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_nativeCloseResponse(JNIEnv *env, jobject jsim) 
{
  return PcapSim_close(env, jsim, RESPONSE);
}

JNIEXPORT void JNICALL 
Java_ch_os_tproxy_core_pcap_PcapSim_release(JNIEnv *env, jclass class, jlong pointer) 
{
  free((pcap_sim_t*)pointer);
}
