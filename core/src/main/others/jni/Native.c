#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <gmodule.h>

#include "Native.h"
#include "osutils.h"

/* To access the underlying Unix FD associated with the socket we
 * navigate from (Server)SocketChannelImpl which implements
 * (Server)SocketChannel to FileDescriptor.
 */
int channel_fd(JNIEnv *env, jobject jchannel)
{
  jclass cls = (*env)->GetObjectClass(env, jchannel);
  jfieldID fid = (*env)->GetFieldID(env, cls, "fd", "Ljava/io/FileDescriptor;");
  jobject jfd = (*env)->GetObjectField(env, jchannel, fid);

  cls = (*env)->GetObjectClass(env, jfd);
  fid = (*env)->GetFieldID(env, cls, "fd", "I");
  
  int fd = (*env)->GetIntField(env, jfd, fid);

  return fd;
}

JNIEXPORT jboolean JNICALL 
Java_ch_os_tproxy_core_Native_00024_setTransparent(JNIEnv *env, jobject jobj, jobject jchannel)
{
  int fd = channel_fd(env, jchannel);
 
  if (fd < 0)
    return JNI_FALSE;
  
  int value = 1;
  int err = setsockopt(fd, SOL_IP, IP_TRANSPARENT, &value, sizeof(value));

  if (err < 0)
    return JNI_FALSE;

  return JNI_TRUE;
}

JNIEXPORT jint JNICALL 
Java_ch_os_tproxy_core_Native_00024_getLocalPort(JNIEnv *env, jobject jobj, jobject jchannel) 
{
  int fd = channel_fd(env, jchannel);

  struct sockaddr_in addr;
  socklen_t len = sizeof(addr);

  getsockname(fd, (struct sockaddr*)&addr, &len);

  jint port = ntohs(addr.sin_port);

  return port;
}

JNIEXPORT jboolean JNICALL 
Java_ch_os_tproxy_core_Native_00024_setTos(JNIEnv *env, jobject jobj, jobject jchannel, jint tos)
{
  int fd = channel_fd(env, jchannel);

  if (fd < 0)
      return JNI_FALSE;
  
  int err = setsockopt(fd, SOL_IP, IP_TOS, &tos, sizeof(tos));

  if (err < 0)
    return JNI_FALSE;

  return JNI_TRUE;
}

JNIEXPORT jint JNICALL 
Java_ch_os_tproxy_core_Native_00024_getUID(JNIEnv *env, jobject jobj)
{
  return (jint) getuid();
}

JNIEXPORT jstring JNICALL
Java_ch_os_tproxy_core_Native_00024_getEnv(JNIEnv *env, jobject jobj, jstring jname)
{
  const char *name = (*env)->GetStringUTFChars(env, jname, 0);
  const char *value = getenv(name);
  
  return (*env)->NewStringUTF(env, value);
}

JNIEXPORT jint JNICALL
Java_ch_os_tproxy_core_Native_00024_setCapForCurrentUser(JNIEnv *env, jobject jobj)
{
  return (jint)set_cap_for_current_user();
}

JNIEXPORT jint JNICALL
Java_ch_os_tproxy_core_Native_00024_loggedAsCurrentUser(JNIEnv *env, jobject jobj)
{
  return (jint)logged_as_current_user();
}

JNIEXPORT void JNICALL 
Java_ch_os_tproxy_core_Native_00024_gLazyModuleOpen(JNIEnv *env, jobject jobj, jstring libFilePath)
{
  const char *path = (*env)->GetStringUTFChars(env, libFilePath, 0);
  g_module_open(path, G_MODULE_BIND_LAZY);
  (*env)->ReleaseStringUTFChars(env, libFilePath, path);
}
