#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

#include "PcapDump.h"
#include "pcap_frame.h"

typedef struct {
        uint32_t magic;
        u_short version_major;
        u_short version_minor;
        int32_t  thiszone;     /* gmt to local correction */
        uint32_t sigfigs;      /* accuracy of timestamps */
        uint32_t snaplen;      /* max length saved portion of each pkt */
        uint32_t linktype;     /* data link type (LINKTYPE_*) */
} pcap_file_header_t;

JNIEXPORT jint JNICALL
Java_ch_os_tproxy_core_pcap_PcapDump_init(JNIEnv *env, jclass class, jstring fileName)
{
  const char *fname = (*env)->GetStringUTFChars(env, fileName, 0);

  int dumpfd = open(fname, O_CREAT | O_TRUNC | O_WRONLY, 0644);

  if (dumpfd >= 0) {
    pcap_file_header_t header;  
    
    memset(&header, 0, sizeof(pcap_file_header_t));
    
    header.magic = 0xa1b2c3d4;
    header.version_major = 2;
    header.version_minor = 4;
    header.snaplen = 0xffff;
    header.linktype = 1;

    write(dumpfd, &header, sizeof(pcap_file_header_t));
  }

  (*env)->ReleaseStringUTFChars(env, fileName, fname);

  return dumpfd;
}

JNIEXPORT void JNICALL 
Java_ch_os_tproxy_core_pcap_PcapDump_write(JNIEnv *env, jclass class, jint dumpfd, jobjectArray jframes)
{
  jint nframes = (*env)->GetArrayLength(env, jframes);

  if (nframes == 0)
    return;

  jobject jframe = (*env)->GetObjectArrayElement(env, jframes, 0);
  jclass cls = (*env)->GetObjectClass(env, jframe);
  jmethodID mid_getptr = (*env)->GetMethodID(env, cls, "getPointer", "()J");

  for (int i=0; i<nframes; ++i) {
    jobject jframe = (*env)->GetObjectArrayElement(env, jframes, i);
    jlong pointer = (jlong) ((*env)->CallLongMethod(env, jframe, mid_getptr));

    pcap_frame_t *frame = (pcap_frame_t*)pointer;
    write(dumpfd, frame->data, frame->size);
  }
}
