#include <stdlib.h>

#include "PcapFrame.h"
#include "pcap_frame.h"

JNIEXPORT void JNICALL 
Java_ch_os_tproxy_core_pcap_PcapFrame_release(JNIEnv *env, jclass class, jlong pointer)
{
  pcap_frame_t *frame = (pcap_frame_t*)pointer;

  free(frame->data);
  free(frame);
}

JNIEXPORT jbyteArray JNICALL Java_ch_os_tproxy_core_pcap_PcapFrame_getBytes
  (JNIEnv *env, jclass class, jlong pointer){

  pcap_frame_t *frame = (pcap_frame_t*)pointer;

  const char* data = (char*)frame->data;

  jbyteArray bytes = (*env)->NewByteArray(env, frame->size);
  (*env)->SetByteArrayRegion(env, bytes, 0, frame->size, (jbyte *)(data));
  return bytes;
}
