#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/prctl.h>
#include <grp.h>
#include "osutils.h"

upwd_t *upwd_from_passwd(struct passwd *pwd) {
  upwd_t *upwd = (upwd_t*)malloc(sizeof(upwd_t));

  upwd->uid = pwd->pw_uid;
  upwd->gid = pwd->pw_gid;

  size_t n = strlen(pwd->pw_dir) + 1;
  upwd->dir = (char*)malloc(n*sizeof(char));

  strncpy(upwd->dir, pwd->pw_dir, n);
  
  return upwd;
}

void upwd_free(upwd_t *upwd) {
  free(upwd->dir);
  free(upwd);
}

upwd_t *upwd_current(void) {
  uid_t uid = getuid();
  struct passwd *pwd = getpwuid(uid);
  
  return upwd_from_passwd(pwd);
}

upwd_t *upwd_logged(void) {
  char *login = getlogin();
  struct passwd *pwd = getpwnam(login);

  return upwd_from_passwd(pwd);
}

/*
Inspired from
http://stackoverflow.com/questions/13183327/drop-root-uid-while-retaining-cap-sys-nice/13186076#13186076 
*/

int set_cap_for_current_user(void) {
  cap_t cap_p = cap_get_proc();
  int result = cap_clear(cap_p);
  
  if (result != 0) {
#ifdef DEBUG
    perror("set_cap_for_current_user: error on cap_clear");
#endif
    return 1;
  }
  
  cap_flag_t flags[] = { CAP_PERMITTED, CAP_EFFECTIVE };
  cap_value_t values[] = { CAP_NET_ADMIN, CAP_SETUID, CAP_SETGID };

  int nflags = sizeof(flags) / sizeof(cap_flag_t);
  int nvalues = sizeof(values) / sizeof(cap_value_t);
  
  uid_t uid = getuid();
  
  if (uid == 0) {
    for (int i=0; i<nflags; ++i)
      result = cap_set_flag(cap_p, flags[i], nvalues, values, CAP_SET);
  }
  else {
    for (int i=0; i<nflags; ++i) {
      result = cap_set_flag(cap_p, flags[i], 1, values, CAP_SET);
      result = cap_set_flag(cap_p, flags[i], nvalues-1, values+1, CAP_CLEAR);
    }
  }
  
  if (result != 0) {
#ifdef DEBUG
    perror("set_cap_for_current_user: error on cap_set_flag");
#endif
    return 2;
  }
  
  result = cap_set_proc(cap_p);

  if (result != 0) {
#ifdef DEBUG
    perror("set_cap_for_current_user: error on cap_set_proc");
#endif
    return 3;
  }

  result = cap_free(cap_p);

  if (result != 0) {
#ifdef DEBUG
    perror("set_cap_for_current_user: error on cap_free");
#endif
    return 4;
  }

  if (uid == 0)
    result = prctl(PR_SET_KEEPCAPS, 1L);
    
  if (result != 0) {
#ifdef DEBUG
    perror("set_cap_for_current_user: error on prctl");
#endif
    return 5;
  }
  
  return 0;
}

int logged_as_current_user(void) {
  char *login = getlogin();
  upwd_t *upwd = upwd_logged();

  int result = initgroups(login, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on initgroups");
#endif
    return 1;
  }

  result = setresgid(upwd->gid, upwd->gid, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on setresgid");
#endif
    return 2;
  }

  result = setresuid(upwd->uid, upwd->uid, upwd->gid);
  
  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on setresuid");
#endif
    return 3;
  }

  size_t dirlen = strlen(upwd->dir);
  size_t varlen = 5+dirlen+1;
  char *homedir = (char*)malloc(varlen*sizeof(char));
  
  snprintf(homedir, varlen, "HOME=%s", upwd->dir);

  result = putenv(homedir);

  if (result != 0) {
#ifdef DEBUG
    perror("logged_as_current_user: error on putenv");
#endif
    return 4;
  }

  return 0;
}
