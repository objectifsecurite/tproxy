#ifndef PCAP_SIM_H
#define PCAP_SIM_H

#include "pcap_frame.h"

typedef struct {
  uint32_t src_addr;
  uint32_t dst_addr;
  uint32_t src_port;
  uint32_t dst_port;
  uint32_t src_seq;
  uint32_t dst_seq;
} pcap_sim_t;

typedef enum {
  REQUEST,
  RESPONSE
} pcap_frame_type_t;

pcap_sim_t *pcap_sim_create(const char *srcAddr, int srcPort,
			    const char *dstAddr, int dstPort);

void pcap_sim_set_frame_type(pcap_sim_t *sim, pcap_frame_t *frame, pcap_frame_type_t type);
void pcap_sim_open(pcap_sim_t *sim, pcap_frame_t *frames[3]);

void pcap_sim_message(pcap_sim_t *sim, pcap_frame_type_t type, 
		      char *data, uint32_t size, pcap_frame_t *frames[2]);

void pcap_sim_close(pcap_sim_t *sim, pcap_frame_type_t type, pcap_frame_t *frames[3]);
#endif
