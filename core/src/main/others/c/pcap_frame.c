#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>
#include <stdio.h>

#include "pcap_frame.h"

void pcap_frame_adjust_time(pcap_frame_t *frame)
{
  struct timeval time;
  gettimeofday(&time, NULL);

  frame->pcap_hdr->ts_sec = time.tv_sec;
  frame->pcap_hdr->ts_usec = time.tv_usec;
}

/*
 * IP checksum. 
 * Taken from TCP-IP illustrated (vol 2)
 * Figure 8.27, page 236
 */
uint16_t my_ip_checksum(struct iphdr *ip_hdr) 
{
  int len = sizeof(struct iphdr);
  uint16_t *data = (uint16_t*)ip_hdr;

  uint32_t sum = 0;

  for (; len > 1; len -= 2, ++data) {
    sum += *data;
    
    if (sum & 0x80000000) 
      sum = (sum & 0xffff) + (sum >> 16);
  }

  if (len)
    sum += (uint32_t)*(uint8_t*)data;

  while (sum >> 16)
    sum = (sum & 0xffff) + (sum >> 16);

  return ~sum;
}

void pcap_frame_adjust_checksum(pcap_frame_t *frame)
{
  frame->ip_hdr->check = 0;
  frame->ip_hdr->check = my_ip_checksum(frame->ip_hdr);
}

pcap_frame_t *pcap_frame_create(char *data, uint32_t size)
{
  assert(data != NULL || (data == NULL && size == 0));

  int fsize = sizeof(struct pcaphdr) + 14 + 20 + 20 + size;
  char *fdata = (char*)malloc(fsize * sizeof(char));
  
  struct pcaphdr *pcap_hdr = (struct pcaphdr*)fdata;
  struct ethhdr *eth_hdr = (struct ethhdr*)(pcap_hdr + 1);
  struct iphdr *ip_hdr = (struct iphdr*)(eth_hdr + 1);
  struct tcphdr *tcp_hdr = (struct tcphdr*)(ip_hdr + 1);

  memset(pcap_hdr, 0, sizeof(struct pcaphdr));
  memset(eth_hdr, 0, sizeof(struct ethhdr));
  memset(ip_hdr, 0, sizeof(struct iphdr));
  memset(tcp_hdr, 0, sizeof(struct tcphdr));

  pcap_hdr->incl_len = 14 + 20 + 20 + size;
  pcap_hdr->orig_len = pcap_hdr->incl_len;

  eth_hdr->h_proto = htons(ETH_P_IP);

  ip_hdr->ihl = 5;
  ip_hdr->version = 4;
  ip_hdr->tot_len = htons(20 + 20 + size);
  ip_hdr->id = htons(0x7cbd); //random
  ip_hdr->frag_off = htons(0x4000);
  ip_hdr->ttl = 64;
  ip_hdr->protocol = 6;
  ip_hdr->check = 0;

  tcp_hdr->doff = 5;
  tcp_hdr->window = htons(0x3908);
  tcp_hdr->check = 0;

  if (data != NULL) {
    char *payload = (char*)(tcp_hdr + 1);
    memcpy(payload, data, size);
  }

  pcap_frame_t *frame = (pcap_frame_t*)malloc(sizeof(pcap_frame_t));

  frame->data = fdata;
  frame->size = fsize;
  frame->pcap_hdr = pcap_hdr;
  frame->eth_hdr = eth_hdr;
  frame->ip_hdr = ip_hdr;
  frame->tcp_hdr = tcp_hdr;

  pcap_frame_adjust_time(frame);

  return frame;
}