#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "pcap_sim.h"

pcap_sim_t *pcap_sim_create(const char *srcAddr, int srcPort,
			    const char *dstAddr, int dstPort)
{
  pcap_sim_t *sim = (pcap_sim_t*)malloc(sizeof(pcap_sim_t));

  struct in_addr src_addr, dst_addr;

  inet_aton(srcAddr, &src_addr);
  inet_aton(dstAddr, &dst_addr);

  sim->src_addr = src_addr.s_addr;
  sim->dst_addr = dst_addr.s_addr;
  sim->src_port = srcPort;
  sim->dst_port = dstPort;

  struct timeval time;
  gettimeofday(&time, NULL);

  sim->src_seq = time.tv_usec + rand();
  sim->dst_seq = time.tv_usec + rand();

  return sim;
}

void pcap_sim_set_frame_type(pcap_sim_t *sim, pcap_frame_t *frame, pcap_frame_type_t type)
{
  switch (type) {
  case REQUEST:
    frame->ip_hdr->saddr = sim->src_addr;
    frame->ip_hdr->daddr = sim->dst_addr;

    frame->tcp_hdr->source = htons(sim->src_port);
    frame->tcp_hdr->dest = htons(sim->dst_port);
    frame->tcp_hdr->seq = htonl(sim->src_seq);
    frame->tcp_hdr->ack_seq = htonl(sim->dst_seq);
    break;

  case RESPONSE:
    frame->ip_hdr->saddr = sim->dst_addr;
    frame->ip_hdr->daddr = sim->src_addr;

    frame->tcp_hdr->source = htons(sim->dst_port);
    frame->tcp_hdr->dest = htons(sim->src_port);
    frame->tcp_hdr->seq = htonl(sim->dst_seq);
    frame->tcp_hdr->ack_seq = htonl(sim->src_seq);
    break;

  default:
    assert(0);
  }
}

void pcap_sim_open(pcap_sim_t *sim, pcap_frame_t *frames[3])
{
  /* SYN sent by the client
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);
    pcap_sim_set_frame_type(sim, frame, REQUEST);

    sim->src_seq += 1;

    frame->tcp_hdr->ack_seq = 0;
    frame->tcp_hdr->syn = 1;

    pcap_frame_adjust_checksum(frame);

    frames[0] = frame;
  }

  /* SYN, ACK sent by the server
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);
    pcap_sim_set_frame_type(sim, frame, RESPONSE);

    sim->dst_seq += 1;

    frame->tcp_hdr->syn = 1;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[1] = frame;
  }

  /* ACK sent by the client
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);
    pcap_sim_set_frame_type(sim, frame, REQUEST);

    frame->tcp_hdr->syn = 0;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[2] = frame;
  }
}

void pcap_sim_message(pcap_sim_t *sim, pcap_frame_type_t type, 
		      char *data, uint32_t size, pcap_frame_t *frames[2])
{
  /* PSH sent by the client/server
   */
  {
    pcap_frame_t *frame = pcap_frame_create(data, size);

    switch (type) {
    case REQUEST: 
      pcap_sim_set_frame_type(sim, frame, REQUEST);
      sim->src_seq += size; 
      break;

    case RESPONSE: 
      pcap_sim_set_frame_type(sim, frame, RESPONSE);
      sim->dst_seq += size; 
      break;

    default: 
      assert(0);
    }

    frame->tcp_hdr->psh = 1;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[0] = frame;
  }

  /* ACK sent by the server/client
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);

    switch (type) {
    case REQUEST:
      pcap_sim_set_frame_type(sim, frame, RESPONSE);
      break;

    case RESPONSE:
      pcap_sim_set_frame_type(sim, frame, REQUEST);
      break;

    default:
      assert(0);
    }

    frame->tcp_hdr->psh = 0;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[1] = frame;
  }
}

void pcap_sim_close(pcap_sim_t *sim, pcap_frame_type_t type, pcap_frame_t *frames[3])
{
  /* FIN, ACK sent by the client/server
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);

    switch (type) {
    case REQUEST:
      pcap_sim_set_frame_type(sim, frame, REQUEST);
      sim->src_seq += 1;
      break;

    case RESPONSE:
      pcap_sim_set_frame_type(sim, frame, RESPONSE);
      sim->dst_seq += 1;
      break;

    default:
      assert(0);
    }

    frame->tcp_hdr->fin = 1;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[0] = frame;
  }

  /* FIN, ACK sent by the server/client
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);

    switch (type) {
    case REQUEST:
      pcap_sim_set_frame_type(sim, frame, RESPONSE);
      sim->dst_seq += 1;
      break;

    case RESPONSE:
      pcap_sim_set_frame_type(sim, frame, REQUEST);
      sim->src_seq += 1;
      break;

    default:
      assert(0);
    }

    frame->tcp_hdr->fin = 1;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[1] = frame;
  }

  /* ACK sent by the client/server
   */
  {
    pcap_frame_t *frame = pcap_frame_create(NULL, 0);

    switch (type) {
    case REQUEST:
      pcap_sim_set_frame_type(sim, frame, REQUEST);
      break;

    case RESPONSE:
      pcap_sim_set_frame_type(sim, frame, RESPONSE);
      break;

    default:
      assert(0);
    }

    frame->tcp_hdr->fin = 0;
    frame->tcp_hdr->ack = 1;

    pcap_frame_adjust_checksum(frame);

    frames[2] = frame;
  }
}
