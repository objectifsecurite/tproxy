#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "osutils.h"

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    return 1;
  }

  {
    upwd_t *upwd = upwd_current();
    printf("Current user: uid: %d, gid: %d, dir: %s\n",
	   upwd->uid, upwd->gid, upwd->dir);
    free(upwd);
  }

  {
    upwd_t *upwd = upwd_logged();
    printf("Logged user: uid: %d, gid: %d, dir: %s\n",
	   upwd->uid, upwd->gid, upwd->dir);
    free(upwd);
  }

  if (getuid() != 0) {
    printf("Not root!\n");
    exit(1);
  }

  set_cap_for_current_user();
  logged_as_current_user();

  {
    upwd_t *upwd = upwd_current();
    printf("Current user: uid: %d, gid: %d, dir: %s\n",
	   upwd->uid, upwd->gid, upwd->dir);
    free(upwd);
  }

  set_cap_for_current_user();
  
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("Error opening socket");
    exit(1);
  }

  struct sockaddr_in serv_addr;
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(9080);

  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    perror("ERROR on binding");
    exit(1);
  }
  listen(sockfd,5);

  int value = 1;
  if (setsockopt(sockfd, SOL_IP, IP_TRANSPARENT, &value, sizeof(value)) < 0) {
    perror("ERROR on setsockopt");
    exit(1);
  }

  return 0;
}
