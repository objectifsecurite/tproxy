#ifndef CORE_PCAP_FRAME_H
#define CORE_PCAP_FRAME_H

#include "../../../../../common/src/main/others/c/pcap_frame.h"

void pcap_frame_adjust_time(pcap_frame_t *frame);
void pcap_frame_adjust_checksum(pcap_frame_t *frame);
pcap_frame_t *pcap_frame_create(char *data, uint32_t size);
#endif
