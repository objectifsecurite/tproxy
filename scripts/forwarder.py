#! /usr/bin/env python3
from dataclasses import dataclass

from tproxy_script import Tproxy, Message
from twisted.internet import protocol
class Forwarder(Tproxy):
    def handle(self, msg: Message) -> None:
        print("Received", msg)
        self.send(msg)

@dataclass
class ForwarderFactory(protocol.Factory):
    def buildProtocol(self, addr: str) -> protocol.Protocol:
        return Forwarder()

if __name__ == "__main__":
    Forwarder.run(ForwarderFactory())
