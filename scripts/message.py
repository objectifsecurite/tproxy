import base64, json
from dataclasses import dataclass, field
from typing import Tuple


@dataclass
class Message:
    cid: str = ""
    msg_type: str = "request"
    clt_addr: Tuple[str, int] = ("", 0)
    srv_addr: Tuple[str, int] = ("", 0)
    close: bool = False
    data: bytes = b""
    json: dict = field(default_factory=dict)

    def __post_init__(self) -> None:
        if self.json:
            self.cid = self.json["cid"]
            self.msg_type = self.json["msg_type"]
            self.clt_addr = (self.json["cltAddr"]["ip"], self.json["cltAddr"]["port"])
            self.srv_addr = (self.json["srvAddr"]["ip"], self.json["srvAddr"]["port"])
            self.close = self.json["close"]
            self.data = base64.b64decode(self.json["data"]) if not self.close else b""

    def __repr__(self) -> str:
        if not self.close:
            if self.msg_type == "request":
                return "<Request cid:{0.cid} from {0.clt_addr} to {0.srv_addr} data: {1}>".format(
                    self, ":".join("{:02x}".format(c) for c in self.data)
                )
            elif self.msg_type == "response":
                return "<Response cid:{0.cid} from {0.srv_addr} to {0.clt_addr} data:{1}>".format(
                    self, ":".join("{:02x}".format(c) for c in self.data)
                )
        else:
            if self.msg_type == "request":
                return "<Request-close cid:{0.cid} from {0.clt_addr} to {0.srv_addr}>".format(
                    self
                )
            elif self.msg_type == "response":
                return "<Response-close cid:{0.cid} from {0.srv_addr} to {0.clt_addr}>".format(
                    self
                )
        return ""

    def to_json(self) -> bytes:
        data = base64.b64encode(self.data).decode()
        return json.dumps(
            {
                "cid": self.cid,
                "msg_type": self.msg_type,
                "cltAddr": {"ip": self.clt_addr[0], "port": self.clt_addr[1]},
                "srvAddr": {"ip": self.srv_addr[0], "port": self.srv_addr[1]},
                "close": self.close,
                "data": data,
            }
        ).encode()

    def with_data(self, data):
        return Message(cid=self.cid, msg_type=self.msg_type, clt_addr=self.clt_addr, srv_addr=self.srv_addr,
                       close=self.close, data=data)

