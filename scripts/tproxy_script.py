#! /usr/bin/env python3
import json
from dataclasses import dataclass
from typing import List, Tuple

from twisted.internet import endpoints, protocol, reactor

from message import Message


@dataclass
class Tproxy(protocol.Protocol):
    """
        Class handling messages received/sent from/to TProxy.
        One should implement the method 'handle(msg)' with the desired behaviour
    """
    header_len: int = 4
    cid: str = ""
    clt_addr: Tuple[str, int] = ("", 0)
    srv_addr: Tuple[str, int] = ("", 0)

    buff: bytes = bytes()

    def to_message(self, data: bytes) -> Message:
        try:
            json_msg = json.loads(data)
            return Message(json=json_msg)
        except json.decoder.JSONDecodeError:
            print("ERROR Cannot decode:", data)
        return None

    def to_messages(self, new_data: bytes) -> List[Message]:
        """Parse TCP data into messages and buffer incomplete messages"""
        remaining_data = self.buff + new_data
        msgs = []
        while len(remaining_data) > self.header_len:
            msg_len = int.from_bytes(remaining_data[:self.header_len], "big")
            if len(remaining_data) < self.header_len + msg_len:
                self.buff = remaining_data
                return msgs
            else:
                msg_data = remaining_data[self.header_len: self.header_len + msg_len]
                remaining_data = remaining_data[self.header_len + msg_len:]
                msg = self.to_message(msg_data)
                if msg is not None:
                    msgs.append(msg)

        self.buff = remaining_data
        return msgs

    def dataReceived(self, data: bytes) -> None:
        msgs = self.to_messages(data)
        for msg in msgs:
            if not self.cid:
                self.cid = msg.cid
                self.clt_addr = msg.clt_addr
                self.srv_addr = msg.srv_addr

            self.handle(msg)

    def send(self, msg: Message) -> None:
        payload = msg.to_json()
        header = len(payload).to_bytes(self.header_len, "big")
        self.transport.write(header + payload)

    def handle(self, msg: Message) -> None:
        raise NotImplementedError()

    def fake_msg(self, msg_type="request", close=False, data=b"") -> Message:
        return Message(
            cid=self.cid,
            clt_addr=self.clt_addr,
            srv_addr=self.srv_addr,
            msg_type=msg_type,
            close=close,
            data=data,
        )

    @staticmethod
    def run(factory):
        endpoints.serverFromString(reactor, "tcp:9081").listen(factory)
        reactor.run()


@dataclass
class TproxyFactory(protocol.Factory):

    def buildProtocol(self, addr: str) -> protocol.Protocol:
        return Tproxy()
