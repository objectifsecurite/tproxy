class Authentication:
    def __init__(self, flavor: int, data: bytes):
        self.flavor: int = flavor
        self.data: bytes = data

    def length(self) -> int:
        return len(self.data)


class Credentials(Authentication):
    def __init__(self, flavor: int, data: bytes):
        super().__init__(flavor, data)


class Verifier(Authentication):
    def __init__(self, flavor: int, data: bytes):
        super().__init__(flavor, data)
