import logging
from functools import wraps

from .const import (NFS3_PROCEDURE_GETATTR, NFS3_PROCEDURE_LOOKUP,
                    NFS3_PROCEDURE_READ, NFS3_PROCEDURE_WRITE,
                    NFS3_PROCEDURE_CREATE, NFS3_PROCEDURE_MKDIR, NFS3_PROCEDURE_REMOVE, NFS3_PROCEDURE_RMDIR,
                    NFS3_PROCEDURE_RENAME, NFS3_PROCEDURE_READDIR, NFS3_PROCEDURE_READDIRPLUS, time_how,
                    SET_TO_CLIENT_TIME, SET_TO_SERVER_TIME)
from .pack import nfs_pro_v3Packer, nfs_pro_v3Unpacker
from .rpc import RPC
from .rtypes import (nfs_fh3, set_uint32, set_uint64, sattr3, set_time, diropargs3, create3args,
                     mkdir3args, readdir3args, readdirplus3args,
                     read3args, write3args, createhow3, rename3args, nfstime3, lookup3res, getattr3res, read3res,
                     write3res, create3res, wcc_data3res, rename3res, readdir3res, readdirplus3res)
from .utils import LONG, str_to_bytes

logger = logging.getLogger(__package__)


class NFSAccessError(Exception):
    pass


def fh_check(function):
    @wraps(function)
    def check_fh(*args, **kwargs):
        logger.debug("Checking if first argument is bytes type as file/directory handler for [%s]" % function.__name__)
        fh = None
        if len(args) > 1:
            fh = args[1]
        else:
            for k in kwargs:
                if k.endswith("_handle"):
                    fh = kwargs.get(k)
                    break
        if fh and not isinstance(fh, bytes):
            raise TypeError("file/directory should be bytes")
        else:
            return function(*args, **kwargs)
    return check_fh


class NFSv3(RPC):
    @classmethod
    def get_sattr3(cls, mode=None, uid=None, gid=None, size=None, atime_flag=None, atime_s=0, atime_ns=0,
                   mtime_flag=None, mtime_s=0, mtime_ns=0):
        if atime_flag not in time_how:
            raise ValueError("atime flag must be one of %s" % time_how.keys())

        if mtime_flag not in time_how:
            raise ValueError("mtime flag must be one of %s" % time_how.keys())

        attrs = sattr3(mode=set_uint32(True, int(mode)) if mode is not None else set_uint32(False),
                       uid=set_uint32(True, int(uid)) if uid is not None else set_uint32(False),
                       gid=set_uint32(True, int(gid)) if gid is not None else set_uint32(False),
                       size=set_uint64(True, LONG(size)) if size is not None else set_uint64(False),
                       atime=set_time(SET_TO_CLIENT_TIME, nfstime3(int(atime_s), int(atime_ns)))
                             if atime_flag == SET_TO_CLIENT_TIME else set_time(atime_flag),
                       mtime=set_time(SET_TO_CLIENT_TIME, nfstime3(int(mtime_s), int(mtime_ns)))
                             if mtime_flag == SET_TO_CLIENT_TIME else set_time(mtime_flag))
        return attrs

    def get_resp_status(self, data) -> int:
        return nfs_pro_v3Unpacker(data).unpack_nfsstat3()

    @fh_check
    def getattr_request(self, file_handle) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_fhandle3(file_handle)

        logger.debug("NFSv3 procedure %d: GETATTR" % (NFS3_PROCEDURE_GETATTR))
        return packer.get_buffer()

    def parse_getattr_resp(self, data) -> getattr3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_getattr3res(data_format='')

    @fh_check
    def lookup_request(self, dir_handle, file_folder) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_diropargs3(diropargs3(dir=nfs_fh3(dir_handle), name=str_to_bytes(file_folder)))

        logger.debug("NFSv3 procedure %d: LOOKUP" % (NFS3_PROCEDURE_LOOKUP))
        return packer.get_buffer()

    def parse_lookup_resp(self, data) -> lookup3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_lookup3res(data_format='')


    @fh_check
    def read_request(self, file_handle, offset=0, chunk_count=1024 * 1024) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_read3args(read3args(file=nfs_fh3(file_handle), offset=offset, count=chunk_count))

        logger.debug("NFSv3 procedure %d: READ" % (NFS3_PROCEDURE_READ))
        return packer.get_buffer()

    def parse_read_resp(self, data) -> read3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_read3res(data_format='')

    @fh_check
    def write_request(self, file_handle, offset, count, content, stable_how) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_write3args(write3args(file=nfs_fh3(file_handle),
                                          offset=offset,
                                          count=count,
                                          stable=stable_how,
                                          data=str_to_bytes(content)))

        logger.debug("NFSv3 procedure %d: WRITE" % (NFS3_PROCEDURE_WRITE))
        return packer.get_buffer()

    def parse_write_resp(self, data) -> write3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_write3res(data_format='')

    @fh_check
    def create_request(self, dir_handle, file_name, create_mode, mode=None, uid=None, gid=None, size=None,
               atime_flag=SET_TO_SERVER_TIME, atime_s=None, atime_us=None,
                mtime_flag=SET_TO_SERVER_TIME, mtime_s=None, mtime_us=None,
               verf='0') -> bytes:
        packer = nfs_pro_v3Packer()
        attrs = self.get_sattr3(mode, uid, gid, size, atime_flag, atime_s, atime_us, mtime_flag, mtime_s, mtime_us)
        packer.pack_create3args(create3args(where=diropargs3(dir=nfs_fh3(dir_handle), name=str_to_bytes(file_name)),
                                            how=createhow3(mode=create_mode, obj_attributes=attrs, verf=verf)))

        logger.debug("NFSv3 procedure %d: CREATE" % (NFS3_PROCEDURE_CREATE))
        return packer.get_buffer()

    def parse_create_resp(self, data) -> create3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_create3res(data_format='')

    @fh_check
    def mkdir_request(self, dir_handle, dir_name, mode=None, uid=None, gid=None,
              atime_flag=SET_TO_SERVER_TIME, atime_s=None, atime_us=None,
              mtime_flag=SET_TO_SERVER_TIME, mtime_s=None, mtime_us=None) -> bytes:
        packer = nfs_pro_v3Packer()
        attrs = self.get_sattr3(mode, uid, gid, None, atime_flag, atime_s, atime_us, mtime_flag, mtime_s, mtime_us)
        packer.pack_mkdir3args(mkdir3args(where=diropargs3(dir=nfs_fh3(dir_handle), name=str_to_bytes(dir_name)),
                                          attributes=attrs))

        logger.debug("NFSv3 procedure %d: MKDIR" % (NFS3_PROCEDURE_MKDIR))
        return packer.get_buffer()


    def parse_mkdir_resp(self, data) -> create3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_mkdir3res(data_format='')

    @fh_check
    def remove_request(self, dir_handle, file_name) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_diropargs3(diropargs3(dir=nfs_fh3(dir_handle), name=str_to_bytes(file_name)))

        logger.debug("NFSv3 procedure %d: REMOVE" % (NFS3_PROCEDURE_REMOVE))
        return packer.get_buffer()

    def parse_remove_resp(self, data) -> wcc_data3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_remove3res(data_format='')

    @fh_check
    def rmdir_request(self, dir_handle, dir_name) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_diropargs3(diropargs3(dir=nfs_fh3(dir_handle), name=str_to_bytes(dir_name)))

        logger.debug("NFSv3 procedure %d: RMDIR" % (NFS3_PROCEDURE_RMDIR))
        return packer.get_buffer()

    def parse_rmdir_resp(self, data) -> wcc_data3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_rmdir3res(data_format='')


    @fh_check
    def rename_request(self, dir_handle_from, from_name, dir_handle_to, to_name) -> bytes:
        if not isinstance(dir_handle_to, bytes):
            raise TypeError("file handle should be bytes")

        packer = nfs_pro_v3Packer()
        packer.pack_rename3args(rename3args(from_v=diropargs3(dir=nfs_fh3(dir_handle_from),
                                                              name=str_to_bytes(from_name)),
                                            to=diropargs3(dir=nfs_fh3(dir_handle_to),
                                                          name=str_to_bytes(to_name))))

        logger.debug("NFSv3 procedure %d: RENAME" % (NFS3_PROCEDURE_RENAME))
        return packer.get_buffer()

    def parse_rename_resp(self, data) -> rename3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_rename3res(data_format='')

    @fh_check
    def readdir_request(self, dir_handle, cookie=0, cookie_verf='0', count=4096) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_readdir3args(readdir3args(dir=nfs_fh3(dir_handle),
                                              cookie=cookie,
                                              cookieverf=str_to_bytes(cookie_verf),
                                              count=count))

        logger.debug("NFSv3 procedure %d: READDIR" % (NFS3_PROCEDURE_READDIR))
        return packer.get_buffer()

    def parse_readdir_resp(self, data) -> readdir3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_readdir3res(data_format='')

    @fh_check
    def readdirplus_request(self, dir_handle, cookie=0, cookie_verf='0', dircount=4096, maxcount=32768) -> bytes:
        packer = nfs_pro_v3Packer()
        packer.pack_readdirplus3args(readdirplus3args(dir=nfs_fh3(dir_handle),
                                                      cookie=cookie,
                                                      cookieverf=str_to_bytes(cookie_verf),
                                                      dircount=dircount,
                                                      maxcount=maxcount))

        logger.debug("NFSv3 procedure %d: READDIRPLUS" % (NFS3_PROCEDURE_READDIRPLUS))
        return packer.get_buffer()

    def parse_readdirplus_resp(self, data) -> readdirplus3res:
        unpacker = nfs_pro_v3Unpacker(data)
        return unpacker.unpack_readdirplus3res(data_format='')
