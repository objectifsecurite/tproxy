import logging
import struct
import time

from nfs.auth_types import Credentials, Verifier

logger = logging.getLogger(__package__)


class RPCProtocolError(Exception):
    pass


class RPC(object):
    connections = list()

    def get_request(self, program, program_version, procedure, data, creds: Credentials, verif: Verifier, xid=None, version=2) -> bytes :
        from nfs.pyNfsClient import L_MAX, AUTH_NULL
        rpc_xid = xid if xid is not None else int(round(time.time() * 1000)) % L_MAX
        rpc_message_type = 0     # 0=call
        rpc_rpc_version = version
        rpc_program = program
        rpc_program_version = program_version
        rpc_procedure = procedure

        proto: bytes = struct.pack(
            # Remote Procedure Call
            '!LLLLLL',
            rpc_xid,
            rpc_message_type,
            rpc_rpc_version,
            rpc_program,
            rpc_program_version,
            rpc_procedure,
        )

        # Credentials
        if creds is None or creds.flavor == AUTH_NULL:    # AUTH_NULL
            proto += struct.pack(
                '!LL',
                0,
                0,
            )
        else:
            length = creds.length()
            proto += struct.pack(
                '!LL',
                creds.flavor,
                length
            )
            if length > 0 :
                proto += creds.data

        # Verifier
        if verif is None or verif.flavor == AUTH_NULL: # AUTH_NULL
            proto += struct.pack('!LL', 0, 0 )
        else:
            verif_len = verif.length()
            proto += struct.pack(
                '!LL',
                verif.flavor,
                verif_len
            )
            if verif_len > 0:
                proto += verif.data

        if data is not None:
            proto += data

        rpc_fragment_header = 0x80000000 + len(proto)

        return struct.pack('!L', rpc_fragment_header) + proto
