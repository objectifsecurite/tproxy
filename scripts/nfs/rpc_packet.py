import time
from typing import Tuple

from message import Message
from nfs.auth_types import Credentials, Verifier
from nfs.pyNfsClient import RPC
from abc import ABC, abstractmethod

class RPCPacket(ABC):
    def __init__(self, cid: str, msg_type: str, clt_addr: Tuple[str, int], srv_addr: Tuple[str, int],
                 fragment_hdr: int, xid: int, rpc_msg_type: int, data: bytes):
        # Message
        self.cid = cid
        self.msg_type = msg_type
        self.clt_addr = clt_addr
        self.srv_addr = srv_addr

        # RPC
        self.fragment_hdr = fragment_hdr
        self.xid = xid
        self.rpc_msg_type = rpc_msg_type
        self.data = data  # NFS payload

    def shorten_data(self) -> str:
        hex_data = ":".join("{:02x}".format(c) for c in self.data)
        threshold = 50
        if len(self.data) > threshold:
            return hex_data[:(threshold // 2) - 1] + ".." + hex_data[-(threshold // 2) + 1:]
        return hex_data

    @abstractmethod
    def get_rpc_data(self) -> bytes:
        pass

    def to_message(self) -> Message:
        data = self.get_rpc_data()
        return Message(self.cid, self.msg_type, self.clt_addr, self.srv_addr, False, data)


class RPCRequest(RPCPacket):
    def __init__(self, cid: str, clt_addr: Tuple[str, int], srv_addr: Tuple[str, int], fragment_hdr: int,
                 xid: int, rpc_msg_type: int, rpc_version: int, program: int, prog_version: int,
                 procedure: int, creds: Credentials, verifier: Verifier, data: bytes):
        assert rpc_msg_type == 0  # request
        super().__init__(cid, "request", clt_addr, srv_addr, fragment_hdr, xid, rpc_msg_type, data)
        self.rpc_version = rpc_version
        self.program = program
        self.prog_version = prog_version
        self.procedure = procedure
        self.creds = creds
        self.verifier = verifier

    def __repr__(self):
        return "RPC Request:\n<Fragment Hdr: {0}, XID: {1}, Msg type: {2}, RPC version: {3}, Program: {4}, " \
               "Program version: {5}, Procedure: {6}, Auth Flavor: {7}, Verif flavor: {8}, Data: {9}>" \
            .format(self.fragment_hdr, hex(self.xid), self.rpc_msg_type, self.rpc_version, self.program,
                    self.prog_version,
                    self.procedure, self.creds.flavor, self.verifier.flavor, self.shorten_data())

    def update_xid(self):
        from nfs.pyNfsClient import L_MAX
        xid = int(round(time.time() * 1000)) % L_MAX
        return RPCRequest(self.cid, self.clt_addr, self.srv_addr, self.fragment_hdr, xid, self.rpc_msg_type,
                          self.rpc_version, self.program, self.prog_version, self.procedure,
                          self.creds, self.verifier, self.data)

    def get_rpc_data(self) -> bytes:
        return RPC().get_request(self.program, self.prog_version, self.procedure, self.data, self.creds,
                                 self.verifier, self.xid, self.rpc_version)


class RPCResponse(RPCPacket):
    def __init__(self, cid: str, clt_addr: Tuple[str, int], srv_addr: Tuple[str, int], fragment_hdr: int, xid: int,
                 rpc_msg_type: int, reply_state: int, verifier: Verifier, accept_state: int, data: bytes):
        assert rpc_msg_type == 1  # response
        super().__init__(cid, "response", clt_addr, srv_addr, fragment_hdr, xid, rpc_msg_type, data)
        self.reply_state = reply_state
        self.verifier = verifier
        self.accept_state = accept_state
        self.data = data

    def __repr__(self):
        return "RPC Response:\nFragment Hdr: {0}, XID: {1}, Msg type: {2}, Reply State: {3}, " \
               "Accept State: {4}, Data: {5}".format(self.fragment_hdr, hex(self.xid), self.rpc_msg_type,
                                                     self.reply_state, self.accept_state, self.shorten_data())

    def get_rpc_data(self):
        from nfs.pyNfsClient.pack import RPC_Packer
        packer = RPC_Packer()
        packer.pack_rpc_response_headers(self)
        return packer.get_buffer() + self.data

    def with_nfs_error(self, nfs_error: int):
        from nfs.pyNfsClient import NFS3_OK
        from nfs.pyNfsClient.pack import nfs_pro_v3Packer
        assert nfs_error != NFS3_OK
        packer = nfs_pro_v3Packer()
        packer.pack_nfsstat3(nfs_error)
        return RPCResponse(self.cid, self.clt_addr, self.srv_addr, self.fragment_hdr, self.xid, self.rpc_msg_type,
                           self.reply_state, self.verifier, self.accept_state, packer.get_buffer() +
                           b"\x00" * (len(self.data) - 4))  # keep same packet size
