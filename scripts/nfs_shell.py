#! /usr/bin/env python3
from nfs.pkt_handler import NfsHandler, NfsHandlerFactory

if __name__ == "__main__":
    nfsHandler = NfsHandler()
    print("Waiting for NFS traffic ...")
    NfsHandler.run(NfsHandlerFactory(proto=nfsHandler))
