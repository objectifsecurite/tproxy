import java.lang.{Runtime => JavaRuntime}
import java.io.FileOutputStream
import java.util.jar.{Attributes, JarOutputStream, Manifest}
import java.nio.file.{Files, Paths}
import java.nio.file.attribute.PosixFilePermissions
import mill._
import mill.define.{TaskModule, Command}
import mill.eval.Evaluator
import scalalib._
import os._

import java.io._
import scala.io.Source


override def clean(evaluator: Evaluator, targets: String*): Command[Seq[PathRef]] = T.command {
  tproxy.core.cleanSharedLibs()
  tproxy.wireshark.cleanSharedLibs()
  super.clean(evaluator, targets: _*)()
}

object tproxy extends SbtModule { outer =>

  val releaseVersion = scala.util.Properties.envOrElse("RELEASE_VERSION","")
  val distro = scala.util.Properties.envOrElse("DISTRO","")
  def scalaVersion = "2.12.8"
  def millSourcePath = super.millSourcePath / up

  object core extends SbtModule with CommonConfig {
    def scalaVersion = outer.scalaVersion
    def moduleDeps = Seq(outer.common)

    object makefileDefs extends MakefileDefs

    def ivyDeps = Agg(
      ivy"com.typesafe.akka::akka-actor:2.5.17",
      ivy"com.typesafe.akka::akka-slf4j:2.5.17",
      ivy"org.bouncycastle:bcprov-jdk15on:1.60",
      ivy"org.bouncycastle:bcpkix-jdk15on:1.60",
      ivy"com.github.karasiq::cryptoutils:1.4.3",
      ivy"ch.qos.logback:logback-classic:1.2.3",
      ivy"com.github.scopt::scopt:3.7.0",
      ivy"com.typesafe:config:1.3.2",
      ivy"org.scala-lang.modules::scala-parser-combinators:1.1.1",
      ivy"com.lihaoyi::upickle::0.9.5",
      ivy"com.lihaoyi::ujson::0.9.5",
    )

    def javacOptions = Seq(
      "-Xlint:deprecation"
    )

    def scalacOptions = Seq(
      "-opt:unreachable-code",
      "-feature",
      "-unchecked",
      "-deprecation",
      "-Xlint",
      //  "-target:jvm-1.8"
      //  "-Xlog-implicits"
    )

    object mainJar extends MainJar with ExecScript {
      def mainClass = T{"ch.os.tproxy.MainCli"}
      def defaultCommandName() = "apply"

      def jarDependencies = T{ super.classpath().filter(_.path.ext == "jar").map(_.path)}
      def mainJarPath = T{ jar().path }

      def apply() = T.command {
        outer.wireshark.mainJar()()
        writeExecScript()()
        super.file()
      }
      def classpath = T {
        Agg(jar()) ++
          Agg(outer.common.jar()) ++
          super.classpath()
      }
    }

    object test extends Tests with TestConfig  {
      def moduleDeps = Seq(outer.core)

      override def testFramework = super[TestConfig].testFramework

      object mainJar extends MainJar with ExecScript {
        def name = "runtests"
        def defaultCommandName() = "apply"

        def jarDependencies = T{ super.classpath().filter(_.path.ext == "jar").map(_.path)}
        def mainJarPath = T{ jar().path }


        def apply() = T.command {
          outer.wireshark.mainJar()()
          writeExecScript()()
          super.file()
        }
        def classpath = T {
          Agg(jar()) ++
            Agg(outer.core.jar()) ++
            Agg(outer.common.jar()) ++
            super.classpath()
        }
      }
    }

  }

  object common extends SbtModule {
    def scalaVersion = outer.scalaVersion
    def ivyDeps = Agg(
      ivy"com.typesafe.akka::akka-actor:2.5.17",
      ivy"com.typesafe.akka::akka-slf4j:2.5.17",
      ivy"com.lihaoyi::upickle::0.9.5",
      ivy"com.lihaoyi::ujson::0.9.5",
      ivy"ch.qos.logback:logback-classic:1.2.3",
    )
  }

  object gui extends SbtModule  {

    def scalaVersion = outer.scalaVersion
    def moduleDeps = Seq(outer.core)
    def ivyDeps = Agg(
      ivy"com.typesafe.akka::akka-actor:2.5.17",
      ivy"com.typesafe.akka::akka-slf4j:2.5.17",
      ivy"com.lihaoyi::upickle::0.9.5",
      ivy"com.lihaoyi::ujson::0.9.5",
      ivy"ch.qos.logback:logback-classic:1.2.3",
    )

    override def resources = T.sources{tproxy.core.resources() ++ super.resources()}

    def unmanagedClasspath = T {
      val dir = millSourcePath / up / "lib"
      if (!exists(dir)) Agg()
      else
        Agg.from(
          for (file <- list(dir) if file.ext == "jar")
            yield PathRef(file)
        )
    }

    object mainJar extends MainJar with ExecScript {
      def mainClass = T{"ch.os.tproxy.gui.Main"}
      def defaultCommandName() = "apply"

      def jarDependencies = T{ compileClasspath().filter(_.path.ext == "jar").map(_.path)}
      def mainJarPath = T{ jar().path }
      def apply() = T.command {
        outer.wireshark.mainJar()()
        writeExecScript()()
        super.file()
      }
      def classpath = T {
        Agg(jar()) ++
          Agg(outer.core.jar()) ++
          Agg(outer.common.jar()) ++
          compileClasspath() filter (_.path.ext == "jar")
      }
    }
    object test extends Tests with TestConfig  {
      def moduleDeps = Seq(outer.gui, outer.core.test)

      override def testFramework = super[TestConfig].testFramework

      object mainJar extends MainJar with ExecScript {
        def name = "runtests"
        def gui_tests = true
        def defaultCommandName() = "apply"

        def jarDependencies = T{ super.classpath().filter(_.path.ext == "jar").map(_.path)}
        def mainJarPath = T{ jar().path }

        def apply() = T.command {
          outer.wireshark.mainJar()()
          writeExecScript()()
          super.file()
        }
        def classpath = T {
          Agg(jar()) ++
            Agg(outer.core.jar()) ++
            Agg(outer.gui.jar()) ++
            Agg(outer.common.jar()) ++
            Agg(outer.core.test.jar()) ++
            super.classpath()
        }
      }

    }
  }

  object wireshark extends SbtModule with CommonConfig  {
    def ivyDeps = Agg(
      ivy"com.typesafe.akka::akka-actor:2.5.17",
      ivy"com.typesafe.akka::akka-slf4j:2.5.17",
      ivy"com.lihaoyi::upickle::0.9.5",
      ivy"com.lihaoyi::ujson::0.9.5",
    )

    def scalaVersion = outer.scalaVersion
    def moduleDeps = Seq(outer.common)
    object makefileDefs extends MakefileDefs



    object mainJar extends MainJar with ExecScript {
      def name = "wiresharkDissection"

      def natlib_path = "$app_home/wireshark/src/main/others/c:$app_home/wireshark/src/main/others/jni"
      def mainClass = T{"ch.os.tproxy.wireshark.Main"}
      def jarDependencies = T{ super.classpath().filter(_.path.ext == "jar").map(_.path)}

      def apply() = T.command{
        writeExecScript()()
        super.apply()()
      }

      def classpath = T {
        Agg(jar()) ++ Agg(outer.common.jar()) ++ super.classpath()
      }
    }
  }

  object dist extends TaskModule with ExecScript with SharedLibsManifest {
    def defaultCommandName = "build"
    def moduleDeps = Seq(tproxy.core, tproxy.core.test, tproxy.gui, tproxy.gui.test, tproxy.wireshark)

    def mainJarNames = Map(
      "gui" -> s"tproxy-gui-$releaseVersion.jar",
      "wireshark" -> s"tproxy-wireshark-$releaseVersion.jar",
      "gui-test" -> s"tproxy-gui-test-$releaseVersion.jar",
    )

    val outDir = s"tproxy_${releaseVersion}_$distro"
    val lib_path = "$app_home/lib/"
    def natlib_path: String = lib_path
    def core_test_classes =  lib_path + mainJarNames("gui-test")
    def gui_test_classes =  lib_path + mainJarNames("gui-test")

    def distPath = millSourcePath / outDir
    val copiedFiles = Seq("README.md", "iptables.sh", "tproxy", "runtests", "wiresharkDissection", "config")

    def build() = T.command{
      tproxy.gui.assembly()
      tproxy.gui.test.assembly()
      tproxy.wireshark.assembly()

      writeExecScript("tproxy", lib_path + mainJarNames("gui"))()
      writeExecScript("runtests", lib_path + mainJarNames("gui-test"))()
      writeExecScript("wiresharkDissection", lib_path + mainJarNames("wireshark"))()

      copyLibs()()
      copyAssembledJars()()
      writeSharedLibsManifest(distPath / "lib")()
      for (fname <- copiedFiles) {
        copy.over(millSourcePath / up / fname, distPath / fname)
      }
      os.proc('zip, "-r", millSourcePath  / s"$outDir.zip", outDir).call(cwd = millSourcePath)
    }

    val regexp = """/lib[^.]+\.so[.0-9]*$""".r

    def unmanagedClasspath() = {
      val dir = millSourcePath / up / "lib"
      if (!exists(dir)) Agg()
      else
        Agg.from(
          for (file <- list(dir) if file.ext == "jar" || (regexp findFirstMatchIn file.toString).isDefined)
            yield file
        )
    }

    def copyLibs() = T.command{
      makeDir.all(distPath / "lib")
      val jars = tproxy.core.sharedLibs().map(_.path) ++
        tproxy.wireshark.sharedLibs().map(_.path) ++
        unmanagedClasspath()

      jars.foreach(path => {
        val fname = path.toString.split("/").last
        copy.over(path , distPath / "lib" / fname)
      })

    }

    def copyAssembledJars() = T.command{
      makeDir.all(distPath / "lib")
      val mainJars = Map(
        mainJarNames("gui") -> tproxy.gui.assembly().path,
        mainJarNames("wireshark") -> tproxy.wireshark.assembly().path,
        mainJarNames("gui-test") -> tproxy.gui.test.assembly().path,
      )
      mainJars.foreach(x =>  {
        copy.over(x._2, distPath / "lib" / x._1)
        }
      )
    }
  }


  trait TestConfig extends CommonConfig {
    def ivyDeps = Agg(
      ivy"com.typesafe.akka::akka-testkit:2.5.17",
      ivy"org.scalatest::scalatest:3.0.5"
    )

    def unmanagedClasspath = T {
      val dir = millSourcePath / up / "lib"
      if (!exists(dir)) Agg()
      else
        Agg.from(
          for (file <- list(dir) if file.ext == "jar")
            yield PathRef(file)
        )
    }

    def mainClass = Some("ch.os.tproxy.test.TestRunner")
    def testFramework = T {"org.scalatest.tools.Framework"}
    def sharedLibs = T { outer.core.sharedLibs() ++ super.sharedLibs() }

    def testOne(args: String*) = T.command {
      super.runMain("org.scalatest.run", args: _*)
    }

    object makefileDefs extends MakefileDefs {
      def mainClassDir = outer.core.makefileDefs.classDir

      def defs = T {
        val origDefs = super.defs()
        val mainClassDirDef = "MAIN_CLASS_DIRECTORY" -> mainClassDir().path
        super.defs() :+ mainClassDirDef
      }
    }
  }


}


trait CommonConfig extends ScalaModule { outer =>

  def unmanagedClasspath = T {
    val dir = millSourcePath / "lib"

    if (!exists(dir)) Agg()
    else
      Agg.from(
        for (file <- list(dir) if file.ext == "jar")
          yield PathRef(file)
      )
  }

  def forkArgs = T {
    val natLibPath = Seq(
      s"$millSourcePath/src/main/others/c",
      s"$millSourcePath/src/main/others/jni",
      s"$millSourcePath/lib"
    ) mkString ":"
    Seq(
      s"-Dnative.library.path=$natLibPath",
      s"-Dapp.home=$millSourcePath"
    )
  }

  def makefileDefs : MakefileDefs

  def sharedLibs = T {
    makefileDefs.file()
    make()()
    walk(make.directory().path) filter (_.ext == "so") map (PathRef(_))
  }

  def cleanSharedLibs = T {
    make.clean()()
  }

  trait MakefileDefs extends build.MakefileDefs {
    def destDir = T {PathRef(sources().head.path / up / "others") }
    def classpath = compileClasspath
    def sharedLibDir = T { PathRef(millSourcePath / "lib") }
    def classDir = T { PathRef(compile().classes.path) }
  }

  object make extends MakeModule {
    def directory = T { PathRef(sources().head.path / up / "others") }
  }

  trait MainJar extends build.MainJar {

    def mainClass = T {
      val mc = outer.mainClass()
      require(mc.isDefined)
      mc.get
    }

    def classpath = T { compileClasspath() filter (_.path.ext == "jar") }
  }
}


trait MakefileDefs extends TaskModule {

  def defaultCommandName = "main"
  def main() = T.command { file() }

  def destDir      : T[PathRef]
  def classpath    : T[Agg[PathRef]]
  def sharedLibDir : T[PathRef]
  def classDir     : T[PathRef]

  def jdkHome = T {
    def isJdk(dir: Path) = exists(dir / "bin" / "javac")
    val javaHome = Path(System getProperty "java.home")
    val dir = if (isJdk(javaHome)) javaHome else (javaHome / up)
    require(isJdk(dir), s"no jdk found in $javaHome or $dir")
    PathRef(dir)
  }

  def scalaLibJar = T {
    def isScalaLib(pf: PathRef) = pf.path.last startsWith "scala-library"
    val jars = classpath() filter isScalaLib
    require(jars.size == 1)
    PathRef(jars.items.next.path)
  }

  def defs = T {
    Seq(
      "JDK_HOME"             -> jdkHome().path,
      "SCALA_LIBRARY_JAR"    -> scalaLibJar().path,
      "SHARED_LIB_DIRECTORY" -> sharedLibDir().path,
      "CLASS_DIRECTORY"      -> classDir().path
    )
  }

  def file = T {
    val lines = for ((k, v) <- defs()) yield s"$k = $v\n"
    val filePath= destDir().path / "Makefile.defs"
    write.over(filePath, lines)
    PathRef(filePath)
  }
}


trait MakeModule extends TaskModule {
  def defaultCommandName() = "apply"

  def directory : T[PathRef]

  def apply(args: String*) = T.command {
    val ncpus = JavaRuntime.getRuntime.availableProcessors
    val dir = directory().path
    val cmd = (s"make -j$ncpus -C $dir" split " ").toSeq
    val res = os.proc(cmd ++ Seq(args: _*)).call()
    require(res.exitCode == 0)
  }

  def clean() = T.command {
    val dir = directory().path
    val cmd = (s"make clean -C $dir" split " ").toSeq
    val res = os.proc(cmd).call()
    require(res.exitCode == 0)
  }
}


trait MainJar extends TaskModule {
  def defaultCommandName() = "apply"
  def apply() = T.command { file() }

  def mainClass : T[String]
  def classpath : T[Agg[PathRef]]

  def dest = T {T.ctx.dest / up / up / "mainJar.dest" / "out.jar"}

  def file = T {

    // A trailing / (or \ on Windows?) must be added at the end of the
    // directories which are included in the Class-Path manifest attribut,
    // otherwise java will not look into them when searching for a class.
    // http://todayguesswhat.blogspot.in/2011/03/jar-manifestmf-class-path-referencing.html
    val cp = classpath() map (_.path) map { p =>
      p.ext match {
        case "jar" => p.toString
        case _ => p.toString + '/'
      }
    }

    val attributes = Seq(
      Attributes.Name.MANIFEST_VERSION -> "1.0",
      Attributes.Name.MAIN_CLASS -> mainClass(),
      Attributes.Name.CLASS_PATH -> (cp mkString " ")
    )

    val manifest = new Manifest
    for ((k, v) <- attributes) manifest.getMainAttributes put (k, v)

    val dest_ = dest()
    makeDir.all(dest_ / up)
    val fos = new FileOutputStream(dest_.toString)
    val jar = new JarOutputStream(fos, manifest)

    jar.close
    fos.close

    PathRef(dest_)
  }
}

trait ExecScript extends TaskModule{
  def name = "tproxy"
  def gui_tests = false
  def millSourcePath: Path
  def natlib_path = "$app_home/core/src/main/others/c:$app_home/core/src/main/others/jni:$app_home/core/src/main/others/gui/cpp:$app_home/lib"
  def core_test_classes = "$app_home/out/tproxy/core/test/compile.dest/classes"
  def gui_test_classes = "$app_home/out/tproxy/gui/test/compile.dest/classes"

  val gui_test_path = if(gui_tests) "-R $app_gui_test_classes" else ""

  def jarPath() = T.command{
    val dest = T.ctx.dest / up / up / "mainJar.dest" / "out.jar"
    val appHome : String = (millSourcePath / up / up).toString() + "/"
    "$app_home/" + dest.toString().split(appHome).toList(1)
  }

  def writeExecScript(name: String = name, mainJarPath : String = "") = T.command {
    val template = Source.fromFile(s"templates/$name-template.sh")
    val outFile = new BufferedWriter(new FileWriter(new File(name)))
    Files.setPosixFilePermissions(Paths.get(name), PosixFilePermissions.fromString("rwxr-x---"))

    val path = if(mainJarPath.nonEmpty) mainJarPath else jarPath()()
    for (line <- template.getLines) {
        val newLine = line.replace("[MAIN_JAR_PATH]", path)
          .replace("[NATLIB_PATH]", natlib_path)
          .replace("[GUI_TESTS]", gui_test_path)
          .replace("[CORE_TEST_PATH]", core_test_classes)
          .replace("[GUI_TEST_PATH]", gui_test_classes)

        outFile.write(newLine + "\n")
    }
    template.close
    outFile.close()
  }
}

trait SharedLibsManifest extends TaskModule {
  val sharedLibs = T {tproxy.core.sharedLibs().map(_.path) ++ tproxy.wireshark.sharedLibs().map(_.path)}
  def writeSharedLibsManifest(libPath:Path) = T.command {
    var libVersions = ""
    sharedLibs().foreach(lib => {
      val dependencies = proc("ldd", lib.toString() ).call(cwd = millSourcePath).out.string.split("\n")
        .foldLeft("")((acc, dep) => acc + dep.split("=>").head.split("\\(").head + "\n")
      libVersions += "Dependencies for " + lib.last + " :\n" + dependencies + "\n"
    })

    val text =s"""
                 |Tproxy's shared libraries have been built with the linked libraries and versions detailed below.
                 |If these versions are not installed on your system, please build Tproxy from the source code.
                 |
                 |$libVersions
                 |""".stripMargin
    write.over(libPath / "MANIFEST.txt", text)
  }
}


